let patientsMedicationTable;

$(function () {
    /**
     * Medication Function Calls
     */
    addMedication();

    //console.log(NowGP.PatientId);

    //patientMedicationTableSearch(NowGP.PatientId);

    patientsMedicationTable = patientMedicationTableSearch(NowGP.PatientId);
});


/**
 *
 * @param PatientId
 * @returns {jQuery}
 */
function patientMedicationTableSearch(PatientId) {
    return $('#patientMedicationTable').DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        lengthChange: false,
        "language": {
            "emptyTable": "No records match the criteria entered. Please change the criteria and try again"
        },
        ajax: NowGP.siteUrl + '/admin/patient/medication/fetch/' + PatientId + '/data',
        columns: [
            {data: 'DatePrescribedOriginally', name: 'DatePrescribedOriginally'},
            {data: 'MedicationId', name: 'MedicationId'},
            {data: 'Name', name: 'Name'},
            {data: 'Dosage', name: 'Dosage'},
            {data: 'PacketSize', name: 'PacketSize'},
            {data: 'Enabled', name: 'Enabled'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "initComplete": function (settings, json) {
            editMedication();
            deleteMedication();
        },
        'drawCallback': function () {
            editMedication();
            deleteMedication();
        }
    });
}

/**
 * Add Patient Medication
 */
function addMedication() {

    let manageMedicationModal = $('#manageMedicationModal');
    let manageMedicationBtn = $('.add-medication');

    manageMedicationBtn.off().on('click', function (e) {
        e.preventDefault();

        let patientId = NowGP.PatientId;

        // Set Modal Options
        manageMedicationModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/medication/add')
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageMedicationModal.find('.modal-title').html('Add Medication');
                manageMedicationModal.find('.modal-body').html(response.data);
                manageMedicationModal.modal('show');
                manageMedicationModal.on('shown.bs.modal', function () {
                    let PatientData = {
                        PatientId: patientId
                    };

                    saveMedication(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });

}


/**
 * Edit Patient Medication
 */
function editMedication() {
    let manageMedicationModal = $('#manageMedicationModal');
    let manageMedicationBtn = $('.edit-medication');


    manageMedicationBtn.off().on('click', function (e) {
        e.preventDefault();

        let medicationId = $(this).data('medication-id');
        let patientId = NowGP.PatientId;

        // Set Modal Options
        manageMedicationModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/medication/edit/' + medicationId)
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageMedicationModal.find('.modal-title').html('Edit Medication');
                manageMedicationModal.find('.modal-body').html(response.data);
                manageMedicationModal.modal('show');
                manageMedicationModal.on('shown.bs.modal', function () {
                    let PatientData = {
                        PatientId: patientId,
                        MedicationId: medicationId
                    };

                    updateMedication(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });
}

/**
 * Delete Patient Medication
 */
function deleteMedication() {
    let deleteMedication = $('.delete-medication');


    deleteMedication.off().on('click', function (e) {
        e.preventDefault();

        let medicationId = $(this).data('medication-id');

        swal({
            title: 'Are you sure?',
            text: "Please confirm you wish to delete this medication",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {
            showLoader();
            axios.get(NowGP.siteUrl + '/admin/patient/medication/delete/' + medicationId)
                .then(function (response) {
                    patientsMedicationTable.draw();
                    hideLoader();
                })
                .catch(function (error) {
                    hideLoader();
                    console.log(error);
                });
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {

            }
        })
    });
}

/**
 *
 * @param PatientData
 */
function saveMedication(PatientData) {

    let manageMedicationModal = $('#manageMedicationModal');
    let saveMedicationBtn = $('#saveMedication');

    console.log(PatientData);
    console.log(saveMedicationBtn);

    saveMedicationBtn.off().on('click', function () {
        // Set vars.
        let form = $('#manageMedicationForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        let data = {
                PatientId: PatientData.PatientId,
                Name: $('#Name').val(),
                Dosage: $('#Dosage').val(),
                PacketSize: $('#PacketSize').val(),
                Enabled: $('#Enabled').val()
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        let submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                patientsMedicationTable.draw();
                manageMedicationModal.modal('hide');
                hideLoader();

            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                if (error.response.status) {
                    let errors = error.response.data;

                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        let formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })
    })


}

/**
 * Update patient Medication
 * @param PatientData
 */
function updateMedication(PatientData) {

    let manageMedicationModal = $('#manageMedicationModal');
    let saveMedicationBtn = $('#saveMedication');

    console.log(PatientData);
    console.log(saveMedicationBtn);

    saveMedicationBtn.off().on('click', function () {
        // Set vars.
        let form = $('#manageMedicationForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        let data = {
                PatientId: PatientData.PatientId,
                MedicationId: PatientData.MedicationId,
                Name: $('#Name').val(),
                Dosage: $('#Dosage').val(),
                PacketSize: $('#PacketSize').val(),
                Enabled: $('#Enabled').val()
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        let submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        resetModalFormErrors();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                manageMedicationModal.modal('hide');

                console.log(patientsMedicationTable);
                patientsMedicationTable.draw();
                hideLoader();


            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                console.log(error);
                if (typeof error.response !== 'undefined') {
                    if (error.response.status) {
                        let errors = error.response.data;

                        // Iterate through errors object.
                        $.each(errors, function (field, message) {
                            //console.error(field + ': ' + message);
                            //handle arrays
                            if (field.indexOf('.') != -1) {
                                field = field.replace('.', '[');
                                //handle multi dimensional array
                                for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                    field = field.replace('.', '][');
                                }
                                field = field + "]";
                            }
                            let formGroup = $('[name=' + field + ']', form).closest('.form-group');
                            formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                        });

                        // Reset submit.
                        if (submit.is('button')) {
                            submit.html(submitOriginal).prop('disabled', false);
                        } else if (submit.is('input')) {
                            submit.val(submitOriginal).prop('disabled', false);
                        }
                    }
                }
            })
    })
}


function resetModalFormErrors() {
    $('.form-group').removeClass('has-error');
    $('.form-group').find('.help-block').remove();
}

