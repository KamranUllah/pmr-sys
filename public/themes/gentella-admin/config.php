<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function ($theme) {
            // You can remove this line anytime.
            $theme->setTitle(' :: PMR Administration');

            //Breadcrumb template.
            $theme->breadcrumb()->setTemplate('
                 <ul class="breadcrumb">
                 <?php foreach($crumbs as $i => $crumb): ?>
                     @if ($i != (count($crumbs) - 1))
                     <li><a href="{{ $crumb["url"] }}">{{ $crumb["label"] }}</a></li>
                     @else
                     <li class="active">{{ $crumb["label"] }}</li>
                     @endif
                 <?php endforeach; ?>
                 </ul>
             ');

            //dd(\CoronaCms\Presenters\SidebarCreator::class);

            $theme->partialCreator('sidebar-nav', \App\Presenters\SidebarCreator::class);
        },

        'asset' => function ($asset) {
            $asset->addVersioning();
            $asset->container('footer')->addVersioning();
            $asset->add('core-style', \Bust::url('themes/gentella-admin/assets/css/app.css'));
            $asset->add('cms-style', \Bust::url('themes/gentella-admin/assets/css/styles.css'));
            $asset->add('custom', []);
            $asset->add('core-script', []);
            $asset->container('footer')->add('lib-scripts', \Bust::url('themes/gentella-admin/assets/js/site_lib.js'), ['core-script']);
            $asset->container('footer')->add('footer-script', [], ['core-script']);
            $asset->container('footer')->add('main', \Bust::url('themes/gentella-admin/assets/js/gentellela.js'), ['footer-script']);
            $asset->container('footer')->add('custom', \Bust::url('themes/gentella-admin/assets/js/custom.js'), ['footer-script']);
            $asset->container('footer-notifications')
                ->add('notification-js', \Bust::url('modules/notification/js/notification.js'), ['custom']);
            /*$asset->container('footer-notifications')
                ->add('user-counter-js', \Bust::url('modules/core/js/user-online-counter.js'), ['custom']);*/

            // You may use elixir to concat styles and scripts.
            /*
            $asset->themePath()->add([
                                        ['styles', 'dist/css/styles.css'],
                                        ['scripts', 'dist/js/scripts.js']
                                     ]);
            */

            // Or you may use this event to set up your assets.
            /*
            $asset->themePath()->add('core', 'core.js');
            $asset->add([
                            ['jquery', 'vendor/jquery/jquery.min.js'],
                            ['jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', ['jquery']]
                        ]);
            */
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.

        'beforeRenderTheme'  => function ($theme) {
            // You may use this event to set up your assets.
            // $theme->asset()->usePath()->add('core', 'core.js');
            // $theme->asset()->add('jquery', 'vendor/jquery/jquery.min.js');
            // $theme->asset()->add('jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', array('jquery'));

            // Partial composer.
            /*$theme->partialComposer('header', function($view)
            {
                $view->with('auth', Auth::user());
            });*/

            /*$theme->asset()->addVersioning();
            $theme->asset()->container('footer')->addVersioning();
            $theme->asset()->usePath()->add('core-style', 'css/app.css');
            $theme->asset()->usePath()->add('cms-style', 'css/styles.css');
            $theme->asset()->add('custom', []);
            $theme->asset()->add('core-script', []);
            $theme->asset()->usePath()->add('lib-scripts','js/site_lib.js', ['core-script']);
            $theme->asset()->container('footer')->add('footer-script',[], ['core-script']);
            $theme->asset()->container('footer')->usePath()->add('main','js/gentellela.js', ['footer-script']);
            $theme->asset()->container('footer')->usePath()->add('custom','js/app.js', ['footer-script']);*/

        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(

            'default' => function ($theme) {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);