<!DOCTYPE html>
<html id="cmsapp">
<head>
    <title>{!! Theme::get('title') !!}</title>
    <meta charset="utf-8">
    <meta name="keywords" content="{!! Theme::get('keywords') !!}">
    <meta name="description" content="{!! Theme::get('description') !!}">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-3.min.js"
            data-apikey="ee5168eb0ef3156cce1244e97d96e150"></script>
    {!! Theme::asset()->styles() !!}
    {!! Theme::asset()->scripts() !!}
</head>
<body class="login">
    {!! Theme::content() !!}
</div>

{!! Theme::partial('footer_vars') !!}

{!! Theme::asset()->container('footer')->scripts() !!}
</body>
</html>