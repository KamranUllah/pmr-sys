<!DOCTYPE html>
<html id="cmsapp">
<head>
    <title>{!! Theme::get('title') !!}</title>
    <meta charset="utf-8">
    <meta name="keywords" content="{!! Theme::get('keywords') !!}">
    <meta name="description" content="{!! Theme::get('description') !!}">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    {!! Theme::asset()->styles() !!}
    {!! Theme::asset()->container('header-custom')->styles() !!}
    {!! Theme::asset()->scripts() !!}
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
</head>
<body class="nav-md footer_fixed">

<!-- Loader -->
<div id="appLoader"></div>

<div class="container body">
    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{ url('admin/main') }}" class="site_title"><i class="fa fa-medkit"></i>
                        <span>PEARL</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        {!! displayUserAvatar(null, ['class' => 'img-circle profile_img'])  !!}
                    </div>
                    <div class="profile_info">
                        <span>Welcome</span>
                        <h2>{{ LoggedInUser() ? LoggedInUser()->first_name: '' }} {{ LoggedInUser() ? LoggedInUser()->last_name: '' }}</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->
                <p>&nbsp;</p>
                <div class="clearfix"></div>

            {!! Theme::partial('sidebar-nav') !!}
                
            </div>
        </div>

        <!-- top navigation -->
    {!! Theme::partial('header') !!}
    <!-- /top navigation -->

        <div class="right_col" role="main">

            <!-- notifications -->
            {!! Theme::partial('notification') !!}
            <!-- ./ notifications -->

            {!! Theme::breadcrumb()->render() !!}
            <div id="app">
                {!! Theme::content() !!}
            </div>

            <div class="clearfix"></div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>

    </div>
</div>

<div id="notifications-watcher"></div>
<div id="userOnlineCounter"><user-counter></user-counter></div>

{!! Theme::partial('footer') !!}
{!! Theme::partial('footer_vars') !!}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
{!! Theme::asset()->container('footer')->scripts() !!}
{!! Theme::asset()->container('footer-custom')->scripts() !!}
{!! Theme::asset()->container('footer-notifications')->scripts() !!}
</body>
</html>