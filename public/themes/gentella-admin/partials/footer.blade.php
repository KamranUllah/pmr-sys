<footer>
    <div class="pull-right">
        {{--Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>--}}
        <ul class="list-inline">
            @if(App::environment() !== 'production')
                <li><strong>Version: {{ config('pmr.pmr_version') }}</strong></li>
            @endif
            <li>Now Healthcare Group - PEARL System</li>
        </ul>

    </div>
    <div class="clearfix"></div>
</footer>