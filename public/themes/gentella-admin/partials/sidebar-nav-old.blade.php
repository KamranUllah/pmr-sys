<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">
            <li><a href="{!! route('admin.dashboard') !!}"><i class="fa fa-home"></i> Home <span class="fa fa-chevron-right"></span></a></li>
            <li><a><i class="fa fa-home"></i> Blog <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{!! route('blog.posts') !!}">Blog Posts</a></li>
                    <li><a href="{!! route('create.blog.post') !!}">Add Blog Post</a></li>
                    <li><a href="{!! route('blog.categories') !!}">Blog Categories</a>
                    <li><a href="{!! route('create.category') !!}">Add Blog Category</a>
                </ul>
            </li>
            <li><a><i class="fa fa-page"></i> Pages <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{!! route('admin.pages') !!}">Pages</a></li>
                    <li><a href="{!! route('admin.create.page') !!}">Add Page</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-page"></i> Portfolio <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{!! route('admin.portfolio') !!}">Portfolio</a></li>
                    <li><a href="{!! route('admin.create.portfolio.item') !!}">Add Portfolio Item</a></li>
                    <li><a href="{!! route('admin.portfolio') !!}">Portfolio Categories</a></li>
                    <li><a href="{!! route('admin.create.portfolio.item') !!}">Add Portfolio Category</a></li>
                    <li><a href="{!! route('admin.portfolio') !!}">Portfolio Skills</a></li>
                    <li><a href="{!! route('admin.create.portfolio.item') !!}">Add Portfolio SKill</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-user"></i> Users <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{!! route('admin.user.list') !!}">Registered Users</a></li>
                    <li><a href="{!! route('admin.user.add') !!}">Add User</a></li>
                    <li><a href="{!! route('admin.user.roles') !!}">User Roles</a>
                    <li><a href="{!! route('admin.user.role.add') !!}">Add Role</a>
                </ul>
            </li>
            <li><a href="{!! route('admin.settings') !!}"><i class="fa fa-home"></i> Settings <span class="fa fa-chevron-right"></span></a></li>
        </ul>
    </div>
</div>
<!-- /sidebar menu -->

{!! $sidebar !!}