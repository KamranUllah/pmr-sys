<?php $defaultAvatar = asset('/themes/gentella-admin/assets/img/default-avatar.png'); ?>

<div class="top_nav">

    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {!! displayUserAvatar(null)  !!}
                        @if (Sentinel::getUser()) <span class="fa fa-circle text-primary"></span>
                        {{ LoggedInUser()->first_name }} {{ LoggedInUser()->last_name }}
                        @endif
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        {{--<li><a href="javascript:;">  Profile</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:;">--}}
                                {{--<span class="badge bg-red pull-right">50%</span>--}}
                                {{--<span>Settings</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="javascript:;">Help</a>--}}
                        {{--</li>--}}
                        <li><a href="{!! route('user.logout') !!}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>

</div>