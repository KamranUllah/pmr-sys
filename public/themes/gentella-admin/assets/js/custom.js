

/**
 * Bog standard JS script goes below
 */

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

window.ParsleyConfig = {
    errorsWrapper: '<div></div>',
    errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
};

/**
 * Initialise bootstrap session timeout
 */
function bootstrapSessionTimeoutInit() {
    /*$.sessionTimeout({
        message: 'Your session will be logged out in 5 minutes if there is no activity',
        keepAlive: true,
        keepAliveInterval: 30000,
        keepAliveUrl: SITE_URL + '/keep-alive',
        logoutUrl: SITE_URL + '/session-timeout-logout',
        redirUrl: SITE_URL + '/session-timeout-logout',
        ignoreUserActivity: false,
        ajaxType: 'GET'
    });*/

    console.info('Idle timeout has been started!');

    IdleTimeoutPlus.start({
        message: 'Your session will be logged out in 5 minutes if there is no activity',
        idleTimeLimit: 1080, // Set this to 18 minutes to be ahead of the default laravel session timeout - default is 1200 (20 minutes)
        bootstrap: true,
        keepAliveInterval: 150, // Set the keep alive check to 2.5 minutes - default is 300
        keepAliveUrl: NowGP.siteUrl + '/keep-alive',
        idleCheckHeartbeat: 10,
        warnCountdownMessage: null,
        warnCountdownBar: false,
        logoutUrl: NowGP.siteUrl + '/session-timeout-logout',
        redirectUrl: NowGP.siteUrl + '/session-timeout-logout',
    });
}



// Sidebar
$(document).ready(function () {
    if (NowGP.currentRoute !== 'user.login') {
        bootstrapSessionTimeoutInit();
    }


    $('#admin-users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/admin/user/users/data',
        columns: [
            {data: 'id', name: 'users.id', searchable: false},
            {data: 'username', name: 'users.username'},
            {data: 'email', name: 'users.email'},
            {data: 'created_at', name: 'users.created_at', searchable: false},
            {data: 'last_login', name: 'users.last_login'},
            {data: 'activated', name: 'activated', searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        initComplete: function(settings, json) {
            $('.has-tooltip').qtip({
                position: {
                    my: 'top center',
                    at: 'bottom center'
                },
                style: {classes: 'qtip-youtube'}
            });
        }
    });

    $('#admin-pages-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/admin/pages/data',
        columns: [
            {data: 'id', name: 'pages.id', searchable: false},
            {data: 'title', name: 'pages.title'},
            {data: 'active', name: 'pages.active', searchable: false},
            {data: 'created_at', name: 'pages.created_at', searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        rowReorder: true,
        initComplete: function(settings, json) {
            $('.has-tooltip').qtip({
                position: {
                    my: 'top center',
                    at: 'bottom center'
                },
                style: {classes: 'qtip-youtube'}
            });

            //$('.form-control').selectize();
        }
    });

    $('.selectizer').selectize();
    $('.selectizer-tags').selectize({
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });

});
// /Sidebar


/**
 *
 * @param dataTable
 * @param selector
 */
function deleteItem(dataTable, selector) {
    var $link = selector;
    bootbox.confirm("Are you sure want to delete!", function (confirmation) {
        if (confirmation) {
            $.get( $link.attr('href'), {})
                .done(function( data ) {
                    dataTable.draw();
                });
        }
    });
}


function showLoader(text) {
    //if (text) {
    //$('#appLoader').addClass('loader loader-double is-active').attr('data-text', text);
    //} else {
    $('#appLoader').addClass('loader loader-double is-active');
    //}
}

function hideLoader() {
    $('#appLoader').removeClass('loader loader-double is-active');
}

$(document).ready(function () {
    //initSummernoteFull();

    $('.chkToggle').bootstrapToggle({
        on: 'Allow',
        off: 'Deny',
        onstyle: 'success',
        offstyle: 'danger'
    });
});

