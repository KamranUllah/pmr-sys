
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import vue from 'vue/dist/vue.esm';
import vueRouter from 'vue-router';
import veeValidate from 'vee-validate';
import xRegexp from 'xregexp';
import * as svgicon from 'vue-svgicon';

window.Vue = vue;
window.VueRouter = vueRouter;
window.VeeValidate = veeValidate;
window.XRegExp = xRegexp;
window.Svgicon = svgicon;

import datePicker from 'vue-bootstrap-datetimepicker';
Vue.use(datePicker);

import Spinner from 'vue-simple-spinner';
Vue.use(Spinner);

//import 'bootstrap/dist/css/bootstrap.css';
//import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css';

Vue.use(VueRouter);
Vue.use(VeeValidate, {fieldsBagName: 'formFields'})
Vue.use(Svgicon, {
    tagName: 'svgicon'
});

import VueNoty from 'vuejs-noty';
Vue.use(VueNoty);

import Vuebar from 'vuebar';
Vue.use(Vuebar);

import Vue2Filters from 'vue2-filters';
Vue.use(Vue2Filters);

Vue.prototype.$ = jQuery;
Vue.prototype.$moment = moment;

export const AppEventBus = new Vue();
Vue.prototype.$appBusEvent = AppEventBus;

//production details
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*
Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});
*/