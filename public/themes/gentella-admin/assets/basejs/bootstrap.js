
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
    window.moment = require('moment');
    window.Vue = require('vue');
    require('moment-timer');
    require('moment-countdown');
    //require('moment-timezone');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-CSRF-TOKEN'] = window.NowGP.csrfToken;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// window.axios.interceptors.response.use((response) => { // intercept the global error
//     return response
// }, function (error) {
//     //-- https://github.com/mzabriskie/axios/issues/690
//     if (error.response.status === 401 || error.response.status === 403) { // if the error is 401 and hasent already been retried
//         // 403 returned
//         swal({
//             title: 'Session Timed Out',
//             html: "Please login again",
//             type: 'info'
//         }).then(function () {
//             window.location.href = NowGP.siteUrl;
//         }, function (dismiss) {
//             // dismiss can be 'cancel', 'overlay',
//             // 'close', and 'timer'
//             if (dismiss === 'cancel') {
//
//             }
//         })
//     }
//
//     if (error.response.status === 404) {
//         return
//     }
//
//     // Do something with response error
//     //return Promise.reject(error)
// });


// Add a 401 response interceptor
window.axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (401 === error.response.status) {
        swal({
            title: "Session Expired",
            html: "Your session has expired. Please login again!",
            type: "warning",
        }).then((result) => {
            window.location.href = window.NowGP.siteUrl;
        })

    } else {
        return Promise.reject(error);
    }
});

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');
window.Dropzone = require('dropzone');
window.Bugsnag = require('bugsnag-js');

require('es6-promise/auto');

//window.Bugsnag.apiKey = 'ee5168eb0ef3156cce1244e97d96e150';

/*if (window.NowGP.broadcastType === 'pusher') {
    window.Echo = new Echo({
        broadcaster: 'pusher',
        key: window.NowGP.pusherKey,
        cluster: window.NowGP.pusherCluster,
        encrypted: window.NowGP.pusherEncrypted,
    });
}*/


