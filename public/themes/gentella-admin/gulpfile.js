/**
 * Gulpfile for Gentella admin theme
 * gulp --cwd ./ --gulpfile public\themes\gentella-admin\gulpfile.js
 */


process.env.DISABLE_NOTIFIER = true;

let elixir = require('laravel-elixir'),
    gulp = require('gulp'),
    unzip = require('gulp-unzip'),
    plumber = require('gulp-plumber'),
    download = require('gulp-download');

let themeInfo = require('./theme.json');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


let paths = {

    bootstrapFonts: [
        './node_modules/bootstrap-sass/assets/fonts/bootstrap/**/*(*.svg|*.ttf|*.woff|*.woff2|*.eot)'
    ],
    fonts: [
        './node_modules/font-awesome/fonts/**/*(*.svg|*.ttf|*.woff|*.woff2|*.eot)',
        './node_modules/propellerkit/dist/fonts/**/*(*.svg|*.ttf|*.woff|*.woff2|*.eot)',
    ],
    materialFonts: [
        './node_modules/bootstrap-material-design-icons/fonts/**/*(*.svg|*.ttf|*.woff|*.woff2|*.eot)',
        './node_modules/material-design-icons/iconfont/**/*(*.svg|*.ttf|*.woff|*.woff2|*.eot)'
    ],
    packageImages: [
        './node_modules/x-editable/dist/bootstrap3-editable/img/**/*(*.gif|*.png|*.jpg|*.jpeg)'
    ],
    iCheckImages: [
        './node_modules/iCheck/skins/flat/**/*(*.gif|*.png|*.jpg|*.jpeg)'
    ],
    dataTablesIcons: [
        './node_modules/datatables/images/**/*(*.gif|*.png|*.jpg|*.jpeg)'
    ]
};


elixir(function (mix) {

    mix.sass([
        './public/themes/' + themeInfo.slug + '/base/sass/app.scss'
    ],'public/themes/' + themeInfo.slug + '/assets/css/app.css');

    mix.sass([
        './public/themes/' + themeInfo.slug + '/base/sass/style.scss'
    ],'public/themes/' + themeInfo.slug + '/assets/css/styles.css');

    mix.styles([
        "./public/themes/" + themeInfo.slug + "/assets/css/app.css",
        './node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',
        "./node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
        './node_modules/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
        './node_modules/bootstrap-toggle/css/bootstrap-toggle.css',
        './node_modules/propellerkit/components/card/css/card.css',
        "./node_modules/qtip2/dist/jquery.qtip.css",
        './node_modules/cropper/dist/cropper.css',
        './node_modules/iCheck/skins/flat/_all.css',
        './node_modules/dropzone/dist/dropzone.css',
        './node_modules/jasny-bootstrap/dist/css/jasny-bootstrap.css',
        './node_modules/sweetalert2/dist/sweetalert2.css',
        "./node_modules/selectize/dist/css/selectize.css",
        "./node_modules/selectize/dist/css/selectize.bootstrap3.css",
        './node_modules/owl.carousel/dist/assets/owl.carousel.css',
        './node_modules/owl.carousel/dist/assets/owl.theme.default.css',
        './node_modules/pure-css-loader/dist/css-loader.css',
        './node_modules/bootstrap-daterangepicker/daterangepicker.css',

        // PROPELLER KIT CSS
        /*'./node_modules/propellerkit/components/accordion/css/accordion.css',
        './node_modules/propellerkit/components/alert/css/alert.css',
        './node_modules/propellerkit/components/card/css/card.css',
        './node_modules/propellerkit/components/checkbox/css/checkbox.css',
        './node_modules/propellerkit/components/radio/css/radio.css',
        './node_modules/propellerkit/components/tab/css/tab.css'*/

    ],'public/themes/' + themeInfo.slug + '/assets/css/app.css');

    mix.scripts([
        './node_modules/lodash/lodash.js',
        './node_modules/bugsnag-js/src/bugsnag.js',
        './node_modules/pusher-js/dist/web/pusher.js',
        './node_modules/axios/dist/axios.js',
        './node_modules/vue/dist/vue.js',
        './node_modules/vue-router/dist/vue-router.js',
        './node_modules/vuex/dist/vuex.js',
        './node_modules/vee-validate/dist/vee-validate.js',
        './node_modules/jquery/dist/jquery.js',
        './node_modules/jquery-migrate/dist/jquery-migrate.js',
        './node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
        './node_modules/modernizr/bin/modernizr',
        './node_modules/selectize/dist/js/standalone/selectize.js',
        './node_modules/bootstrap-toggle/js/bootstrap-toggle.js',
        './node_modules/moment/moment.js',
        './node_modules/cropper/dist/cropper.js',
        './node_modules/fastclick/lib/fastclick.js',
        './node_modules/fullcalendar/dist/fullcalendar.js',
        './node_modules/owl.carousel/dist/owl.carousel.js',
        //'./node_modules/nivo-lightbox/nivo-lightbox.js',
        './node_modules/bootstrap-progressbar/bootstrap-progressbar.min.js',
        './node_modules/dropzone/dist/dropzone.js',
        './node_modules/iCheck/icheck.js',
        './node_modules/flot/jquery.flot.js',
        './node_modules/flot/jquery.flot.pie.js',
        './node_modules/flot/jquery.flot.time.js',
        './node_modules/flot/jquery.flot.stack.js',
        './node_modules/flot/jquery.flot.resize.js',
        './node_modules/Laravel-Bootstrap-Modal-Form/src/laravel-bootstrap-modal-form.js',
        './node_modules/datatables.net/js/jquery.dataTables.js',
        './node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
        './node_modules/datatables.net-buttons/js/dataTables.buttons.js',
        './node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.js',
        './node_modules/datatables.net-fixedheader/js/dataTables.fixedHeader.js',
        './node_modules/datatables.net-keytable/js/dataTables.keyTable.js',
        './node_modules/datatables.net-responsive/js/dataTables.responsive.js',
        './node_modules/datatables.net-rowreorder/js/dataTables.rowReorder.js',
        './node_modules/datatables.net-scroller/js/dataTables.scroller.js',
        './node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
        './node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        './node_modules/moment/min/moment-with-locales.min.js',
        './node_modules/bootstrap-validator/dist/validator.min.js',
        './node_modules/parsleyjs/dist/parsley.min.js',
        './node_modules/bootbox/bootbox.js',
        './node_modules/qtip2/dist/jquery.qtip.js',
        './node_modules/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js',
        './node_modules/gmaps/gmaps.js',
        './node_modules/material-design-lite/material.js',
        './node_modules/jasny-bootstrap/dist/js/jasny-bootstrap.js',
        './node_modules/sweetalert2/dist/sweetalert2.js',
        './node_modules/bootstrap-daterangepicker/daterangepicker.js',
        './node_modules/block-ui/jquery.blockUI.js',
        './node_modules/store-js/dist/store.everything.min.js',

        // PROPELLER KIT JS
        /*'./node_modules/propellerkit/components/accordion/js/accordion.js',
        './node_modules/propellerkit/components/alert/js/alert.js',
        './node_modules/propellerkit/components/checkbox/js/checkbox.js',
        './node_modules/propellerkit/components/radio/js/radio.js',*/

        // JS GENERARTED LARAVEL ROUTES
        './resources/assets/js/routes.js',
        './resources/assets/js/laroute.js'

    ],'public/themes/' + themeInfo.slug + '/assets/js/site_lib.js');


    mix.copy(paths.packageImages, 'public/themes/' + themeInfo.slug + '/assets/img');
    mix.copy(paths.packageImages, 'public/themes/' + themeInfo.slug + '/assets/images');
    mix.copy(paths.dataTablesIcons, 'public/themes/' + themeInfo.slug + '/assets/images');
    mix.copy(paths.iCheckImages, 'public/themes/' + themeInfo.slug + '/assets/css');
    mix.copy(paths.bootstrapFonts, 'public/themes/' + themeInfo.slug + '/assets/fonts/bootstrap');
    mix.copy(paths.materialFonts, 'public/themes/' + themeInfo.slug + '/assets/fonts');
    mix.copy(paths.fonts, 'public/themes/' + themeInfo.slug + '/assets/fonts');

    mix.browserSync();
});