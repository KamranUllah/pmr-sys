<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class AppBuild extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:build {env? : Environment to compile base assets and libraries for} {--cache-clean : Clean system cached data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Determine whether wto build for dev or production';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Symfony\Component\Process\Exception\ProcessFailedException
     */
    public function handle()
    {
        $environment = $this->argument('env');
        $cacheClean = $this->option('cache-clean');

        if (!$cacheClean) {
            $cacheClean = false;
        }

        if ($environment) {
            switch($environment) {
                case 'dev':
                    $this->line("\nBuilding assets for a development environment...");
                    //if (config('pmr.enable_v2_chat')) {
                        $process = new Process('php artisan laroute:generate && gulp --cwd ./ --gulpfile public\themes\gentella-admin\gulpfile.webpack.js');
                    /*} else {
                        $process = new Process('php artisan laroute:generate && gulp --cwd ./ --gulpfile public\themes\gentella-admin\gulpfile.js');
                    }*/

                    $process->run();

                    // executes after the command finishes
                    if (!$process->isSuccessful()) {
                        throw new ProcessFailedException($process);
                    }

                    echo $process->getOutput();
                    break;

                case 'prod':
                    $this->line("\nBuilding assets for a production environment...");
                    //if (config('pmr.enable_v2_chat')) {
                        $process = new Process('php artisan laroute:generate && gulp --cwd ./ --gulpfile public\themes\gentella-admin\gulpfile.webpack.js --production');
                    /*} else {
                        $process = new Process('php artisan laroute:generate && gulp --cwd ./ --gulpfile public\themes\gentella-admin\gulpfile.js --production');
                    }*/

                    $process->run();

                    // executes after the command finishes
                    if (!$process->isSuccessful()) {
                        throw new ProcessFailedException($process);
                    }

                    echo $process->getOutput();
                    break;

                default:
                    $this->line("\nSelected environment does not exist");
                    break;
            }
        }

        if ($cacheClean) {
            $this->line("\nCleaning view caches");
            //$this->call('route:clear');
            $this->call('view:clear');

            $this->line("\nCleaning app caches");
            //$this->call('route:clear');
            $this->call('cache:clear');

            $this->line("\nCleaning config caches");
            //$this->call('config:clear');
            $this->call('config:cache');

            $this->line("\nCleaning route caches");
            //$this->call('route:clear');
            $this->call('route:cache');
        }
    }
}
