<?php

namespace App\Console\Commands;

use Artisan;
use Illuminate\Console\Command;
use Module;
use Nwidart\Modules\Publishing\AssetPublisher;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class PublishModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publish:module {module? : The name of the module to publish} {--minify : Minify and transpile module assets}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish selected module(s)';
    /**
     * @var Process
     */
    private $process;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Symfony\Component\Process\Exception\ProcessFailedException
     */
    public function handle()
    {
        $moduleName = $this->argument('module');
        $minify = $this->option('minify');

        if (!$minify) {
            $minify = false;
        }

        if (!$moduleName) {
            if ($this->confirm('Publish all modules? [y|N]') && $minify) {
                foreach ($this->laravel['modules']->enabled() as $module) {
                    $this->line("\nPublishing module " . ucfirst($module) . '...');
                    $this->publish($module);

                    if (file_exists(app_path("Modules/{$module}/gulpfile.js"))) {
                        $this->line("\nTranspiling and minifying assets...");
                        $process = new Process("gulp --cwd ./ --gulpfile app\\Modules\\{$module}\\gulpfile.js --production");
                        $process->run();

                        // executes after the command finishes
                        if (!$process->isSuccessful()) {
                            throw new ProcessFailedException($process);
                        }

                        echo $process->getOutput();
                    }
                }
            } else {
                foreach ($this->laravel['modules']->enabled() as $module) {
                    $this->line("\nPublishing module " . ucfirst($module) . '...');
                    $this->publish($module);

                    if (file_exists(app_path("Modules/{$module}/gulpfile.js"))) {
                        $this->line("\nTranspiling assets...");
                        $process = new Process("gulp --cwd ./ --gulpfile app\\Modules\\{$module}\\gulpfile.js");
                        $process->run();

                        // executes after the command finishes
                        if (!$process->isSuccessful()) {
                            throw new ProcessFailedException($process);
                        }

                        echo $process->getOutput();
                    }
                }
            }
        } else {
            if ($minify) {
                $this->line("\nPublishing module " . ucfirst($moduleName) . '...');
                $this->publish($moduleName);

                if (file_exists(app_path("Modules/{$moduleName}/gulpfile.js"))) {
                    $this->line("\nTranspiling and minifying assets...");
                    $process = new Process("gulp --cwd ./ --gulpfile app\\Modules\\{$moduleName}\\gulpfile.js --production");
                    $process->run();

                    // executes after the command finishes
                    if (!$process->isSuccessful()) {
                        throw new ProcessFailedException($process);
                    }

                    echo $process->getOutput();
                }
            } else {
                $this->line("\nPublishing module " . ucfirst($moduleName) . '...');
                $this->publish($moduleName);

                if (file_exists(app_path("Modules/{$moduleName}/gulpfile.js"))) {
                    $this->line("\nTranspiling assets...");
                    $process = new Process("gulp --cwd ./ --gulpfile app\\Modules\\{$moduleName}\\gulpfile.js");
                    $process->run();

                    // executes after the command finishes
                    if (!$process->isSuccessful()) {
                        throw new ProcessFailedException($process);
                    }

                    echo $process->getOutput();
                }
            }
        }
    }

    /**
     * Publish assets from the specified module.
     *
     * @param string $name
     */
    public function publish($name)
    {
        if ($name instanceof Module) {
            $module = $name;
        } else {
            $module = $this->laravel['modules']->findOrFail($name);
        }

        with(new AssetPublisher($module))
            ->setRepository($this->laravel['modules'])
            ->setConsole($this)
            ->publish();

        $this->line("<info>Published</info>: {$module->getStudlyName()}");
    }
}
