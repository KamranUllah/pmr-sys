new Vue({
    name: 'notifications-watch',

    el: '#notifications-watcher',

    mounted() {

    },

    created() {
        var self = this;

        if (NowGP.chat_enabled) {

            // Disable chat polling for administrators
            console.log(NowGP.user.roles[0].slug);
            if (NowGP.user.roles[0].slug === 'super-administrator' || NowGP.user.roles[0].slug === 'administrator') {
                // Do nothing
            } else {
                setTimeout(function () {

                    // self.checkActiveConversationStatus();

                    var newConversationsCheck = setInterval(function () {
                        self.checkForNewConversations();
                    }, 1000 * NowGP.new_conversation_interval_check); // Check for new conversations every x seconds


                    var newMessagesCheck = setInterval(function () {
                        // Get the next appointment
                        self.checkForNewConversationMessages();

                    }, 1000 * NowGP.new_chat_interval_check); // Check for new appointments every 10 seconds

                    if (NowGP.currentRoute === 'pmr.chats') {
                        var conversationStatusCheck = setInterval(function () {
                            // Get the next appointment
                            self.checkActiveConversationStatus();

                        }, 1000 * NowGP.new_conversation_interval_check); // Check for new appointments every 10 seconds
                    }

                }, 2000); // Delay the polling for about 3 seconds
            }
        }

    },

    data() {
        return {
            lastConversationId: 0,
            lastMessageId: 0
        }
    },

    methods: {
        appBus() {
            return this.$appBusEvent;
        },

        checkForNewConversations() {
            var self = this;
            axios.get(laroute.route('check.for.conversations', {lastConversationId: self.lastConversationId}))
            //axios.get(NowGP.siteUrl + '/admin/chat/check_new_conversations/' + self.lastConversationId)
                .then(function (response) {


                    self.lastConversationId = response.data.lastConversationId;

                    if (response.data.result > 0) {
                        if (NowGP.currentRoute === 'pmr.chats') {
                            self.appBus().$emit('refresh-waiting-conversations');
                        } else {
                            new Noty({
                                type: 'success',
                                theme: 'bootstrap-v3',
                                text: 'You have ' + response.data.result + ' new waiting conversations(s)',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button'],
                                buttons: [
                                    Noty.button('View', 'btn btn-default btn-sm', function () {
                                        showLoader();
                                        //window.location.href = laroute.route('notifications.index');
                                        window.location.href = laroute.route('pmr.chats');
                                    }, {id: 'button1', 'data-status': 'ok'})

                                ]
                            }).show();
                        }
                    }

                })
                .catch(function (error) {
                    console.log(error);
                })
        },

        checkForNewConversationMessages() {
            var self = this;
            axios.get(laroute.route('check.for.messages', {
                lastMessageId: self.lastMessageId
            }))
                .then(function (response) {

                    //self.lastConversationId = response.data.lastConversationId;
                    self.lastMessageId = response.data.lastMessageId;

                    //if (response.data.result > 0 && (response.data.lastUserType === 'Support' || response.data.lastUserType === 'Pharmacy')) {
                    if (response.data.result > 0 && response.data.lastUserType === 'User') {

                        if (NowGP.currentRoute === 'pmr.chats') {
                            console.log('Emitting event: ====> refresh-active-conversation');
                            self.appBus().$emit('refresh-active-conversation');
                        } else {
                            new Noty({
                                type: 'success',
                                theme: 'bootstrap-v3',
                                text: 'You have ' + response.data.result + ' new message(s)',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button'],
                                buttons: [
                                    Noty.button('View', 'btn btn-default btn-sm', function () {
                                        showLoader();
                                        //window.location.href = laroute.route('notifications.index');
                                        window.location.href = laroute.route('pmr.chats');
                                    }, {id: 'button1', 'data-status': 'ok'})

                                ]
                            }).show();
                        }
                    }

                })
                .catch(function (error) {
                    //
                    /*if (error.response.status === 403 || error.response.status === 401) {
                        handleAuthUserErrors(error.response.status); // DNV-4771
                    }*/
                })
        },

        checkActiveConversationStatus() {
            var self = this;
            self.appBus().$emit('check-active-conversation-status');
        }
    },

    computed: {}
});