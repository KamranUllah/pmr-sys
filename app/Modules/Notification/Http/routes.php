<?php

Route::group(['middleware' => 'web', 'prefix' => 'notification', 'namespace' => 'App\\Modules\Notification\Http\Controllers'], function()
{
    Route::get('/', 'NotificationController@index');
});
