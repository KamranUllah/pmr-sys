const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

elixir(function (mix) {

    mix.webpack(
        './app/Modules/Notification/Assets/js/notification.js'
        , 'public/modules/notification/js/notification.js');

});
