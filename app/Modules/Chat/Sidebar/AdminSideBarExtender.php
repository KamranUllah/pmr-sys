<?php
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 05/09/2016
 * Time: 16:22
 */

namespace App\Modules\Chat\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\SidebarExtender;
use Sentinel;

class AdminSideBarExtender implements SidebarExtender
{

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group('Chat', function (Group $group) {
            $group->hideHeading();
            $group->weight(-1);
            $group->item('Chat', function (Item $item) {
                $item->toggleIcon('fa fa-chevron-down');
                $item->icon('fa fa-comment');
                $item->weight(0);
                $item->route('pmr.chats');
                /*$item->badge(function (Badge $badge, Page $page) {
                    $badge->setClass('bg-green');
                    $badge->setValue($page->count());
                });
                $item->authorize(
                    request()->user()->hasAccess('page.pages.index')
                );*/
            });

            if (config('pmr.enable_chat') && Sentinel::check()) {
                $group->authorize(
                    Sentinel::getUser()->inRole('super-administrator') ||
                    Sentinel::getUser()->inRole('administrator') ||
                    Sentinel::getUser()->inRole('nurse') ||
                    Sentinel::getUser()->inRole('clinical') ||
                    Sentinel::getUser()->inRole('pharmacist-level-1') ||
                    Sentinel::getUser()->inRole('pharmacist-level-2') ||
                    Sentinel::getUser()->inRole('lead-pharmacist')
                );
            }
        });

        return $menu;
    }
}
