<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 07/03/2017
 * Time: 12:11
 */

namespace App\Modules\Chat\NowGP;

use App\Services\DrNowV2API;

class ConversationApi extends DrNowV2API
{

    public function __construct()
    {
        parent::__construct();
        //$this->conversationApiUrl = config('chat.conversation_api_url');
    }

    /**
     * Get Conversation Messages
     *
     * @param $doctorId
     * @param $conversationId
     * @return mixed
     */
    public function getConversationMessages($doctorId, $conversationId)
    {
        $action = $this->conversationApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations/{$conversationId}/messages";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Get conversation details
     *
     * @param $doctorId
     * @param $conversationId
     * @return mixed
     */
    public function getConversationDetails($doctorId, $conversationId)
    {
        $action = $this->conversationApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations/{$conversationId}";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Post new message to conversation
     *
     * @param $doctorId
     * @param $conversationId
     * @return mixed
     */
    public function postToConversation($doctorId, $conversationId, $message)
    {
        $action = $this->conversationApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations/{$conversationId}/messages";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('POST', $action, true, $message);

        return json_decode($response->getBody());
    }

    /**
     * Get available conversations for the doctor
     *
     * @param $doctorId
     * @return mixed
     */
    public function getDoctorConversations($doctorId)
    {
        $action = $this->conversationApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Get the most recent/newest conversations for the doctor
     *
     * @param $doctorId
     * @param $lastConversationId
     * @return mixed
     * @internal param array $params
     */
    public function getLatestDoctorConversations($doctorId, $lastConversationId)
    {
        $action = $this->conversationApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations?overId=" . $lastConversationId;

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Get the most recent/newest messages for a conversation
     *
     * @param $doctorId
     * @param $lastMessageId
     * @return mixed
     */
    public function getLatestConversationMessages($doctorId, $lastMessageId)
    {
        $action = $this->conversationApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations/all/messages?overId=" . $lastMessageId;

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Join a conversation
     *
     * @param $doctorId
     * @param $conversationId
     * @return mixed
     */
    public function joinConversation($doctorId, $conversationId)
    {
        $action = $this->conversationApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations/{$conversationId}/join";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('PUT', $action, true);

        return json_decode($response->getBody());
    }
}
