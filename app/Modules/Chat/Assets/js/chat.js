/**
 * Created by DevAdmin on 07/03/2017.
 */
//custom function to add and remove active class
$(function () {
    $(".collapse.in").parents(".panel").addClass("active");
    $('a[data-toggle="collapse"]').on('click', function () {
        var objectID = $(this).attr('href');
        var expandale = $(this).attr('data-expandable');
        if (expandale === 'true') {
            if ($(objectID).hasClass('in')) {
                $(objectID).collapse('hide');
            }
            else {
                $(objectID).collapse('show');
            }
        }
        $accoID = $(this).parents(".panel-group").attr("id");
        $availiblity = $(this).parents(".panel").children(".in").length;
        $expandable = $(this).attr("data-expandable");
        $expanded = $(this).attr("aria-expanded");
        $current = $(this).parent().parent().parent().parent().attr("id");
        if ($expandable === "false") {
            if ($expanded === "true") {
                $("#" + $current + " .active").removeClass("active");
            }
            else {
                $("#" + $current + " .active").removeClass("active");
                $(this).parents('.panel').addClass("active");
            }
        }
        if ($expandable === "true") {
            if ($expanded === "true") {
                $(this).parents('.panel').addClass("active");
            }
            else {
                $(this).parents('.panel').removeClass("active");
            }
        }
    });

    // custom function for expand all and collapse all button
    $('#expandAll').on('click', function () {
        var GetID = $(this).attr("data-target");
        $('#' + GetID + ' ' + 'a[data-toggle="collapse"]').each(function () {
            var objectID = $(this).attr('href');
            if ($(objectID).hasClass('in') === false) {
                $(objectID).collapse('show');
                $(objectID).parent().addClass("active");
            }
        });
    });
    // collapse
    $('#collapseAll').on('click', function () {
        var GetID = $(this).attr("data-target");
        $('#' + GetID + ' ' + 'a[data-toggle="collapse"]').each(function () {
            var objectID = $(this).attr('href');
            $(objectID).collapse('hide');
            $(objectID).parent().removeClass("active");
        });
    });

});

var chatCheckInterval, currentConversationStatusCheck;
var $body = $('body');

// We use the working variable to prevent
// multiple form submissions:
var working = false;

var chat = {
    // data holds variables for use in the class:

    data: {
        lastID: 0,
        noActivity: 0,
        lastUserType: '',
        updatedConversations: '',
        currentOpenConversationId: 0
    },

    addChatLine: function (params) {

        // All times are displayed in the user's timezone

        var d = new Date();
        if (params.time) {

            // PHP returns the time in UTC (GMT). We use it to feed the date
            // object and later output it in the user's timezone. JavaScript
            // internally converts it for us.

            d.setUTCHours(params.time.hours, params.time.minutes);
        }

        params.time = (d.getHours() < 10 ? '0' : '' ) + d.getHours() + ':' +
            (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();

        var markup = chat.render('chatLine', params),
            exists = $('#chatLineHolder .chat-' + params.id);

        if (exists.length) {
            exists.remove();
        }

        /*if (!chat.data.lastID) {
         // If this is the first chat, remove the
         // paragraph saying there aren't any:

         $('#chatLineHolder p').remove();
         }*/

        // If this isn't a temporary chat:
        if (params.id.toString().charAt(0) != 't') {
            var previous = $('#chatLineHolder .chat-' + (+params.id - 1));
            if (previous.length) {
                previous.after(markup);
            }
            else chat.data.jspAPI.getContentPane().append(markup);
        }
        else chat.data.jspAPI.getContentPane().append(markup);

        // As we added new content, we need to
        // reinitialise the jScrollPane plugin:

        //chat.data.jspAPI.reinitialise();
        //chat.data.jspAPI.scrollToBottom(true);
    },
    render: function (template, params) {

        var arr = [];
        switch (template) {
            case 'patientChatLine':

                arr = [
                    '<div class="row msg_container base_receive chat chat-', params.chatId, '" data-patient-id="', params.userId, '" data-user-type="', params.userType, '" >' +
                    ' <div class="col-md-1 col-sm-2 col-xs-2 avatar">' +
                    '<img src="', params.avatar, '" class=" img-circle img-responsive "> ' +
                    '</div>' +
                    '<div style="max-width: 60%">' +
                    '<div class="messages msg_receive break-word">' +
                    '<p>', params.text, '</p> ' +
                    '<time datetime="', params.time, '">' +
                    '<span class="patient-name">', params.author, '</span> • ', params.time, '</time>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                ];

                /*arr = [
                 '<div class="chat chat-', params.id, ' rounded"><span class="gravatar">' +
                 '<img src="', params.gravatar, '" width="23" height="23" ' +
                 'onload="this.style.visibility=\'visible\'" />',
                 '</span><span class="author">', params.author,
                 ':</span><span class="text">', params.text,
                 '</span><span class="time">', params.time, '</span></div>'];*/
                break;

            case 'doctorChatLine':
                arr = [
                    '<div class="row msg_container base_sent chat chat-', params.chatId, '" data-patient-id="', params.userId, '" data-user-type="', params.userType, '" >' +
                    ' <div class="col-md-1 col-sm-2 col-xs-2 avatar">' +
                    '<img src="', params.avatar, '" class=" img-circle img-responsive "> ' +
                    '</div>' +
                    '<div style="max-width: 60%">' +
                    '<div class="messages msg_sent break-word">' +
                    '<p>', params.text, '</p> ' +
                    '<time datetime="', params.time, '">' +
                    '<span class="doctor-name">', params.author, '</span> • ', params.time, '</time>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                ];
                /*arr = [
                 '<div class="chat chat-', params.id, ' rounded"><span class="gravatar">' +
                 '<img src="', params.gravatar, '" width="23" height="23" ' +
                 'onload="this.style.visibility=\'visible\'" />',
                 '</span><span class="author">', params.author,
                 ':</span><span class="text">', params.text,
                 '</span><span class="time">', params.time, '</span></div>'];*/
                break;
        }

        // A single array join is faster than
        // multiple concatenations

        return arr.join('');

    }
};

var conversation = {
    // data holds variables for use in the class:

    data: {
        lastID: 0,
        noActivity: 0,
        lastNewConversationCount: 0
    }
};


/**
 * Log and audit user actions for ajax related things
 * @param message
 */
function ajaxChatAudit(message) {
    if (typeof message !== "undefined") {
        // axios.post(laroute.route('chat.logger'), {audit: message})
        return axios.post(NowGP.siteUrl + '/chat/chat_log', {audit: message})
            .then(function () {
            })
            .catch(function (error) {
                console.log(error);
            })
    }
}


$(window).load(function () {

    if (NowGP.currentRoute === "pmr.chats") {
        updateConversationLists();
    }


    if (NowGP.currentRoute === "pmr.chats") {
        setInterval(function () {
            // Get the next appointment
            getNextAppointment();
            refreshJSAppointmentVariables();
        }, 1000 * 60 * 5); // Check for new appointments every 5 minutes
    }

    if (NowGP.currentRoute === "pmr.chats") {
        chatCheckInterval = setInterval(function () {
            // Update the current conversation screen
            if ($body.data('current-conversation-id') !== "" && $body.data('current-conversation-status') === "open") {
                var conversationData = {
                    conversationId: $body.data('current-conversation-id'),
                    conversationStatus: $body.data('current-conversation-status')
                };
                checkforNewMessages(conversationData);
            }

        }, 1000 * NowGP.chat_interval_check); // Check for new conversation messages every x seconds
    }


    var newConversationsCheck = setInterval(function () {
        // Get the next appointment
        checkForNewConversations();

    }, 1000 * NowGP.new_conversation_interval_check); // Check for new conversations every x seconds


    var newMessagesCheck = setInterval(function () {
        // Get the next appointment
        checkForLatestMessages();

    }, 1000 * NowGP.new_chat_interval_check); // Check for new appointments every 10 seconds

});

$(document).ready(function () {

    initMScrollbar();
    fetchOpenMessage();
    fetchWaitingMessage();
    fetchClosedMessage();

    $('#submitMyForm').on('click', function () {
        $('#chatReplyForm').submit();
    });

    $('#chatReplyForm').on('submit', function (e) {
        e.preventDefault();

        if (working) return false;
        working = true;

        var conversationData = {
            conversationId: $('#chatReply').data('conversation-id'),
            conversationMessage: $('#chatReply').val(),
            conversationStatus: $(this).data('conversation-status')
        };

        // Assigning a temporary ID to the chat:
        /*var tempID = 't' + Math.round(Math.random() * 1000000),
         params = {
         chatId: tempID,
         userId: NowGP.doctorId,
         author: NowGP.doctorName,
         avatar: NowGP.doctorAvatar,
         text: conversationData.conversationMessage.replace(/</g, '&lt;').replace(/>/g, '&gt;')
         };*/

        // Using our addChatLine method to add the chat
        // to the screen immediately, without waiting for
        // the AJAX request to complete:

        //chat.addChatLine($.extend({}, params));


        //$('.sending-animation').html('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> Sending message...');
        //$('#submitMyForm').prop('disabled', true);
        //$('#chatReplyForm').unbind('submit');
        $("#submitMyForm").css("pointer-events", "none").html('<i class="fa fa-spinner fa-pulse fa-fw fa-2x margin-bottom"></i>');
        axios.post(laroute.route('conversation.messages.add'), conversationData)
        return axios.post(NowGP.siteUrl + '/chat/add_to_conversation', conversationData)
            .then(function (response) {
                // console.log(viewConversation);
                checkforNewMessages(conversationData);
                // Update the current message with the temp ID with the correct message id

                /*$('div.chat-'+tempID).remove();

                 params['id'] = response.data.messagesView.chatMessageId;
                 chat.addChatLine($.extend({},params));*/

            })
            .then(function () {
                working = false;
                if ($('#chatReplyForm').is(':visible')) {
                    $('#chatReplyForm').get(0).reset();
                    $('#chatReplyForm').parsley().reset();
                }
                $('.sending-animation').html('');
                $("#submitMyForm").css("pointer-events", "auto").html('Send');
                //$('#submitMyForm').prop('disabled', false);
                /*$('#chatReplyForm').bind('submit', function (e) {
                 e.preventDefault();
                 });*/
            })
            .catch(function (error) {
                //console.log(error);
            });

    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //initFreewall();

        //console.log(e.target);
        //console.log(e.relatedTarget);

        var target = $(e.target).attr("href");

        var userData;

        switch (target) {
            case '#chatFeed':
                $body.data('current-conversation-status', '');
                $body.data('current-conversation-id', '');
                ajaxChatAudit('Doctor has accessed the open chat tab');
                $('.panel-footer').addClass('panel-footer-basic').removeClass('text-danger text-center').css('font-weight', 'normal').html('');
                break;
            case '#closedChats':
                $body.data('current-conversation-status', '');
                $body.data('current-conversation-id', '');
                ajaxChatAudit('Doctor has accessed the closed chat tab');
                $('.panel-footer').addClass('panel-footer-basic').removeClass('text-danger text-center').css('font-weight', 'normal').html('');
                axios.post(laroute.route('conversation.lists'))
                    .then(function (response) {
                            $('#closedConversationsList').html(response.data.closedConversationsList);
                            fetchClosedMessage();
                            initMScrollbar();
                            getpatientIdsFromClosedConversations();
                        }
                    );
                break;
        }

    })
});


/**
 * Add doctor to conversation and load the messages in the conversation
 */
function fetchWaitingMessage() {
    $('.conversation-waiting').on('click', function () {

        var conversationData = {
            conversationId: $(this).data('conversation-id'),
            conversationStatus: $(this).data('conversation-status')
        };

        if ($('#chatReplyForm').is(':visible')) {
            $('#chatReplyForm').get(0).reset();
            $('#chatReplyForm').parsley().reset();
        }

        //console.log(conversationData.conversationId);
        $('#conversation-fetch-msg').html('<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> Loading...');
        $('.chat-panel').block({message: '<span class="fa fa-spin fa-spinner fa-lg"></span><br />..Please wait..'});
        return axios.post(laroute.route('conversation.messages.join'), conversationData)
            .then(function (response) {
                /*var conversationDiv = $(".conversation-waiting[data-conversation-id='" + conversationData.conversationId + "']").html();
                 $(".conversation-waiting[data-conversation-id='" + conversationData.conversationId + "']").remove();

                 var appendOpener = '<div class="pmd-card pmd-card-default pmd-z-depth conversation-open" ' +
                 'data-conversation-id="' + conversationData.conversationId + '" ' +
                 'data-patient-id="" ' +
                 'data-doctor-id="" ' +
                 'style="margin-bottom: 5px !important;">';
                 var appendCloser = '</div>';
                 $(".conversation-open").parent().append(appendOpener + conversationDiv + appendCloser);*/
                $body.data('current-conversation-status', response.data.conversationStatus);
                $body.data('current-conversation-id', response.data.conversationId);

                updateConversationLists();

                var responseConversationData = {
                    conversationId: response.data.conversationId,
                    conversationStatus: response.data.conversationStatus
                };
                
                console.log('conversation joined!');



                $('#chatReplyForm').show();
                $body.data('current-conversation-status', 'open');
                $('.conversation-open[data-conversation-id=' + response.data.conversationId + ']').addClass('pmd-card-open-active');
                if ($('.conversation-open[data-conversation-id=' + response.data.conversationId + ']').find('.updated-conversation-label').is(':visible')) {
                    $('.conversation-open[data-conversation-id=' + response.data.conversationId + ']').find('.updated-conversation-label').addClass('hidden');
                }
                $('#chatReplyFromContainer').removeClass('hidden');
                $('.panel-footer').removeClass('panel-footer-basic');

                if ($('#chatReply').is(':visible')) {
                    $('#chatReply').data('conversation-id', response.data.conversationId);
                }

                if ($('#chatReplyForm').is(':visible')) {
                    $('#chatReplyForm').get(0).reset();
                    $('#chatReplyForm').parsley().reset();
                }

                //console.log(conversationData.conversationId);
                if ($('#chatReply').is(':visible')) {
                    $('#chatReplyForm').parsley().reset();
                }

                checkforNewMessages(responseConversationData);
                fetchOpenMessage();
                getCurrentConversationStatus(responseConversationData);

            })
            .catch(function (error) {
                console.log(error);
            });
    });
}

/**
 * Fetch messages on closed conversation
 */
function fetchClosedMessage() {
    $('.conversation-closed').on('click', function () {

        var conversationData = {
            conversationId: $(this).data('conversation-id'),
            conversationStatus: $(this).data('conversation-status')
        };

        if ($('#chatReplyForm').is(':visible')) {
            $('#chatReplyForm').get(0).reset();
            $('#chatReplyForm').parsley().reset();
        }
        $('.closed-chat-panel').block({message: '<span class="fa fa-spin fa-spinner fa-lg"></span><br />..Please wait..'});
        getClosedConversationMessages(conversationData);
    });
}

/**
 * Get the messages for the closed conversation
 * @param conversationData
 * @returns {*|Promise<R>}
 */
function getClosedConversationMessages(conversationData) {
    $('#closed-conversation-fetch-msg').html('<i class="fa fa-spinner fa-spin fa-fw margin-bottom"></i> Loading...');
    return axios.post(laroute.route('conversation.messages'), conversationData)
        .then(function (response) {
            //console.log(response);

            $('#chatReplyForm').hide();
            $body.data('current-conversation-status', 'closed');
            $('#chatReplyFromContainer').addClass('hidden');
            $body.data('current-conversation-id', response.data.conversationId);


            $('.msg_closed_container_base').html(response.data.messagesView);
            $('.closed-chat-panel').unblock();
            $('#closed-conversation-fetch-msg').html('');
            //scrollToLatestMessage();
            initMsgBaseScrollbar();
        })
        .then(function () {
            // do something else here
            getConversationPatientDetails();
        }).then(function () {
            getConversationDoctorDettails();
        })
        .catch(function (error) {
            //console.log(error);
            $('#closed-conversation-fetch-msg').html('Unable to load closed chats.');
        });
}

/**
 * Fetch messages on currently ongoing conversation
 */
function fetchOpenMessage() {
    $('.conversation-open').on('click', function () {

        // Reset  conversation details
        $body.data('current-conversation-id', '');
        $body.data('current-conversation-status', '');
        chat.data.currentOpenConversationId = 0;

        var conversationData = {
            conversationId: $(this).data('conversation-id'),
            conversationStatus: $(this).data('conversation-status')
        };

        $body.data('current-conversation-id', conversationData.conversationId);
        chat.data.currentOpenConversationId = conversationData.conversationId;
        $('#chatReplyForm').show();
        $body.data('current-conversation-status', 'open');
        $('.conversation-open[data-conversation-id=' + conversationData.conversationId + ']').addClass('pmd-card-open-active');
        if ($('.conversation-open[data-conversation-id=' + conversationData.conversationId + ']').find('.updated-conversation-label').is(':visible')) {
            $('.conversation-open[data-conversation-id=' + conversationData.conversationId + ']').find('.updated-conversation-label').addClass('hidden');
        }
        $('#chatReplyFromContainer').removeClass('hidden');
        $('.panel-footer').removeClass('panel-footer-basic');

        if ($('#chatReply').is(':visible')) {
            $('#chatReply').data('conversation-id', conversationData.conversationId);
        }

        if ($('#chatReplyForm').is(':visible')) {
            $('#chatReplyForm').get(0).reset();
            $('#chatReplyForm').parsley().reset();
        }

        //if ($(this).hasClass('pmd-card-open-active')) {
        $('.conversation-open').removeClass('pmd-card-open-active');
        // }

        $('.chat-panel').block({message: '<span class="fa fa-spin fa-spinner fa-lg"></span><br />..Please wait..'});

        //console.log(conversationData.conversationId);
        if ($('#chatReply').is(':visible')) {
            $('#chatReplyForm').parsley().reset();
        }
        checkforNewMessages(conversationData);
        getCurrentConversationStatus(conversationData);
    });
}

/**
 * Check for a new messages on a selected conversation
 * @param conversationData
 * @returns {*|Promise<R>}
 */
function checkforNewMessages(conversationData) {
    $('#conversation-fetch-msg').html('<i class="fa fa-spinner fa-spin fa-fw margin-bottom"></i> Loading...');
    return axios.post(laroute.route('conversation.messages'), conversationData)
        .then(function (response) {
            //console.log(response);
            initMsgBaseScrollbar();
            $('.msg_container_base').html(response.data.messagesView);
            $('.chat-panel').unblock();
            $('#conversation-fetch-msg').html('');
            scrollToLatestMessage();
            initMsgBaseScrollbar();
        })
        .then(function () {
            // do something else here
            getConversationPatientDetails();
        }).then(function () {
            getConversationDoctorDettails();
        }).then(function () {

        })
        .catch(function (error) {
            //console.log(error);
        });
}


/**
 * Initiate the scrollbar on the conversation lists
 */
function initMScrollbar() {
    //language=JQuery-CSS
    $('.conversations-list').mCustomScrollbar({
        theme: "dark-thick",
        /*setHeight: 405,*/
        //setHeight: $('#schedule-calendar').height(),
        advanced: {
            updateOnContentResize: true,
            updateOnBrowserResize: true
        }
    });
}


/**
 * Initial the custom scrollar styles on the chat message window
 */
function initMsgBaseScrollbar() {
    //console.log('msg_base_container_scroll');

    //language=JQuery-CSS
    $('.msg_container_base').mCustomScrollbar({
        theme: "dark-thick",
        /*setHeight: 405,*/
        //setHeight: $('#schedule-calendar').height(),
        advanced: {
            updateOnContentResize: true,
            updateOnBrowserResize: true
        }
    });
}

/**
 * Scroll to the last message on a selected conversation
 */
function scrollToLatestMessage() {
    var msgWindow = $('.msg_container_base');
    var height = msgWindow[0].scrollHeight;
    msgWindow.scrollTop(height);
}

/**
 * Update the list of waiting and open conversations
 */
function updateConversationLists() {
    //return axios.post(laroute.route('conversation.lists'))
    return axios.post(laroute.route('conversation.lists'))
        .then(function (response) {
                $('#waitingConversationsList').html(response.data.waitingConversationsList);
                //$('#closedConversationsList').html(response.data.closedConversationsList);
                $('#ongoingConversationsList').html(response.data.ongoingConversationsList);

                fetchOpenMessage();
                fetchWaitingMessage();
                //fetchClosedMessage();
                initMScrollbar();
            }
        )
        .then(function () {
            getpatientIdsFromWaitingConversations();

        })
        .then(function () {
            getpatientIdsFromOpenConversations();
        })
        .catch(function (error) {
            console.log(error);
        });
}

/**
 * Get the patient details from waiting conversations
 */
function getpatientIdsFromWaitingConversations() {
    var dataList = $(".conversation-waiting").map(function () {
        return $(this).data("patient-id");
    }).get();

    var uniquePatientIds = _.sortedUniq(dataList);
    _.each(uniquePatientIds, function (value) {

        if (value !== "") {
            if (typeof store.get('patient-' + value) === 'undefined') {
                axios.get(laroute.route('fetch.patient.data', {patientId: value}))
                    .then(function (response) {
                        store.set('patient-' + value, {avatar: response.data.avatar, name: response.data.name});
                        $('.conversation-waiting[data-patient-id=' + value + ']').find('.avatar-list-img > img').attr('src', store.get('patient-' + value).avatar);
                        $('.conversation-waiting[data-patient-id=' + value + ']').find('.pmd-card-title-text').html(store.get('patient-' + value).name);

                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                $('.conversation-waiting[data-patient-id=' + value + ']').find('.avatar-list-img > img').attr('src', store.get('patient-' + value).avatar);
                $('.conversation-waiting[data-patient-id=' + value + ']').find('.pmd-card-title-text').html(store.get('patient-' + value).name);
            }
        }
    })
}

/**
 * Get the patient details from open conversations
 */
function getpatientIdsFromOpenConversations() {
    var dataList = $(".conversation-open").map(function () {
        return $(this).data("patient-id");
    }).get();

    var uniquePatientIds = _.sortedUniq(dataList);
    _.each(uniquePatientIds, function (value) {

        if (value !== "") {
            if (typeof store.get('patient-' + value) === 'undefined') {
                axios.get(laroute.route('fetch.patient.data', {patientId: value}))
                    .then(function (response) {
                        store.set('patient-' + value, {avatar: response.data.avatar, name: response.data.name});
                        $('.conversation-open[data-patient-id=' + value + ']').find('.avatar-list-img > img').attr('src', response.data.avatar);
                        $('.conversation-open[data-patient-id=' + value + ']').find('.pmd-card-title-text').html(response.data.name);

                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                $('.conversation-open[data-patient-id=' + value + ']').find('.avatar-list-img > img').attr('src', store.get('patient-' + value).avatar);
                $('.conversation-open[data-patient-id=' + value + ']').find('.pmd-card-title-text').html(store.get('patient-' + value).name);
            }
        }

    })
}

/**
 * Get the patient details from closed conversations
 */
function getpatientIdsFromClosedConversations() {
    var dataList = $(".conversation-closed").map(function () {
        return $(this).data("patient-id");
    }).get();

    var uniquePatientIds = _.sortedUniq(dataList);
    _.each(uniquePatientIds, function (value) {

        if (value !== "") {
            if (typeof store.get('patient-' + value) === 'undefined') {
                axios.get(laroute.route('fetch.patient.data', {patientId: value}))
                    .then(function (response) {
                        store.set('patient-' + value, {avatar: response.data.avatar, name: response.data.name});
                        $('.conversation-closed[data-patient-id=' + value + ']').find('.avatar-list-img > img').attr('src', response.data.avatar);
                        $('.conversation-closed[data-patient-id=' + value + ']').find('.pmd-card-title-text').html(response.data.name);

                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                $('.conversation-closed[data-patient-id=' + value + ']').find('.avatar-list-img > img').attr('src', store.get('patient-' + value).avatar);
                $('.conversation-closed[data-patient-id=' + value + ']').find('.pmd-card-title-text').html(store.get('patient-' + value).name);
            }
        }

    })
}

/**
 * Get the patient details for a selected conversation
 */
function getConversationPatientDetails() {
    var dataList = $(".base_receive").map(function () {
        return $(this).data("patient-id");
    }).get();

    var uniquePatientIds = _.sortedUniq(dataList);
    _.each(uniquePatientIds, function (value) {

        if (value !== "") {
            if (typeof store.get('patient-' + value) === 'undefined') {
                axios.get(laroute.route('fetch.patient.data', {patientId: value}))
                    .then(function (response) {
                        store.set('patient-' + value, {avatar: response.data.avatar, name: response.data.name});
                        $('.base_receive[data-patient-id=' + value + ']').find('.avatar > img').attr('src', response.data.avatar);
                        $('.base_receive[data-patient-id=' + value + ']').find('.patient-name').html(response.data.name);

                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                $('.base_receive[data-patient-id=' + value + ']').find('.avatar > img').attr('src', store.get('patient-' + value).avatar);
                $('.base_receive[data-patient-id=' + value + ']').find('.patient-name').html(store.get('patient-' + value).name);
            }
        }

    })
}

/**
 * Get doctor details for a selected conversation
 */
function getConversationDoctorDettails() {
    var dataList = $(".base_sent").map(function () {
        return $(this).data("doctor-id");
    }).get();

    var uniquePatientIds = _.sortedUniq(dataList);
    _.each(uniquePatientIds, function (value) {

        if (value !== "") {
            if (typeof store.get('doctor-' + value) === 'undefined') {
                axios.get(laroute.route('fetch.pmr.data', {doctorId: value}))
                    .then(function (response) {
                        store.set('doctor-' + value, {avatar: response.data.avatar, name: response.data.name});
                        $('.base_sent[data-doctor-id=' + value + ']').find('.avatar > img').attr('src', response.data.avatar);
                        $('.base_sent[data-doctor-id=' + value + ']').find('.doctor-name').html(response.data.name);

                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                $('.base_sent[data-doctor-id=' + value + ']').find('.avatar > img').attr('src', store.get('doctor-' + value).avatar);
                $('.base_sent[data-doctor-id=' + value + ']').find('.doctor-name').html(store.get('doctor-' + value).name);
            }
        }

    })
}

/**
 * Check for new conversations
 * @returns {*|Promise<R>}
 */
function checkForNewConversations() {
    return axios.get(laroute.route('check.for.conversations', {lastID: conversation.data.lastID}))
        .then(function (response) {

            //if (typeof response.data.waitingConversations.length !== 'undefined') {
            for (var i = 0; i < response.data.waitingConversations.length; i++) {
                //chat.addChatLine(response.chats[i]);
            }

            //console.log(response.data.waitingConversations);
            //console.log(_.last[response.data.waitingConversations]);

            if (response.data.waitingConversations.length > 0) {
                conversation.data.noActivity = 0;
                conversation.data.lastID = response.data.waitingConversations[i - 1].chatConversationId;
                //conversation.data.lastNewConversationCount = response.data.newConversationCount;
            }
            else {
                conversation.data.noActivity++;
            }

            if (conversation.data.noActivity === 0 && response.data.waitingConversations.length > 0 && !response.data.firstLogin) {
                $('.chat-count').removeClass('hidden').find('.new-chat-total').text(response.data.waitingConversations.length);
                notifyUser('<span class="fa fa-bell fa-lg fa-fw"></span> You have <strong>' + response.data.waitingConversations.length + '</strong> new conversation(s)', 'bottomRight', 'relax', 'information');
            } else if (conversation.data.noActivity === 0 && response.data.waitingConversations.length > 0 && !response.data.firstLogin) {
                $('.chat-count').removeClass('hidden').find('.new-chat-total').text(response.data.waitingConversations.length);
                notifyUser('<span class="fa fa-bell fa-lg fa-fw"></span> You have <strong>' + response.data.waitingConversations.length + '</strong> new conversation(s)', 'bottomRight', 'relax', 'information');
            }

            if (NowGP.currentRoute === 'pmr.chats') {
                refreshWaitingConversations();
            }
            //}
        })
        .catch(function (error) {
            console.log(error);
        })
}

/**
 * Refresh list of waiting conversations
 * @returns {*|Promise<R>}
 */
function refreshWaitingConversations() {
    $('#waitingSpinner').removeClass('hidden');
    return axios.get(laroute.route('refresh.waiting.conversations'))
        .then(function (response) {
                $('#waitingConversationsList').html(response.data.waitingConversationsList);
                $('#waitingSpinner').addClass('hidden');
                fetchWaitingMessage();
                initMScrollbar();
            }
        )
        .then(function () {
            getpatientIdsFromWaitingConversations();
        })
        .catch(function (error) {
            console.log(error);
        });
}

/**
 * Check for new messages
 * @returns {*|Promise<R>}
 */
function checkForLatestMessages() {
    return axios.get(laroute.route('check.for.messages', {lastID: chat.data.lastID}))
        .then(function (response) {
            //console.log(response.data.newConversationCount);

            //console.log(response.data.chats.length);


            for (var i = 0; i < response.data.chats.length; i++) {
                //chat.addChatLine(response.chats[i]);
            }

            if (response.data.chats.length) {
                chat.data.noActivity = 0;
                chat.data.lastID = response.data.chats[i - 1].chatMessageId;
                chat.data.lastUserType = response.data.chats[i - 1].userType;
                chat.data.updatedConversations = response.data.updatedConversations
            }
            else {
                // If no chats were received, increment
                // the noActivity counter.

                chat.data.noActivity++;
            }

            if (NowGP.currentRoute !== 'pmr.appointment.conference') {
                //console.log(response.data);
                if (chat.data.noActivity === 0 && chat.data.lastUserType === 'User' && response.data.chats.length > 0 && !response.data.firstLogin) {
                    notifyUser('<span class="fa fa-bell fa-lg fa-fw"></span> You have <strong>' + response.data.chats.length + '</strong> new messages', 'bottomRight', 'relax', 'information');
                } else if (chat.data.noActivity === 0 && chat.data.lastUserType === 'User' && response.data.chats.length === 1 && !response.data.firstLogin) {
                    notifyUser('<span class="fa fa-bell fa-lg fa-fw"></span> You have <strong>' + response.data.chats.length + '</strong> new messages', 'bottomRight', 'relax', 'information');
                }
                if (NowGP.currentRoute === 'pmr.chats') {
                    refreshOpenConversations();
                }
            }
        })
        .catch(function (error) {
            console.log(error);
        })
}

/**
 * Refresh list of open conversations
 * @returns {*|Promise<R>}
 */
function refreshOpenConversations() {
    $('#openSpinner').removeClass('hidden');
    return axios.get(laroute.route('refresh.open.conversations'))
        .then(function (response) {
                $('#ongoingConversationsList').html(response.data.ongoingConversationsList);
                $('#openSpinner').addClass('hidden');
                fetchOpenMessage();
                initMScrollbar();
            }
        )
        .then(function () {
            addUpdatedLabelsToUpdatedConversations();
            if (typeof chat.data.currentOpenConversationId !== 'undefined' && chat.data.currentOpenConversationId !== 0) {
                $('.conversation-open[data-conversation-id=' + chat.data.currentOpenConversationId + ']').addClass('pmd-card-open-active');
            }

        })
        .then(function () {
            getpatientIdsFromOpenConversations();
        })
        .catch(function (error) {
            console.log(error);
        });
}

/**
 * Add Updated Labels to conversations that have been updated.
 */
function addUpdatedLabelsToUpdatedConversations() {
    if (typeof chat.data.updatedConversations !== 'undefined' && chat.data.updatedConversations !== '') {
        var uniqueConversationIds = _.sortedUniq(chat.data.updatedConversations);
        _.each(uniqueConversationIds, function (value) {
            if (value !== "") {
                if (chat.data.currentOpenConversationId === value) {
                    $('.conversation-open[data-conversation-id=' + value + ']').find('.updated-conversation-label').addClass('hidden');
                } else {
                    $('.conversation-open[data-conversation-id=' + value + ']').find('.updated-conversation-label').removeClass('hidden');
                }
                $('.conversation-open[data-conversation-id=' + value + ']').find('.posted-timestamp').addClass('hidden');
                $('.conversation-open[data-conversation-id=' + value + ']').find('.updated-timestamp').removeClass('hidden');
            }
        });
    }
}

/**
 * Get the status of a conversation
 */
function getCurrentConversationStatus(conversationData) {

    currentConversationStatusCheck = setInterval(function () {
        // Update the current conversation screen
        //getCurrentConversationStatus(conversationData);
        if ($body.data('current-conversation-id') !== "" && $body.data('current-conversation-status') === "open") {
            return axios.get(laroute.route('get.conversation.status', {conversationId: conversationData.conversationId}))
                .then(function (response) {
                        //console.log(response);
                        /*if (response.data.status === 'Closed') {
                         $('#chatReplyFromContainer').addClass('hidden');
                         $('.panel-footer').addClass('panel-footer-basic');
                         }*/

                        if (response.data.status === 'Closed') {
                            $body.data('current-conversation-id', response.data.chatConversationId);
                            var closedDate = moment(response.data.updatedTime).format('DD-MM-YYYY');
                            var closedTime = moment(response.data.updatedTime).tz('Europe/London').format('h:mm a');
                            $('#chatReplyForm').hide();
                            $body.data('current-conversation-status', 'closed');
                            $('#chatReplyFromContainer').addClass('hidden');
                            $('.panel-footer').addClass('panel-footer-basic text-danger text-center').css('font-weight', 'bold').html('Chat closed by patient on <br />' + closedDate + ' at ' + closedTime);
                        }
                        /*else {
                         $body.data('current-conversation-id', response.data.chatConversationId);
                         chat.data.currentOpenConversationId = response.data.chatConversationId;
                         $('#chatReplyForm').show();
                         $body.data('current-conversation-status', 'open');
                         $('.conversation-open[data-conversation-id=' + response.data.chatConversationId + ']').addClass('pmd-card-open-active');
                         if ($('.conversation-open[data-conversation-id=' + response.data.chatConversationId + ']').find('.updated-conversation-label').is(':visible')) {
                         $('.conversation-open[data-conversation-id=' + response.data.chatConversationId + ']').find('.updated-conversation-label').addClass('hidden');
                         }
                         $('#chatReplyFromContainer').removeClass('hidden');
                         $('.panel-footer').removeClass('panel-footer-basic');
                         }
                         if ($('#chatReply').is(':visible')) {
                         $('#chatReply').data('conversation-id', response.data.chatConversationId);
                         }*/
                    }
                )
                .catch(function (error) {
                    console.log(error);
                });
        }

    }, 1000 * NowGP.new_conversation_interval_check); // Check for new conversation messages every x seconds


}
