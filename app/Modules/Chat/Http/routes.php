<?php

Route::group([
    'middleware' => 'web',
    'namespace'  => 'App\Modules\Chat\Http\Controllers',
], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::group(['prefix' => 'chat'], function () {
            Route::get('/', [
                'as'   => 'pmr.chats',
                'uses' => 'Admin\ChatController@index',
            ]);

            Route::get('/support', [
                'as'   => 'pmr.chats.support',
                'uses' => 'Admin\ChatController@index',
            ]);

            Route::get('/pharmacy', [
                'as'   => 'pmr.chats.pharmacy',
                'uses' => 'Admin\ChatController@index',
            ]);

            Route::get('/closed_chats', [
                'as'   => 'pmr.closed.chats',
                'uses' => 'Admin\ChatController@fetchClosedChats',
            ]);

            Route::post('update_conversation_lists', [
                'as'   => 'conversation.lists',
                'uses' => 'Admin\ChatController@fetchConversationLists',
            ]);

            Route::post('conversation_messages', [
                'as'   => 'conversation.messages',
                'uses' => 'Admin\ChatController@show',
            ]);

            Route::post('join_conversation', [
                'as'   => 'conversation.messages.join',
                'uses' => 'Admin\ChatController@store',
            ]);

            Route::post('add_to_conversation', [
                'as'   => 'conversation.messages.add',
                'uses' => 'Admin\ChatController@update',
            ]);

            Route::get('fetch_chat_data', ['as' => 'fetch.patient.data', 'uses' => 'Admin\ChatController@fetchPatientData']);
            Route::get('fetch_pmr_data', ['as' => 'fetch.pmr.data', 'uses' => 'Admin\ChatController@fetchPmrData']);

            Route::get('check_new_conversations/{lastConversationId?}', [
                'as'   => 'check.for.conversations',
                'uses' => 'Admin\ConversationPollingController@index',
            ]);

            Route::get('refresh_waiting_conversations', [
                'as'   => 'refresh.waiting.conversations',
                'uses' => 'Admin\ChatController@refreshWaitingConversations',
            ]);

            Route::get('refresh_open_conversations', [
                'as'   => 'refresh.open.conversations',
                'uses' => 'Admin\ChatController@refreshOpenConversations',
            ]);

            Route::get('refresh_closed_conversations', [
                'as'   => 'refresh.closed.conversations',
                'uses' => 'Admin\ChatController@refreshClosedConversations',
            ]);

            Route::get('check_new_messages/{lastMessageId?}', [
                'as'   => 'check.for.messages',
                'uses' => 'Admin\ChatMsgPollingController@index',
            ]);

            Route::post('chat_log', [
                'as'   => 'chat.logger',
                'uses' => 'Admin\ChatController@logChatActions',
            ]);


            Route::get('conversation_status/{conversationId?}', [
                'as'   => 'get.conversation.status',
                'uses' => 'Admin\ConversationPollingController@getConversationStatus',
            ]);
        });
    });
});
