<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 22/03/2017
 * Time: 11:37
 */

namespace App\Modules\Chat\Http\Controllers\Admin;

use App\Modules\Chat\Repositories\ChatRepository;
use App\Modules\Chat\Repositories\ConversationApi;
use App\Modules\Core\Http\Controllers\BackEndController;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ConversationPollingController extends BackEndController
{
    /**
     * @var ChatRepository
     */
    private $chatRepository;

    public function __construct(ChatRepository $chatRepository)
    {
        parent::__construct();
        $this->chatRepository = $chatRepository;

        //ini_set('max_execution_time', 9999);
    }

    /**
     * @param $lastConversationId
     *
     * @return bool|\Illuminate\Http\Response
     */
    public function index($lastConversationId = null)
    {
        //$lastConversationId = request()->get('lastConversationId');

        //dd('====> ' . $lastConversationId);

        $userType = 0;
        $conversations = [];

        if (LoggedInUser()->inRole('super-administrator') ||
            LoggedInUser()->inRole('administrator')) {
            return false;
        }

        if (LoggedInUser()->inRole('nurse')) {
            $userType = ConversationApi::CHAT_USER_TYPE_TRIAGE;
        }

        if (LoggedInUser()->inRole('clinical')) {
            $userType = ConversationApi::CHAT_USER_TYPE_SUPPORT;
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') ||
            LoggedInUser()->inRole('pharmacist-level-2') ||
            LoggedInUser()->inRole('lead-pharmacist')) {
            $userType = ConversationApi::CHAT_USER_TYPE_PHARMACY;
        }


        if (((int)$lastConversationId === 0 ||
                $lastConversationId === 'undefined' ||
                null === $lastConversationId) || (int)$lastConversationId > 0) {
            $conversations = collect($this->chatRepository->getLatestDoctorConversations(LoggedInUser()->getUserId(), $userType, $lastConversationId))->filter(function ($conversation) {
                return $conversation->status === 'Unassigned';
            });

            //dd($conversations);

            //dd($conversations);
            /*$conversationsPrefetch = [];
            if (!$conversations->isEmpty()) {
                $conversationsPrefetch = $conversations->filter(function ($conversation) {
                    return $conversation->status === 'Unassigned';
                })->values()->all();
            }*/

            //dd($conversations);

            if ($conversations->isNotEmpty()) {
                $lastConversationId = $conversations->max()->chatConversationId;
            }

            return response()->json([
                'result'             => $conversations->count(),
                'lastConversationId' => $lastConversationId,
            ], 200);
        }

        return response()->json([
            'result'             => 0,
            'lastConversationId' => $lastConversationId ?? 0
        ]);


    }

    /**
     * @return ConversationPollingController|Collection
     */
    public function getCurrentConversations()
    {
        //dd(LoggedInUser()->getUserId());

        $conversations = $this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId());

        $waitingConversations = collect($conversations)->filter(function ($conversation) {
            return (string)$conversation->status === 'Unassigned';
        })->values();

        return $waitingConversations;
    }

    /**
     * @param $lastConversationId
     * @return ConversationPollingController|Collection
     */
    public function getNewestConversations($lastConversationId)
    {
        $conversations = $this->chatRepository->getLatestDoctorConversations(
            LoggedInUser()->getUserId(),
            $lastConversationId
        );

        $newestConversations = null;

        if ($conversations) {
            $newestConversations = collect($conversations)->filter(function ($conversation) {
                return (string)$conversation->status === 'Unassigned';
            })->values();

            return $newestConversations;
        }

        return $newestConversations;
    }

    /**
     * Get details of a conversation
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConversationStatus($conversationId): \Illuminate\Http\JsonResponse
    {

        //dd($conversationId);

        $userType = 0;
        //$conversationId = request()->get('conversationId');

        if ($conversationId > 0) {

            if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
                return false;
            }

            if (LoggedInUser()->inRole('nurse')) {
                $userType = ConversationApi::CHAT_USER_TYPE_TRIAGE;
            }

            if (LoggedInUser()->inRole('clinical')) {
                $userType = ConversationApi::CHAT_USER_TYPE_SUPPORT;
            }

            if (LoggedInUser()->inRole('pharmacist-level-1') || LoggedInUser()->inRole('pharmacist-level-2') || LoggedInUser()->inRole('lead-pharmacist')) {
                $userType = ConversationApi::CHAT_USER_TYPE_PHARMACY;
            }

            $conversation = $this->chatRepository->getConversationDetails(LoggedInUser()->getUserId(), $userType, $conversationId);

            return response()->json($conversation);
        }

        return response()->json([]);

    }
}
