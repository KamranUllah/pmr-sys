<?php

namespace App\Modules\Chat\Http\Controllers\Admin;

use App\Modules\Chat\Repositories\ConversationApi;
use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Chat\Http\Requests\AddConversationRequest;
use App\Modules\Chat\Http\Requests\JoinConversationRequest;
use App\Modules\Chat\Repositories\ChatRepository;
use App\Modules\User\Http\Middleware\SentinelAuth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JavaScript;
use Response;

class ChatController extends BackEndController
{

    /**
     * @var ChatRepository
     */
    private $chatRepository;

    public function __construct(ChatRepository $chatRepository)
    {
        parent::__construct();

        $this->middleware(SentinelAuth::class);
        $this->chatRepository = $chatRepository;


    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $this->theme->prependTitle('Chat');

        if (config('pmr.enable_v2_chat')) {
            // Add required JS files
            $requiredJs = [
                'chat_new.js',
            ];
            $this->addModuleJSFiles($requiredJs, 'chat', true);

            $requiredCss = [
                'chat_new.css',
            ];
            $this->addModuleCSSFiles($requiredCss, 'chat', true);
            return $this->theme->of('chat::chat_vue_landing')->render();
        }

        // Add required JS files
        $requiredJs = [
            'chat.js',
            'chat_conversation_poller.js',
        ];
        $this->addModuleJSFiles($requiredJs, 'chat', true);

        $requiredCss = [
            'chat.css',
        ];
        $this->addModuleCSSFiles($requiredCss, 'chat', true);
        return $this->theme->of('chat::index')->render();


        //auditLog('PMR User has accessed the chat page');


    }


    /**
     * Get Conversation By PMR User Id and Conversation Id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function show()
    {
        $conversationId = request()->get('conversationId');

        $userType = 0;

        if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
            return false;
        }

        if (LoggedInUser()->inRole('nurse')) {
            $userType = ConversationApi::CHAT_USER_TYPE_TRIAGE;
        }

        if (LoggedInUser()->inRole('clinical')) {
            $userType = ConversationApi::CHAT_USER_TYPE_SUPPORT;
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') || LoggedInUser()->inRole('pharmacist-level-2') || LoggedInUser()->inRole('lead-pharmacist')) {
            $userType = ConversationApi::CHAT_USER_TYPE_PHARMACY;
        }

        $conversationMsgs = collect($this->chatRepository->getDoctorConversationMessages(
            LoggedInUser()->getUserId(),
            $conversationId,
            $userType
        ))->each(function ($message) {
            $message->timestamp = Carbon::parse($message->timestamp)->timezone('Europe/London')->toDateTimeString();
            if (strtolower($message->userType) === 'user') {
                //$conversation->user = getPatientInfo($conversation->externalId, true);
                $user = getPatientInfo($message->externalId, true);
                $message->patientId = $user->Patient->PatientId;
                $message->userAvatar = $user->ContactDetails->AvatarUri;
                $message->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            } else {
                $message->userAvatar = asset('themes/gentella-admin/assets/img/default-avatar.png');
                $message->userFullName = LoggedInUser()->first_name . ' ' . LoggedInUser()->last_name;
            }
        })/*->reject(function ($message) {
            return $message->type === 'JSON';
        })*/->values()->all();
        //dump($conversationMsgs);
/*
        $conversationMsgs = collect($conversationMsgs)->filter(function ($message) {
            return (
            (strpos((string)$message->message, 're really busy at the moment') !== false ||
                (string)$message->userType !== 'Robot'));
        });*/

        auditLog('PMR User has loaded conversation #' . $conversationId);

        if (config('pmr.enable_v2_chat')) {
            return response()->json([
                'consersationId' => $conversationId,
                'messages'       => $conversationMsgs,
                'status'         => 1,
            ],
                200);
        }


        return response([
            'status'         => 1,
            'conversationId' => $conversationId,
            'messagesView'   => view('chat::chat_messages', compact('conversationMsgs'))->render(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('chat::create');
    }

    /**
     * Add a PMR User to the selected conversation
     *
     * @param JoinConversationRequest|Request $request
     *
     * @return Response
     */
    public function store(JoinConversationRequest $request)
    {
        $conversationId = $request->get('conversationId');
        auditLog('PMR User has joined conversation #' . $conversationId);

        $userType = 0;

        if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
            return false;
        }

        if (LoggedInUser()->inRole('nurse')) {
            $userType = ConversationApi::CHAT_USER_TYPE_TRIAGE;
        }

        if (LoggedInUser()->inRole('clinical')) {
            $userType = ConversationApi::CHAT_USER_TYPE_SUPPORT;
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') || LoggedInUser()->inRole('pharmacist-level-2') || LoggedInUser()->inRole('lead-pharmacist')) {
            $userType = ConversationApi::CHAT_USER_TYPE_PHARMACY;
        }

        $conversationMsgs = $this->chatRepository->joinConversation(
            LoggedInUser()->getUserId(),
            $conversationId,
            $userType
        );

        return response([
            'status'             => 1,
            'conversationId'     => $conversationId,
            'conversationStatus' => 'open',
            'messagesView'       => $conversationMsgs,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('chat::edit');
    }

    /**
     * Update the selected PMR User conversation
     *
     * @param AddConversationRequest|Request $request
     *
     * @return Response
     */
    public function update(AddConversationRequest $request)
    {
        $conversationId = $request->get('conversationId');
        $chats = [];
        auditLog('PMR User has posted a message to conversation #' . $conversationId);

        $userType = 0;

        if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
            return false;
        }

        if (LoggedInUser()->inRole('nurse')) {
            $userType = ConversationApi::CHAT_USER_TYPE_TRIAGE;
        }

        if (LoggedInUser()->inRole('clinical')) {
            $userType = ConversationApi::CHAT_USER_TYPE_SUPPORT;
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') || LoggedInUser()->inRole('pharmacist-level-2') || LoggedInUser()->inRole('lead-pharmacist')) {
            $userType = ConversationApi::CHAT_USER_TYPE_PHARMACY;
        }

        $conversationMsgs = $this->chatRepository->postMessageToConversation(
            LoggedInUser()->getUserId(),
            $conversationId,
            $userType,
            [
                'message'                => strip_tags(clean(request()->get('conversationMessage'))),
                'respondedChatMessageId' => 1,
                'resumptionCookie'       => '',
                'type'                   => 'Text',
            ]
        );

        $chats[] = $conversationMsgs;

        return response([
            'status'       => 1,
            'chats'        => $chats,
            'messagesView' => $conversationMsgs,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * @DEPRECATED
     * Fetch a list of available conversations
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function fetchConversationLists()
    {
        $conversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId()));

        $ongoingConversations = $conversations->filter(function ($conversation) {

            if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
                return (string)$conversation->status === 'Open' && ((int)$conversation->chatType === 2 || (int)$conversation->chatType === 3);
            }

            if (LoggedInUser()->inRole('clinical')) {
                return (string)$conversation->status === 'Open' && (int)$conversation->chatType === 3;
            }

            if (LoggedInUser()->inRole('pharmacist-level-1') ||
                LoggedInUser()->inRole('pharmacist-level-2') ||
                LoggedInUser()->inRole('lead-pharmacist')) {
                return (string)$conversation->status === 'Open' && (int)$conversation->chatType === 2;
            }

            //return (string)$conversation->status === 'Open';
        })->each(function ($conversation) {
            if ($conversation->userType === 'user') {
                $conversation->user = getPatientInfo($conversation->externalId, true);
            } else {
                $conversation->user = LoggedInUser();
            }
        })->sortBy('startTime')->values()->all();

        $waitingConversations = $conversations->filter(function ($conversation) {

            if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
                return (string)$conversation->status === 'Unassigned' && ((int)$conversation->chatType === 2 || (int)$conversation->chatType === 3);
            }

            if (LoggedInUser()->inRole('clinical')) {
                return (string)$conversation->status === 'Unassigned' && (int)$conversation->chatType === 3;
            }

            if (LoggedInUser()->inRole('pharmacist-level-1') ||
                LoggedInUser()->inRole('pharmacist-level-2') ||
                LoggedInUser()->inRole('lead-pharmacist')) {
                return (string)$conversation->status === 'Unassigned' && (int)$conversation->chatType === 2;
            }

            //return (string)$conversation->status === 'Unassigned';
        })->each(function ($conversation) {
            if ($conversation->userType === 'user') {
                $conversation->user = getPatientInfo($conversation->externalId, true);
            } else {
                $conversation->user = LoggedInUser();
            }
        })->sortBy('startTime')->values()->all();

        $closedConversations = $conversations->filter(function ($conversation) {

            if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
                return (string)$conversation->status === 'Closed' && ((int)$conversation->chatType === 2 || (int)$conversation->chatType === 3);
            }

            if (LoggedInUser()->inRole('clinical')) {
                return (string)$conversation->status === 'Closed' && (int)$conversation->chatType === 3;
            }

            if (LoggedInUser()->inRole('pharmacist-level-1') ||
                LoggedInUser()->inRole('pharmacist-level-2') ||
                LoggedInUser()->inRole('lead-pharmacist')) {
                return (string)$conversation->status === 'Closed' && (int)$conversation->chatType === 2;
            }

            //return (string)$conversation->status === 'Closed' || (string)$conversation->status === 'Deleted';
        })->each(function ($conversation) {
            if ($conversation->userType === 'user') {
                $conversation->user = getPatientInfo($conversation->externalId, true);
            } else {
                $conversation->user = LoggedInUser();
            }
        })->sortBy('startTime')->values()->all();

        //dd($waitingConversations);

        return response()->json([
            'ongoingConversationsList' => view(
                'chat::partials.conversations.open',
                compact(
                    'ongoingConversations'
                )
            )->render(),
            'closedConversationsList'  => view(
                'chat::partials.conversations.closed',
                compact(
                    'closedConversations'
                )
            )->render(),
            'waitingConversationsList' => view(
                'chat::partials.conversations.waiting',
                compact(
                    'waitingConversations'
                )
            )->render(),
        ],
            200);
    }

    public function sortConversations($collection)
    {
    }

    /**
     *
     * Fetch patient details from the API and send it back to the view
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchPatientData(Request $request)
    {
        $patientId = $request->get('patientId');

        $userAvatar = $this->theme->asset()->url('img/default-avatar.png');
        $userName = 'Name Not Available';

        if (null !== $patientId && $user = getPatientInfo($patientId, true)) {
            $userAvatar = resizeImageAndCache($user->ContactDetails->AvatarUri);
            $userName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
        }

        return response()->json([
            'patientId' => $patientId,
            'avatar'    => $userAvatar,
            'name'      => $userName,
        ]);
    }

    /**
     * Fetch PMR User details from API and send it back to the view
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchPmrData(Request $request)
    {
        //$patientId = $request->get('doctorId');

        $userAvatar = asset('/themes/gentella-admin/assets/img/default-avatar.png');

        /*if (null !== $patientId && $user = getDoctorDetailsApi($patientId, true)) {
            $userAvatar = resizeImageAndCache($user->Avatar);
            $userName = $user->Title . ' ' .
                $user->Forename . ' ' .
                $user->Surname;
        }*/

        return response()->json([
            'doctorId' => LoggedInUser()->getUserId(),
            'avatar'   => $userAvatar,
            'name'     => LoggedInUser()->first_name,
        ]);
    }

    /**
     * Refresh the list of waiting conversations
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function refreshWaitingConversations()
    {
        //$conversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId()));

        $waitingConversations = [];

        if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
            //$triageConversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(), ConversationApi::CHAT_USER_TYPE_TRIAGE));
            $pharmacyConversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_PHARMACY)
            );
            $supportConversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_SUPPORT)
            );

            $merged1 = $pharmacyConversations->merge($pharmacyConversations);
            $merged2 = $merged1->merge($supportConversations);

            $waitingConversations = $merged2->filter(function ($conversation) {
                return (string)$conversation->status === 'Unassigned';
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
                /*if ($conversation->userType === 'user') {
                    $conversation->user = getPatientInfo($conversation->externalId, true);
                } else {
                    $conversation->user = LoggedInUser();
                }*/
            })->sortBy('startTime')->values()->all();
        }

        if (LoggedInUser()->inRole('nurse')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_TRIAGE));
            $waitingConversations = $conversations->filter(function ($conversation) {
                return (string)$conversation->status === 'Unassigned';
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (LoggedInUser()->inRole('clinical')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_SUPPORT));
            $waitingConversations = $conversations->filter(function ($conversation) {
                return (string)$conversation->status === 'Unassigned';
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') ||
            LoggedInUser()->inRole('pharmacist-level-2') ||
            LoggedInUser()->inRole('lead-pharmacist')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_PHARMACY));
            $waitingConversations = $conversations->filter(function ($conversation) {
                return (string)$conversation->status === 'Unassigned';
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (config('pmr.enable_v2_chat')) {

            /*$waitingConversations = [];
            $faker = \Faker\Factory::create();
            for ($i = 0; $i < 15; $i++) {
                $waitingConversations[] = [
                    "chatConversationId"       => $faker->numberBetween(1, 10000),
                    "frameworkId"              => null,
                    "startTime"                => $faker->dateTimeThisYear,
                    "endTime"                  => null,
                    "chatConversationStatusId" => 0,
                    "summary"                  => $faker->text(50),
                    "status"                   => 'Unassigned',
                    "recentUserExternalId"     => 6943,
                    "recentDoctorExternalId"   => null,
                    "priorityTag"              => null,
                    "updatedTime"              => $faker->dateTimeThisYear,
                    "updatedTimeHumanOnly"     => null,
                    "chatType"                 => $faker->numberBetween(1, 3),
                ];
            }*/

            return response()->json($waitingConversations, 200);
        }

        return response()->json([
            'waitingConversationsList' => view(
                'chat::partials.conversations.waiting',
                compact(
                    'waitingConversations'
                )
            )->render(),
        ],
            200);

    }

    /**
     * Refresh the list of open conversations
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function refreshOpenConversations()
    {
        $ongoingConversations = [];

        if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
            //$triageConversations = collect($this->chatRepository->getDoctorConversations(
            //LoggedInUser()->getUserId(),
            // ConversationApi::CHAT_USER_TYPE_TRIAGE)
            //);
            $pharmacyConversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_PHARMACY)
            );
            $supportConversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_SUPPORT)
            );

            $merged1 = $pharmacyConversations->merge($pharmacyConversations);
            $merged2 = $merged1->merge($supportConversations);

            $ongoingConversations = $merged2->filter(function ($conversation) {
                return (string)$conversation->status === 'Open';
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (LoggedInUser()->inRole('nurse')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_TRIAGE)
            );
            $ongoingConversations = $conversations->filter(function ($conversation) {
                return (string)$conversation->status === 'Open';
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (LoggedInUser()->inRole('clinical')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_SUPPORT)
            );
            $ongoingConversations = $conversations->filter(function ($conversation) {
                return (string)$conversation->status === 'Open';
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })
            ->sortBy('startTime')->values()->all();
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') ||
            LoggedInUser()->inRole('pharmacist-level-2') ||
            LoggedInUser()->inRole('lead-pharmacist')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_PHARMACY)
            );
            $ongoingConversations = $conversations->filter(function ($conversation) {
                return (string)$conversation->status === 'Open';
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (config('pmr.enable_v2_chat')) {
            return response()->json($ongoingConversations, 200);
        }

        return response()->json([
            'ongoingConversationsList' => view(
                'chat::partials.conversations.open',
                compact(
                    'ongoingConversations'
                )
            )->render(),
        ],
            200);
    }

    /**
     * Refresh the list of closed conversations
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function refreshClosedConversations()
    {
        $closedConversations = [];

        if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
            //$triageConversations = collect($this->chatRepository->getDoctorConversations(
            //LoggedInUser()->getUserId(),
            // ConversationApi::CHAT_USER_TYPE_TRIAGE)
            //);
            $pharmacyConversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_PHARMACY)
            );
            $supportConversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),

                ConversationApi::CHAT_USER_TYPE_SUPPORT)
            );

            $merged1 = $pharmacyConversations->merge($pharmacyConversations);
            $merged2 = $merged1->merge($supportConversations);

            $closedConversations = $merged2->filter(function ($conversation) {
                return ((string)$conversation->status === 'Closed' || (string)$conversation->status === 'Deleted');
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (LoggedInUser()->inRole('nurse')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_TRIAGE)
            );
            $closedConversations = $conversations->filter(function ($conversation) {
                return ((string)$conversation->status === 'Closed' || (string)$conversation->status === 'Deleted');
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (LoggedInUser()->inRole('clinical')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_SUPPORT)
            );
            $closedConversations = $conversations->filter(function ($conversation) {
                return ((string)$conversation->status === 'Closed' || (string)$conversation->status === 'Deleted');
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') ||
            LoggedInUser()->inRole('pharmacist-level-2') ||
            LoggedInUser()->inRole('lead-pharmacist')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(
                LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_PHARMACY)
            );
            $closedConversations = $conversations->filter(function ($conversation) {
                return ((string)$conversation->status === 'Closed' || (string)$conversation->status === 'Deleted');
            })->each(function ($conversation) {
                $conversation->startTime = Carbon::parse($conversation->startTime)->timezone('Europe/London')->toDateTimeString();
                $user = getPatientInfo($conversation->recentUserExternalId, true);
                $conversation->patientId = $user->Patient->PatientId;
                $conversation->userAvatar = $user->ContactDetails->AvatarUri;
                $conversation->userFullName = $user->ContactDetails->FirstName . ' ' . $user->ContactDetails->LastName;
            })->sortBy('startTime')->values()->all();
        }

        if (config('pmr.enable_v2_chat')) {
            return response()->json($closedConversations, 200);
        }

        return response()->json([
            'closedConversationsList' => view(
                'chat::partials.conversations.open',
                compact(
                    'closedConversations'
                )
            )->render(),
        ],
            200);
    }

    /**
     * Logging of PMR User actions on chat via AJAX
     *
     * @param Request $request
     */
    public function logChatActions(Request $request)
    {
        auditLog($request->get('audit'));
    }
}
