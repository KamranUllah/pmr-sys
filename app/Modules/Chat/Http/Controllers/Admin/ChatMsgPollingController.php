<?php

namespace App\Modules\Chat\Http\Controllers\Admin;


use App\Modules\Chat\Repositories\ChatRepository;
use App\Modules\Chat\Repositories\ConversationApi;
use App\Modules\Core\Http\Controllers\BackEndController;
use Illuminate\Http\Request;
use Session;

class ChatMsgPollingController extends BackEndController
{

    /**
     * @var ChatRepository
     */
    private $chatRepository;


    public function __construct(ChatRepository $chatRepository)
    {
        parent::__construct();
        $this->chatRepository = $chatRepository;

        //ini_set('max_execution_time', 9999);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($maxMessageId = null)
    {
        //$this->sse->sleep_time = 1; //The time to sleep after the data has been sent in seconds. Default: 0.5.
        //$this->sse->client_reconnect = 10;

        //$maxMessageId = session('maxMessageId') ?: 0;

        //dd(session('maxMessageId'));
        //$maxMessageId = 1900;

        //$messages = $this->getNewestMessagesForConversations($maxMessageId);
        //dd($messages);

        // $maxMessageId = request()->get('lastMessageId');

        $userType = 0;
        $messages = [];

        if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
            return false;
        }

        if (LoggedInUser()->inRole('nurse')) {
            $userType = ConversationApi::CHAT_USER_TYPE_TRIAGE;
        }

        if (LoggedInUser()->inRole('clinical')) {
            $userType = ConversationApi::CHAT_USER_TYPE_SUPPORT;
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') ||
            LoggedInUser()->inRole('pharmacist-level-2') ||
            LoggedInUser()->inRole('lead-pharmacist')) {
            $userType = ConversationApi::CHAT_USER_TYPE_PHARMACY;
        }

        if ((int)$maxMessageId === 0 || (int)$maxMessageId === 'undefined' || (int)$maxMessageId > 0) {

            $messages = collect($this->chatRepository->getLatestDoctorConversationMessages(
                LoggedInUser()->getUserId(),
                $userType,
                $maxMessageId
            ))->reject(function ($message) {
                return $message->userType !== 'User';
            });

            if ($messages->isNotEmpty()) {
                $maxMessageId = $messages->max()->chatMessageId;
            }

            return response()->json([
                'result'        => $messages->count(),
                'lastMessageId' => $maxMessageId,
                'lastUserType'  => 'User',
            ]);
        }

        return response()->json([
            'result'        => 0,
            'lastMessageId' => $maxMessageId,
            'lastUserType'  => 'User',
        ]);


    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('chat::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('chat::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * @return ConversationPollingController|\Illuminate\Support\Collection
     */
    public function getCurrentOpenConversations()
    {
        //$conversations = $this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId());

        /*$openConversations = collect($conversations)->filter(function ($conversation) {
            return (string)$conversation->status === 'Open';
        })->values();*/


        if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
            //$triageConversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(), ConversationApi::CHAT_USER_TYPE_TRIAGE));
            $pharmacyConversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_PHARMACY));
            $supportConversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_SUPPORT));

            $merged1 = $pharmacyConversations->merge($pharmacyConversations);
            $merged2 = $merged1->merge($supportConversations);

            $ongoingConversations = $merged2->filter(function ($conversation) {
                return (string)$conversation->status === 'Open';
            })->values()->all();
        }

        if (LoggedInUser()->inRole('nurse')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_TRIAGE));
            $ongoingConversations = $conversations->filter(function ($conversation) {
                return (string)$conversation->status === 'Open';
            })->values()->all();
        }

        if (LoggedInUser()->inRole('clinical')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_SUPPORT));
            $ongoingConversations = $conversations->filter(function ($conversation) {
                return (string)$conversation->status === 'Open';
            })->values()->all();
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') || LoggedInUser()->inRole('pharmacist-level-2') || LoggedInUser()->inRole('lead-pharmacist')) {
            $conversations = collect($this->chatRepository->getDoctorConversations(LoggedInUser()->getUserId(),
                ConversationApi::CHAT_USER_TYPE_PHARMACY));
            $ongoingConversations = $conversations->filter(function ($conversation) {
                return (string)$conversation->status === 'Open';
            })->values()->all();
        }


        return $ongoingConversations;
    }

    /**
     * @param $lastMessageId
     *
     * @return ConversationPollingController
     */
    public function getNewestMessagesForConversations($lastMessageId): ConversationPollingController
    {

        //$lastConversationId = request()->get('lastConversationId');
        //$lastMessageId = request()->get('lastMessageId');

        $userType = 0;

        if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator')) {
            return false;
        }

        if (LoggedInUser()->inRole('nurse')) {
            $userType = ConversationApi::CHAT_USER_TYPE_TRIAGE;
        }

        if (LoggedInUser()->inRole('clinical')) {
            $userType = ConversationApi::CHAT_USER_TYPE_SUPPORT;
        }

        if (LoggedInUser()->inRole('pharmacist-level-1') ||
            LoggedInUser()->inRole('pharmacist-level-2') ||
            LoggedInUser()->inRole('lead-pharmacist')) {
            $userType = ConversationApi::CHAT_USER_TYPE_PHARMACY;
        }

        $lastMessageId = $lastMessageId === 'undefined' ? 0 : $lastMessageId;

        $messages = $this->chatRepository->getLatestDoctorConversationMessages(
            LoggedInUser()->getUserId(),
            $userType,
            $lastMessageId
        );

        return $messages;
    }
}
