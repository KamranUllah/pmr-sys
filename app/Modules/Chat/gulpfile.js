const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*if (typeof moduleName === 'undefined') {
    console.log('Plunder more riffiwobbles!');
    return false;
}*/

elixir(function (mix) {

    mix.styles(
        './app/Modules/Chat/Assets/css/chat.css'
        , 'public/modules/chat/css/chat.css');

    mix.scripts(
        './app/Modules/Chat/Assets/js/chat.js'
        , 'public/modules/chat/js/chat.js');

    mix.styles(
        './app/Modules/Chat/Assets/css/chat_new.css'
        , 'public/modules/chat/css/chat_new.css');

    mix.webpack(
        './app/Modules/Chat/Assets/js/chat_new.js'
        , 'public/modules/chat/js/chat_new.js');

    mix.scripts(
        './app/Modules/Chat/Assets/js/chat_conversation_poller.js'
    , 'public/modules/chat/js/chat_conversation_poller.js');

});
