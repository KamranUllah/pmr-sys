{{--@php(dump($conversationMsgs))--}}
@if(count($conversationMsgs) > 0)
    @foreach($conversationMsgs as $message)
        @if((string)$message->userType === 'Doctor')
            @php
                $userAvatar = Theme::asset()->url('img/default-avatar.png');
                $userName = 'Name Not Available';
            @endphp
            <div class="row msg_container base_sent chat-{{ $message->chatMessageId }}" data-conversation-id="{{ $message->chatConversationId }}" data-doctor-id="{{ $message->externalId }}" data-user-type="{{ $message->userType }}">
                <div style="max-width: 60%">
                    <div class="messages msg_sent break-word">
                        <p>{!! nl2br(clean($message->message)) !!}</p>
                        <time datetime="{{ Carbon\Carbon::parse($message->timestamp)->format('d-m-Y') }}">
                            <span class="doctor-name">{{ $userName }}</span>
                            • {{ Carbon\Carbon::parse($message->timestamp)->tz('Europe/London')->diffForHumans() }}</time>
                    </div>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-2 avatar">
                    <img src="{!! $userAvatar !!}"
                         onerror='this.src="{!! Theme::asset()->url('img/default-avatar.png') !!}"'
                         class=" img-circle img-responsive ">
                </div>
            </div>
        @elseif ((string)$message->userType === 'User')
            @php
                $userAvatar = Theme::asset()->url('img/default-avatar.png');
                $userName = 'Name Not Available';
            @endphp
            <div class="row msg_container base_receive chat-{{ $message->chatMessageId }}" data-conversation-id="{{ $message->chatConversationId }}" data-patient-id="{{ $message->externalId }}" data-user-type="{{ $message->userType }}">
                <div class="col-md-1 col-sm-2 col-xs-2 avatar">
                    <img src="{!! $userAvatar !!}"
                         onerror='this.src="{!! Theme::asset()->url('img/default-avatar.png') !!}"'
                         class=" img-circle img-responsive ">
                </div>
                <div style="max-width: 60%">
                    <div class="messages msg_receive break-word">
                        <p>{!! nl2br(clean($message->message)) !!}</p>
                        <time datetime="{{ Carbon\Carbon::parse($message->timestamp)->format('d-m-Y') }}">
                            <span class="patient-name">{{ $userName }}</span> • {{ Carbon\Carbon::parse($message->timestamp)->tz('Europe/London')->diffForHumans() }}</time>
                    </div>
                </div>
            </div>
        @else
            @php
                unset($message);
            @endphp
        @endif
    @endforeach
@else
    <p>&nbsp;</p>
    <div class="alert alert-warning text-center">Nothing to see here!</div>
@endif

