<!-- TAB NAVIGATION -->
<ul class="nav nav-pills" role="tablist">
    <li class="active"><a href="#chatFeed" role="tab" data-toggle="tab">Chat Feed</a></li>
    <li><a href="#closedChats" role="tab" data-toggle="tab">Closed Chats</a></li>
</ul>
<!-- TAB CONTENT -->
<div class="tab-content">
    <p>&nbsp;</p>
    <div class="active tab-pane fade in" id="chatFeed">
        <div class="row">
            <div class="col-md-2 col-lg-3">
                <!--Accordion with all expandable example -->
                <div class="panel-group pmd-accordion" id="accordion2" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-warning">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2"
                                                       href="#waitingConversations"
                                                       aria-expanded="false" aria-controls="waitingConversations"
                                                       data-expandable="true"> Waiting Chats <span id="waitingSpinner" class="fa fa-spin fa-spinner fa-lg fa-fw hidden"></span> <i
                                            class="material-icons md-dark pmd-sm pmd-accordion-arrow">keyboard_arrow_down</i></a>
                            </h4>

                        </div>
                        <div id="waitingConversations" class="panel-collapse collapse in" role="tabpanel"
                             aria-labelledby="headingThree">
                            <div class="panel-body" id="waitingConversationsList" style="padding: 5px !important;">
                                {{--@include('chat::partials.conversations.waiting')--}}
                                <p class="text-center">... <i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> Fetching waiting conversations ...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-3">
                <div class="panel-group pmd-accordion" id="accordion3" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-info">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3"
                                                       href="#yourConversations"
                                                       aria-expanded="true" aria-controls="yourConversations"
                                                       data-expandable="true"> Active Chats <i
                                            class="material-icons md-dark pmd-sm pmd-accordion-arrow">keyboard_arrow_down</i></a>
                            </h4>
                        </div>
                        <div id="yourConversations" class="panel-collapse collapse in" role="tabpanel"
                             aria-labelledby="headingOne">
                            <div class="panel-body" id="ongoingConversationsList" style="padding: 5px !important; overflow: hidden;">
                                {{--@include('chat::partials.conversations.open')--}}
                                <p class="text-center">... <i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> Fetching open conversations ...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-6">
                <div class="row chat-window col-md-12" id="chat_window_1" style="margin-left: 0; padding: 0; margin-top: 15px;">
                    <div class="col-xs-12 col-md-12" style="padding: 0">
                        <div class="panel panel-default no-margin-bottom chat-panel">
                            <div class="panel-heading top-bar">
                                <div class="col-md-8 col-xs-8">
                                    <h3 class="panel-title"><span class="fa fa-comments"></span> Chat</h3>
                                </div>
                                <div class="col-md-4 col-xs-4" style="text-align: right;">
                                    <div id="conversation-fetch-msg"></div>
                                </div>
                            </div>
                            <div class="panel-body msg_container_base">
                                <p>&nbsp;</p>
                                <div class="text-center" style="width: 100%; padding: 60px;">
                                    <img class="profile_patient_pik img-responsive"
                                         src="{!! asset('/themes/gentella-admin/assets/img/chat_placeholder.png') !!}"
                                         data-src="{!! asset('/themes/gentella-admin/assets/img/chat_placeholder.png') !!}"
                                         alt="" style="margin: auto;"/>
                                </div>
                                <h4 class="text-center">Select a conversation to view messages</h4>
                            </div>
                            <div id="form-message-reply" class="panel-footer panel-footer-basic">
                                <div id="chatReplyFromContainer" class="hidden">
                                    {!! BootForm::open()->role('form')->id('chatReplyForm')->data('parsley-validate', true) !!}
                                    <div class="input-group">
                            <textarea type="text" name="chatReply" id="chatReply" class="form-control"
                                      style="resize:none"
                                      placeholder="Write your message here..." rows="3"
                                      required="required"
                                      data-parsley-required-message="Please enter a reply"
                                      data-parsley-minlength="1"
                                      data-parsley-maxlength="500"
                                      data-parsley-minlength-message="Please enter a mininum of 1 characters"
                                      data-parsley-maxlength-message="Only a maximum of 500 characters is allowed"
                                      data-parsley-errors-container=".form-errors"
                                      data-conversation-id=""></textarea>
                                        <span class="input-group-addon btn btn-nav-blue" id="submitMyForm" style="pointer-events: auto;">Send</span>
                                    </div><!-- /input-group -->
                                    <span class="form-errors"></span>
                                    {!! BootForm::close() !!}
                                    <div class="sending-animation"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="closedChats">
        <div class="row">
            <div id="closedChatLeft" class="col-md-6">
                <!--Accordion with all expandable example -->
                <div class="panel-group pmd-accordion" id="accordion2" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-success">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2"
                                                       href="#closedConversations"
                                                       aria-expanded="false" aria-controls="closedConversations"
                                                       data-expandable="true"> Closed Chats <i
                                            class="material-icons md-dark pmd-sm pmd-accordion-arrow">keyboard_arrow_down</i></a>
                            </h4>
                        </div>
                        <div id="closedConversations" class="panel-collapse collapse in" role="tabpanel"
                             aria-labelledby="headingTwo">
                            <div id="closedConversationsList" class="panel-body"
                                 style="padding: 5px !important; overflow: hidden;">
                                {{--@include('chat::partials.conversations.closed')--}}
                                <p class="text-center">... <i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> Fetching closed conversations ...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="closedChatRight" class="col-md-6">
                <div class="row chat-window col-md-12" id="chat_window_2" style="margin-left: 0; padding: 0; margin-top: 15px;">
                    <div class="col-xs-12 col-md-12" style="padding: 0">
                        <div class="panel panel-default no-margin-bottom closed-chat-panel">
                            <div class="panel-heading top-bar">
                                <div class="col-md-8 col-xs-8">
                                    <h3 class="panel-title"><span class="fa fa-comments"></span> Chat</h3>
                                </div>
                                <div class="col-md-4 col-xs-4" style="text-align: right;">
                                    <div id="closed-conversation-fetch-msg"></div>
                                </div>
                            </div>
                            <div class="panel-body msg_closed_container_base">
                                <p>&nbsp;</p>
                                <div class="text-center" style="width: 100%; padding: 60px;">
                                    <img class="profile_patient_pik img-responsive"
                                         src="{!! asset('/themes/gentella-admin/assets/img/chat_placeholder.png') !!}"
                                         data-src="{!! asset('/themes/gentella-admin/assets/img/chat_placeholder.png') !!}"
                                         alt="" style="margin: auto;"/>
                                </div>
                                <h4 class="text-center">Select a conversation to view messages</h4>
                            </div>
                            <div id="chat-window-msg" class="panel-footer panel-footer-basic-closed">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>