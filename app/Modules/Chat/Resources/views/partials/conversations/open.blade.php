{{--@php(dump($ongoingConversations))--}}
<div class="conversations-list" style="padding: 5px !important;">
@forelse($ongoingConversations as $conversation)
    @php
        $userAvatar = asset('/themes/gentella-admin/assets/img/default-avatar.png');
        $userName = 'Open Chat';
    @endphp
    <!--Media, Title, Description and Action area -->
        <div class="pmd-card pmd-card-default pmd-z-depth conversation-open"
             data-conversation-id="{{ $conversation->chatConversationId }}"
             data-conversation-status="open"
             data-patient-id="{{ $conversation->recentUserExternalId }}"
             data-doctor-id="{{ $conversation->recentDoctorExternalId }}"
             style="margin-bottom: 5px !important;">
            <!-- Card header -->
            <div class="pmd-card-title">
                <div class="media-left">
                    <a href="javascript:void(0);" class="avatar-list-img">
                        <img width="40" height="40" class="img-circle"
                             src="{!! $userAvatar !!}"
                             onerror='this.src="{!! asset('/themes/gentella-admin/assets/img/default-avatar.png') !!}"'>
                    </a>
                </div>
                <div class="media-body media-middle">
                    <h3 class="pmd-card-title-text">#{{ $conversation->chatConversationId }} {{ $userName }}</h3>
                    <div class="updated-timestamp hidden">
                        <strong><span
                                    class="pmd-card-subtitle-text">Date Posted: {{ \Carbon\Carbon::parse($conversation->updatedTime)->tz('Europe/London')->format('d-m-Y H:i:s') }}</span></strong>
                        <br/>
                    </div>
                    <div class="posted-timestamp">
                        <span class="pmd-card-subtitle-text">Date Posted: {{ \Carbon\Carbon::parse($conversation->updatedTime)->tz('Europe/London')->format('d-m-Y H:i:s') }}</span>
                        <br/>
                    </div>
                    <span class="updated-conversation-label label label-success hidden">Updated</span>
                </div>
            </div>
        </div>
    @empty
        <p class="text-center">There are no conversations available to view</p>
    @endforelse
</div>