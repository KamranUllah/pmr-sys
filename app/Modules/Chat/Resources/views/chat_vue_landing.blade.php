<div class="row">
    <div class="col-md-12">
        @if (LoggedInUser()->inRole('super-administrator') || LoggedInUser()->inRole('administrator'))
            <div class="alert alert-info">
                <strong>Info!</strong> Administrator roles are not able to view chats.
            </div>
        @else
            <div id="chatApp">
                <chat
                        pmrUserId="{{ LoggedInUser()->getUserId() }}"
                        pmrUserAvata="{{ asset('themes/gentella-admin/assets/img/default-avatar.png') }}">
                </chat>
            </div>
        @endif
    </div>
</div>