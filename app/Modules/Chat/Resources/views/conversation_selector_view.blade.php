@php
    $userAvatar = Theme::asset()->url('img/default-avatar.png');
    if (null !== $conversation->recentUserExternalId) {
        $userAvatar = getPatientInfo($conversation->recentUserExternalId, true) ? resizeImageAndCache(getPatientInfo($conversation->recentUserExternalId, true)->ContactDetails->AvatarUri) : Theme::asset()->url('img/default-avatar.png');
    }
@endphp
<div class="pmd-card pmd-card-default pmd-z-depth conversation-waiting"
     data-conversation-id="{{ $conversation->chatConversationId }}"
     data-patient-id=""
     data-doctor-id=""
     style="margin-bottom: 5px !important;">
    <!-- Card header -->
    <div class="pmd-card-title">
        <div class="media-left">
            <a href="javascript:void(0);" class="avatar-list-img">
                <img width="40" height="40" class="img-circle"
                     src="{!! $userAvatar !!}"
                     onerror='this.src="{!! Theme::asset()->url('img/default-avatar.png') !!}"'>
            </a>
        </div>
        <div class="media-body media-middle">
            <h3 class="pmd-card-title-text">Two-line item
                - {{ $conversation->chatConversationId }}</h3>
            <span class="pmd-card-subtitle-text">Secondary text - {{ \Carbon\Carbon::parse($conversation->startTime)->format('d-m-Y H:i:s') }}</span>
        </div>
    </div>
</div>