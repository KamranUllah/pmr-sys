<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 07/03/2017
 * Time: 12:54
 */

namespace App\Modules\Chat\Repositories;

use App\Services\NowGP\Data\Models;

class ChatRepository
{
    protected $conversationApi;

    public function __construct(ConversationApi $conversationApi)
    {
        $this->conversationApi = $conversationApi;
    }

    /**
     * Get Doctor Conversations
     *
     * @param $doctorId
     * @param $userType
     * @param null $overId
     * @return mixed
     */
    public function getDoctorConversations($doctorId, $userType, $overId = null)
    {
        $result = false;

        try {
            $result = $this->conversationApi->getDoctorConversations($doctorId, $userType, $overId);
        } catch (\Exception $ex) {
            auditLog('Error getting conversations for pmr user id ' . $doctorId . ': ' . $ex->getMessage());
            \Bugsnag::notifyError('Request Exception', 'Error getting conversations for pmr user id ' .
                $doctorId .
                ': ' .
                $ex->getMessage());
        }

        return $result;
    }

    /**
     * Get conversation details
     *
     * @param $doctorId
     * @param $userType
     * @param $conversationId
     *
     * @return mixed
     */
    public function getConversationDetails($doctorId, $userType, $conversationId)
    {
        $result = false;

        try {
            $result = $this->conversationApi->getConversationDetails($doctorId, $userType, $conversationId);
        } catch (\Exception $ex) {
            auditLog('Error getting conversation #'. $conversationId .' details for pmr user id ' . $doctorId . ': ' . $ex->getMessage());
            \Bugsnag::notifyError('Request Exception', 'Error getting conversation #'. $conversationId .'  for pmr user id ' .
                $doctorId .
                ': ' .
                $ex->getMessage());
        }

        return $result;
    }

    /**
     * Get Latest Doctor Conversations
     *
     * @param $doctorId
     * @param $userType
     * @param $lastConversationId
     * @return mixed
     */
    public function getLatestDoctorConversations($doctorId, $userType, $lastConversationId)
    {
        $result = false;

        try {
            $result = $this->conversationApi->getLatestDoctorConversations($doctorId, $userType, $lastConversationId);
        } catch (\Exception $ex) {
            auditLog('Error getting conversations for pmr user id ' . $doctorId . ': ' . $ex->getMessage());
            \Bugsnag::notifyError('Request Exception', 'Error getting conversations for pmr user id ' .
                $doctorId .
                ': ' .
                $ex->getMessage());
        }

        return $result;
    }

    /**
     * Get Conversation Messages
     *
     * @param $doctorId
     * @param $conversationId
     * @return mixed
     */
    public function getDoctorConversationMessages($doctorId, $conversationId, $userType)
    {
        $result = false;

        try {
            $result = $this->conversationApi->getConversationMessages($doctorId, $conversationId, $userType);
        } catch (\Exception $ex) {
            auditLog('Error getting messages for conversation id ' . $conversationId . ': ' . $ex->getMessage());
            \Bugsnag::notifyError('Request Exception', 'Error getting messages for conversation id ' .
                $conversationId .
                ': ' .
                $ex->getMessage());
        }

        return $result;
    }

    /**
     * Get latest conversation messages
     *
     * @param $doctorId
     * @param $userType
     * @param $lastMessageId
     * @return mixed
     */
    public function getLatestDoctorConversationMessages($doctorId, $userType, $lastMessageId)
    {
        $result = false;

        try {
            $result = $this->conversationApi->getLatestConversationMessages($doctorId, $userType, $lastMessageId);
        } catch (\Exception $ex) {
            auditLog('Error getting messages for conversation id ' . $lastMessageId . ': ' . $ex->getMessage());
            \Bugsnag::notifyError('Request Exception', 'Error getting messages for conversation id ' .
                $lastMessageId .
                ': ' .
                $ex->getMessage());
        }

        return $result;
    }

    /**
     * Join a conversation
     *
     * @param $doctorId
     * @param $conversationId
     * @param $userType
     * @return mixed
     */
    public function joinConversation($doctorId, $conversationId, $userType)
    {
        $result = false;

        try {
            $result = $this->conversationApi->joinConversation($doctorId, $conversationId, $userType);
        } catch (\Exception $ex) {
            auditLog('Unable to join conversation id ' . $conversationId . ': ' . $ex->getMessage());
            \Bugsnag::notifyError('Request Exception', 'Unable to join conversation id ' .
                $conversationId .
                ': ' .
                $ex->getMessage());
        }

        return $result;
    }

    /**
     * Add message to conversation
     *
     * @param $doctorId
     * @param $conversationId
     * @param $userType
     * @param $message
     * @return mixed
     */
    public function postMessageToConversation($doctorId, $conversationId, $userType, $message)
    {

        $result = false;

        try {
            $result = $this->conversationApi->postToConversation($doctorId, $conversationId, $userType, $message);
        } catch (\Exception $ex) {
            auditLog('Unable to add message to conversation id ' . $conversationId . ': ' . $ex->getMessage());
            \Bugsnag::notifyError('Request Exception', 'Unable to add message to conversation id ' .
                $conversationId .
                ': ' .
                $ex->getMessage());
        }

        return $result;
    }

    public function getConversationById($doctorId, $userType, $conversationId)
    {
        $result = false;

        try {
            $result = $this->conversationApi->getConversationById($doctorId, $userType, $conversationId);
        } catch (\Exception $ex) {
            auditLog('Error getting conversation id ' . $conversationId . ': ' . $ex->getMessage());
            \Bugsnag::notifyError('Request Exception', 'Error getting messages for conversation id ' .
                $conversationId .
                ': ' .
                $ex->getMessage());
        }

        return $result;
    }
}
