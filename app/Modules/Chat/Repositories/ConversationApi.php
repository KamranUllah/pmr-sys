<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 07/03/2017
 * Time: 12:11
 */


namespace App\Modules\Chat\Repositories;


use App\Services\NowGP\Data\Models;
use App\Services\NowGP\Data\BaseApi;


class ConversationApi extends BaseApi
{

    CONST CHAT_USER_TYPE_TRIAGE = 4;
    CONST CHAT_USER_TYPE_PHARMACY = 5;
    CONST CHAT_USER_TYPE_SUPPORT = 6;

    public function __construct()
    {
        parent::__construct();
        //$this->coreApiUrl = config('chat.conversation_api_url');
    }

    /**
     * Get Conversation Messages
     *
     * @param $doctorId
     * @param $conversationId
     * @param $userType
     * @return mixed
     */
    public function getConversationMessages($doctorId, $conversationId, $userType)
    {
        //$action = $this->coreApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations/{$conversationId}/messages";
        $action = $this->coreApiUrl . "ChatAdmin/usertype/{$userType}/externalId/{$doctorId}/conversations/{$conversationId}/messages";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Get conversation details
     *
     * @param $doctorId
     * @param $userType
     * @param $conversationId
     *
     * @return mixed
     */
    public function getConversationDetails($doctorId, $userType, $conversationId)
    {
        $action = $this->coreApiUrl . "ChatAdmin/usertype/{$userType}/externalId/{$doctorId}/conversations/{$conversationId}";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Post new message to conversation
     *
     * @param $doctorId
     * @param $conversationId
     * @param $userType
     * @param $message
     * @return mixed
     */
    public function postToConversation($doctorId, $conversationId, $userType, $message)
    {
        //$action = $this->coreApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations/{$conversationId}/messages";
        $action = $this->coreApiUrl . "ChatAdmin/usertype/{$userType}/externalId/{$doctorId}/conversations/{$conversationId}/messages";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('POST', $action, true, $message);

        return json_decode($response->getBody());
    }

    /**
     * Get available conversations for the doctor
     *
     * @param $doctorId
     * @param $userType
     * @param null $overId
     * @return mixed
     */
    public function getDoctorConversations($doctorId, $userType, $overId = null)
    {
        //$action = $this->coreApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations";
        $action = $this->coreApiUrl . "ChatAdmin/usertype/{$userType}/externalId/{$doctorId}/conversations";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Get the most recent/newest conversations for the doctor
     *
     * @param $doctorId
     * @param $userType
     * @param $lastConversationId
     * @return mixed
     * @internal param array $params
     */
    public function getLatestDoctorConversations($doctorId, $userType, $lastConversationId)
    {
        $action = $this->coreApiUrl . "ChatAdmin/usertype/{$userType}/externalId/{$doctorId}/conversations?overId=" . $lastConversationId;
        //dd($action);
        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Get the most recent/newest messages for a conversation
     *
     * @param $doctorId
     * @param $userType
     * @param $lastMessageId
     * @return mixed
     */
    public function getLatestConversationMessages($doctorId, $userType, $lastMessageId)
    {
        $action = $this->coreApiUrl . "ChatAdmin/usertype/{$userType}/externalId/{$doctorId}/conversations/all/messages?overId=" . $lastMessageId;

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * Join a conversation
     *
     * @param $doctorId
     * @param $conversationId
     * @param $userType
     * @return mixed
     */
    public function joinConversation($doctorId, $conversationId, $userType)
    {
        //$action = $this->coreApiUrl . "ChatAdmin/doctors/{$doctorId}/conversations/{$conversationId}/join";
        $action = $this->coreApiUrl . "ChatAdmin/usertype/{$userType}/externalId/{$doctorId}/conversations/{$conversationId}/join";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('PUT', $action, true);

        return json_decode($response->getBody());
    }

    /**
     * @param $doctorId
     * @param $userType
     * @param $conversationId
     *
     * @return mixedGte conversation detai8ls
     * @internal param $lastMessageId
     *
     */
    public function getConversationById($doctorId, $userType, $conversationId)
    {
        $action = $this->coreApiUrl . "ChatAdmin/usertype/{$userType}/externalId/{$doctorId}/conversations/{$conversationId}";

        //dd($this->coreUrl . "patients/{$patientId}/savedCards/{$cardId}");
        $response = $this->sendGuzzleRequest('GET', $action, true);

        return json_decode($response->getBody());
    }
}
