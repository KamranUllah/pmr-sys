<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 14/06/2017
 * Time: 16:01
 */

return [
    // User Management Permissions
    'Chat' => [
        'chat.admin.chat.index'                       => [
            'name'        => '',
            'description' => 'Access the chat landing page',
        ],
        'chat.admin.chat.show'                        => [
            'name'        => '',
            'description' => 'Show chat conversation',
        ],
        'chat.admin.chat.store'                       => [
            'name'        => '',
            'description' => 'Add Message to Conversation',
        ],
        'chat.admin.chat.update'                      => [
            'name'        => '',
            'description' => 'Update conversation',
        ],
        'chat.admin.chat.fetchConversationLists'      => [
            'name'        => '',
            'description' => 'Get All Conversations',
        ],
        'chat.admin.chat.fetchPatientData'            => [
            'name'        => '',
            'description' => 'Get Patient data',
        ],
        'chat.admin.chat.fetchPmrData'                => [
            'name'        => '',
            'description' => 'Get PMR user data',
        ],
        'chat.admin.chat.refreshWaitingConversations' => [
            'name'        => '',
            'description' => 'Refresh List Of Waiting Conversations',
        ],
        'chat.admin.chat.refreshOpenConversations'    => [
            'name'        => '',
            'description' => 'Refresh List Of Open Conversations',
        ],
        'chat.admin.chat.refreshClosedConversations'  => [
            'name'        => '',
            'description' => 'Refresh List Of Closed Conversations',
        ],
        'chat.admin.chat.logChatActions'              => [
            'name'        => '',
            'description' => 'Log All Chat Actions',
        ],

    ],

    'Chat Message Polling' => [
        'chat.admin.chatmsgpolling.index'                             => [
            'name'        => '',
            'description' => 'Chat message polling landing',
        ],
        'chat.admin.chatmsgpolling.getCurrentOpenConversations'       => [
            'name'        => '',
            'description' => 'Show chat conversation',
        ],
        'chat.admin.chatmsgpolling.getNewestMessagesForConversations' => [
            'name'        => '',
            'description' => 'Add Message to Conversation',
        ],
    ],

    'Chat Conversation Polling' => [
        'chat.admin.conversationpolling.index'                   => [
            'name'        => '',
            'description' => 'Conversation polling landing',
        ],
        'chat.admin.conversationpolling.getCurrentConversations' => [
            'name'        => '',
            'description' => 'Get Current Conversation',
        ],
        'chat.admin.conversationpolling.getNewestConversations'  => [
            'name'        => '',
            'description' => 'Get newest conversations',
        ],
        'chat.admin.conversationpolling.getConversationStatus'   => [
            'name'        => '',
            'description' => 'Get conversation status',
        ]

    ],

];
