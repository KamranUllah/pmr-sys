<?php

return [
    'name'        => 'Chat',
    'enable_chat' => env('ENABLE_CHAT', false),
    'conversation_api_url' => env('CONVERSATION_API_URL', null),
    'chat_interval_check' => env('CHAT_INTERVAL_CHECK', 10), // In seconds
    'new_conversation_interval_check' => env('NEW_CONVERSATION_INTERVAL_CHECK', 5), // In seconds
    'new_chat_interval_check' => env('NEW_CHAT_INTERVAL_CHECK', 10), // In seconds
];
