<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 14/06/2017
 * Time: 16:01
 */

return [

    // User Management Permissions
    'Prescription' => [
        'prescription.dashboard.prescription.index'     => [
            'name'        => '',
            'description' => 'Show Prescription on dashboard Index',
        ],
        'prescription.prescription.privateprescription.index'     => [
            'name'        => '',
            'description' => 'Access Private Prescriptions management pages',
        ],

        'prescription.prescription.privateprescription.getPrivatePrescriptions'     => [
            'name'        => '',
            'description' => 'Get all private prescriptions',
        ],

        'prescription.prescription.privateprescription.updatePrescriptionAddress'     => [
            'name'        => '',
            'description' => 'Update private prescription delivery address',
        ],

        'prescription.prescription.privateprescription.getPrescription'     => [
            'name'        => '',
            'description' => 'Fetch prescription by id',
        ],

        'prescription.prescription.privateprescription.getPrescriptionStatus'     => [
            'name'        => '',
            'description' => 'Fetch prescription status',
        ],


        'prescription.admin.dashboard.index'     => [
            'name'        => '',
            'description' => 'Show Prescription Dashboard',
        ],
        'prescription.admin.prescription.index'     => [
            'name'        => '',
            'description' => 'Show Prescription Index',
        ],
        'prescription.admin.prescription.show'     => [
            'name'        => '',
            'description' => 'Show Prescription data',
        ],
        'prescription.admin.prescription.markAsReady'     => [
            'name'        => '',
            'description' => 'Mark Prescription as ready',
        ],
        'prescription.admin.prescription.markAsSent'     => [
            'name'        => '',
            'description' => 'Mark Prescription as sent',
        ],
        'prescription.admin.prescription.markAsDelivered'     => [
            'name'        => '',
            'description' => 'Mark Prescription as delivered',
        ],
    ],

    // User Management Permissions
    'Medicines' => [
        'prescription.admin.medicine.index'     => [
            'name'        => '',
            'description' => 'Access / Search / Update medicines',
        ],

        'prescription.admin.medicine.medicines'     => [
            'name'        => '',
            'description' => 'Grant access to medicines',
        ],

        'prescription.admin.medicine.getAllMedicines'     => [
            'name'        => '',
            'description' => 'Fetch the medicines data (required)',
        ]
    ],

    // User Management Permissions
    'Dispensary' => [
        'prescription.admin.medicine.dispensary'     => [
            'name'        => '',
            'description' => 'Access / Search / Update formularies',
        ]
    ],

    // Role Management Permissions
    'Formulary' => [
        'prescription.admin.medicine.formulary'  => [
            'name'        => '',
            'description' => 'Access / Search for formularies',
        ],
        'prescription.admin.medicine.getFormulary'  => [
            'name'        => '',
            'description' => 'Access for formularies search',
        ],
        'prescription.admin.medicine.updateFormulary'  => [
            'name'        => '',
            'description' => 'Update formulary',
        ]
    ],
];
