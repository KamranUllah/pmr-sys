<?php namespace App\Modules\Prescription\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Prescription\Entities\Endorsement;

class EndorsementRepository extends BaseRepository
{

    protected $model = Endorsement::class;
    protected $relationships = ['prescription'];

}