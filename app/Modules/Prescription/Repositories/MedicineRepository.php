<?php namespace App\Modules\Prescription\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Prescription\Entities\Medicine;

/**
 * MedicineRepository Class
 * 
 */
class MedicineRepository extends BaseRepository {

	//use Medicine model
	protected $model = Medicine::class;

}