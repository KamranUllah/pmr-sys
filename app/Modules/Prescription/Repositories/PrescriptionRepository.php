<?php namespace App\Modules\Prescription\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Prescription\Entities\Prescription;

class PrescriptionRepository extends BaseRepository
{

    protected $model = Prescription::class;
    protected $relationships = ['patient', 'endorsements'];

    public function getByStatus($status, $paginate = 0)
    {
        if ($paginate === 0) {
            return $this->model
                ->with($this->relationships)
                ->where([['status', '=', $status],['repeat', '=', 0]])
                ->orderBy('raised_at', 'desc')
                ->get();
        } else {
            return $this->model
                ->with($this->relationships)
                ->where([['status', '=', $status],['repeat', '=', 0]])
                ->orderBy('raised_at', 'desc')
                ->paginate($paginate);
        }

    }


    public function getPrivate($paginate = 0){

        if ($paginate === 0) {
            return $this->model
                ->with($this->relationships)
                ->where('repeat', '=', 0)
                ->orderBy('raised_at', 'desc')
                ->get();
        } else {
            return $this->model
                ->with($this->relationships)
                ->where('repeat', '=', 0)
                ->orderBy('raised_at', 'desc')
                ->paginate($paginate);
        }

    }
}