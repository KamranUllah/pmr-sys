<?php namespace App\Modules\Prescription\Entities;

use Illuminate\Database\Eloquent\Model;

class Endorsement extends Model
{

    protected $fillable = [
        'dnpid',
        'drug_name',
        'direction_1',
        'direction_2',
        'quantity',
        'units',
        'prescription_id',
    ];

    public function prescription()
    {
        return $this->belongsTo(Prescription::class);
    }

}
