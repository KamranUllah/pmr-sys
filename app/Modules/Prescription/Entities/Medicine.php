<?php namespace App\Modules\Prescription\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Medicine Class
 *
 *
 */
class Medicine extends Model
{

    //set primary key to dnpid
    // (need to do as assumed primary key is 'id')
    protected $primaryKey = 'dnpid';

    //protected $table = 'medicines';

    protected $fillable = ['IN_STOCK', 'QUANTITY', 'FORMULARY'];


    //Relationships
    public function formulary()
    {
        return $this->hasOne(Formulary::class, 'dnpid', 'DNPID');
    }

}
