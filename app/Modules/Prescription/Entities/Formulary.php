<?php namespace App\Modules\Prescription\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Medicine Class
 *
 *
 */
class Formulary extends Model
{

    //set table name
    protected $table = 'formulary';

    //fillable fills
    protected $fillable = ['dnpid', 'available'];

    //ignore timestamps
    public $timestamps = false;

    //Relationships
    public function medicine()
    {
        return $this->hasOne(Medicine::class);
    }

}
