<?php namespace App\Modules\Prescription\Entities;

use App\Modules\Patient\Entities\Patient;
use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{

    protected $fillable = [
        'status',
        'drn_id',
        'raised_at',
        'expired_at',
        'completed_at',
        'collected_at',
        'delivered_at',
        'doctor_gmc',
        'doctor_name',
        'sgncode',
        'courier_name',
        'delivery_detail',
        'courier_notes',
        'tracking_ref',
        'repeat',
        'patient_id',
    ];

    //protected $dates = ['raised_at', 'expired_at', 'completed_at', 'collected_at', 'delivered_at'];

    protected $statuses = [
        1 => 'waiting',
        2 => 'ready',
        8 => 'sent',
        9 => 'delivered'];

    const WAITING = 1;
    const READY = 2;
    const SENT = 8;
    const DELIVERED = 9;

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function endorsements()
    {
        return $this->hasMany(Endorsement::class);
    }

    // Helpers

    public function getStatus()
    {
        return array_key_exists($this->status,$this->statuses) ? $this->statuses[$this->status] : null ;
    }

    public function formatOrNa($value)
    {
        return $this->{$value} ? $this->{$value}->format('d/m/Y') : 'n/a';
    }
}
