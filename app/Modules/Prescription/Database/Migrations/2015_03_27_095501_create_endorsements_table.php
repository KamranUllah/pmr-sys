<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEndorsementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('endorsements', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('vpid');
            $table->string('drug_name');
            $table->text('direction_1');
            $table->text('direction_2');
            $table->string('quantity');
            $table->string('units');
            $table->integer('prescription_id')->unsigned();
            $table->foreign('prescription_id')->references('id')->on('prescriptions')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('endorsements');
	}

}
