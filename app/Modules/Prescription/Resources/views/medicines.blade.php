{{-- View to display dispensary items --}}
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Dispensary</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                {{-- Success message --}}
                @if (Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{{Session::get('flash_message')}}}
                    </div>
                @endif
                {{-- Error message --}}
                @if (Session::has('errors'))
                    <div class="alert alert-danger">
                        {{{Session::get('errors')}}}
                    </div>
                @endif

                <div style=" clear: both; padding-top:40px;">
                    <!-- display information from database if present -->
                    @if (isset($data))
                        <table class="table table-striped" id="dispensaryTable" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="col-md-1">DNPID</th>
                                <th>Product</th>
                                <th>Pack</th>
                                <th>Price (£)</th>
                                <th>RRP (£)</th>
                                <th>Code</th>
                                <th>Supplier</th>
                                <th>Qty</th>
                                <th>Formulary</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>DNPID</th>
                                <th>Product</th>
                                <th>Pack</th>
                                <th>Price (£)</th>
                                <th>RRP (£)</th>
                                <th>Code</th>
                                <th>Supplier</th>
                                <th>Qty</th>
                                <th>Formulary</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <!-- iterate through database array -->
                            @foreach ($data as $medicine)

                                <tr>
                                    <td>{{--{!! Form::open(array('action'=>array('MedicineController@updateFormulary'))) !!}--}}
                                        {!! BootForm::open()->role('form')->route('prescription.formulary.update') !!}
                                        {{{$medicine->DNPID}}}
                                        {!! BootForm::hidden('dnpid_'.$medicine->DNPID, $medicine->DNPID) !!}</td>
                                    <td>{{{$medicine->NM}}}</td>
                                    <td>{{{$medicine->PACK}}}</td>
                                    <td>{{{$medicine->PRICE}}}</td>
                                    <td>{!! Form::text('rrp_'.$medicine->DNPID, $medicine->RRP, array('id' => 'rrp_'.$medicine->DNPID, 'style' => 'font-size: 11px;')) !!}</td>
                                    <td>{{{$medicine->CODE}}}</td>
                                    <td>{{{$medicine->SOURCE}}}</td>
                                    <td>{!! Form::text('qty_'.$medicine->DNPID, $medicine->QUANTITY, array('id' => 'qty_'.$medicine->DNPID, 'style' => 'font-size: 11px; width: 50px; text-align: center;')) !!}</td>
                                    <td style='text-align: center;'>{!! Form:: checkbox('inform_'.$medicine->DNPID, $medicine->FORMULARY != 0 ? $medicine->FORMULARY = 1 : $medicine->FORMULARY = 0, $medicine->FORMULARY == 1) !!}</td>
                                    <td style="text-align: center;">
                                        {!! Form::button('UPDATE', array('id' => 'btn_'.$medicine->DNPID, 'class' => 'btn btn-primary btn-xs', 'type'=>'submit')) !!}
                                        {!! BootForm::close() !!}</td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                        {{--{{ $data->links() }}--}}
                    @else
                        @include('prescription::not_found')
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

