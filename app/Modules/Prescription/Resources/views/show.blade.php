<div class="col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Prescription
                {{  ucwords($prescription->getStatus()) }}</h2>
            <ul class="nav navbar-right panel_toolbox">
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <ul class="small-block-grid-3 prescription-list" data-equalizer>
                <li class="col-xs-12 no-padding">
                    @include('prescription::partials.patient')
                </li>
                <li class="col-xs-12 no-padding">
                    @include('prescription::partials.prescriber')
                </li>
                <li class="col-xs-12 no-padding">
                    @include('prescription::partials.dispatch')
                </li>
                <li class="col-xs-12 no-padding">
                    @include('prescription::partials.endorsements')
                </li>
            </ul>

            <div class="text-center col-xs-12">
                <ul class="button-group prescription-action-buttons">
                    <li>
                        <button type="submit" class="btn btn-primary btn-xs printContent" >
                            <i class="fa fa-print"></i>
                            Print Script
                        </button>
                    </li>
                    @if((int) $prescription->status === \App\Modules\Prescription\Entities\Prescription::WAITING)
                        <li>
                            {!! Form::open(['route' => ['prescriptions.status.ready', $prescription->id]]) !!}
                            <button type="submit" class="btn btn-primary btn-xs">
                                <i class="fa fa-check"></i>
                                Mark As Ready
                            </button>

                            {!! Form::close() !!}
                        </li>
                    @endif
                    @if((int) $prescription->status === \App\Modules\Prescription\Entities\Prescription::READY)
                        <li>
                            {!! Form::open(['route' => ['prescriptions.status.sent', $prescription->id]]) !!}
                            <button type="submit" class="btn btn-primary btn-xs">
                                <i class="fa fa-check"></i>
                                Mark As Sent
                            </button>
                            {!! Form::close() !!}
                        </li>
                    @endif
                    @if((int) $prescription->status === \App\Modules\Prescription\Entities\Prescription::SENT)
                        <li>
                            {!! Form::open(['route' => ['prescriptions.status.delivered', $prescription->id]]) !!}
                            <button type="submit" class="btn btn-primary btn-xs">
                                <i class="fa fa-home"></i>
                                Mark As Delivered
                            </button>
                            {!! Form::close() !!}
                        </li>
                    @endif
                </ul>
            </div>


            @if((int) $prescription->status !== \App\Modules\Prescription\Entities\Prescription::SENT &&
            (int) $prescription->status !== \App\Modules\Prescription\Entities\Prescription::DELIVERED)
                <div id="mark-as-sent" class="reveal-modal small col-xs-12" data-reveal>
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <h2 class="text-center" style="border-bottom: 2px solid #E6E9ED;">Mark Prescription As Sent</h2>
                            <div class="text-center">
                                <div class="form-group">
                                {!! Form::open(['route' => ['prescriptions.status.sent', $prescription->id], 'method' => 'post']) !!}

                                {!! Form::label('tracking_ref', 'Tracking Reference*:',[
                                'class' => 'tracking-text control-label'
                                ]) !!}
                                {!! Form::text('tracking_ref', null, [
                                'id' => 'tracking_ref',
                                'placeholder' => 'Reference provided by courier ',
                                'class' => 'tracking-input form-control',
                                'style' => 'width: 100% !important;padding: 5px'
                                ]) !!}
                                </div>
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <i class="fa fa-check"></i>
                                    Mark As Sent
                                </button>
                                <p>
                                    <small class="label alert">* indicates a required field</small>
                                </p>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                </div>
            @endif

            <div id="printable-script" style="display: none">
                <table style="width: 100%;border: 0">
                    <tbody>
                    <tr>
                        <td style="width: 96%!important; display: block">@include('prescription::partials.patient')</td>
                        <td style="width: 50%">@include('prescription::partials.prescriber')</td>
                    </tr>
                    </tbody>
                </table>
                <hr/>
                @include('prescription::partials.endorsements')

            </div>
        </div>
    </div>
</div>


<style type="text/css">

    .prescription-list {
        list-style-type: none;
        padding:0px;
    }

    .prescription-list li h2 {
        border-bottom: 2px solid #E6E9ED;
    }

    .no-padding{
        padding:0px;
    }

    .prescription-action-buttons {
        list-style-type: none;
        display: inline-block;
        margin-top: 20px;
    }

    .prescription-action-buttons li{
        float:left;
    }


</style>

@section('scripts')
    <script>
        function PrintContent() {
            var DocumentContainer = document.getElementById('printable-script');
            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln('<!DOCTYPE html>');
            WindowObject.document.writeln('<html><head><title>Prescription #{{ $prescription->id }} (DrNoW #{{ $prescription->drn_id }})</title>');
            var str = '<link rel="stylesheet" href="{{ asset('stylesheets/app.css') }}" />';
            WindowObject.document.writeln(str);
            WindowObject.document.writeln('</head><body>');
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.writeln('</body></html>');
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
    </script>
@endsection