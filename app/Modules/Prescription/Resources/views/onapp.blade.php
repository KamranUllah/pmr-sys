{{-- View to display formulary items --}}

@extends('template.master')

@section('content')

<h2>On App Formulary</h2>

@include('medicine.partials.left_sidebar')

<div class="main">

	<div style=" clear: both; padding-top:40px;">
		<!-- display information from database if present -->
		<table style="margin: 0 auto;">
			<thead>
				<tr>
					<th>DNPID</th>
					<th>Product</th>
				</tr>
			</thead>
			<tbody>
			<!-- iterate through database array -->
			@foreach ($appFormulary as $medicine => $meds) 
				<tr>
					<td>{{{ $meds['vpid'] }}}</td>
					<td>{{{ $meds['nm'] }}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>

@stop
