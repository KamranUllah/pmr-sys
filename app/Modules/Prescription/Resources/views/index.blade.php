<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Medicines <small>View a list of of medicines</small></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <table id="medicines-table" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                <tr>
                    <th class="col-md-1">Dnpid</th>
                    <th class="col-md-7">Nm</th>
                    <th class="col-md-1">Pack</th>
                    <th class="col-md-1">In Stock</th>
                    <th class="col-md-2">Action</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>
</div>