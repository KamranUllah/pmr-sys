<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Formulary</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div style=" clear: both; padding-top:40px;">
                    <!-- display information from database if present -->
                    @if (isset($data))
                        <table class="table table-striped table-bordered" id="formularyTable">
                            <thead>
                            <tr>
                                <th class="col-md-1">DNPID</th>
                                <th>Product</th>
                                <th class="col-md-1">Pack</th>
                                <th class="col-md-1">In Stock</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- iterate through database array -->
                            {{--@foreach ($data as $medicine)--}}
                                {{--<tr>--}}
                                    {{--<td>{{{$medicine->DNPID}}}</td>--}}
                                    {{--<td>{{$medicine->NM}}</td>--}}
                                    {{--<td>{{{$medicine->PACK}}}</td>--}}
                                    {{--<td>@if($medicine->IN_STOCK)--}}
                                            {{--<span class="green">Yes</span>--}}
                                        {{--@else--}}
                                            {{--<span class="red">No</span>--}}
                                        {{--@endif--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}
                            </tbody>
                        </table>
                        {{--{{ $data->links() }}--}}
                    @elseif ((!isset($data)) && (strlen($formulary_search) > 0))
                        @include('prescription::not_found', array('search'=>$formulary_search))
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script src="../../Assets/prescriptions.js"></script>
@endpush
