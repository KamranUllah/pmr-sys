<h2>Endorsements</h2>
<table  class="table table-striped table-bordered">
    <thead>
    <tr>
        <th class="col-md-2">#</th>
        <th class="col-md-5">Item</th>
        <th class="col-md-1">VPID</th>
        <th class="col-md-3">Quantity</th>
        <th class="col-md-1">Units</th>
    </tr>
    </thead>
    <tbody>
    @foreach($prescription->endorsements as $key => $item)
        <tr>
            <td>
                {{ $key + 1 }}
            </td>
            <td>
                {{ $item->drug_name }} <br/>
                {{ $item->direction_1 }} <br/>
                {{ $item->direction_2 }}
            </td>
            <td>
                {{ $item->vpid }}
            </td>
            <td>
                {{ $item->quantity }}
            </td>
            <td>
                {{ $item->units }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>