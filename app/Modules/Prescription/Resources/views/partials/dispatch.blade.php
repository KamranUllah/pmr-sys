<h2>
    Dispatch
</h2>
<table  class="table table-striped table-bordered" data-equalizer-watch>
    <tbody>
    <tr>
        <td class="col-md-2">
            <strong>
                Courier
            </strong>
        </td>
        <td>
            {{ $prescription->courier_name }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Collected
            </strong>
        </td>
        <td>
            {{ $prescription->collected_at ? \Carbon\Carbon::parse($prescription->collected_at)->format('d/m/Y') : 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Delivered
            </strong>
        </td>
        <td>
            {{ $prescription->delivered_at ? \Carbon\Carbon::parse($prescription->delivered_at)->format('d/m/Y') : 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Ref
            </strong>
        </td>
        <td>
            {{ $prescription->tracking_ref ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Status
            </strong>
        </td>
        <td>
            {{ $prescription->getStatus() }}
        </td>
    </tr>

    </tbody>
</table>