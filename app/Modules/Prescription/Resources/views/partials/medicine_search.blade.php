{{-- Partial view to search medicines --}}
<h6>Enter all or part of the medicine then click Search</h6>
{!! BootForm::open()->role('form') !!}
	<div class="form-group">
		<div class="medicine_search">
			<div id="medicine_search_input">
				{!! BootForm::text('search', 'search')->value($search)->name('search') !!}
			</div>
			<div id="medicine_search_button">
				{!! BootForm::submit('Submit') !!}
			</div>
		</div>
	</div>
{!! Bootform::close() !!}