{{-- Partial view to display sidebar on pharmaceutocal pages --}}

<div id="left_sidebar" class="col-sm-3 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
		<li 
			@if((Route::currentRouteName() == 'prescription::medicines') ||
				(Route::currentRouteName() == 'prescription::dispensary') ||
				(Request::path() == 'pharmaceutical/dispensary') ||
				(Request::path() == 'pharmaceutical')) 
					class="active"
			@endif>
			{{--<a href="{{action('MedicineController@dispensary')}}">Dispensary</a>--}}
		{{--</li>--}}
		{{--<li --}}
			{{--@if((Route::currentRouteName() == 'prescription::formulary') ||--}}
				{{--(Request::path() == 'pharmaceutical/formulary'))--}}
				{{--class="active"--}}
			{{--@endif>--}}
			{{--<a href="{{action('MedicineController@formulary')}}">Formulary</a>--}}
		{{--</li>--}}
		{{--<li --}}
			{{--@if((Route::currentRouteName() == 'prescription::onapp') ||--}}
				{{--(Request::path() == 'pharmaceutical/onapp'))--}}
				{{--class="active"--}}
			{{--@endif>--}}
			{{--<a href="{{action('MedicineController@onApp')}}">On App</a>--}}
		{{--</li>--}}
	</ul>
</div>