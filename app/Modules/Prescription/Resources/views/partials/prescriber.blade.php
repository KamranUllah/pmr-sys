<h2>
    Prescriber
</h2>
<table class="table table-striped table-bordered" data-equalizer-watch>
    <tbody>
    <tr>
        <td class="col-md-2">
            <strong>
                Date Prescribed
            </strong>
        </td>
        <td>
            {{ $prescription->raised_at ? \Carbon\Carbon::parse($prescription->raised_at)->format('d/m/Y') : 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Date Expired
            </strong>
        </td>
        <td>
            {{  $prescription->expired_at ? \Carbon\Carbon::parse($prescription->expired_at)->format('d/m/Y') : 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Date Completed
            </strong>
        </td>
        <td>
            {{  $prescription->completed_at ? \Carbon\Carbon::parse($prescription->completed_at)->format('d/m/Y') : 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Prescriber
            </strong>
        </td>
        <td>
            {{ $prescription->doctor_name  ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                G.M.C.
            </strong>
        </td>
        <td>
            {{ $prescription->doctor_gmc  ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                SGN
            </strong>
        </td>
        <td>
            {{ $prescription->sgncode  ?? 'N/A' }}
        </td>
    </tr>
	<tr>
        <td>
            <strong>
                Prescription ID
            </strong>
        </td>
        <td>
            {{ $prescription->id  ?? 'N/A' }}
        </td>
    </tr>
	<tr>
        <td>
            <strong>
                Prescriber's Address
            </strong>
        </td>
        <td>
            {{--1 Lowry Plaza,<br>Salford Quays,<br>Manchester <br>M50 3UB--}}
            {{ $prescription->patient->address ?? 'N/A' }}
        </td>
    </tr>

    </tbody>
</table>