<h2>
    Patient
</h2>
<table class="table table-striped table-bordered" data-equalizer-watch>
    <tbody>
    <tr>
        <td class="col-md-2">
            <strong>
                Patient
            </strong>
        </td>
        <td>
            {{ $prescription->patient->name }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Address
            </strong>
        </td>
        <td>
            {{ $prescription->patient->address }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Contact Number
            </strong>
        </td>
        <td>
            {{ null !== $prescription->patient->contact_number &&
            strlen($prescription->patient->contact_number) > 0 ? $prescription->patient->contact_number : 'N/A'
            }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                DOB
            </strong>
        </td>
        <td>
            {{ $prescription->patient->dob->format('d/m/Y') }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Age
            </strong>
        </td>
        <td>
            {{ $prescription->patient->dob->age }}
        </td>
    </tr>

    </tbody>
</table>