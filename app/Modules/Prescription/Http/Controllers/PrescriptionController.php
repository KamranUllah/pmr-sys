<?php namespace App\Modules\Prescription\Http\Controllers;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Prescription\Entities\Prescription;
use App\Modules\Prescription\Events\PrescriptionStatusChanged;
use App\Modules\Prescription\Http\Requests\MarkScriptAsSent;
use App\Modules\Prescription\Repositories\PrescriptionRepository;
use Carbon\Carbon;
use Event;


class PrescriptionController extends BackEndController {

    /**
     * @var PrescriptionRepository
     */
    private $prescriptions;

    /**
     * PrescriptionController constructor.
     * @param PrescriptionRepository $prescriptions
     */
    public function __construct(PrescriptionRepository $prescriptions)
    {
        parent::__construct();
        $this->prescriptions = $prescriptions;

        $requiredJs = [
            'medicine.js',
            'prescriptions.js',
        ];

        $this->addModuleJSFiles($requiredJs, 'prescription', true);
    }

    public function index(){
        return redirect()->route('admin.dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $prescription = $this->prescriptions->getById($id);
        $this->theme->prependTitle(ucwords($prescription->getStatus()));
        return $this->theme->of('prescription::show', compact('prescription'))->render();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsReady($id)
    {
        $prescription = $this->prescriptions->getById($id);
        $prescription->status = Prescription::READY;
        $prescription->completed_at = Carbon::now();
        $prescription->save();

        Event::fire(new PrescriptionStatusChanged($prescription));

        return redirect()->route('prescriptions.show', $id);
    }

    /**
     * @param MarkScriptAsSent $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsSent(MarkScriptAsSent $request ,$id)
    {


        $prescription = $this->prescriptions->getById($id);
        $prescription->status = Prescription::SENT;
        $prescription->tracking_ref = $request->get('tracking_ref');
        $prescription->collected_at = Carbon::now();
        $prescription->save();

        Event::fire(new PrescriptionStatusChanged($prescription));

        return redirect()->route('prescriptions.show', $id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsDelivered($id)
    {
        $prescription = $this->prescriptions->getById($id);
        $prescription->status = Prescription::DELIVERED;
        $prescription->delivered_at = Carbon::now();
        $prescription->save();

        Event::fire(new PrescriptionStatusChanged($prescription));

        return redirect()->route('prescriptions.show', $id);
    }
}
