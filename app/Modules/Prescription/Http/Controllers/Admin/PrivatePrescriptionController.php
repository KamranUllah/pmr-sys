<?php

namespace App\Modules\Prescription\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Prescription\Entities\Prescription;
use App\Modules\Prescription\Repositories\PrescriptionRepository;
use Carbon\Carbon;
use Event;
use App\Modules\RepeatPrescription\Repositories\PrescriptionRepository as mainbasePrescriptionsRepository;

use DB;
use Faker\Factory;
use Faker\Generator;
use File;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class PrivatePrescriptionController extends BackEndController {


    /**
     * @var PrescriptionRepository
     */
    private $prescription;

    /**
     * @var mainbasePrescriptionsRepository
     */

    private $mainbasePrescriptionRepository;
    /**
     * PrivatePrescriptionController constructor.
     */
    public function __construct(
        PrescriptionRepository $prescriptions,
        mainbasePrescriptionsRepository $mainBasePrescriptions
    )
    {
        parent::__construct();
        $this->prescriptions = $prescriptions;

        $this->mainbasePrescriptions = $mainBasePrescriptions;

        $requiredJs = [
            'medicine.js',
            'prescriptions.js',
        ];

        $this->addModuleJSFiles($requiredJs, 'prescription', true);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $this->theme->breadcrumb()->add('Private Prescriptions');
        $this->theme->prependTitle('Private Prescriptions');
        $this->theme->setSectionTitle('Private Prescriptions');
        $this->theme->asset()
            ->container('footer-custom')
            ->add('rp-scripts', \Bust::url('modules/prescription/js/private-prescription-vue.js'));
        return $this->theme->of('prescription::admin.private_vue')->render();
    }


    public function getPrivatePrescriptions(Request $request){

        $result = false;

        $params = [];
        $searchFilters = $request->has('filter') ? $request->get('filter') : null;

        $searchSortable = $request->has('sort') ? $request->get('sort') : null;

        $statusFilters = $request->has('status') ? strtoupper($request->get('status')) : null;

        if($searchFilters !== null){
            $params['filterby'] = $searchFilters;
        }

        if($searchSortable !== null){
            $data = explode('|',$searchSortable);
            $params['sortby'] = $data[0];
            $params['type'] = $data[1];
        }

        if($statusFilters !== null){
            $params['status'] = $statusFilters;
        }


        try {
            //call the service to get the private prescriptions
            $result = $this->mainbasePrescriptions->getPrivate(50,$params);
        } catch (Exception $ex) {
            auditLog('Attempt to fetch private prescriptions failed with Exception:'.$ex);
            \Bugsnag::notifyException($ex);
        }


        return response()->json($result);
    }



    /**
     * Update Patient NHS Number
     * Request $request
     * @param  int $id
     *
     * @return Response
     */
    public function updatePrescriptionAddress(Request $request, $prescriptionId)
    {
        $prescriptionData = [
            'address'            => $request->get('address'),
        ];

        if(!$prescriptionData['address'] || count($prescriptionData['address']) < 1){
            return response()->json('Prescription Address is not valid', 500);
        }


        // try to find patient
        try {
            $prescription = $this->mainbasePrescriptions->getById($prescriptionId);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to find prescription',
                'There was a problem attempting to find prescription # ' . $prescriptionId . ' :: ' . $exception->getMessage(),
                null,
                'error'
            );

            return response()->json('There was a problem finding the prescription', 500);

        }
        $prescription->address = $request->get('address');
        //try to update the patient data
        if ($prescription->save()) {
            auditLog('User #' . \Sentinel::getUser()
                    ->getUserId() . ' has updated Address for Prescription #' . $prescription->prescriptionid);
            if ($request->ajax()) {
                return response()->json([
                    'prescriptionId' => $prescription->prescriptionid,
                    'message'   => 'Prescription Address has been updated successfully',
                ]);
            }


        } else {
            \Bugsnag::notifyError(
                'Unable to update prescription',
                'There was a problem attempting to update  Address for Prescription # ' . $prescriptionId . ' :: ',
                null,
                'error'
            );

            return response()->json('There was a problem updating  Address for Prescription ', 500);
        }

    }

    /**
     * Return the prescription data
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getPrescription(Request $request){

        $result = false;
        $prescriptionId = (int)$request->get('id');
        try {
            //call the service to get the private prescriptions
            $result = $this->prescriptions->getById($prescriptionId);
            $result['statusText'] = $result->getStatus();
        } catch (Exception $ex) {
            auditLog('Attempt to fetch private prescription id '.$prescriptionId.'failed with Exception:'.$ex);
            \Bugsnag::notifyException($ex);
        }


        return response()->json($result);
    }

}
