<?php namespace App\Modules\Prescription\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Prescription\Entities\Medicine;
use App\Modules\Prescription\Events\FormularyUpdated;
use App\Modules\Prescription\Repositories\MedicineRepository;
use Request;
use DB;
use Event;
use Route;
use Yajra\Datatables\Datatables;



/**
 * Medicine controller
 *
 * Controller that contains functions pertaining to the pharmaceutical supplies
 *
 *
 */
class MedicineController extends BackEndController
{


    private $medicines;

    public function __construct(MedicineRepository $medicines)
    {
        parent::__construct();
        $this->medicines = $medicines;

        $requiredJs = [
            'medicine.js',
            'prescriptions.js',
        ];

        $this->addModuleJSFiles($requiredJs, 'prescription', true);
    }

    public function index()
    {
        $this->theme->prependTitle('Medicines');
        return $this->theme->of('prescription::index')->render();
    }

    /**
     * Method to display landing page in pharmaceutical section
     *
     * @return view
     */
    public function medicines()
    {
        return $this->theme->of('prescription::medicines')->render();
    }

    /**
     * Method to display dispensary view
     *
     * @return view
     */
    public function dispensary()
    {

        //fetch all medicines from database
        $data = Medicine::paginate(100);

//        dump($data);
        //return the view with 'data'
        return $this->theme->of('prescription::medicines', compact('data'))->render();

    }


    public function getDispensary()
    {

        $data = Medicine::all();
        return Datatables::of($data)->make(true);

    }

    /**
     * Method to display formulary view
     *
     * @return view
     */
    public function formulary()
    {

        //fetch all medicines from database
//        $data = Medicine::paginate(1000);
        $data = Medicine::all();

//        dd(json_encode($data));

//        return json_encode($data);
        //return the view with 'data'
        return $this->theme->of('prescription::formulary', compact('data'))->render();
    }


    public function getFormulary()
    {

        $data = Medicine::all();
        return Datatables::of($data)->make(true);

    }

    /**
     * Method to update to formulary
     *
     * @return view
     */
    public function updateFormulary()
    {

        //set variables
        $rrp = '';
        $dnpid = '';
        $qty = '';
        $in_formulary = '';
        //set posted data
        $data = request()->except('_token');

        foreach ($data as $key => $value) {
            $searchString = explode('_', $key);
            $dnpid = $searchString[1];
            if ($key === 'rrp_' . $dnpid) {
                $rrp = $value;
            }
            if ($key === 'qty_' . $dnpid) {
                $qty = $value;
            }
            if ($key === 'inform_' . $dnpid) {
                $in_formulary = $value === 0 ? 1 : $value;
            }
        }

        // if $qty number update database
        if (is_numeric($qty)) {
            isset($in_formulary) ? $in_formulary = 1 : $in_formulary = 0;
            $qty == 0 ? $inStock = 0 : $inStock = 1;

            //update or create record formulary table
            Medicine::find($dnpid)->formulary()->updateOrCreate(['dnpid' => $dnpid],
                ['dnpid' => $dnpid, 'available' => $in_formulary]);
            $formularyId = Medicine::find($dnpid)->formulary->id;
            //update medicines table
            if (!empty($formularyId)) {
                Medicine::where('DNPID', $dnpid)->update([
                    'FORMULARY' => $formularyId,
                    'QUANTITY'  => $qty,
                    'IN_STOCK'  => $inStock,
                ]);
            }

            //notify API
            $medicine = $this->medicines->getById($dnpid);
            //convert formulary value to 1 or 0 (true/false)
            $medicine->FORMULARY !== 0 ? $medicine->FORMULARY = 1 : $medicine->FORMULARY = 0;

            Event::fire(new FormularyUpdated($medicine));
        } else {
            //return with error
            return redirect()->back()->with('errors', 'Invalid quantity!');
        }
        //return back to dispensary page
        return redirect()->back()->with('flash_message', 'Formulary Updated!');
    }


    /**
     * @deprecated
     * Method to display formulary from Dr Now app
     *
     * @return view
     */
    public function onApp()
    {

        $data = [
            'action' => 'getformulary',
        ];

        $url = 'https://api.drnow.co.uk/apiweb.php';
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //results
        $resultfromapi = curl_exec($ch);
        $result = json_decode($resultfromapi, true);

        $appFormulary = [];
        foreach ($result as $res => $drugs) {
            foreach ($drugs as $drug) {
                $appFormulary[] = $drug;
            }
        }

        return $this->theme->of('prescription::onapp', compact('appFormulary', $appFormulary))->render();

    }

    /**
     * @return mixed
     */
    public function getAllMedicines()
    {
        $medicines = $this->medicines->getForSelect([
            'DNPID',
            'NM',
            'PACK',
            'IN_STOCK',
        ]);


        return Datatables::of($medicines)
            ->addColumn('action', function ($medicine) {
                return '<a href="javascript:void(0);" class="btn btn-xs btn-primary" data-medicine-id="' . $medicine->DNPID . '"><i class="glyphicon glyphicon-edit"></i> Edit</a> ' .
                    '<a href="javascript:void(0);" class="btn btn-xs btn-danger" data-medicine-id="' . $medicine->DNPID . '"><i class="glyphicon glyphicon-edit"></i> Delete</a> ';
            })
            ->editColumn('IN_STOCK', function ($medicine) {

                if ($medicine->IN_STOCK) {
                    return '<span class="green">Yes</span>';
                }

                return '<span class="red">No</span>';
            })
            ->editColumn('id', '{{$DNPID}}')
            ->make(true);
    }

}
