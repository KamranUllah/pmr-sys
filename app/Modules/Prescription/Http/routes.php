<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\Modules\Prescription\Http\Controllers'], function () {
    Route::get('/', 'PrescriptionController@index');

    Route::group(['prefix' => 'admin'], function () {
        Route::group(['prefix' => 'pharmaceutical'], function () {
            Route::get('/', [
                'as'   => 'prescription.medicines',
                'uses' => 'MedicineController@medicines',
            ]);

            Route::get('dispensary', [
                'as'   => 'prescription.dispensary',
                'uses' => 'MedicineController@dispensary',
            ]);

            Route::get('dispensary-get', [
                'as'   => 'prescription.dispensary.get.all',
                'uses' => 'MedicineController@getDispensary',
            ]);

            Route::get('formulary', [
                'as'   => 'prescription.formulary',
                'uses' => 'MedicineController@formulary',
            ]);

            Route::get('formulary-get', [
                'as'   => 'prescription.formulary.get.all',
                'uses' => 'MedicineController@getFormulary',
            ]);

            Route::get('update', [
                'as'   => 'prescription.formulary.update',
                'uses' => 'MedicineController@updateFormulary',
            ]);

            Route::get('onapp', [
                'as'   => 'medicine.onapp',
                'uses' => 'MedicineController@onApp',
            ]);

            Route::get('medicines', [
            'as'   => 'prescription.get.medicines',
                'uses' => 'MedicineController@index',
            ]);

            Route::get('fetch-medicines', [
                'as'   => 'prescription.get.medicines.data',
                'uses' => 'MedicineController@getAllMedicines',
            ]);

            Route::post('/', 'MedicineController@dispensary');
            Route::post('dispensary', 'MedicineController@dispensary');
            Route::post('formulary', 'MedicineController@formulary');
            Route::post('update', 'MedicineController@updateFormulary');
        });


        Route::group(['prefix' => 'prescriptions'], function () {

            Route::get('{id}', [
                'as'   => 'prescriptions.show',
                'uses' => 'PrescriptionController@show',
            ]);

            Route::post('{id}/update-status/ready', [
                'as'   => 'prescriptions.status.ready',
                'uses' => 'PrescriptionController@markAsReady',
            ]);

            Route::post('{id}/update-status/sent', [
                'as'   => 'prescriptions.status.sent',
                'uses' => 'PrescriptionController@markAsSent',
            ]);

            Route::post('{id}/update-status/delivered', [
                'as'   => 'prescriptions.status.delivered',
                'uses' => 'PrescriptionController@markAsDelivered',
            ]);

        });
    });
});




Route::group(['middleware' => 'web', 'namespace' => 'App\Modules\Prescription\Http\Controllers'], function () {

    Route::group(['prefix' => 'prescription'], function () {
        Route::group(['prefix' => 'admin'], function () {

            Route::get(
                '/',
                'Admin\PrivatePrescriptionController@index'
            )->name('admin.private.prescription.index');


            Route::get(
                '/get-prescriptions',
                'Admin\PrivatePrescriptionController@getPrivatePrescriptions'
            )->name('admin.private.prescriptions.get');

            Route::get(
                '/get-prescription',
                'Admin\PrivatePrescriptionController@getPrescription'
            )->name('admin.private.prescription.get');

            Route::post(
                '/update-prescription/address/{prescriptionId}',
                'Admin\PrivatePrescriptionController@updatePrescriptionAddress'
            )->name('admin.private.prescription.updateaddress');



        });
    });

});
