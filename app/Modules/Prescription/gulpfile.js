const elixir = require('laravel-elixir');
const argv = require('yargs').argv;

require('laravel-elixir-vue-2');

/*if (typeof moduleName === 'undefined') {
    console.log('Plunder more riffiwobbles!');
    return false;
}*/

elixir(function (mix) {

    mix.scripts(
        './app/Modules/Prescription/Assets/js/medicine.js'
        , 'public/modules/prescription/js/medicine.js');


    mix.webpack(
        './app/Modules/Prescription/Assets/js/private-prescription-vue.js'
        , 'public/modules/prescription/js/private-prescription-vue.js');



    mix.scripts(
        './app/Modules/Prescription/Assets/js/prescriptions.js'
        , 'public/modules/prescription/js/prescriptions.js');

});
