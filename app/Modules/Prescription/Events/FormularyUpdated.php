<?php namespace App\Modules\Prescription\Events;

use App\Modules\Prescription\Entities\Medicine;
use Event;
use Illuminate\Queue\SerializesModels;

class FormularyUpdated extends Event {

	use SerializesModels;
	/**
	 * @var Medicine
	 */
	public $medicine;

	/**
	 * Create a new event instance.
	 * @param Medicine $medicine
	 */
	public function __construct(Medicine $medicine)
	{
		$this->medicine = $medicine;
	}

}
