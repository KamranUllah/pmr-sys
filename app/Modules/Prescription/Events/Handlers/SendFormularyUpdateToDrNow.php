<?php namespace App\Modules\Prescriptions\Events\Handlers;


use App\Modules\Core\Services\DrNow\Api;
use App\Modules\Prescription\Events\FormularyUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Log;

class SendFormularyUpdateToDrNow {

	/**
	 * @var Api
	 */
	private $drNowApi;

	/**
	 * Create the event handler.
	 * @param Api $drNowApi
	 */
	public function __construct(Api $drNowApi)
	{
		$this->drnow = $drNowApi;
	}

	/**
	 * Handle the event.
	 *
	 * @param  FormularyUpdated  $event
	 * @return void
	 */
	public function handle(FormularyUpdated $event)
	{
		$response = $this->drnow->notifyFormularyUpdate($event->medicine->DNPID, $event->medicine->RRP,$event->medicine->FORMULARY,$event->medicine->NM);
		$data = ['drnResponse' => $response->json()];
		Log::info("Script ID {$event->medicine->dnpid} changed rrp to {$event->medicine->rrp}.", $data);
	}

}
