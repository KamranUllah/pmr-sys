<?php namespace App\Modules\Prescriptions\Events\Handlers;

use App\Modules\Core\Services\DrNow\Api;
use App\Modules\Prescription\Events\PrescriptionStatusChanged;
use Illuminate\Queue\InteractsWithQueue;
use Log;

class SendStatusUpdateToDrNow {

    /**
     * @var Api
     */
    private $drNowApi;

    /**
     * Create the event handler.
     * @param Api $drNowApi
     */
	public function __construct(Api $drNowApi)
	{
        $this->drnow = $drNowApi;
    }

	/**
	 * Handle the event.
	 *
	 * @param  PrescriptionStatusChanged  $event
	 * @return void
	 */
	public function handle(PrescriptionStatusChanged $event)
	{
        $response = $this->drnow->notifyStatusUpdate($event->prescription->drn_id, $event->prescription->status);
        $data = ['drnResponse' => $response->json()];
		Log::info("Script ID {$event->prescription->id} changed status to {$event->prescription->getStatus()}.", $data);
	}

}
