<?php namespace App\Modules\Prescription\Events;

use App\Modules\Prescription\Entities\Prescription;
use Event;
use Illuminate\Queue\SerializesModels;

class PrescriptionStatusChanged extends Event {

	use SerializesModels;
    /**
     * @var Prescription
     */
    public $prescription;

    /**
     * Create a new event instance.
     * @param Prescription $prescription
     */
	public function __construct(Prescription $prescription)
	{
		//
        $this->prescription = $prescription;
    }

}
