<?php
/**
 * Created by PhpStorm.
 * User: emman
 * Date: 25/10/2016
 * Time: 21:03
 */

namespace App\Modules\Prescription\Sidebar;

use App\Modules\User\Entities\Role;
use App\Modules\User\Entities\User;
use Maatwebsite\Sidebar\Badge;
use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\SidebarExtender;

class AdminSideBarExtender implements SidebarExtender
{

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group('Prescriptions', function (Group $group) {
            $group->hideHeading();
            $group->weight(100);
            $group->item('Private Prescriptions', function (Item $item) {
                $item->toggleIcon('fa fa-chevron-down');
                $item->icon('fa fa-medkit');
                $item->weight(14);


                $item->item('Manage Prescriptions', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-tasks');
                    $item->url('/prescription/admin#/manage');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                    });
                });

                $item->item('Waiting', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-clock-o');
                    $item->route('pharmacy.waiting');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    /*if (null !== request()->user()) {
                        $item->authorize(
                            request()->user()->hasAccess('user.admin.user.index')
                        );
                    }*/


                });

                $item->item('Sent', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-check');
                    $item->route('pharmacy.sent');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    /*if (null !== request()->user()) {
                        $item->authorize(
                            request()->user()->hasAccess('user.admin.user.index')
                        );
                    }*/


                });

                $item->item('Ready', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-truck');
                    $item->route('pharmacy.ready');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    /*if (null !== request()->user()) {
                        $item->authorize(
                            request()->user()->hasAccess('user.admin.user.index')
                        );
                    }*/


                });

                $item->item('Delivered', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-home');
                    $item->route('pharmacy.delivered');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    /*if (null !== request()->user()) {
                        $item->authorize(
                            request()->user()->hasAccess('user.admin.user.index')
                        );
                    }*/


                });

//                /*$item->item('Medicines', function (Item $item) {
//                    $item->weight(0);
//                    $item->icon('fa fa-check');
//                    $item->route('prescription.get.medicines');
//                    $item->badge(function (Badge $badge, User $user) {
//                        $badge->setClass('bg-green');
//                        //$badge->setValue($user->count());
//                    });
//
//                    /*if (null !== request()->user()) {
//                        $item->authorize(
//                            request()->user()->hasAccess('user.admin.user.index')
//                        );
//                    }
//
//
//                });*/
//
//                $item->item('Formulary', function (Item $item) {
//                    $item->weight(0);
//                    $item->icon('fa fa-check');
//                    $item->route('prescription.formulary');
//                    $item->badge(function (Badge $badge, User $user) {
//                        $badge->setClass('bg-green');
//                        //$badge->setValue($user->count());
//                    });
//
//                    /*if (null !== request()->user()) {
//                        $item->authorize(
//                            request()->user()->hasAccess('user.admin.user.index')
//                        );
//                    }*/
//
//
//                });
//
//                $item->item('Dispensary', function (Item $item) {
//                    $item->weight(0);
//                    $item->icon('fa fa-truck');
//                    $item->route('prescription.dispensary');
//                    $item->badge(function (Badge $badge, User $user) {
//                        $badge->setClass('bg-green');
//                        //$badge->setValue($user->count());
//                    });
//
//                    /*if (null !== request()->user()) {
//                        $item->authorize(
//                            request()->user()->hasAccess('user.admin.user.index')
//                        );
//                    }*/
//
//
//                });

            });


            //if (null !== request()->user()) {
                //$group->authorize(
                    //request()->user()->hasAccess('user.admin.*')
                //);
            //}
        });

        return $menu;
    }
}
