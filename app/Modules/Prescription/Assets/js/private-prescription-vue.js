
//repeat prescription import
import privatePrescriptionsComponent from '../../Resources/components/landing';
import managePrivatePrescriptionsComponent from '../../Resources/components/private/manage';
import viewPrivatePrescriptionComponent from '../../Resources/components/private/view';
// import resultImportComponent from '../../Resources/components/import/result';
// Repeat Prescriptions Feature
// Start Of VueJS usage
document.addEventListener('DOMContentLoaded', function() {
    /**
     *
     * @type {[*]}
     */
    const routes = [{
        path: '/',
        component: privatePrescriptionsComponent,
        meta: {
            title: 'Private Prescriptions'
        },
        children: [
            {
                path: '/manage',
                component: managePrivatePrescriptionsComponent,
                name: "managePrescriptions",
                meta: {
                    title: 'Manage Private Prescriptions'
                },
            },
            {
                path: '/view/:id',
                component: viewPrivatePrescriptionComponent,
                name: "viewPrescription",
                meta: {
                    title: 'Private Prescription : View'
                },
            },
        ]
    },
    ];

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.

    const router = new VueRouter({
        routes // short for routes: routes
    });

    switch (NowGP.currentRoute) {
        case 'admin.private.prescription.index':
            let privatePrescriptionVm = new Vue({
                el: '#privatePrescriptionApp',
                router
            });

            break;
    }
});

