$(document).ready(function() {

    $('#formularyTable').DataTable({
        "searching": { "regex": true },
        "destroy": true,
        "lengthMenu": [ [10, 25, 50, 100], [10, 25, 50, 100] ],
        "pageLength": 25,
        "serverSide": true,
        "processing": true,
        "ajax" : {
            url:NowGP.siteUrl + '/admin/pharmaceutical/formulary-get',
            dataSrc: 'data',
        },
        "columns": [
            {data: 'DNPID'},
            {data: 'NM'},
            {data: 'PACK'},
            {data: 'IN_STOCK'},
        ]
    }

    );
    $('#dispensaryTable').DataTable();

    // $('').DataTable({
    //         "searching": { "regex": true },
    //         "lengthMenu": [ [10, 25, 50, 100], [10, 25, 50, 100] ],
    //         "pageLength": 25,
    //         "serverSide": true,
    //         "processing": true,
    //         "ajax" : {
    //             url:NowGP.siteUrl + '/admin/pharmaceutical/dispensary-get',
    //             dataSrc: 'data',
    //         },
    //         "columns": [
    //             {data: 'DNPID'},
    //             {data: 'NM'},
    //             {data: 'PACK'},
    //             {data: 'PRICE'},
    //             {data: 'RRP'},
    //             {data: 'CODE'},
    //             {data: 'SOURCE'},
    //             {data: 'QUANTITY'},
    //             {data: 'FORMULARY'},
    //             {data: 'IN_STOCK'},
    //         ]
    //     }
    //
    // );
} );