//let users = function() {
$('#medicines-table').DataTable({
    processing: true,
    destroy: true,
    serverSide: true,
    ajax: NowGP.siteUrl + '/admin/user/users/list',
    columns: [
        {data: 'DNPID', name: 'DNPID'},
        {data: 'NM', name: 'NM'},
        {data: 'PACK', name: 'PACK'},
        {data: 'IN_STOCK', name: 'IN_STOCK'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});


//};

//users();