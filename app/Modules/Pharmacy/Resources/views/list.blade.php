<div class="x_panel">
    <div class="x_title">
        <h2>Prescriptions {{ $status }}</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
@if($prescriptions->count() > 0)
    <table class="table table-striped">
        <thead>
        <tr>
            <th>
                Patient Name
            </th>
            <th>
                Items
            </th>
            <th>
                Issue Date
            </th>
            <th>
                Completed Date
            </th>
            <th>
                Collected By Courier
            </th>
            <th>
                Delivered To Patient
            </th>

            <th>
                View
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($prescriptions as $prescription)
            <tr>
                <td>
                    {{ $prescription->patient->name }}
                </td>
                <td>
                    {{ $prescription->endorsements->count() }}
                </td>
                <td>
                    {{--{{ Carbon\Carbon::now() }}--}}
                    {{ $prescription->raised_at }}
                </td>
                <td>
                    {{--{{ Carbon\Carbon::now() }}--}}
                    {{ $prescription->completed_at }}
                </td>
                <td>
                    {{--{{ Carbon\Carbon::now() }}--}}
                    {{ $prescription->collected_at }}
                </td>
                <td>
                    {{--{{ Carbon\Carbon::now() }}--}}
                    {{ $prescription->delivered_at }}
                </td>

                <td>
                    <a href="{{ URL::route('prescriptions.show', $prescription->id) }}">
                        <i class="fa fa-file-text-o fa-2x"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p>
        No prescriptions to show
    </p>
@endif
    </div>
</div>
