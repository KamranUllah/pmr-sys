<?php

namespace App\Modules\Pharmacy\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Prescription\Entities\Prescription;
use App\Modules\Prescription\Repositories\PrescriptionRepository;
use Illuminate\Http\Request;

class PharmacyController extends BackEndController
{

    /**
     * @var PrescriptionRepository
     */
    private $prescriptions;

    public function __construct(PrescriptionRepository $prescriptions)
    {

        parent::__construct();
        $this->prescriptions = $prescriptions;

        $requiredJs = [
            'pharmacy.js',
        ];

        $this->addModuleJSFiles($requiredJs, 'pharmacy', true);
    }

    public function dashboard()
    {
        return $this->theme->of('pharmacy::dashboard')->render();
    }

    public function waiting()
    {
        $prescriptions = $this->prescriptions->getByStatus(Prescription::WAITING);
        $status = 'Waiting';
        $this->theme->prependTitle("Prescriptions {$status}");
        return $this->theme->of('pharmacy::list', compact('prescriptions', 'status'))->render();
    }

    public function ready()
    {
        $prescriptions = $this->prescriptions->getByStatus(Prescription::READY);
        $status = 'Ready';
        $this->theme->prependTitle("Prescriptions {$status}");
        return $this->theme->of('pharmacy::list', compact('prescriptions', 'status'))->render();
    }

    public function sent()
    {
        $prescriptions = $this->prescriptions->getByStatus(Prescription::SENT);
        $status = 'Sent';
        $this->theme->prependTitle("Prescriptions {$status}");
        return $this->theme->of('pharmacy::list', compact('prescriptions', 'status'))->render();
    }

    public function delivered()
    {
        $prescriptions = $this->prescriptions->getByStatus(Prescription::DELIVERED);
        $status = 'Delivered';
        $this->theme->prependTitle("Prescriptions {$status}");
        return $this->theme->of('pharmacy::list', compact('prescriptions', 'status'))->render();
    }


}
