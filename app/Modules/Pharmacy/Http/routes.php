<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\Modules\Pharmacy\Http\Controllers'], function () {
    //Route::get('/', 'PharmacyController@index');

    Route::group(['prefix' => 'admin'], function () {

        //pharmacy route handler
        Route::group(['prefix' => 'pharmacy'], function () {

            Route::get('waiting', [
                'as'   => 'pharmacy.waiting',
                'uses' => 'PharmacyController@waiting',
            ]);

            Route::get('ready', [
                'as'   => 'pharmacy.ready',
                'uses' => 'PharmacyController@ready',
            ]);

            Route::get('sent', [
                'as'   => 'pharmacy.sent',
                'uses' => 'PharmacyController@sent',
            ]);

            Route::get('delivered', [
                'as'   => 'pharmacy.delivered',
                'uses' => 'PharmacyController@delivered',
            ]);
        });
    });


});
