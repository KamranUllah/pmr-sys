<?php

namespace App\Modules\Pharmacy\Providers;

use Illuminate\Support\ServiceProvider;

class PharmacyServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('pharmacy.php'),
            __DIR__.'/../Config/permissions.php' => config_path('pharmacy_permissions.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'pharmacy'
        );

        $this->mergeConfigFrom(
            __DIR__ . '/../Config/permissions.php',
            'pharmacy-permissions'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/pharmacy');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/pharmacy';
        }, \Config::get('view.paths')), [$sourcePath]), 'pharmacy');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/pharmacy');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'pharmacy');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'pharmacy');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
