const elixir = require('laravel-elixir');
const argv = require('yargs').argv;

require('laravel-elixir-vue-2');

/*if (typeof moduleName === 'undefined') {
    console.log('Plunder more riffiwobbles!');
    return false;
}*/

elixir(function (mix) {

    mix.scripts(
        './app/Modules/Pharmacy/Assets/js/pharmacy.js'
        , 'public/modules/pharmacy/js/pharmacy.js');

});
