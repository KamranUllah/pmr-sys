<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 14/06/2017
 * Time: 16:01
 */

return [
    // User Management Permissions
    'Pharmacy' => [
        'pharmacy.admin.pharmacy.waiting'     => [
            'name'        => '',
            'description' => 'View list of waiting pharmacy items',
        ],
        'pharmacy.admin.pharmacy.ready'       => [
            'name'        => '',
            'description' => 'View list of ready pharmacy items',
        ],
        'pharmacy.admin.pharmacy.sent'     => [
            'name'        => '',
            'description' => 'View list of sent pharmacy items',
        ],
        'pharmacy.admin.pharmacy.delivered'      => [
            'name'        => '',
            'description' => 'View list of delivered pharmacy items',
        ],
    ],

];
