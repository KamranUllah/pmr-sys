<?php

namespace App\Modules\Dashboard\Http\Controllers;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Core\Http\Controllers\FrontEndController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DashboardController extends FrontEndController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $this->theme->prependTitle('Dashboard');
        return $this->theme->of('dashboard::dashboard')->render();
    }

    /**
     * Display 403
     * @return Response
     */
    public function display403()
    {
        $this->theme->prependTitle('Dashboard');
        return $this->theme->of('dashboard::admin.error.403')->render();
    }
}
