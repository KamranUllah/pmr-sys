<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\Modules\Dashboard\Http\Controllers'],
    function () {

//        Route::get('/', function () {
//            return redirect()->route('admin.dashboard');
//        });

        Route::get('/unauthorised', [
                'as'   => 'admin.unauthorised',
                'uses' => 'DashboardController@display403',
            ]
        );

        Route::group(['prefix' => 'admin'], function () {

            Route::get('/', [
                    'as'   => 'admin.index',
                    'uses' => 'DashboardController@index',
                ]
            );

            Route::get('main', [
                    'as'   => 'admin.main',
                    'uses' => 'Admin\DashboardController@main',
                ]
            );

            Route::group(['prefix' => 'dashboard'], function () {
                Route::get('/', [
                        'as'   => 'admin.dashboard',
                        'uses' => 'DashboardController@index',
                    ]
                );
            });


        });
    }
);
