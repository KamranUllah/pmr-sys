<?php
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 05/09/2016
 * Time: 16:22
 */

namespace App\Modules\Dashboard\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\SidebarExtender;

class AdminSideBarExtender implements SidebarExtender
{

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group('Dashboard', function (Group $group) {
            $group->hideHeading();
            $group->weight(-9999);
            $group->item('Dashboard', function (Item $item) {
                $item->toggleIcon('fa fa-chevron-down');
                $item->icon('fa fa-home');
                $item->weight(0);
                $item->url('/admin/dashboard');
                /*$item->badge(function (Badge $badge, Page $page) {
                    $badge->setClass('bg-green');
                    $badge->setValue($page->count());
                });
                $item->authorize(
                    request()->user()->hasAccess('page.pages.index')
                );*/
            });

            /*$group->authorize(
                request()->user()->hasAccess('page.*')
            );*/
        });

        return $menu;
    }
}
