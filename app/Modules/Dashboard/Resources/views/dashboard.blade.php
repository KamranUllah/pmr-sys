<div class="row top_tiles">
    <div class="row x_title">
        <div class="col-xs-12">
            <h3>Private Prescriptions</h3>
        </div>
    </div>
    <div class="animated flipInY col-lg-6 col-md-3 col-sm-6 col-xs-12">
        <a href="{!! route('pharmacy.waiting') !!}">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-clock-o"></i></div>
                <div class="count">{{ $waiting }}</div>
                <h3>Waiting</h3>
            </div>
        </a>
    </div>
    <div class="animated flipInY col-lg-6 col-md-3 col-sm-6 col-xs-12">
        <a href="{!! route('pharmacy.sent') !!}">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-check"></i></div>
                <div class="count">{{ $sent }}</div>
                <h3>Sent</h3>
            </div>
        </a>
    </div>
    <div class="animated flipInY col-lg-6 col-md-3 col-sm-6 col-xs-12">
        <a href="{!! route('pharmacy.ready') !!}">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-truck"></i></div>
                <div class="count">{{ $ready }}</div>
                <h3>Ready</h3>
            </div>
        </a>
    </div>
    <div class="animated flipInY col-lg-6 col-md-3 col-sm-6 col-xs-12">
        <a href="{!! route('pharmacy.delivered') !!}">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-home"></i></div>
                <div class="count">{{ $delivered }}</div>
                <h3>Delivered</h3>
            </div>
        </a>
    </div>
</div>
<br>