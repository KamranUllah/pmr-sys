<?php
/**
 * Created by PhpStorm.
 * User: emman
 * Date: 01/06/2017
 * Time: 21:39
 */

namespace App\Modules\Dashboard\Composers;


use App\Modules\Prescription\Entities\Prescription;
use App\Modules\Prescription\Repositories\PrescriptionRepository;

class DashboardComposer
{
    /**
     * @var PrescriptionRepository
     */
    private $prescriptions;

    /**
     * @param PrescriptionRepository $prescriptions
     */
    public function __construct(PrescriptionRepository $prescriptions)
    {

        $this->prescriptions = $prescriptions;
    }

    /**
     * @param $view
     */
    public function compose($view)
    {
        $view->with('waiting', $this->prescriptions->getByStatus(Prescription::WAITING)->count());
        $view->with('ready', $this->prescriptions->getByStatus(Prescription::READY)->count());
        $view->with('sent', $this->prescriptions->getByStatus(Prescription::SENT)->count());
        $view->with('delivered', $this->prescriptions->getByStatus(Prescription::DELIVERED)->count());
    }
}