<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 14/06/2017
 * Time: 16:01
 */

return [

    // User Management Permissions
    'Dashboard' => [
        'dashboard.admin.dashboard.main'     => [
            'name'        => '',
            'description' => 'Allow access to the main administrator dashboard',
        ],

        'dashboard.admin.dashboard.index'       => [
            'name'        => '',
            'description' => 'Allow access to the non-admin dashboard',
        ],
    ],

];
