<?php

Route::group(['middleware' => 'web', 'prefix' => 'core', 'namespace' => 'App\\Modules\Core\Http\Controllers'], function()
{
    Route::get('/', 'CoreController@index');

});

Route::group(['middleware' => 'web', 'namespace' => 'App\Modules\Core\Http\Controllers'], function()
{
    Route::get('/keep-alive', 'CoreController@keepAlive');
});
