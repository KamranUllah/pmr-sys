<?php

namespace App\Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use JavaScript;
use Route;
use Sentinel;

class BuildJSVariables
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $jsVariables = [
            'siteUrl'      => url('/'),
            'currentRoute' => Route::currentRouteName(),
            'csrfToken'    => csrf_token(),
        ];

        $jsVariables['broadcastType'] = config('broadcasting.default');
        $jsVariables['pusherKey'] = config('broadcasting.connections.pusher.key');
        $jsVariables['pusherRegion'] = config('broadcasting.connections.pusher.options.cluster');
        $jsVariables['pusherEncrypted'] = config('broadcasting.connections.pusher.options.encrypted');

        if (Sentinel::check()) {
            $jsVariables['userId'] = LoggedInUser()->getUserId();
            $jsVariables['username'] = LoggedInUser()->username;
            $jsVariables['user'] = LoggedInUser();
            //$jsVariables['user_role'] = Sentinel::findById(LoggedInUser()->getUserId())->roles()->get();
        }

        // Chat Functionality
        $jsVariables['chat_enabled'] = config('pmr.enable_chat');

        if (config('pmr.enable_chat')) {
            $jsVariables['new_conversation_interval_check'] = config('pmr.new_conversation_interval_check');
            $jsVariables['chat_interval_check'] = config('pmr.chat_interval_check');
            $jsVariables['new_chat_interval_check'] = config('pmr.new_chat_interval_check');
        }

        JavaScript::put($jsVariables);

        return $next($request);
    }
}
