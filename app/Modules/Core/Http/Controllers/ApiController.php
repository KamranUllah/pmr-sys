<?php namespace App\Modules\Core\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Prescription\Entities\Endorsement;
use App\Modules\Prescription\Entities\Prescription;
use App\Modules\Prescription\Events\PrescriptionStatusChanged;
use App\Repositories\PatientRepository;
use Carbon\Carbon;
use Event;
use Illuminate\Support\Facades\Input;

class ApiController extends Controller {

    /**
     * @var PatientRepository
     */
    private $patients;

    public function __construct(PatientRepository $patients)
    {
        $this->patients = $patients;
    }

    /**
     * @return string
     * @todo serious refactor :)
     */
    public function createPrescription()
    {
        $payload = (array) json_decode(Input::get('payload')) ?: Input::json()->all();

        $patientData = [
            'drn_id'         => $payload['patientid'],
            'name'           => $payload['patient'],
            'address'        => $payload['address'],
            'contact_number' => $payload['contactno'],
            'dob'            => Carbon::createFromFormat('d/m/Y', $payload['dob'])
        ];

        $patient = $this->patients->updateOrCreate(['drn_id'], $patientData);

        $prescriptionData = [
            'status'          => 0,
            'drn_id'          => $payload['prescriptionid'],
            'raised_at'       => $payload['dateraised'],
            'expired_at'      => $payload['dateexpiry'],
            'delivered_at'    => null,
            'doctor_gmc'      => $payload['doctorGMC'],
            'doctor_name'     => $payload['doctorname'],
            'sgncode'         => $payload['sgncode'],
            'courier_name'    => $payload['couriername'],
            'delivery_detail' => $payload['deliverydetail'],
            'courier_notes'   => $payload['couriernotes'],
            'tracking_ref'    => null,
            'repeat'          => $payload['repeatprescription'],
            'patient_id'      => $patient->id
        ];

        $prescription = Prescription::create($prescriptionData);

        foreach ($payload['endorsements'] as $endorsement)
        {
            if ( ! is_array($endorsement))
                $endorsement = (array) $endorsement;

            $endorsementData = [
                'dnpid'           => $endorsement['VPID'],
                'drug_name'       => $endorsement['NM'],
                'direction_1'     => $endorsement['Directions1'],
                'direction_2'     => $endorsement['Directions2'],
                'quantity'        => $endorsement['Quantity'],
                'units'           => $endorsement['Units'],
                'prescription_id' => $prescription['id'],
            ];

            Endorsement::create($endorsementData);
        }

        Event::fire(new PrescriptionStatusChanged($prescription));

        return \Response::json(['code' => 201, 'message' => 'success'], 201);
    }

}
