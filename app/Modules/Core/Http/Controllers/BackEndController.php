<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 01/06/2017
 * Time: 11:58
 */

namespace App\Modules\Core\Http\Controllers;


use App\Presenters\SidebarCreator;
use JavaScript;
use View;

class BackEndController extends CoreController
{
    protected $jsVariables;

    public function __construct()
    {
        $this->middleware('sentinel.auth');
        //$this->middleware('backend.access');
        $this->middleware('permissions');

        $this->setupTheme('gentella-admin', 'default');

        $this->jsVariables = [];

        if (config('pmr.enable_chat')) {
            $this->jsVariables['chat_interval_check'] = config('chat.chat_interval_check');
            $this->jsVariables['new_conversation_interval_check'] = config('chat.new_conversation_interval_check');
            $this->jsVariables['new_chat_interval_check'] = config('chat.new_chat_interval_check');
        }

        Javascript::put($this->jsVariables);

        // Add required JS files
        /*$requiredJs = [
            'notification.js'
        ];
        $this->addModuleJSFiles($requiredJs, 'notification', true);*/
    }


    /*public function index()
    {
        return redirect()->route('admin.dashboard');
    }*/
}