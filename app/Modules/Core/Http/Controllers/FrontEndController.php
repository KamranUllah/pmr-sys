<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 01/06/2017
 * Time: 11:58
 */

namespace App\Modules\Core\Http\Controllers;


class FrontEndController extends CoreController
{
    public function __construct()
    {
        $this->setupTheme('gentella-admin', 'default');

        // Add required JS files
        /*$requiredJs = [
            'notification.js'
        ];
        $this->addModuleJSFiles($requiredJs, 'notification', true, 'custom');*/
    }
}