<?php

namespace App\Modules\Core\Http\Controllers;

use Module;
use Theme;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use File;
use Validator;

class CoreController extends Controller
{
    protected $theme;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //return view('core::index');
        if (\Sentinel::getUser()->inRole('super-administrator') || \Sentinel::getUser()->inRole('administrator')) {
            return redirect()->route('admin.main');
        }

        return redirect()->route('admin.dashboard');
    }

    /**
     * @param string $theme
     * @param string $layout
     */
    protected function setupTheme($theme = 'default', $layout = 'default')
    {
        $this->theme = Theme::uses($theme)->layout($layout);
    }

    /**
     * Abort and display 404 page if the selected item is not found.
     *
     * @return \Illuminate\Http\Response
     * @throws \Facuz\Theme\UnknownLayoutFileException
     */
    protected function display404()
    {
        $this->setupTheme('default', '404');
        return $this->theme->of('errors.404', [])->render(404);
    }

    /**
     *
     */
    protected function display403()
    {
    }

    /**
     * Add Asset files to theme
     * Cache busting flag can be set to true to prevent browser caching the file
     *
     * @param array $jsFiles
     * @param null $module
     * @param bool $bustCache
     * @param string $dependency
     *
     * @return bool
     */
    public function addModuleJSFiles(array $jsFiles, $module = null, $bustCache = false, $dependency = 'main')
    {
        if (count($jsFiles) > 0) {
            if ($bustCache) {
                foreach ($jsFiles as $key => $value) {
                    $fileDetails = explode('.', $value);
                    $source = public_path("modules/{$module}/js/{$value}");

                    if (File::exists($source)) {
                        $mtime = File::lastModified($source);
                        $source = preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $source);
                        $source = str_replace(public_path() . DIRECTORY_SEPARATOR, '', $source);
                        $source = pathinfo($source)['basename'];
                    }

                    $this->theme->asset()->container('footer')->add("{$fileDetails[0]}", Module::asset("{$module}:js/{$source}"), ["{$dependency}"]);
                }
            } else {
                foreach ($jsFiles as $key => $value) {
                    $fileDetails = explode('.', $value);
                    $this->theme->asset()->container('footer')->add("{$fileDetails[0]}", Module::asset("{$module}:js/$value"), ["{$dependency}"]);
                }
            }
        }

        return false;
    }

    /**
     * Add Asset files to theme
     * Cache busting flag can be set to true to prevent browser caching the file
     *
     * @param array $jsFiles
     * @param null $module
     * @param bool $bustCache
     * @param string $dependency
     *
     * @return bool
     */
    public function addModuleCSSFiles(array $jsFiles, $module = null, $bustCache = false, $dependency = 'cms-style')
    {
        if (count($jsFiles) > 0) {
            if ($bustCache) {
                foreach ($jsFiles as $key => $value) {
                    $fileDetails = explode('.', $value);
                    $source = public_path("modules/{$module}/css/{$value}");

                    if (File::exists($source)) {
                        $mtime = File::lastModified($source);
                        $source = preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $source);
                        $source = str_replace(public_path() . DIRECTORY_SEPARATOR, '', $source);
                        $source = pathinfo($source)['basename'];
                    }

                    $this->theme->asset()->container('footer')->add("{$fileDetails[0]}", Module::asset("{$module}:css/{$source}"), ["{$dependency}"]);
                }
            } else {
                foreach ($jsFiles as $key => $value) {
                    $fileDetails = explode('.', $value);
                    $this->theme->asset()->container('footer')->add("{$fileDetails[0]}", Module::asset("{$module}:css/$value"), ["{$dependency}"]);
                }
            }
        }

        return false;
    }

    public function keepAlive() {
        return response('ok', 200);
    }

    /**
     * {@inheritdoc}
     */
    protected function formatValidationErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

}
