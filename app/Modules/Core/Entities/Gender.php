<?php

namespace App\Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'sex';

    protected $primaryKey = 'SexCodeId';

    protected $fillable = [
        'SexDescription',
        'Synonyms'
    ];

}
