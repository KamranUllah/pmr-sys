<?php

namespace App\Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'Languages';

    protected $primaryKey = 'Id';

    protected $fillable = [
        'Abbreviation',
        'Name',
        'Local_Name'
    ];
}
