import UserCounter from '../../Resources/components/UserCounter';
Vue.component(UserCounter.name, UserCounter);

document.addEventListener('DOMContentLoaded', function () {

    setTimeout(() => {
        new Vue({
            name: 'pmr-user-counter',

            el: '#userOnlineCounter'
        });
    }, 500)

})