<?php namespace App\Modules\Core\Services\DrNow;

use App\Models\Prescription;
use GuzzleHttp\Client;

class Api {

	protected $guzzle;

	public function __construct()
	{
		$this->guzzle = new Client(['base_url' => 'https://api.drnow.co.uk']);
		$this->key = $this->generateAuthKey();
	}

	public function notifyStatusUpdate($drNowId, $status)
	{
		$data = [
			'action'		 => 'updateprescription',
			'passkey'		 => $this->key,
			'prescriptionid' => $drNowId,
			'statusid'		 => $this->translateToDrNowStatusCode($status),
		];

		return $this->guzzle->post('/apipha.php', ['json' => $data]);
	}

	private function translateToDrNowStatusCode($status)
	{
		$drNowStatuses = [
			Prescription::WAITING   => [
				'code'        => 5,
				'description' => "returns upon receipt of the prescription",
			],
			999999                  => [
				'code'        => 6,
				'description' => "when the prescription is being prepared",
			],
			Prescription::READY     => [
				'code'        => 7,
				'description' => "when the prescription has been dispensed",
			],
			Prescription::SENT      => [
				'code'        => 8,
				'description' => "when the courier collects",
			],
			Prescription::DELIVERED => [
				'code'        => 9,
				'description' => "when the courier delivers.",
			],
		];

		if ( ! isset($drNowStatuses[$status]))
		{
			throw new \InvalidArgumentException("Status code provided: {$status} is not a valid prescription status");
		}

		return $drNowStatuses[$status]['code'];

	}

	private function generateAuthKey()
	{
		$genericSalt = 'mg1q=:xx2sCFLf;Wf%7!44*p<iX(2rrdddsq./$9*7Y9O-fgh?1pOLIDrK+LHb|,zZ6';
		$thisKey = 'politeness';

		return hash("sha512", $thisKey . $genericSalt);
	}
	
	
	/**
	 * Method to contact API to update Dr Now
	 * 
	 * @param type $dnpid
	 * @param type $rrp
	 * @param type $inFormulary
	 * @return type
	 */
	
	public function notifyFormularyUpdate($dnpid, $rrp, $inFormulary, $nm)
	{
		$data = [
			'action'	 => 'addtoformulary',
			'NM'		 => $nm,
			'passkey'	 => $this->key,
			'DNPId'		 => $dnpid,
			'UnitPrice'	 => $rrp,
			'Available'	 => $inFormulary
		];

		return $this->guzzle->post('/pharmapi.php', ['json' => $data]);
	}
	
}