<?php

if (!function_exists('gratavarUrl')) {
    /**
     * Gravatar URL from Email address
     *
     * @param string $email Email address
     * @param string $size Size in pixels
     * @param string $default Default image [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $rating Max rating [ g | pg | r | x ]
     *
     * @return string
     */
    function gratavarUrl($email, $size = 60, $default = 'mm', $rating = 'g')
    {
        return 'http://www.gravatar.com/avatar/' . md5(strtolower(trim($email))) . "?s={$size}&d={$default}&r={$rating}";
    }
}

if (!function_exists('clean')) {
    function clean($dirty_html)
    {
        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        return $purifier->purify($dirty_html);
    }
}

if (!function_exists('ellipsize')) {
    /**
     * @param        $str
     * @param        $max_length
     * @param int $position
     * @param string $ellipsis
     *
     * @param bool $strip_tags
     *
     * @return string
     */
    function ellipsize($str, $max_length, $position = 1, $ellipsis = '&hellip;', $strip_tags = false)
    {
        // Strip tags
        if ($strip_tags) {
            $str = trim(strip_tags($str));
        }

        // Is the string long enough to ellipsize?
        if (strlen($str) <= $max_length) {
            return $str;
        }

        $beg = substr($str, 0, floor($max_length * $position));

        $position = ($position > 1) ? 1 : $position;

        if ($position === 1) {
            $end = substr($str, 0, -($max_length - strlen($beg)));
        } else {
            $end = substr($str, -($max_length - strlen($beg)));
        }

        return $beg . $ellipsis . $end;
    }
}

/**
 * @param $value
 * @param $expectedValue
 *
 * @return string
 */
if (!function_exists('isChecked')) {
    function isChecked($value, $expectedValue)
    {
        return ($value === $expectedValue) ? 'checked="checked"' : '';
    }
}

/**
 * @param $selected
 *
 * @return string
 */
if (!function_exists('isSelected')) {
    function isSelected($selected)
    {
        return ($selected) ? 'selected="selected"' : '';
    }
}

if (!function_exists('write_file')) {
    /**
     * Write File
     *
     * Writes data to the file specified in the path.
     * Creates a new file if non-existent.
     *
     * @param    string $path File path
     * @param    string $data Data to write
     * @param    string $mode fopen() mode (default: 'wb')
     *
     * @return    bool
     */
    function write_file($path, $data, $mode = 'wb')
    {
        if (!$fp = @fopen($path, $mode)) {
            return false;
        }

        flock($fp, LOCK_EX);
        fwrite($fp, $data);
        flock($fp, LOCK_UN);
        fclose($fp);

        return true;
    }
}
if (!function_exists('getLoggedUser')) {
    function getLoggedUser()
    {
        return Sentinel::getUser();
    }
}

if (!function_exists('getTimeAgo')) {
    function getTimeAgo($date_value)
    {
        $date = \Carbon\Carbon::createFromTimeStamp(strtotime($date_value))->diffForHumans();

        return $date;
    }
}

/**
 * @param $address
 *
 * @return array
 */
if (!function_exists('geoLocate')) {
    function geoLocate($address)
    {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string);
        $lng_lat = [
            'latitude'  => $result->results[0]->geometry->location->lat,
            'longitude' => $result->results[0]->geometry->location->lng,
        ];

        return $lng_lat;
    }
}
if (!function_exists('cleanRoute')) {
    function cleanRoute($str, $separator = '.')
    {
        $str = ucwords(preg_replace('/[' . $separator . ']+/', ' ', strtolower(trim($str))));

        return $str = str_replace(' ', '', $str);
    }
}

if (!function_exists('humanize')) {
    function humanize($str)
    {
        return ucwords(preg_replace('/[_]+/', ' ', strtolower(trim($str))));
    }
}

if (!function_exists('getAge')) {
    function getAge($str)
    {
        return \Carbon\Carbon::createFromTimestamp(strtotime($str))->age;
    }
}

if (!function_exists('ago')) {
    function ago($str)
    {
        return \Carbon\Carbon::createFromTimestamp(strtotime($str))->diffForHumans();
    }
}

/**
 *  Given a file, i.e. /css/base.css, replaces it with a string containing the
 *  file's mtime, i.e. /css/base.1221534296.css.
 *
 * @param $file  The file to be loaded.  Must be an absolute path (i.e.
 *                starting with slash).
 *
 * @return mixed
 */
if (!function_exists('auto_version')) {
    function auto_version($file)
    {
        if (strpos($file, '/') !== 0 || !file_exists($_SERVER['DOCUMENT_ROOT'] . $file)) {
            return $file;
        }

        $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] . $file);

        return preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
    }
}


/**
 *add has-error to form-group
 *
 * @param string $key key/name of input field being checked
 * @param object $errors just passing the global $errors variable to the function
 *
 * @return string
 */
if (!function_exists('set_error')) {
    function set_error($key, $errors)
    {
        return $errors->has($key) ? 'has-error' : '';
    }
}

/**
 * get error message and add to a help-block
 *
 * @param string $key key/name of input field being checked
 * @param object $errors just passing the global $errors variable to the function
 *
 * @return string
 */
if (!function_exists('get_error')) {
    function get_error($key, $errors)
    {
        return $errors->has($key) ? $errors->first($key, '<span class="help-block">:message</span>') : '';
    }
}

/**
 * @param $key
 * @param $errors
 *
 * @return bool
 */
if (!function_exists('has_error')) {
    function has_error($key, $errors)
    {
        return $errors->has($key) ? 'has-error' : null;
    }
}


/**
 * Check Internet Connection.
 *
 * @param            string $sCheckHost Default: www.google.com
 *
 * @return           boolean
 */
if (!function_exists('check_internet_connection')) {
    function check_internet_connection($sCheckHost = 'www.google.com')
    {
        return (bool)@fsockopen($sCheckHost, 80, $iErrno, $sErrStr, 5);
    }
}

if (!function_exists('timezones')) {
    /**
     * Timezones
     *
     * Returns an array of timezones. This is a helper function
     * for various other ones in this library
     *
     * @param    string    timezone
     *
     * @return array|string
     */
    function timezones($tz = '')
    {
        // Note: Don't change the order of these even though
        // some items appear to be in the wrong order

        $zones = [
            'UM12'   => -12,
            'UM11'   => -11,
            'UM10'   => -10,
            'UM95'   => -9.5,
            'UM9'    => -9,
            'UM8'    => -8,
            'UM7'    => -7,
            'UM6'    => -6,
            'UM5'    => -5,
            'UM45'   => -4.5,
            'UM4'    => -4,
            'UM35'   => -3.5,
            'UM3'    => -3,
            'UM2'    => -2,
            'UM1'    => -1,
            'UTC'    => 0,
            'UP1'    => +1,
            'UP2'    => +2,
            'UP3'    => +3,
            'UP35'   => +3.5,
            'UP4'    => +4,
            'UP45'   => +4.5,
            'UP5'    => +5,
            'UP55'   => +5.5,
            'UP575'  => +5.75,
            'UP6'    => +6,
            'UP65'   => +6.5,
            'UP7'    => +7,
            'UP8'    => +8,
            'UP875'  => +8.75,
            'UP9'    => +9,
            'UP95'   => +9.5,
            'UP10'   => +10,
            'UP105'  => +10.5,
            'UP11'   => +11,
            'UP115'  => +11.5,
            'UP12'   => +12,
            'UP1275' => +12.75,
            'UP13'   => +13,
            'UP14'   => +14,
        ];

        if ($tz === '') {
            return $zones;
        }

        return $zones[$tz] ?? 0;
    }
}

if (!function_exists('timezone_menu')) {
    /**
     * Timezone Menu
     *
     * Generates a drop-down menu of timezones.
     *
     * @param    string    timezone
     * @param    string    classname
     * @param    string    menu name
     * @param    mixed    attributes
     *
     * @return    string
     */
    function timezone_menu($default = 'UTC', $class = '', $name = 'timezones', $attributes = '')
    {
        $default = ($default === 'GMT') ? 'UTC' : $default;

        $menu = '<select name="' . $name . '"';

        if ($class !== '') {
            $menu .= ' class="' . $class . '"';
        }

        $menu .= _stringify_attributes($attributes) . ">\n";

        foreach (timezones() as $key => $val) {
            $selected = ($default === $key) ? ' selected="selected"' : '';
            $menu .= '<option value="' . $key . '"' . $selected . '>' . Lang::get('dates.' . $key) . "</option>\n";
        }

        return $menu . '</select>';
    }
}

if (!function_exists('_stringify_attributes')) {
    /**
     * Stringify attributes for use in HTML tags.
     *
     * Helper function used to convert a string, array, or object
     * of attributes to a string.
     *
     * @param    mixed    string, array, object
     * @param    bool
     *
     * @return    string
     */
    function _stringify_attributes($attributes, $js = false)
    {
        $atts = null;

        if (empty($attributes)) {
            return $atts;
        }

        if (is_string($attributes)) {
            return ' ' . $attributes;
        }

        $attributes = (array)$attributes;

        foreach ($attributes as $key => $val) {
            $atts .= ($js) ? $key . '=' . $val . ',' : ' ' . $key . '="' . $val . '"';
        }

        return rtrim($atts, ',');
    }
}

if (!function_exists('byte_format')) {
    /**
     * Formats a numbers as bytes, based on size, and adds the appropriate suffix
     *
     * @param    mixed    will be cast as int
     * @param    int
     *
     * @return    string
     */
    function byte_format($num, $precision = 1)
    {
        if ($num >= 1000000000000) {
            $num = round($num / 1099511627776, $precision);
            $unit = 'TB';
        } elseif ($num >= 1000000000) {
            $num = round($num / 1073741824, $precision);
            $unit = 'GB';
        } elseif ($num >= 1000000) {
            $num = round($num / 1048576, $precision);
            $unit = 'MB';
        } elseif ($num >= 1000) {
            $num = round($num / 1024, $precision);
            $unit = 'KB';
        } else {
            $unit = 'B';

            return number_format($num) . ' ' . $unit;
        }

        return number_format($num, $precision) . ' ' . $unit;
    }
}

if (!function_exists('boolean')) {
    function boolean($val)
    {
        if ($val) {
            return 'On';
        }

        return 'Off';
    }
}

if (!function_exists('bs3_boolean')) {
    function bs3_boolean($val)
    {
        if ($val) {
            return '<span class="label label-success">On</span>';
        }

        return '<span class="label label-danger">Off</span>';
    }
}

/**
 * Method to generate a boolean message for a value
 *
 * @param   boolean $val is the value set?
 *
 * @return  string html code
 */

if (!function_exists('_set')) {
    function _set($val)
    {
        if ($val) {
            return 'Yes';
        }

        return 'No';
    }
}


/**
 * Method to generate an integer from a value
 *
 * @param   string $val a php ini value
 *
 * @return  string html code
 *
 * @deprecated  4.0  Use intval() or casting instead.
 */

if (!function_exists('integer')) {
    function integer($val)
    {
        return (int)$val;
    }
}

/**
 * Unzip a zip file
 *
 * @param $file
 * @param $path
 *
 * @return bool
 */

if (!function_exists('Unzip')) {
    function Unzip($file, $path)
    {
        $zip = new ZipArchive;
        $res = $zip->open($file);
        if ($res === true) {
            // extract it to the path we determined above
            try {
                $zip->extractTo($path);
            } catch (ErrorException $e) {
                //skip
            }
            $zip->close();

            return true;
        } else {
            return false;
        }
    }
}


if (!function_exists('getPermValue')) {
    function getPermValue($value)
    {
        if ((int)$value === 1) {
            $value = true;
        } elseif ((int)$value === 0) {
            $value = false;
        } else {
            $value = null;
        }

        return $value;
    }
}


if (!function_exists('getReversePermValue')) {
    function getReversePermValue($permission)
    {
        $data = explode("@", $permission);

        if ($data[1]) {
            $value = 1;
        } elseif (!$data[1]) {
            $value = -1;
        } else {
            $value = 0;
        }

        return $value;
    }
}


if (!function_exists('isDomainAvailable')) {

//returns true, if domain is availible, false if not
    function isDomainAvailable($domain)
    {
        //dd($domain);

        //check, if a valid url is provided
        if (!filter_var($domain, FILTER_VALIDATE_URL)) {
            return false;
        }

        //initialize curl
        $curlInit = curl_init($domain);
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curlInit, CURLOPT_HEADER, true);
        curl_setopt($curlInit, CURLOPT_NOBODY, true);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);

        //get answer
        $response = curl_exec($curlInit);

        curl_close($curlInit);

        if ($response) {
            return true;
        }

        return false;
    }
}


if (!function_exists('ip_exists_in_range')) {
    /**
     * Checks whether an IP address exists within a range of IP addresses
     *
     * @param array $range
     * @param $ip
     *
     * @return bool
     */
    function ip_exists_in_range(array $range, $ip)
    {
        if (ip2long($ip) >= ip2long($range[0]) && ip2long($ip) <= ip2long($range[1])) {
            return true;
        }

        return false;
    }
}


if (!function_exists('cleanInput')) {
    function cleanInput($string)
    {
        return strip_tags(clean($string));
    }
}

/**
 * Log doctor actions across the Dr System
 *
 * @param $text
 */
if (!function_exists('auditLog')) {
    function auditLog($text)
    {
        if (LoggedInUser()) {
            $postData = [
                'user_id'    => (string)LoggedInUser()->getUserId(),
                'text'       => (string)$text,
                'ip_address' => (string)request()->ip(),
                'user_agent' => (string)request()->header('User-Agent'),
                /*'screen_resolution' => (string)json_encode([
                    'screenSize' => [
                        'width'  => request()->hasCookie('sWidth') ? request()->cookie('sWidth') : 'N/A',
                        'height' => request()->hasCookie('sHeight') ? request()->cookie('sHeight') : 'N/A',
                    ],
                    'clientSize' => [
                        'width'  => request()->hasCookie('cWidth') ? request()->cookie('cWidth') : 'N/A',
                        'height' => request()->hasCookie('cHeight') ? request()->cookie('cHeight') : 'N/A',
                    ],
                ]),*/
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ];

            try {
                DB::connection('sqlsrv_audit')->table('pmr_system_log')->insert($postData);
            } catch (Exception $exception) {
                // Bugnsag stuff here!!!
            }
        }
    }
}


if (!function_exists('resizeImageAndCache')) {
    function resizeImageAndCache($imgUrl)
    {
        /*$img = Image::cache(function ($image) use ($imgUrl) {
            $image->make($imgUrl)->fit(200, 200)->encode('data-url');
        }, 10, true);*/

        //return (string) $img;
        if (!empty($imgUrl)) {
            return (string)Image::make($imgUrl)->fit(200, 200)->encode('data-url');
        }

        return null;
    }
}

if (!function_exists('getPatientInfo')) {
    function getPatientInfo($patientId, $cache = false)
    {
        $drnowv2Api = new \App\Services\NowGP\Data\DrNowV2API();
        $result = null;

        if ($cache) {
            try {
                $result = Cache::remember("patient_id{$patientId}",
                    22 * 60,
                    function () use ($drnowv2Api, $patientId) {
                        return $drnowv2Api->getBundledPatientInfoById($patientId);
                    });
            } catch (GuzzleHttp\Exception\ClientException $ex) {
                auditLog('400 Level error, attempted to get bundled patient info by id. The patient id was:' .
                    ' [' . $patientId . '], ' .
                    'and the error message was: ' . $ex->getMessage());
                \Bugsnag::notifyError('Client Exception',
                    '400 Level error, attempted to get bundled patient info by id. The patient id was:' .
                    ' [' . $patientId . '], ' .
                    'and the error message was: ' . $ex->getMessage());
            } catch (GuzzleHttp\Exception\TransferException $ex) {
                auditLog('Possible networking error (connection timeout, DNS errors, etc.), ' .
                    'attempted to get patient gp info by id. The patient id was: ' .
                    '[' . $patientId . '], and the error message was: ' . $ex->getMessage());
                \Bugsnag::notifyError('Transfer Exception',
                    'Possibly networking error (connection timeout, DNS errors, etc.), ' .
                    'attempted to get patient gp info by id. The patient id was:' .
                    ' [' . $patientId . '], and the error message was: ' . $ex->getMessage());
            }
        } else {
            try {
                $result = $drnowv2Api->getBundledPatientInfoById($patientId);
            } catch (GuzzleHttp\Exception\ClientException $ex) {
                auditLog('400 Level error, attempted to get bundled patient info by id. ' .
                    'The patient id was: [' . $patientId . '], and the error message was: ' . $ex->getMessage());
                \Bugsnag::notifyError('Client Exception',
                    '400 Level error, attempted to get bundled patient info by id. ' .
                    'The patient id was: [' . $patientId . '], and the error message was: ' . $ex->getMessage());
            } catch (GuzzleHttp\Exception\TransferException $ex) {
                auditLog('Possible networking error (connection timeout, DNS errors, etc.), ' .
                    'attempted to get patient gp info by id. The patient id was: [' . $patientId . '], ' .
                    'and the error message was: ' . $ex->getMessage());
                \Bugsnag::notifyError('Transfer Exception',
                    'Possibly networking error (connection timeout, DNS errors, etc.), ' .
                    'attempted to get patient gp info by id. The patient id was: [' . $patientId . '], ' .
                    'and the error message was: ' . $ex->getMessage());
            }
        }


        return $result;
    }
}

if (!function_exists('getPatientEpsStatus')) {
    function getPatientEpsStatus()
    {
        \DB::connection('sqlsrv_mainbase')->statement('exec sp_GET_EPSEstimatedValue');
    }
}