const elixir = require('laravel-elixir');
const argv = require('yargs').argv;

require('laravel-elixir-vue-2');

elixir(function (mix) {

    mix.webpack(
        './app/Modules/Core/Assets/js/user-online-counter.js'
        , 'public/modules/core/js/user-online-counter.js');

});
