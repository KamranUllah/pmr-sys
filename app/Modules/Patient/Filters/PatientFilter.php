<?php namespace App\Modules\Patient\Filters;

use EloquentFilter\ModelFilter;

class PatientFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relatedModel => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    /**
     * Filter By Patient Forename
     * @param $value
     * @return $this
     */
    public function firstName($value)
    {
        return $this->where('PatientForname', e($value));
    }

    /**
     * Filter By Patient Surname
     * @param $value
     * @return $this
     */
    public function lastName($value)
    {
        return $this->where('PatientSurname', e($value));
    }

    /**
     * Filter By Patient Sex
     * @param $value
     * @return $this
     */
    public function gender($value)
    {
        return $this->where('PatientSex', e($value));
    }

    /**
     * Filter By NHSNumber
     * @param $value
     * @return $this
     */
    public function nhsNumber($value)
    {
        return $this->where('NHSNumber', e($value));
    }

    /**
     * Filter by Postcode
     * @param $value
     * @return $this
     */
    public function postcode($value)
    {
        return $this->where('PatientAddress5', e($value));
    }

    /**
     * Filter by Date Of Birth
     * @param $value
     * @return $this
     */
    public function dob($value)
    {
        return $this->where('PatientDOB', e($value));
    }
}
