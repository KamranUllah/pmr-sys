<?php

namespace App\Modules\Patient\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EPSEstimatedValue extends Model implements AuditableContract
{

    use Auditable;

    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'EPSEstimatedValue';

    protected $primaryKey = 'PatientId';

    protected $fillable = [];

    //const CREATED_AT = 'DateAdded';
//    const UPDATED_AT = 'DateAdded';

    //protected $dates = ['DateAdded'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(MainBasePatient::class);
    }
}
