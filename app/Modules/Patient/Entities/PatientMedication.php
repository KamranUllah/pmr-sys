<?php

namespace App\Modules\Patient\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Models\Audit;

class PatientMedication extends Model implements AuditableContract
{
    use Auditable;

    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'Medication';

    protected $primaryKey = 'MedicationId';

    protected $fillable = [
        'PatientId',
        'Name',
        'Dosage',
        'PacketSize',
        'Enabled',
        'DatePrescribedOriginally',
        'DateLastPrescribed',
        'Acute'
    ];

    const CREATED_AT = 'DatePrescribedOriginally';
    const UPDATED_AT = 'DatePrescribedOriginally';

    protected $dates = ['DatePrescribedOriginally', 'DateLastPrescribed'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(MainBasePatient::class);
    }

}
