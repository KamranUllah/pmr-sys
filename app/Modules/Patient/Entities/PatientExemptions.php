<?php

namespace App\Modules\Patient\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PatientExemptions extends Model implements AuditableContract
{

    use Auditable;

    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'PatientExemptions';

    protected $primaryKey = 'PatientExemptionId';

    protected $fillable = [
        'PatientExemptionId',
        'CreatedDate',
        'PatientId',
        'IsExempt',
        'ExemptionReasonId',
        'CertificateNumber'
    ];

//    const CREATED_AT = 'CreatedDate';
//    const UPDATED_AT = 'DateAdded';

//    protected $dates = ['CreatedDate'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(MainBasePatient::class);
    }
}
