<?php

namespace App\Modules\Patient\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PatientNextOfKin extends Model implements AuditableContract
{
    use Auditable;

    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'NextOfKin';

    protected $primaryKey = 'NextOfKinId';

    protected $fillable = [
        'Name',
        'Telephone',
        'Relationship',
        'DateAdded',
        'PatientId'
    ];

    const CREATED_AT = 'DateAdded';
    const UPDATED_AT = 'DateAdded';

    protected $dates = ['DateAdded'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(MainBasePatient::class);
    }
}
