<?php namespace App\Modules\Patient\Entities;

use App\Modules\Patient\Filters\PatientFilter;
use App\Modules\Prescription\Entities\Prescription;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class MainBasePatient extends Model implements AuditableContract
{

    use Filterable, Auditable;

    public $timestamps = false;

    protected $dateFormat = 'Y-m-d H:i:s';

    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'patients';

    protected $primaryKey = 'PatientId';

    protected $fillable = [
        "PatientId",
        "PatientTitle",
        "PatientForename",
        "PatientSurname",
        "PatientDOB",
        "PatientSex",
        "PatientEmail",
        "PatientPassword",
        "PatientMobile",
        "PatientAddress1",
        "PatientAddress2",
        "PatientAddress3",
        "PatientAddress4",
        "PatientAddress5",
        "PatientNI",
        "PatientAllergies",
        "PatientPreExisting",
        "PatientAvatar",
        "PatientPrMed",
        "PaymentType",
        "Active",
        "BankName",
        "BankAccountName",
        "BankAccountNumber",
        "BankAccountSortcode",
        "AlternateAddress1",
        "AlternateAddress2",
        "AlternateAddress3",
        "AlternateAddress4",
        "AlternateAddress5",
        "AccountType",
        "Parent1",
        "Parent2",
        "PrimaryAccount",
        "DateJoined",
        "DateLeft",
        "DDPrice",
        "AppPrice",
        "CDDPrice",
        "DDAppts",
        "DDAnniversary",
        "SurgeryName",
        "SurgeryPhone",
        "GPfirstname",
        "GPlastname",
        "SurgeryAddress1",
        "SurgeryAddress2",
        "SurgeryCity",
        "SurgeryCounty",
        "SurgeryPostcode",
        "WorkAddress1",
        "WorkAddress2",
        "WorkCity",
        "WorkCounty",
        "WorkPostcode",
        "WorkPhone",
        "UserType",
        "PolicyNumber",
        "ValidStartDate",
        "ValidEndDate",
        "CompanyType",
        "PersonalAccessCode",
        "IsAvatarSet",
        "IsNameSet",
        "IsAddressSet",
        "LanguagePreferenceId",
        "DateVerified",
        "ccg_id",
        "NHSNumber",
        "GPGMCCode",
        "GPCode",
        "SurgeryFax",
        "SurgeryEmail",
        "NextOfKinId",
        "Consent",
    ];

    protected $dates = ['DateJoined'];

    protected $appends = ['full_name'];

    protected $with = ['allergies', 'medication', 'exemptions', 'eps','notes'];

    const CREATED_AT = 'DateJoined';

    const UPDATED_AT = 'DateJoined';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prescriptions()
    {
        return $this->hasMany(Prescription::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany(ClinicalNotes::class, 'PatientId','PatientId');
    }

    /**
     * @return \EloquentFilter\ModelFilter
     */
    public function modelFilter()
    {
        return $this->provideFilter(PatientFilter::class);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->PatientForename . ' ' . $this->PatientSurname;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function allergies()
    {
        return $this->hasMany(PatientAllergy::class, 'PatientId');
    }

    public function medication()
    {
        return $this->hasMany(PatientMedication::class, 'PatientId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exemptions()
    {
        return $this->hasOne(PatientExemptions::class, 'PatientId','PatientId')->latest('CreatedDate')
            ->leftJoin('ExemptionReasons', function ($join) {
                $join->on('ExemptionReasons.ExemptionReasonId', '=', 'PatientExemptions.ExemptionReasonId');
            });

    }

    public function eps()
    {
        return $this->hasOne(EPSEstimatedValue::class, 'PatientId','PatientId');
    }


}
