<?php

namespace App\Modules\Patient\Entities;

use App\Modules\Prescription\Entities\Prescription;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable = [
        'drn_id',
        'name',
        'address',
        'contact_number',
        'dob',
    ];

    protected $dates = ['dob'];

    public function prescriptions()
    {
        return $this->hasMany(Prescription::class);
    }
}
