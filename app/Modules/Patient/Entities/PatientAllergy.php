<?php

namespace App\Modules\Patient\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class PatientAllergy extends Model implements AuditableContract
{

    use Auditable;

    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'PatientAllergies';

    protected $primaryKey = 'PatientAllergyId';

    protected $fillable = [
        'PatientAllergyId',
        'PatientId',
        'DateAdded',
        'AllergyName',
        'AllergyDescription',
        'Certainty',
        'Severity',
    ];

    const CREATED_AT = 'DateAdded';
//    const UPDATED_AT = 'DateAdded';

    protected $dates = ['DateAdded'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(MainBasePatient::class);
    }
}
