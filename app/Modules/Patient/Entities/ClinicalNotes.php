<?php

namespace App\Modules\Patient\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class ClinicalNotes extends Model implements AuditableContract
{

    use Auditable;

    use SoftDeletes;


    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'ClinicalNotes';

    protected $primaryKey = 'ClinicalNoteId';

    protected $fillable = [
        'ClinicalNoteId',
        'PatientId',
        'UserId',
        'SourceSystem',
        'Note',
        'CreatedAt',
        'UpdatedAt',
        'DeletedAt',
    ];


    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    const DELETED_AT = 'DeletedAt';

    protected $dates = ['CreatedAt','UpdatedAt','DeletedAt'];

    public $timestamps = true;


    public function patient()
    {
        return $this->belongsTo(MainBasePatient::class);
    }

}
