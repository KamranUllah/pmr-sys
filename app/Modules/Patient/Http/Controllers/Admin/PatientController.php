<?php namespace App\Modules\Patient\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Core\Entities\Gender;
use App\Modules\Core\Entities\Language;
use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Patient\Entities\MainBasePatient;
use App\Modules\Patient\Entities\Patient;
use App\Modules\Patient\Entities\PatientAllergy;
use App\Modules\Patient\Entities\PatientMedication;
use App\Modules\Patient\Entities\PatientNextOfKin;
use App\Modules\Patient\Http\Requests\PatientDataImportRequest;
use App\Modules\Patient\Http\Requests\PatientDetailsRequest;
use App\Modules\Patient\Http\Requests\PatientSearchRequest;
use App\Services\NowGP\Data\Repositories\PatientsAdminRepository;
use App\Services\NowGP\Data\Repositories\PatientsPrescriptionAdminRepository;
use Faker\Factory;
use Faker\Generator;
use File;
use Illuminate\Http\Request;
use JavaScript;
use OwenIt\Auditing\Models\Audit;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use DB;
use Sentinel;
use App\Modules\User\Entities\Role;
use App\Modules\User\Entities\User;

use App\Modules\RepeatPrescription\Entities\Prescription;
use App\Modules\Patient\Entities\ClinicalNotes;
use App\Modules\RepeatPrescription\Repositories\PrescriptionRepository;
use App\Modules\Patient\Repositories\ClinicalNotesRepository;



class PatientController extends BackEndController
{

    /**
     * @var Patient
     */
    private $mainBasePatient;
    /**
     * @var PatientMedication
     */
    private $patientMedication;
    /**
     * @var PatientAllergy
     */
    private $patientAllergy;
    /**
     * @var Gender
     */
    private $gender;
    /**
     * @var Language
     */
    private $language;
    /**
     * @var PatientNextOfKin
     */
    private $patientNextOfKin;
    /**
     * @var PatientsAdminRepository
     */
    private $patientsAdminRepository;
    /**
     * @var PatientsAdminRepository
     */
    private $patientsPrescriptionAdminRepository;
    /**
     * @var PrescriptionRepository
     */
    private $prescriptions;
    /**
     * @var ClinicalNotesRepository
     */
    private $clinicalnotes;


    public function __construct(
        MainBasePatient $mainBasePatient,
        PatientMedication $patientMedication,
        PatientAllergy $patientAllergy,
        PatientNextOfKin $patientNextOfKin,
        Gender $gender,
        Language $language,
        PatientsAdminRepository $patientsAdminRepository,
        PatientsPrescriptionAdminRepository $patientsPrescriptionAdminRepository,
        PrescriptionRepository $prescriptions,
        ClinicalNotesRepository $clinicalnotes
    ) {
        parent::__construct();

        $requiredJs = [
            'clinical-notes.js',
            'patients.js',
            'allergy.js',
            'acute-medication.js',
            'current-medication.js',
            'discontinued-medication.js',
            'nextofkin.js',
            'patient-details.js',
        ];

        $this->addModuleJSFiles($requiredJs, 'patient', true);

        $requiredCss = [
            'patients.css',
        ];

        //$this->addModuleCSSFiles($requiredCss, 'patient', true);

        $this->mainBasePatient = $mainBasePatient;
        $this->patientMedication = $patientMedication;
        $this->patientAllergy = $patientAllergy;
        $this->gender = $gender;
        $this->language = $language;
        $this->patientNextOfKin = $patientNextOfKin;
        $this->patientsAdminRepository = $patientsAdminRepository;
        $this->patientsPrescriptionAdminRepository = $patientsPrescriptionAdminRepository;
        $this->prescriptions = $prescriptions;
        $this->clinicalnotes = $clinicalnotes;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $this->theme->prependTitle('Patients');
        return $this->theme->of('patient::admin.index')->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $patient = [];
        $genders = $this->gender->pluck('SexDescription', 'SexCodeId');
        $languages = $this->language->pluck('Name', 'Id');

        $allergyLastUpdated = [];
        $acuteMedicationLastUpdated = [];
        $currentMedicationLastUpdated = [];
        $discontinuedMedicationLastUpdated = [];
        $nextOfKinLastUpdated = [];
        $patientLastUpdated = [];
        $lastMedicationLastUpdated = [];

        //$this->theme->asset()->container('header-custom')->add('dp_override_style', \Bust::url('/modules/patient/css/datepicker_override.css'));

        Javascript::put([
            'PatientId' => 0,
        ]);

        // Breadcrumb Layout
        $this->theme->breadcrumb()->add('Back to Patients',
            route('admin.patients.index'))->add('New Patient');
        $this->theme->prependTitle('Patients');
        return $this->theme->of('patient::admin.view_patient_details',
            compact('patient',
                'genders',
                'languages',
                'patientLastUpdated',
                'lastMedicationLastUpdated',
                'currentMedicationLastUpdated',
                'acuteMedicationLastUpdated',
                'discontinuedMedicationLastUpdated',
                'allergyLastUpdated',
                'nextOfKinLastUpdated'))->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PatientDetailsRequest $request
     *
     * @return Response
     */
    public function store(PatientDetailsRequest $request)
    {
        // Set Patient Company As Default
        $privateCompanyGroup = DB::connection('sqlsrv_mainbase')
            ->table('companyclientgroup')
            ->where('ccg_name', 'LIKE', '%Private Default Group%')
            ->first();

        $timestamp = Carbon::now()->timestamp;
        $generatedDummyEmail = $request->get('PatientForename') . $request->get('PatientSurname') . $timestamp . '@drnow.co.uk';
        $patientData = [
            'PatientTitle'         => $request->get('PatientTitle'),
            'PatientForename'      => $request->get('PatientForename'),
            'PatientSurname'       => $request->get('PatientSurname'),
            'PatientSex'           => $request->get('PatientSex'),
            'LanguagePreferenceId' => $request->get('LanguagePreferenceId'),
            'NHSNumber'            => $request->get('NHSNumber'),
            'PatientDOB'           => $request->get('PatientDOB'),
            'PatientAddress1'      => $request->get('PatientAddress1'),
            'PatientAddress2'      => $request->get('PatientAddress2'),
            'PatientAddress3'      => $request->get('PatientAddress3'),
            'PatientAddress4'      => $request->get('PatientAddress4'),
            'PatientAddress5'      => $request->get('PatientAddress5'),
            'PatientMobile'        => $request->get('PatientMobile'),
            'AlternateAddress1'    => $request->get('AlternateAddress1'),
            'AlternateAddress2'    => $request->get('AlternateAddress2'),
            'AlternateAddress3'    => $request->get('AlternateAddress3'),
            'AlternateAddress4'    => $request->get('AlternateAddress4'),
            'AlternateAddress5'    => $request->get('AlternateAddress5'),
            'Consent'              => $request->get('Consent'),
            'ccg_id'               => $privateCompanyGroup->ccg_id,

            //unused variables
            'PatientEmail'         => $generatedDummyEmail,
        ];


//        dd($patientData);


//        if($patient = $this->mainBasePatient->create($patientData)){
//            flash()->success('Patient has been created successfully');
//            auditLog('User #' . \Sentinel::getUser()->getUserId() . ' created a new patient');
//            return response()->json([
//                'patientId' => $patient->PatientId,
//                'message' => 'Patient has been created successfully',
//            ]);
//        }else
//        {
//            dd("Error");
//        }

        try {
            $patient = $this->mainBasePatient->create($patientData);
            flash()->success('Patient has been created successfully');
            auditLog('User #' . \Sentinel::getUser()->getUserId() . ' created a new patient');
            return response()->json([
                'patientId' => $patient->PatientId,
                'message'   => 'Patient has been created successfully',
            ]);
            //return redirect()->route('admin.patients.show', [$patient->PatientId]);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to create patient',
                'There was a problem attempting to create a new patient :: ' . $exception->getMessage(),
                null,
                'error'
            );
            flash()->error('There was a problem creating this patient. Please try again');
            //return redirect()->back();
            return response()->json([
                'patientId' => 0,
                'message'   => 'There was a problem creating patient',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        //try {
        $patient = $this->mainBasePatient->find($id);
        $genders = $this->gender->pluck('SexDescription', 'SexCodeId');
        $languages = $this->language->pluck('Name', 'Id');

        $patientLastUpdated = [];
        if (null !== $patient) {
            $patientLastUpdated = $patient->audits()->orderBy('created_at', 'desc')->first();
        }

        // Allergies last updated
        $lastAllergy = $this->patientAllergy
            ->where('PatientId', $patient->PatientId)
            ->orderBy('DateAdded', 'desc')
            ->first();

        $allergyLastUpdated = [];
        if (null !== $lastAllergy) {
            $allergyLastUpdated = $lastAllergy->audits()->orderBy('created_at', 'desc')->first();
        }

        // Acute Medication Last Updated
        $lastAcuteMedication = $this->patientMedication
            ->where('PatientId', $patient->PatientId)
            ->where('Acute', 1)
            //->where('Enabled', 1)
            ->orderBy('DatePrescribedOriginally', 'desc')
            ->first();

        $acuteMedicationLastUpdated = [];
        if (null !== $lastAcuteMedication) {
            $acuteMedicationLastUpdated = $lastAcuteMedication->audits()->orderBy('created_at',
                'desc')->first();
        }

        // Current Medication Last Updated
        $lastCurrentMedication = $this->patientMedication
            ->where('PatientId', $patient->PatientId)
            ->where('Acute', 0)
            ->where('Enabled', 1)
            ->orderBy('DatePrescribedOriginally', 'desc')
            ->first();

        $currentMedicationLastUpdated = [];
        if (null !== $lastCurrentMedication) {
            $currentMedicationLastUpdated = $lastCurrentMedication->audits()->orderBy('created_at',
                'desc')->first();
        }


        // Discontinued Medication Last Updated
        $lastDiscontinuedMedication = $this->patientMedication
            ->where('PatientId', $patient->PatientId)
            ->where('Acute', 0)
            ->where('Enabled', 0)
            ->orderBy('DatePrescribedOriginally', 'desc')
            ->first();

        $discontinuedMedicationLastUpdated = [];
        if (null !== $lastDiscontinuedMedication) {
            $discontinuedMedicationLastUpdated = $lastDiscontinuedMedication->audits()->orderBy('created_at',
                'desc')->first();
        }

        // get latest Medication update date
        $lastMedication = $this->patientMedication
            ->where('PatientId', $patient->PatientId)
            ->orderBy('DatePrescribedOriginally', 'desc')
            ->first();

        $lastMedicationLastUpdated = [];
        if (null !== $lastMedication) {
            $lastMedicationLastUpdated = $lastMedication->audits()->orderBy('created_at',
                'desc')->first();
        }

        // Next Of Kin last updated
        $lastNextOfKin = $this->patientNextOfKin
            ->where('PatientId', $patient->PatientId)
            ->orderBy('DateAdded', 'desc')
            ->first();

        $nextOfKinLastUpdated = [];
        if (null !== $lastNextOfKin) {
            $nextOfKinLastUpdated = $lastNextOfKin->audits()->orderBy('created_at', 'desc')->first();
        }

        Javascript::put([
            'PatientId' => $patient->PatientId,
        ]);

        //$this->theme->asset()->container('header-custom')->add('dp_override_style', \Bust::url('/modules/patient/css/datepicker_override.css'));

        // Breadcrumb Layout
        $this->theme->breadcrumb()->add('Back to Patients',
            route('admin.patients.index'))->add($patient->full_name);
        $this->theme->prependTitle('Patients');
        auditLog('User #' . \Sentinel::getUser()
                ->getUserId() . ' has loaded details for patient #' . $patient->PatientId);
        return $this->theme->of('patient::admin.view_patient_details',
            compact('patient',
                'genders',
                'languages',
                'patientLastUpdated',
                'lastMedicationLastUpdated',
                'currentMedicationLastUpdated',
                'acuteMedicationLastUpdated',
                'discontinuedMedicationLastUpdated',
                'allergyLastUpdated',
                'nextOfKinLastUpdated'
            ))->render();
//        } catch (\Exception $exception) {
//            \Bugsnag::notifyError(
//                'Patient Not Found',
//                'Unable to find patient #' . $id . ' :: ' . $exception->getMessage(),
//                null,
//                'error'
//            );
//        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /*$patient = $this->mainBasePatient->find($id);

        //dd($patient);

        //$allergy = $patient->allergies()->latest()->first()->audits();
        //dd($allergy);

        //$medication = $patient->medication()->latest()->first()->audits();
        //dd($medication);

        $lastAllergy = $this->patientAllergy
            ->where('PatientId', $patient->PatientId)
            ->orderBy('DateAdded', 'desc')
            ->first();

        $allergyLastUpdated = [];
        if (null !== $lastAllergy) {
            $allergyLastUpdated = $lastAllergy->audits()->orderBy('created_at', 'desc')->first();
        }


        $lastMedication = $this->patientMedication
            ->where('PatientId', $patient->PatientId)
            ->orderBy('DatePrescribedOriginally', 'desc')
            ->first();


        $lastMedicationUpdated = [];
        if (null !== $lastMedication) {
            //dd($lastMedication);
            $lastMedicationUpdated = $lastMedication->audits()->orderBy('created_at', 'desc')->first();
            //dd($lastMedicationUpdated);
        }

        dd($lastMedicationUpdated->created_at->format('d/m/Y h:i'));*/
    }


    /**
     * Update Patient Active Status
     * Request $request
     * @param  int $id
     *
     * @return Response
     */
    public function updateStatus(Request $request, $id)
    {
        $patientData = [
            'Active'            => $request->get('active'),
        ];



        // try to find patient
        try {
            $patient = $this->mainBasePatient->find($id);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to find patient',
                'There was a problem attempting to find patient# ' . $id . ' :: ' . $exception->getMessage(),
                null,
                'error'
            );

            return response()->json('There was a problem finding the patient', 500);

        }

        //try to update the patient data

        if ($patient->update($patientData)) {
            auditLog('User #' . \Sentinel::getUser()
                    ->getUserId() . ' has updated Active Status for patient #' . $patient->PatientId);
            if ($request->ajax()) {
                return response()->json([
                    'patientId' => $patient->PatientId,
                    'message'   => 'Patient Active Status has been updated successfully',
                ]);
            }

        } else {
            \Bugsnag::notifyError(
                'Unable to create patient',
                'There was a problem attempting to update the Active Status for patient # ' . $id . ' :: ',
                null,
                'error'
            );

            return response()->json('There was a problem updating patient Active Status', 500);
        }

        //return redirect()->route('admin.patients.show', [$patient->PatientId]);
    }



    /**
     * Update Patient NHS Number
     * Request $request
     * @param  int $id
     *
     * @return Response
     */
    public function updateNHSNumber(Request $request, $id)
    {
        $patientData = [
            'NHSNumber'            => $request->get('NHSNumber'),
        ];

        if(!$patientData['NHSNumber'] || count($patientData['NHSNumber']) < 1){
            return response()->json('NHS Number is not valid', 500);
        }


        // try to find patient
        try {
            $patient = $this->mainBasePatient->find($id);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to find patient',
                'There was a problem attempting to find patient# ' . $id . ' :: ' . $exception->getMessage(),
                null,
                'error'
            );

            return response()->json('There was a problem finding the patient', 500);

        }

        //try to update the patient data

        if ($patient->update($patientData)) {
            auditLog('User #' . \Sentinel::getUser()
                    ->getUserId() . ' has updated NHS Number for patient #' . $patient->PatientId);
            if ($request->ajax()) {
                return response()->json([
                    'patientId' => $patient->PatientId,
                    'message'   => 'Patient NHS Number has been updated successfully',
                ]);
            }

        } else {
            \Bugsnag::notifyError(
                'Unable to create patient',
                'There was a problem attempting to update NHS Number for patient # ' . $id . ' :: ',
                null,
                'error'
            );

            return response()->json('There was a problem updating patient NHS Number', 500);
        }

        //return redirect()->route('admin.patients.show', [$patient->PatientId]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param PatientDetailsRequest $request
     * @param  int $id
     *
     * @return Response
     */
    public function update(PatientDetailsRequest $request, $id)
    {
        $patientData = [
            'PatientTitle'         => $request->get('PatientTitle'),
            'PatientForename'      => $request->get('PatientForename'),
            'PatientSurname'       => $request->get('PatientSurname'),
            'PatientSex'           => $request->get('PatientSex'),
            'LanguagePreferenceId' => $request->get('LanguagePreferenceId'),
            'NHSNumber'            => $request->get('NHSNumber'),
            'PatientDOB'           => $request->get('PatientDOB'),
            'PatientAddress1'      => $request->get('PatientAddress1'),
            'PatientAddress2'      => $request->get('PatientAddress2'),
            'PatientAddress3'      => $request->get('PatientAddress3'),
            'PatientAddress4'      => $request->get('PatientAddress4'),
            'PatientAddress5'      => $request->get('PatientAddress5'),
            'PatientMobile'        => $request->get('PatientMobile'),
            'AlternateAddress1'    => $request->get('AlternateAddress1'),
            'AlternateAddress2'    => $request->get('AlternateAddress2'),
            'AlternateAddress3'    => $request->get('AlternateAddress3'),
            'AlternateAddress4'    => $request->get('AlternateAddress4'),
            'AlternateAddress5'    => $request->get('AlternateAddress5'),
            'Consent'              => $request->get('Consent'),
        ];

        // try to find patient
        try {
            $patient = $this->mainBasePatient->find($id);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to find patient',
                'There was a problem attempting to find patient# ' . $id . ' :: ' . $exception->getMessage(),
                null,
                'error'
            );
            return response()->json([
                'message' => 'There was a problem finding the patient',
            ]);
        }

        //try to update the patient data

        if ($patient->update($patientData)) {
            auditLog('User #' . \Sentinel::getUser()
                    ->getUserId() . ' has updated details for patient #' . $patient->PatientId);
            if ($request->ajax()) {
                return response()->json([
                    'patientId' => $patient->PatientId,
                    'message'   => 'Patient has been updated successfully',
                ]);
            }

            return redirect()->route('admin.patients.show', [$patient->PatientId]);

        } else {
            \Bugsnag::notifyError(
                'Unable to create patient',
                'There was a problem attempting to updated patient # ' . $id . ' :: ',
                null,
                'error'
            );
            flash()->error('There was a problem updating this patient. Please try again');
            return response()->json([
                'message' => 'There was a problem updating patient',
            ]);
        }

        //return redirect()->route('admin.patients.show', [$patient->PatientId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function getPatientsData(PatientSearchRequest $request)
    {

        $patientsData = $this->mainBasePatient
            ->select([
                'PatientId',
                'PatientForename',
                'PatientSurname',
                'PatientDOB',
                'PatientSex',
                'NHSNumber',
                'Active',
            ]);


        //        $patientsData = $patientsData
//            ->join('PatientExemptions', function ($join) {
//                $join->on('PatientExemptions.PatientId', '=', 'patients.PatientId')
//                    ->orderBy('PatientExemptions.PatientExemptionId','desc')
//                    ->take(5);
//            })
//            ->join('ExemptionReasons', function ($join) {
//                $join->on('ExemptionReasons.ExemptionReasonId', '=', 'PatientExemptions.PatientExemptionId');
//            })
////            ->orderBy('PatientExemptions.PatientExemptionId','desc')
////            ->latest('patients.PatientId')
////            ->take(5)
//            ->select([
//                'patients.PatientId',
//                'patients.PatientForename',
//                'patients.PatientSurname',
//                'patients.PatientDOB',
//                'patients.PatientSex',
//                'patients.NHSNumber',
//                'ExemptionReasons.Reason',
//                'PatientExemptions.IsExempt',
//                'PatientExemptions.CreatedDate',
//            ]);


        try {
            return Datatables::of($patientsData)
                //->setTotalRecords(20)
                ->filter(function ($query) use ($request) {
                    /**
                     * Filter By Id
                     */
                    if ($request->get('patient_id')) {
                        $query->where('patients.PatientId',
                            'LIKE',
                            '%' . clean(trim($request->get('patient_id')) . '%'));
                    }


                    /**
                     * Filter By Forename
                     */
                    if ($request->get('first_name')) {
                        $query->where('PatientForename', 'LIKE', '%' . clean(trim($request->get('first_name')) . '%'));
                    }

                    /**
                     * Filter By Surname
                     */
                    if ($request->get('last_name')) {
                        $query->where('PatientSurname', 'LIKE', '%' . clean(trim($request->get('last_name')) . '%'));
                    }


                    /**
                     * Filter By Gender
                     */
                    if (($request->get('gender') && clean(trim($request->get('gender'))) != 999) || (clean(trim($request->get('gender'))) == 0)) {
                        $query->where('PatientSex', '=', clean(trim($request->get('gender'))));
                    }


                    /**
                     * Filter By Date Of Birth
                     */
                    if ($request->get('dob')) {
                        $query->where('PatientDOB', clean(trim($request->get('dob'))));
                    }


                    /**
                     * Filter By NHS Number
                     */
                    if ($request->get('nhs_number')) {
                        $query->where('NHSNumber', clean(trim($request->get('nhs_number'))));
                    }

                    /**
                     * Filter By Postcode
                     */
                    if ($request->get('postcode')) {
                        $query->where('PatientAddress5', clean(trim($request->get('postcode'))));
                    }
                })
                ->addColumn('action',
                    function ($patient) {
                        return '<a href="' . route('admin.patients.show',
                                [$patient->PatientId]) . '" class="btn btn-xs btn-primary" data-patient-id="' . $patient->PatientId . '"><i class="glyphicon glyphicon-edit"></i> Edit</a> ' .
                            ((int)$patient->Active === 1 ? '<a href="javascript:void(0)" class="btn btn-xs btn-danger disable-patient" data-patient-id="' . $patient->PatientId . '"><i class="glyphicon glyphicon-off"></i> Disable</a> '
                            : '<a href="javascript:void(0)" class="btn btn-xs btn-success enable-patient" data-patient-id="' . $patient->PatientId . '"><i class="glyphicon glyphicon-off"></i> Enable</a> ').
                            '<a href="javascript:void(0);" class="btn btn-xs btn-success view-rp"  data-patient-id="' . $patient->PatientId . '"><i class="glyphicon glyphicon-hourglass"></i> Repeat Prescription</a> '.
                            '<a href="javascript:void(0);" class="btn btn-xs btn-info view-note"  data-patient-id="' . $patient->PatientId . '"><i class="glyphicon glyphicon-sticky-note"></i> Notes ('.count($patient->notes).')</a> ';
                    })
                ->editColumn('PatientSex',
                    function ($patient) {
                        return (int)$patient->PatientSex === 1
                            ? '<i class="fa fa-male fa-lg fa-fw"></i> Male'
                            : ((int)$patient->PatientSex === 0
                                ? '<i class="fa fa-male fa-lg fa-fw"></i> Female'
                                : '<i class="fa fa-question fa-lg fa-fw"></i> Unknown');
                    })
                ->editColumn('exemptions.IsExempt',
                    function ($patient) {
                        if (!($patient->exemptions)) {
                            return '<div style="text-align:center"><i class="fa fa-question-circle-o text-info fa-2x"></i><br><strong>Not Declared</strong></div>';
                        }
                        $isExempt = (int)$patient->exemptions->IsExempt ? (int)$patient->exemptions->IsExempt : 0;
                        return (int)$patient->exemptions->IsExempt === 1
                            ? '<div style="text-align:center"><i class="fa fa-check-circle-o text-success fa-2x"></i><br><strong>Exempt</strong></div>'
                            : '<div style="text-align:center"><i class="fa fa-times-circle-o text-danger fa-2x"></i><br><strong>Paid</strong></div>';
                    })
                ->editColumn('exemptions.Reason',
                    function ($patient) {
                        if (!($patient->exemptions)) {
                            return 'Paid';
                        }
                        return $patient->exemptions->Reason;
                    })
                ->editColumn('id', '{{$PatientId}}')
                ->rawColumns(['PatientSex', 'exemptions.IsExempt', 'action'])
                ->make(true);
        } catch (\Exception $e) {
            \Bugsnag::notifyException($e);
        }
    }


    public function getnotImportedPatients()
    {

        // Breadcrumb Layout
        $this->theme->breadcrumb()->add('Back to Patients',
            route('admin.patients.index'))->add('Not Imported Data Patient');
        $this->theme->prependTitle('Patients');
        return $this->theme->of('patient::admin.view_not_imported_patients')->render();

    }

    public function getnotImportedPatientsData()
    {

        //get patient list
        $patientsWithAgreement = $this->mainBasePatient
            ->join('PatientInterests', 'patients.PatientId', '=', 'PatientInterests.PatientId')
            ->where('PatientInterests.ServiceId', '=', 2)
            ->join('PatientAgreementApprovals', 'patients.PatientId', '=', 'PatientAgreementApprovals.PatientId')
            ->where('PatientAgreementApprovals.AgreementId', '=', 5)
            ->distinct('patients.PatientId')
            ->select([
                'patients.PatientId',
                'PatientForename',
                'PatientSurname',
                'PatientDOB',
                'NHSNumber',
                'PatientAddress5',
                'IsDataImportComplete',
                'PatientAgreementApprovals.DateAdded'
            ]);


        //remove data similar to test data
        if (config('pmr.removed_test_data')) {
            $patientsWithAgreement = $patientsWithAgreement->where('PatientForename', 'NOT LIKE', '%test%')
                ->where('PatientSurname', 'NOT LIKE', '%test%')
                ->where('PatientEmail', 'NOT LIKE', '%test%');
        }


        //remove inactive patients
        $activePatients = $patientsWithAgreement->where('Active', '=', 1);

        //not imported patients
        $notImported = $activePatients->where('IsDataImportComplete', '=', 0);


        //}

        //Datatables return
        try {
            return Datatables::of($notImported)
                ->addColumn('action',
                    function ($patient) {
                        return '<a href="' . route('admin.patients.show',
                                [$patient->PatientId]) . '" class="btn btn-xs btn-primary" data-patient-id="' . $patient->PatientId . '"><i class="glyphicon glyphicon-edit"></i> Edit</a>'.
                                 '<a href="javascript:void(0);" class="btn btn-xs btn-info view-note"  data-patient-id="' . $patient->PatientId . '"><i class="glyphicon glyphicon-comment"></i> Notes ('.count($patient->notes).')</a> '.
                            '<a href="javascript:void(0);" class="btn btn-xs btn-danger disable-patient"  data-patient-id="' . $patient->PatientId . '"><i class="glyphicon glyphicon-off"></i> Disable</a> ';


                    })
                ->editColumn('exemptions.IsExempt',
                    function ($patient) {
                        if (!($patient->exemptions)) {
                            return '<div style="text-align:center"><i class="fa fa-question-circle-o text-info fa-2x"></i><br><strong>Not Declared</strong></div>';
                        }
                        $isExempt = (int)$patient->exemptions->IsExempt ? (int)$patient->exemptions->IsExempt : 0;
                        return (int)$patient->exemptions->IsExempt === 1
                            ? '<div style="text-align:center"><i class="fa fa-check-circle-o text-success fa-2x"></i><br><strong>Exempt</strong></div>'
                            : '<div style="text-align:center"><i class="fa fa-times-circle-o text-danger fa-2x"></i><br><strong>Paid</strong></div>';
                    })
                ->editColumn('exemptions.Reason',
                    function ($patient) {
                        if (!($patient->exemptions)) {
                            return 'Paid';
                        }
                        return $patient->exemptions->Reason;
                    })
                ->editColumn('PatientDOB',
                    function ($patient) {
                        return "<span class='hide'>" . date('Y-m-d',
                                strtotime(str_replace('/',
                                    '-',
                                    $patient->PatientDOB))) . "</span>" . $patient->PatientDOB;
                    })
                ->editColumn('NHSNumber',
                    function ($patient) {
                        return '<div style="text-align:center;font-weight:bold;width:100%;" ><a href="javascript:void(0);" class="nhsManage" data-nhsnumber="' . $patient->NHSNumber . '" data-patientid="' . $patient->PatientId . '">'.($patient->NHSNumber ? $patient->NHSNumber : '0N/A').'</a> </div>';
                    })
                ->editColumn('DateAdded',
                    function ($patient) {
                        return "<span class='hide'>" . $patient->DateAdded . "</span>" . \Carbon\Carbon::parse($patient->DateAdded)
                                ->tz('Europe/London')
                                ->format('d-m-Y H:i:s');
                    })
                ->editColumn('id', '{{$PatientId}}')
                ->order(function ($query) {
                    $query->orderBy('patients.PatientId', 'desc');
                })
                ->rawColumns(['PatientDOB', 'DateAdded', 'NHSNumber','exemptions.IsExempt', 'action'])
                ->make(true);
        } catch (\Exception $e) {
            \Bugsnag::notifyException($e);
        }


    }

    /**
     * @param PatientDataImportRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function markPatientDataImportAsComplete(PatientDataImportRequest $request)
    {
        $patientId = $request->get('PatientId');

        // Update Patient's Details
        $patientData = [
            //'PatientTitle'         => $request->get('PatientTitle'),
            'PatientForename' => $request->get('PatientForename'),
            'PatientSurname'  => $request->get('PatientSurname'),
            //'PatientSex'           => $request->get('PatientSex'),
            //'LanguagePreferenceId' => $request->get('LanguagePreferenceId'),
            'NHSNumber'       => $request->get('NHSNumber'),
            'PatientDOB'      => $request->get('PatientDOB'),
            'PatientAddress1' => $request->get('PatientAddress1'),
            'PatientAddress2' => $request->get('PatientAddress2'),
            'PatientAddress3' => $request->get('PatientAddress3'),
            'PatientAddress4' => $request->get('PatientAddress4'),
            'PatientAddress5' => $request->get('PatientAddress5'),
            //'PatientMobile'        => $request->get('PatientMobile'),
            //'AlternateAddress1'    => $request->get('AlternateAddress1'),
            //'AlternateAddress2'    => $request->get('AlternateAddress2'),
            //'AlternateAddress3'    => $request->get('AlternateAddress3'),
            //'AlternateAddress4'    => $request->get('AlternateAddress4'),
            //'AlternateAddress5'    => $request->get('AlternateAddress5'),
            //'Consent'              => $request->get('Consent'),

            'SurgeryName'     => clean($request->get('SurgeryName')),
            //'SurgeryPhone'    => clean($request->get('SurgeryPhone')),
            'GPfirstname'     => clean($request->get('GPfirstname')),
            'GPlastname'      => clean($request->get('GPlastname')),
            'SurgeryAddress1' => clean($request->get('SurgeryAddress1')),
            'SurgeryAddress2' => clean($request->get('SurgeryAddress2')),
            'SurgeryCity'     => clean($request->get('SurgeryCity')),
            'SurgeryCounty'   => clean($request->get('SurgeryCounty')),
            'SurgeryPostcode' => clean($request->get('SurgeryPostcode')),
            //'GPGMCCode'       => clean($request->get('GPGMCCode')),
            //'GPCode'          => clean($request->get('GPCode')),
            'SurgeryFax'      => clean($request->get('SurgeryFax')),
            'SurgeryEmail'    => clean($request->get('SurgeryEmail')),
        ];

        // try to find patient
        try {
            $patient = $this->mainBasePatient->find($patientId);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to find patient',
                'There was a problem attempting to find patient# ' . $patientId . ' :: ' . $exception->getMessage(),
                null,
                'error'
            );
            return response()->json([
                'message' => 'There was a problem finding the patient',
            ]);
        }

        $patientDetailsUpdate = $patient->update($patientData);

        //if ($patientDetailsUpdate) {
        $result = $this->patientsAdminRepository->markDataImportAsComplete($patientId);
        //}

        if ($result) {
            return response()->json();
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function getPatientEPSStatus()
    {
        $epsDataValue = getPatientEpsStatus();
        return response()->json([
            'epsMatchValue'    => $epsDataValue,
            'espStatusMatched' => null !== $epsDataValue ? round($epsDataValue, 2) * 100 : -1,
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPatientRepeatPrescriptions($patientId)
    {
        $filterType = 1; // repeat prescription

        $prescriptionHistory = collect($this->prescriptions->getAllByPatientId($patientId))
            ->sortBy('dateRaised')
            ->values();

//        dd($prescriptionHistory);
        return view('patient::admin.modals.view_rp_body', compact('prescriptionHistory'))->render();
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClinicalNotes($patientId)
    {
        $filterType = 1; // repeat prescription

        $clinicalNotes = collect($this->clinicalnotes->getByPatientId($patientId))
            ->sortByDesc('dateRaised')
            ->values();
        return view('patient::admin.modals.patient_notes_body', compact('clinicalNotes'))->render();
    }



    /**
     * Update Patient Surgery Fax
     * Request $request
     * @param  int $patientId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePatientSurgeryFax(Request $request, $patientId)
    {
        $patientData = [
            'SurgeryFax'            => $request->get('SurgeryFax'),
        ];

        if(!$patientData['SurgeryFax'] || count($patientData['SurgeryFax']) < 1){
            return response()->json('Patient Surgery Fax Number is not valid', 500);
        }


        // try to find patient
        try {
            $patient = $this->mainBasePatient->find($patientId);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to find patient',
                'There was a problem attempting to find patient # ' . $patientId . ' :: ' . $exception->getMessage(),
                null,
                'error'
            );

            return response()->json('There was a problem finding the patient', 500);

        }
        $patient->SurgeryFax = $request->get('SurgeryFax');
        //try to update the patient data
        if ($patient->save()) {
            auditLog('User #' . \Sentinel::getUser()
                    ->getUserId() . ' has updated Surgery Fax Number for Patient #' . $patientId);
            if ($request->ajax()) {
                return response()->json([
                    'patientId' => $patientId,
                    'message'   => 'Patient Surgery Fax has been updated successfully',
                ]);
            }


        } else {
            \Bugsnag::notifyError(
                'Unable to update patient',
                'There was a problem attempting to update  Surgery Fax for Patient # ' . $patientId . ' :: ',
                null,
                'error'
            );

            return response()->json('There was a problem updating  Address for Prescription ', 500);
        }

    }


}
