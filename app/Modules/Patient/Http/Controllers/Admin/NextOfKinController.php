<?php

namespace App\Modules\Patient\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Patient\Entities\PatientNextOfKin;
use App\Modules\Patient\Http\Requests\NextOfKinRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;

class NextOfKinController extends BackEndController
{

    /**
     * @var PatientNextOfKin
     */
    private $patientNextOfKin;

    public function __construct(PatientNextOfKin $patientNextOfKin)
    {
        parent::__construct();
        $this->patientNextOfKin = $patientNextOfKin;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('patient::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $nextOfKin = [];
        return view('patient::admin.partials.nextofkin.manage', compact('nextOfKin'))->render();
    }

    /**
     * Store a newly created resource in storage.
     * @param NextOfKinRequest|Request $request
     * @return Response
     */
    public function store(NextOfKinRequest $request)
    {
        try {
            $this->patientNextOfKin->create(
                [
                    'PatientId'    => clean($request->get('PatientId')),
                    'Name'         => clean($request->get('Name')),
                    'Telephone'    => clean($request->get('Telephone')),
                    'Relationship' => clean($request->get('Relationship')),
                ]
            );

            return response()->json([
                'success' => 1,
                'message' => 'Medication has been created for patient',
            ]);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to create next of kin',
                'There was a problem attempting to create next of kin data for Patient #' . clean($request->get('PatientId')),
                null,
                'error'
            );

            return response()->json([
                'success' => 0,
                'message' => 'There was a problem attempting to create next of kin data for this patient',
            ]);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('patient::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param $nextOfKinId
     * @return Response
     */
    public function edit($nextOfKinId)
    {
        try {
            $nextOfKin = $this->patientNextOfKin->find($nextOfKinId);
            return view('patient::admin.partials.nextofkin.manage', compact('nextOfKin'))->render();
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to retrieve next of kin details',
                'There was a problem attempting to retrieve next of kin details for Patient',
                null,
                'error'
            );
        }

    }

    /**
     * Update the specified resource in storage.
     * @param NextOfKinRequest|Request $request
     * @return Response
     */
    public function update(NextOfKinRequest $request, $nextOfKinId)
    {
        try {
            $this->patientNextOfKin->find($nextOfKinId)->update(
                [
                    //'NextOfKinId'  => clean($request->get('NextOfKinId')),
                    'PatientId'    => clean($request->get('PatientId')),
                    'Name'         => clean($request->get('Name')),
                    'Telephone'    => clean($request->get('Telephone')),
                    'Relationship' => clean($request->get('Relationship')),
                ]
            );

            return response()->json([
                'success' => 1,
                'message' => 'Next Of Kin details have been updated for patient',
            ]);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to create next of kin',
                'There was a problem attempting to update next of kin data for Patient #' . clean($request->get('PatientId')),
                null,
                'error'
            );

            return response()->json([
                'success' => 0,
                'message' => 'There was a problem attempting to update next of kin details for this patient',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $nextOfKinId
     * @return Response
     */
    public function destroy($nextOfKinId)
    {
        try {
            $medication = $this->patientNextOfKin->find($nextOfKinId)->delete();
            return response()->json([
                'success' => 1,
                'message' => 'Next of Kin has been deleted successfully!',
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => 0,
                'message' => 'There was a problem deleting this next of kin.',
            ]);
        }
    }


    /**
     * @param $patientId
     * @return
     * @internal param PatientsAllergiesDataRequest $request
     */
    public function getPatientNextOfKinData($patientId)
    {
        $patientNextOfKin = $this->patientNextOfKin->select(
            [
                'NextOfKinId',
                'Name',
                'Telephone',
                'Relationship',
                'DateAdded',
                'PatientId',
            ]
        )
            ->where('PatientId', $patientId)
            ->orderBy('DateAdded', 'desc')
            ->orderBy('NextOfKinId', 'asc')
            ->get();

        return Datatables::of($patientNextOfKin)
            ->addColumn('action', function ($nextOfKin) {
                return '<a href="javascript:void(0);" class="btn btn-xs btn-primary edit-nextofkin" ' .
                    'data-nextofkin-id="' . $nextOfKin->NextOfKinId . '" data-patient-id="' . $nextOfKin->PatientId . '">' .
                    '<i class="glyphicon glyphicon-edit"></i> Edit</a> ' .
                    '<a href="javascript:void(0);" class="btn btn-xs btn-danger delete-nextofkin" ' .
                    'data-nextofkin-id="' . $nextOfKin->NextOfKinId . '" data-patient-id="' . $nextOfKin->PatientId . '">' .
                    '<i class="glyphicon glyphicon-edit"></i> Delete</a> ';
            })
            ->editColumn('DateAdded', function ($nextOfKin) {
                return null !== $nextOfKin->DateAdded ?
                    Carbon::createFromTimestamp(strtotime($nextOfKin->DateAdded))->format('d/m/Y H:i a') :
                    'N/A';
            })
            //->editColumn('id', '{{$MedicationId}}')
            ->make(true);
    }
}
