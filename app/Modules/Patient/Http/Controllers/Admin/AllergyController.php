<?php

namespace App\Modules\Patient\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Patient\Entities\PatientAllergy;
use App\Modules\Patient\Http\Requests\ManagePatientAllergyRequest;
use App\Modules\Patient\Http\Requests\PatientsAllergiesDataRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;

class AllergyController extends BackEndController
{
    /**
     * @var PatientAllergy
     */
    private $patientAllergy;

    /**
     * AllergyController constructor.
     * @param PatientAllergy $patientAllergy
     */
    public function __construct(PatientAllergy $patientAllergy)
    {
        parent::__construct();
        $this->patientAllergy = $patientAllergy;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('patient::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $allergy = [];
        return view('patient::admin.partials.allergies.manage', compact('allergy'))->render();
    }

    /**
     * Store a newly created resource in storage.
     * @param ManagePatientAllergyRequest|Request $request
     * @return Response
     */
    public function store(ManagePatientAllergyRequest $request)
    {
        try {
            $this->patientAllergy->create(
                [
                    'PatientId'          => clean($request->get('PatientId')),
                    'AllergyName'        => clean($request->get('AllergyName')),
                    'AllergyDescription' => clean($request->get('AllergyDescription')),
                    'Certainty'          => clean($request->get('Certainty')),
                    'Severity'           => clean($request->get('Severity')),
                    'DateAdded'          => Carbon::now(),
                ]
            );

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has created an allergy for Patient Id: ' . $request->get('PatientId'));

            return response()->json([
                'success' => 1,
                'message' => 'Allergy has been created for patient',
            ]);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to create allergy',
                'There was a problem attempting to create allergy data for Patient #' . clean($request->get('PatientId')),
                null,
                'error'
            );

            return response()->json([
                'success' => 0,
                'message' => 'There was a problem attempting to create allergy data for this patient',
            ]);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('patient::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param $allergyId
     * @return Response
     */
    public function edit($allergyId)
    {
        try {
            $allergy = $this->patientAllergy->find($allergyId);
            return view('patient::admin.partials.allergies.manage', compact('allergy'))->render();
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to retrieve allergy details',
                'There was a problem attempting to retrieve allergy details for Patient',
                null,
                'error'
            );
        }

    }

    /**
     * Update the specified resource in storage.
     * @param ManagePatientAllergyRequest|Request $request
     * @param $allergyId
     * @return Response
     */
    public function update(ManagePatientAllergyRequest $request,$allergyId)
    {


        try {
            $allergy = $this->patientAllergy->find($allergyId);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to retrieve allergy details',
                'There was a problem attempting to retrieve allergy details for Patient',
                null,
                'error'
            );
        }

        try {
            $this->patientAllergy->find($allergyId)->update(
                [
                    'PatientAllergyId'   => $allergyId,
                    'PatientId'          => $request->get('PatientId'),
                    'AllergyName'        => clean($request->get('AllergyName')),
                    'AllergyDescription' => clean($request->get('AllergyDescription')),
                    'Certainty'          => clean($request->get('Certainty')),
                    'Severity'           => clean($request->get('Severity')),
                    'DateAdded'          => Carbon::now(),
                ]
            );

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has updated an allergy for Patient Id: ' . $request->get('PatientId'));

            return response()->json([
                'success' => 1,
                'message' => 'Allergy details have been updated for patient',
            ]);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to create allergy',
                'There was a problem attempting to update allergy data for Patient #' . clean($request->get('PatientId')),
                null,
                'error'
            );

            return response()->json([
                'success' => 0,
                'message' => 'There was a problem attempting to update allergy details for this patient',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $allergyId
     * @return Response
     */
    public function destroy($allergyId)
    {
        try {
            $allergy = $this->patientAllergy->find($allergyId)->delete();

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has deleted allergy id: ' . $allergyId);

            return response()->json([
                'success' => 1,
                'message' => 'Allergy has been deleted successfully!',
            ]);
        } catch(\Exception $exception) {
            return response()->json([
                'success' => 0,
                'message' => 'There was a problem deleting this allergy.',
            ]);
        }
    }


    /**
     * @param $patientId
     * @return
     * @internal param PatientsAllergiesDataRequest $request
     */
    public function getPatientAllergiesData($patientId)
    {
        $patientAllergies = $this->patientAllergy->select(
            [
                'PatientAllergyId',
                'AllergyName',
                'AllergyDescription',
                'Certainty',
                'Severity',
                'DateAdded',
                'PatientId'
            ]
        )->where('PatientId', $patientId)->get();

        return Datatables::of($patientAllergies)
            ->addColumn('action', function ($allergy) {
                return '<a href="javascript:void(0);" class="btn btn-xs btn-primary edit-allergy" data-allergy-id="' . $allergy->PatientAllergyId . '" data-patient-id="' . $allergy->PatientId . '"><i class="glyphicon glyphicon-edit"></i> Edit</a> ' .
                    '<a href="javascript:void(0);" class="btn btn-xs btn-danger delete-allergy" data-allergy-id="' . $allergy->PatientAllergyId . '" data-patient-id="' . $allergy->PatientId . '"><i class="glyphicon glyphicon-edit"></i> Delete</a> ';
            })
            ->editColumn('DateAdded', function($allergy) {
                return Carbon::createFromTimestamp(strtotime($allergy->DateAdded))->format('d/m/Y H:i a');
            })
            ->editColumn('id', '{{$PatientAllergyId}}')
            ->make(true);
    }
}
