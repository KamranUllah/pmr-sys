<?php

namespace App\Modules\Patient\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Patient\Entities\ClinicalNotes;
use App\Modules\Patient\Repositories\ClinicalNotesRepository;
use App\Modules\Patient\Http\Requests\ManageClinicalNotesRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;

class ClinicalNotesController extends BackEndController
{
    /**
     * @var ClinicalNotes
     */
    private $clinicalNotes;

    /**
     * ClinicalNotesController constructor.
     * @param ClinicalNotes $clinicalNotes
     */
    public function __construct(
        ClinicalNotes $clinicalNotes)
    {
        parent::__construct();
        $this->clinicalNotes = $clinicalNotes;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('patient::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $clinicalNotes = [];
        return view('patient::admin.partials.manage_patient_notes', compact('clinicalNotes'))->render();
    }

    /**
     * Store a newly created resource in storage.
     * @param ManageClinicalNotesRequest|Request $request
     * @return Response
     */
    public function store(ManageClinicalNotesRequest $request)
    {
        try {
            $this->clinicalNotes->create(
                [
                    'PatientId'          => clean($request->get('PatientId')),
                    'UserId'        => LoggedInUser()->getUserId(),
                    'SourceSystem'  => 'PEARL',
                    'Note'        => clean($request->get('Notes')),
                    'CreatedAt'          => Carbon::now(),
                ]
            );

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has created a note for Patient Id: ' . $request->get('PatientId'));

            return response()->json([
                'success' => 1,
                'message' => 'Note has been added for patient',
            ]);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to create note',
                'There was a problem attempting to create a clinical note for Patient #' . clean($request->get('PatientId')).'Exception: '.$exception,
                null,
                'error'
            );

            return response()->json([
                'success' => 0,
                'message' => 'There was a problem attempting to create a clinical note for this patient! <strong>Please try again</strong>',
            ]);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('patient::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param $clinicalNoteId
     * @return Response
     */
    public function edit($clinicalNoteId)
    {
        try {
            $clinicalNotes = $this->clinicalNotes->find($clinicalNoteId);
            return view('patient::admin.partials.manage_patient_notes', compact('clinicalNotes'))->render();
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to retrieve Notes  details',
                'There was a problem attempting to retrieve the clinical notes for Patient',
                null,
                'error'
            );

            return false;
        }

    }

    /**
     * Update the specified resource in storage.
     * @param ManageClinicalNotesRequest|Request $request
     * @param $clinicalNoteId
     * @return Response
     */
    public function update(ManageClinicalNotesRequest $request,$clinicalNoteId)
    {

        try {
            $clinicalNote = $this->clinicalNotes->find($clinicalNoteId);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to retrieve clinical note details',
                'There was a problem attempting to retrieve clinica note details for Patient',
                null,
                'error'
            );
        }

        try {
            $this->clinicalNotes->find($clinicalNoteId)->update(
                [
                    'ClinicalNotesId'   => $clinicalNoteId,
                    'PatientId'          => $request->get('PatientId'),
                    'UserId'        => LoggedInUser()->getUserId(),
                    'SourceSystem'  => 'PEARL',
                    'Note'        => clean($request->get('Notes')),
                    'UpdatedAt'          => Carbon::now(),
                ]
            );

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has updated an Note for Patient Id: ' . $request->get('PatientId'));

            return response()->json([
                'success' => 1,
                'message' => 'Clinical notes details have been updated for patient',
            ]);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to update clinical note',
                'There was a problem attempting to update clinical note'.$clinicalNoteId.'data for Patient #' . clean($request->get('PatientId').'Exception:'.$exception),
                null,
                'error'
            );

            return response()->json([
                'success' => 0,
                'message' => 'There was a problem attempting to update clinical note data for this patient. Exception:'.$exception,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param $clinicalNoteId
     * @return Response
     */
    public function destroy($clinicalNoteId)
    {
        try {
            $clinicalNote = $this->clinicalNotes->find($clinicalNoteId)->delete();

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has deleted clinical note id: ' . $clinicalNoteId);

            return response()->json([
                'success' => 1,
                'message' => 'Clinical Note has been deleted successfully!',
            ]);
        } catch(\Exception $exception) {
            return response()->json([
                'success' => 0,
                'message' => 'There was a problem deleting this Clinical Note.',
            ]);
        }
    }

}
