<?php

namespace App\Modules\Patient\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Patient\Entities\PatientMedication;
use App\Modules\Patient\Http\Requests\ManagePatientMedicationRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;

class AcuteMedicationController extends BackEndController
{
    /**
     * @var patientMedication
     */
    private $patientMedication;

    /**
     * AllergyController constructor.
     *
     * @param PatientMedication $patientMedication
     */
    public function __construct(PatientMedication $patientMedication)
    {
        parent::__construct();
        $this->patientMedication = $patientMedication;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('patient::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     * @throws \Throwable
     */
    public function create()
    {
        $medication = [];
        return view('patient::admin.partials.medication.manage_acute', compact('medication'))->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ManagepatientMedicationRequest|Request $request
     *
     * @return Response
     */
    public function store(ManagePatientMedicationRequest $request)
    {
        try {
            $medicationData = [
                'PatientId'                => strip_tags(clean($request->get('PatientId'))),
                'Name'                     => strip_tags(clean($request->get('Name'))),
                'Dosage'                   => strip_tags(clean($request->get('Dosage'))),
                'PacketSize'               => strip_tags(clean($request->get('PacketSize'))),
                'Enabled'                  => strip_tags(clean($request->get('Enabled'))),
                'DateLastPrescribed'       => Carbon::createFromFormat('d/m/Y H:i', $request->get('DateLastPrescribed')),
                'DatePrescribedOriginally' => Carbon::now(),
                'Acute'                    => 1,
                //'DateAdded'                => Carbon::now(),
            ];

            /*return response()->json([
                'medication' => $medicationData
            ], 400);*/

            $this->patientMedication->create($medicationData);

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has created an acute medication for Patient Id: ' . $request->get('PatientId'));

            return response()->json([
                'success' => 1,
                'message' => 'Medication has been created for patient',
            ]);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to create medication',
                'There was a problem attempting to create medication data for Patient #' . clean($request->get('PatientId')),
                null,
                'error'
            );

            \Bugsnag::notifyException($exception);

            return response()->json([
                'success' => 0,
                'message' => 'There was a problem attempting to create medication data for this patient',
            ], 400);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('patient::show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $medicationId
     *
     * @return Response
     * @throws \Throwable
     */
    public function edit($medicationId)
    {
        try {
            $medication = $this->patientMedication->find($medicationId);
            return view('patient::admin.partials.medication.manage_acute', compact('medication'))->render();
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to retrieve medication details',
                'There was a problem attempting to retrieve medication details for Patient',
                null
            );
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param ManagePatientMedicationRequest|Request $request
     *
     * @return Response
     */
    public function update(ManagePatientMedicationRequest $request)
    {
        try {
            $medicationData = [
                'MedicationId'             => strip_tags(clean($request->get('MedicationId'))),
                'PatientId'                => strip_tags(clean($request->get('PatientId'))),
                'Name'                     => strip_tags(clean($request->get('Name'))),
                'Dosage'                   => strip_tags(clean($request->get('Dosage'))),
                'PacketSize'               => strip_tags(clean($request->get('PacketSize'))),
                'Enabled'                  => strip_tags(clean($request->get('Enabled'))),
                'DateLastPrescribed'       => $request->get('DateLastPrescribed') ? Carbon::createFromFormat('d/m/Y H:i', $request->get('DateLastPrescribed')) : Carbon::now(),
                'DatePrescribedOriginally' => Carbon::now(),
                'Acute'                    => 1,
            ];

            $this->patientMedication->find($request->get('MedicationId'))->update($medicationData);

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has update an acute medication for Patient Id: ' . $request->get('PatientId'));

            return response()->json([
                'success' => 1,
                'message' => 'Medication details have been updated for patient',
            ]);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to create medication',
                'There was a problem attempting to update medication data for Patient #' . clean($request->get('PatientId')),
                null
            );

            return response()->json([
                'success' => 0,
                'message' => 'There was a problem attempting to update medication details for this patient',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $medicationId
     *
     * @return Response
     */
    public function destroy($medicationId)
    {
        try {
            $medication = $this->patientMedication->find($medicationId)->delete();

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has deleted an acute medication: ' . $medicationId);

            return response()->json([
                'success' => 1,
                'message' => 'Medication has been deleted successfully!',
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => 0,
                'message' => 'There was a problem deleting this medication.',
            ]);
        }
    }


    /**
     * @param $patientId
     *
     * @return
     * @internal param PatientsAllergiesDataRequest $request
     * @throws \Exception
     */
    public function getPatientMedicationData($patientId)
    {
        $patientMedication = $this->patientMedication->select(
            [
                'MedicationId',
                'Name',
                'Dosage',
                'PacketSize',
                'DatePrescribedOriginally',
                'Enabled',
                'PatientId',
            ]
        )
            ->where('PatientId', $patientId)
            ->where('Acute', 1)
            ->orderBy('DatePrescribedOriginally', 'desc')
            ->orderBy('MedicationId', 'asc')
            ->get();

        try {
            return Datatables::of($patientMedication)
                ->addColumn('action',
                    function ($medication) {
                        return '<a href="javascript:void(0);" class="btn btn-xs btn-primary edit-acute-medication" ' .
                            'data-medication-id="' . $medication->MedicationId . '" data-patient-id="' . $medication->PatientId . '">' .
                            '<i class="glyphicon glyphicon-edit"></i> Edit</a> ' .
                            '<a href="javascript:void(0);" class="btn btn-xs btn-danger delete-acute-medication" ' .
                            'data-medication-id="' . $medication->MedicationId . '" data-patient-id="' . $medication->PatientId . '">' .
                            '<i class="glyphicon glyphicon-edit"></i> Delete</a> ';
                    })
                ->editColumn('DatePrescribedOriginally',
                    function ($medication) {
                        return null !== $medication->DatePrescribedOriginally ?
                            Carbon::createFromTimestamp(strtotime($medication->DatePrescribedOriginally))
                                ->format('d/m/Y H:i a') :
                            'N/A';
                    })
                ->editColumn('Enabled',
                    function ($medication) {
                        return $medication->Enabled ? '<i class="fa fa-check-circle-o text-success fa-2x"></i>' : '<i class="fa fa-times-circle-o text-danger fa-2x"></i>';
                    })
                //->editColumn('id', '{{$MedicationId}}')
                ->rawColumns(['Enabled', 'action'])
                ->make(true);
        } catch (\Exception $e) {
            \Bugsnag::notifyException($e);
        }
    }
}
