<?php

namespace App\Modules\Patient\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Patient\Entities\MainBasePatient;
use App\Modules\Patient\Http\Requests\GPDetailsRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class GPDetailController extends BackEndController
{
    /**
     * @var MainBasePatient
     */
    private $mainBasePatient;

    public function __construct(MainBasePatient $mainBasePatient)
    {
        parent::__construct();
        $this->mainBasePatient = $mainBasePatient;
    }

    /**
     * Update the specified resource in storage.
     * @param GPDetailsRequest|Request $request
     * @param $id
     * @return Response
     */
    public function update(GPDetailsRequest $request, $id)
    {
        /*"SurgeryName",
        "SurgeryPhone",
        "GPfirstname",
        "GPlastname",
        "SurgeryAddress1",
        "SurgeryAddress2",
        "SurgeryCity",
        "SurgeryCounty",
        "SurgeryPostcode",*/


        $patientData = [
            'SurgeryName'     => clean($request->get('SurgeryName')),
            'SurgeryPhone'    => clean($request->get('SurgeryPhone')),
            'GPfirstname'     => clean($request->get('GPfirstname')),
            'GPlastname'      => clean($request->get('GPlastname')),
            'SurgeryAddress1' => clean($request->get('SurgeryAddress1')),
            'SurgeryAddress2' => clean($request->get('SurgeryAddress2')),
            'SurgeryCity'     => clean($request->get('SurgeryCity')),
            'SurgeryCounty'   => clean($request->get('SurgeryCounty')),
            'SurgeryPostcode' => clean($request->get('SurgeryPostcode')),
            'GPGMCCode'       => clean($request->get('GPGMCCode')),
            'GPCode'          => clean($request->get('GPCode')),
            'SurgeryFax'      => clean($request->get('SurgeryFax')),
            'SurgeryEmail'    => clean($request->get('SurgeryEmail')),
        ];

        // try to find patient
        $patient = $this->mainBasePatient->find($id);

        if(!$patient) {
            \Bugsnag::notifyError(
                'Unable to find patient',
                'There was a problem attempting to find patient# ' . $id . ' :: ',
                null,
                'error'
            );
            return response()->json([
                'message'   => 'There was a problem finding the patient',
            ]);
        }

        $update = $patient->update($patientData);

        if($update){
            flash()->success('Patient details have been updated successfully');
            auditLog('User #' . \Sentinel::getUser()->getUserId() . ' has updated details for patient #' . $patient->PatientId);
            return response()->json([
                'patientId' => $patient->PatientId,
                'message'   => 'Patient has been updated successfully',
            ]);
        }

        \Bugsnag::notifyError(
            'Unable to update patient',
            'There was a problem attempting to updated patient # ' . $patient->PatientId,
            null,
            'error'
        );
        flash()->error('There was a problem updating this patient. Please try again');
        return response()->json([
            'message'   => 'There was a problem updating patient',
        ]);

        return redirect()->route('admin.patients.show', [$patient->PatientId]);
    }
}
