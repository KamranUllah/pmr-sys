<?php

namespace App\Modules\Patient\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GPDetailsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'SurgeryName'     => "nullable|sometimes|min:2|max:32",
            'SurgeryPhone'    => 'nullable|sometimes',
//            'GPfirstname'     => "required|min:2|max:32",
//            'GPlastname'      => "required|min:2|max:32",
            'SurgeryAddress1' => "required|min:2|max:32",
            'SurgeryAddress2' => "nullable|sometimes|min:2|max:32",
            'SurgeryCity'     => 'required|min:2|max:45',
            'SurgeryCounty'   => 'nullable|sometimes|min:2|max:45',
            'SurgeryPostcode' => 'required',
            'GPGMCCode'       => '',
            'GPCode'          => '',
            'SurgeryFax'      => 'nullable|sometimes',
            'SurgeryEmail'    => 'nullable|sometimes|email',
        ];
    }


    public function messages() {
        return [
//            'GPfirstname.required' => 'The GP first name is required',
//            'GPlastname.required' => 'The GP last name is required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
