<?php

namespace App\Modules\Patient\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientDataImportRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            //
            'PatientId'       => 'required|numeric',
            'NHSNumber'       => "required",
            'PatientDOB'      => "required|date_format:d/m/Y",
            'PatientForename' => "required|min:2|max:32|"
                . "regex:/^[a-zA-Zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'PatientSurname'  => "required|min:2|max:32|"
                . "regex:/^[a-zA-Zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'PatientAddress1' => "required|min:2|max:32|"
                . "regex:/^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'PatientAddress2' => "nullable|sometimes|min:2|max:32|"
                . "regex:/^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'PatientAddress3' => 'required|min:2|max:45|regex:' .
                "/^[a-zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšž'\-, ]*$/i",
            'PatientAddress4' => 'nullable|sometimes|min:2|max:45|regex:' .
                "/^[a-zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšž'\-, ]*$/i",
            'PatientAddress5' => 'required',
            'SurgeryName'     => "required|min:2|max:32|"
                . "regex:/^[a-zA-Zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'GPfirstname'     => "sometimes|min:2|max:32|"
                . "regex:/^[a-zA-Zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'GPlastname'      => "sometimes|min:2|max:32|"
                . "regex:/^[a-zA-Zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'SurgeryAddress1' => "required|min:2|max:32|"
                . "regex:/^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'SurgeryAddress2' => "nullable|sometimes|min:2|max:32|"
                . "regex:/^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'SurgeryCity'     => 'required|min:2|max:45|regex:' .
                "/^[a-zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšž'\-, ]*$/i",
            'SurgeryCounty'   => 'nullable|sometimes|min:2|max:45|regex:' .
                "/^[a-zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšž'\-, ]*$/i",
            'SurgeryPostcode' => 'required|regex:/^([A-Za-z]{1,2}[0-9]{1,2}[A-Za-z]?[ ]?)([0-9]{1}[A-Za-z]{2})$/',
            'SurgeryFax'      => 'nullable|sometimes|regex:/^[+0?][0-9]{6,20}$/',
        ];

        return [
            'PatientId'       => 'required|numeric',
            'NHSNumber'       => 'required',
            'PatientDOB'      => 'required|date_format:d/m/Y',
            'PatientForename' => 'required|min:2|max:32',
            'PatientSurname'  => 'required|min:2|max:32',
            'PatientAddress1' => 'required|min:2|max:32',
            'PatientAddress2' => 'nullable|sometimes|min:2|max:32',
            'PatientAddress3' => 'required|min:2|max:45',
            'PatientAddress4' => 'nullable|sometimes|min:2|max:45',
            'PatientAddress5' => 'required',
            'SurgeryName'     => 'required|min:2|max:32',
            'SurgeryAddress1' => 'required|min:2|max:32',
            'SurgeryAddress2' => 'nullable|sometimes|min:2|max:32',
            'SurgeryCity'     => 'required|min:2|max:45',
            'SurgeryCounty'   => 'nullable|sometimes|min:2|max:45',
            'SurgeryPostcode' => 'required',
            'SurgeryFax'      => 'nullable|sometimes',
        ];
    }


    public function messages()
    {
        return [
            'NHSNumber.required'       => 'The NHS number is required',
            'PatientDOB.required'      => 'The patient date of birth is required',
            'GPfirstname.required'     => 'The GP first name is required',
            'GPlastname.required'      => 'The GP last name is required',
            'PatientAddress1.required' => 'The patient address line 1 is required',
            'PatientAddress3.required' => 'The patient town/city is required',
            'PatientAddress5.required' => 'The patient postcode is required',
            'PatientAddress1.regex'    => 'The patient address line 1 is not a valid format',
            'PatientAddress2.regex'    => 'The patient address line 2 is not a valid format',
            'PatientAddress3.regex'    => 'The patient town/city is not a valid format',
            'PatientAddress4.regex'    => 'The patient county is not a valid format',
            'PatientAddress5.regex'    => 'The patient postcode is not a valid format',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
