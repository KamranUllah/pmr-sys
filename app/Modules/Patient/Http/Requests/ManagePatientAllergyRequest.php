<?php

namespace App\Modules\Patient\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManagePatientAllergyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'PatientId'          => 'required',
            'AllergyName'        => 'required|max:30',
            'AllergyDescription' => 'nullable|sometimes|max:200',
            'Certainty'          => 'nullable|sometimes|max:30',
            'Severity'           => 'nullable|sometimes|max:15',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
