<?php

namespace App\Modules\Patient\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientDetailsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'PatientTitle'         => "required|min:2|max:4|in:Mr,Mrs,Miss,Ms,Dr",
            'PatientForename'      => "required|min:2|max:32|"
                . "regex:/^[a-zA-Zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'PatientSurname'       => "required|min:2|max:32|"
                . "regex:/^[a-zA-Zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'PatientSex'           => "required|numeric",
            'LanguagePreferenceId' => "required|numeric",
            'NHSNumber'            => "required",
            'PatientDOB'           => "required|date_format:d/m/Y",
            'PatientAddress1'      => "required|min:2|max:32|"
                . "regex:/^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'PatientAddress2'      => "nullable|sometimes|min:2|max:32|"
                . "regex:/^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'PatientAddress3'      => 'required|min:2|max:45|regex:' .
                "/^[a-zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšž'\-, ]*$/i",
            'PatientAddress4'      => 'nullable|sometimes|min:2|max:45|regex:' .
                "/^[a-zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšž'\-, ]*$/i",
            'PatientAddress5'      => 'required|regex:/^([A-Za-z]{1,2}[0-9]{1,2}[A-Za-z]?[ ]?)([0-9]{1}[A-Za-z]{2})$/',
            'PatientMobile'        => 'nullable|sometimes|regex:/^[+0?][0-9]{6,20}$/',
            'AlternateAddress1'    => "nullable|sometimes|min:2|max:32|"
                . "regex:/^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'AlternateAddress2'    => "nullable|sometimes|min:2|max:32|"
                . "regex:/^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð \'-]+$/u",
            'AlternateAddress3'    => 'nullable|sometimes|min:2|max:45|regex:' .
                "/^[a-zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšž'\-, ]*$/i",
            'AlternateAddress4'    => 'nullable|sometimes|min:2|max:45|regex:' .
                "/^[a-zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšž'\-, ]*$/i",
            'AlternateAddress5'    => 'nullable|sometimes|regex:/^([A-Za-z]{1,2}[0-9]{1,2}[A-Za-z]?[ ]?)([0-9]{1}[A-Za-z]{2})$/',
            'Consent'              => 'nullable|sometimes|max:255',
        ];

        return [
            'PatientTitle'         => 'required|min:2|max:4|in:Mr,Mrs,Miss,Ms,Dr',
            'PatientForename'      => 'required|min:2|max:32',
            'PatientSurname'       => 'required|min:2|max:32',
            'PatientSex'           => 'required|numeric',
            'LanguagePreferenceId' => 'required|numeric',
            'NHSNumber'            => 'required',
            'PatientDOB'           => 'required|date_format:d/m/Y',
            'PatientAddress1'      => 'required|min:2|max:32|',
            'PatientAddress2'      => 'nullable|sometimes|min:2|max:32',
            'PatientAddress3'      => 'required|min:2|max:45',
            'PatientAddress4'      => 'nullable|sometimes|min:2|max:45',
            'PatientAddress5'      => 'required',
            'PatientMobile'        => 'nullable',
            'AlternateAddress1'    => 'nullable|sometimes|min:2|max:32',
            'AlternateAddress2'    => 'nullable|sometimes|min:2|max:32',
            'AlternateAddress3'    => 'nullable|sometimes|min:2|max:45',
            'AlternateAddress4'    => 'nullable|sometimes|min:2|max:45',
            'AlternateAddress5'    => 'nullable|sometimes',
            'Consent'              => 'nullable|sometimes|max:255',
        ];
    }

    public function messages()
    {
        return [
            'NHSNumber.required'       => 'The NHS number is required',
            'PatientDOB.required'      => 'The patient date of birth is required',
            'GPfirstname.required'     => 'The GP first name is required',
            'GPlastname.required'      => 'The GP last name is required',
            'PatientAddress1.required' => 'The patient address line 1 is required',
            'PatientAddress3.required' => 'The patient town/city is required',
            'PatientAddress5.required' => 'The patient postcode is required',
            'PatientAddress1.regex'    => 'The patient address line 1 is not a valid format',
            'PatientAddress2.regex'    => 'The patient address line 2 is not a valid format',
            'PatientAddress3.regex'    => 'The patient town/city is not a valid format',
            'PatientAddress4.regex'    => 'The patient county is not a valid format',
            'PatientAddress5.regex'    => 'The patient postcode is not a valid format',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
