<?php

namespace App\Modules\Patient\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManagePatientMedicationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'MedicationId'              => 'nullable|sometimes|numeric',
            'PatientId'                 => 'required|numeric',
            'Name'                      => 'required|max:200',
            'Dosage'                    => 'required|max:500',
            'PacketSize'                => 'required|numeric',
            'Enabled'                   => 'required|numeric'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
