<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\Modules\Patient\Http\Controllers'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::group(['prefix' => 'patient'], function () {

            Route::get('/', [
                'as'   => 'admin.patients.index',
                'uses' => 'Admin\PatientController@index',
            ]);

            Route::get('notimported', [
                'as'   => 'admin.patients.view.not.imported',
                'uses' => 'Admin\PatientController@getnotImportedPatients',
            ]);

            Route::get('notimported-get', [
                'as'   => 'prescription.get.not.imported',
                'uses' => 'Admin\PatientController@getnotImportedPatientsData',
            ]);

            Route::get('view/{patientId}', [
                'as'   => 'admin.patients.show',
                'uses' => 'Admin\PatientController@show',
            ]);

            Route::get('edit/{patientId}', [
                'as'   => 'admin.patients.edit',
                'uses' => 'Admin\PatientController@edit',
            ]);

            Route::post('edit/nhs-number/{patientId}', [
                'as'   => 'admin.patients.update.nhsnumber',
                'uses' => 'Admin\PatientController@updateNHSNumber',
            ]);

            Route::post('edit/status/{patientId}', [
                'as'   => 'admin.patients.update.status',
                'uses' => 'Admin\PatientController@updateStatus',
            ]);


            Route::get('add', [
                'as'   => 'admin.patients.add',
                'uses' => 'Admin\PatientController@create',
            ]);

            Route::post('add', [
                'as'   => 'admin.patients.store',
                'uses' => 'Admin\PatientController@store',
            ]);

            Route::post('view/{patientId}/update', [
                'as'   => 'admin.patients.update',
                'uses' => 'Admin\PatientController@update',
            ]);

            Route::post('fetch-patients', [
                'as'   => 'admin.patients.data',
                'uses' => 'Admin\PatientController@getPatientsData',
            ]);

            Route::post('mark-data-import-complete', [
                'as'   => 'mark.data.import.as.complete',
                'uses' => 'Admin\PatientController@markPatientDataImportAsComplete',
            ]);

            Route::get('notes/view/{patientId}', [
                'as'   => 'admin.patients.notes.view',
                'uses' => 'Admin\PatientController@getClinicalNotes',
            ]);


            Route::post('update/surgeryfax/{patientId}', [
                'as'   => 'admin.patients.update.surgeryfax',
                'uses' => 'Admin\PatientController@updatePatientSurgeryFax',
            ]);


            /**
             * Manage Patient Clinical Notes Routes
             */
            Route::group(['prefix' => 'clinical-notes'], function () {
                Route::get('add', [
                    'as'   => 'admin.patients.clinicalnotes.add',
                    'uses' => 'Admin\ClinicalNotesController@create',
                ]);

                Route::post('add', [
                    'as'   => 'admin.patients.clinicalnotes.store',
                    'uses' => 'Admin\ClinicalNotesController@store',
                ]);

                Route::get('edit/{clinicalNoteId}', [
                    'as'   => 'admin.patients.clinicalnotes.edit',
                    'uses' => 'Admin\ClinicalNotesController@edit',
                ]);

                Route::post('edit/{clinicalNoteId}', [
                    'as'   => 'admin.patients.clinicalnotes.update',
                    'uses' => 'Admin\ClinicalNotesController@update',
                ]);

                Route::get('delete/{clinicalNoteId}', [
                    'as'   => 'admin.patients.clinicalnotes.delete',
                    'uses' => 'Admin\ClinicalNotesController@destroy',
                ]);
//
//                Route::get('fetch/{patientId}/data', [
//                    'as'   => 'admin.patients.allergy.data',
//                    'uses' => 'Admin\AllergyController@getPatientAllergiesData',
//                ]);
            });


            /**
             * GP Details Update
             */
            Route::group(['prefix' => 'gp-details'], function () {
                Route::post('edit/{medicationId}', [
                    'as'   => 'admin.patients.gpdetails.update',
                    'uses' => 'Admin\GPDetailController@update',
                ]);
            });

            /**
             * Manage Patient Allergies Routes
             */
            Route::group(['prefix' => 'allergies'], function () {
                Route::get('add', [
                    'as'   => 'admin.patients.allergy.add',
                    'uses' => 'Admin\AllergyController@create',
                ]);

                Route::post('add', [
                    'as'   => 'admin.patients.allergy.store',
                    'uses' => 'Admin\AllergyController@store',
                ]);

                Route::get('edit/{allergyId}', [
                    'as'   => 'admin.patients.allergy.edit',
                    'uses' => 'Admin\AllergyController@edit',
                ]);

                Route::post('edit/{allergyId}', [
                    'as'   => 'admin.patients.allergy.update',
                    'uses' => 'Admin\AllergyController@update',
                ]);

                Route::get('delete/{allergyId}', [
                    'as'   => 'admin.patients.allergy.delete',
                    'uses' => 'Admin\AllergyController@destroy',
                ]);

                Route::get('fetch/{patientId}/data', [
                    'as'   => 'admin.patients.allergy.data',
                    'uses' => 'Admin\AllergyController@getPatientAllergiesData',
                ]);
            });

            /**
             * Manage Patient acute medication Routes
             */
            Route::group(['prefix' => 'acute-medication'], function () {
                Route::get('add', [
                    'as'   => 'admin.patients.acutemedication.add',
                    'uses' => 'Admin\AcuteMedicationController@create',
                ]);

                Route::post('add', [
                    'as'   => 'admin.patients.acutemedication.store',
                    'uses' => 'Admin\AcuteMedicationController@store',
                ]);

                Route::get('edit/{medicationId}', [
                    'as'   => 'admin.patients.acutemedication.edit',
                    'uses' => 'Admin\AcuteMedicationController@edit',
                ]);

                Route::post('edit/{medicationId}', [
                    'as'   => 'admin.patients.acutemedication.update',
                    'uses' => 'Admin\AcuteMedicationController@update',
                ]);

                Route::get('delete/{medicationId}', [
                    'as'   => 'admin.patients.acutemedication.delete',
                    'uses' => 'Admin\AcuteMedicationController@destroy',
                ]);

                Route::get('fetch/{patientId}/data', [
                    'as'   => 'admin.patients.acutemedication.data',
                    'uses' => 'Admin\AcuteMedicationController@getPatientMedicationData',
                ]);
            });

            /**
             * Manage Patient current medication Routes
             */
            Route::group(['prefix' => 'current-medication'], function () {
                Route::get('add', [
                    'as'   => 'admin.patients.currentmedication.add',
                    'uses' => 'Admin\CurrentMedicationController@create',
                ]);

                Route::post('add', [
                    'as'   => 'admin.patients.currentmedication.store',
                    'uses' => 'Admin\CurrentMedicationController@store',
                ]);

                Route::get('edit/{medicationId}', [
                    'as'   => 'admin.patients.currentmedication.edit',
                    'uses' => 'Admin\CurrentMedicationController@edit',
                ]);

                Route::post('edit/{medicationId}', [
                    'as'   => 'admin.patients.currentmedication.update',
                    'uses' => 'Admin\CurrentMedicationController@update',
                ]);

                Route::get('delete/{medicationId}', [
                    'as'   => 'admin.patients.currentmedication.delete',
                    'uses' => 'Admin\CurrentMedicationController@destroy',
                ]);

                Route::get('fetch/{patientId}/data', [
                    'as'   => 'admin.patients.currentmedication.data',
                    'uses' => 'Admin\CurrentMedicationController@getPatientMedicationData',
                ]);
            });


            /**
             * Manage Patient discontinued medication Routes
             */
            Route::group(['prefix' => 'discontinued-medication'], function () {
                Route::get('add', [
                    'as'   => 'admin.patients.discontinuedmedication.add',
                    'uses' => 'Admin\DiscontinuedMedicationController@create',
                ]);

                Route::post('add', [
                    'as'   => 'admin.patients.discontinuedmedication.store',
                    'uses' => 'Admin\DiscontinuedMedicationController@store',
                ]);

                Route::get('edit/{medicationId}', [
                    'as'   => 'admin.patients.discontinuedmedication.edit',
                    'uses' => 'Admin\DiscontinuedMedicationController@edit',
                ]);

                Route::post('edit/{medicationId}', [
                    'as'   => 'admin.patients.discontinuedmedication.update',
                    'uses' => 'Admin\DiscontinuedMedicationController@update',
                ]);

                Route::get('delete/{medicationId}', [
                    'as'   => 'admin.patients.discontinuedmedication.delete',
                    'uses' => 'Admin\DiscontinuedMedicationController@destroy',
                ]);

                Route::get('fetch/{patientId}/data', [
                    'as'   => 'admin.patients.discontinuedmedication.data',
                    'uses' => 'Admin\DiscontinuedMedicationController@getPatientMedicationData',
                ]);
            });

            /**
             * Manage Patient Next Of Kin Routes
             */
            Route::group(['prefix' => 'next-of-kin'], function () {
                Route::get('add', [
                    'as'   => 'admin.patients.nextofkin.add',
                    'uses' => 'Admin\NextOfKinController@create',
                ]);

                Route::post('add', [
                    'as'   => 'admin.patients.nextofkin.store',
                    'uses' => 'Admin\NextOfKinController@store',
                ]);

                Route::get('edit/{medicationId}', [
                    'as'   => 'admin.patients.nextofkin.edit',
                    'uses' => 'Admin\NextOfKinController@edit',
                ]);

                Route::post('edit/{medicationId}', [
                    'as'   => 'admin.patients.nextofkin.update',
                    'uses' => 'Admin\NextOfKinController@update',
                ]);

                Route::get('delete/{medicationId}', [
                    'as'   => 'admin.patients.nextofkin.delete',
                    'uses' => 'Admin\NextOfKinController@destroy',
                ]);

                Route::get('fetch/{patientId}/data', [
                    'as'   => 'admin.patients.nextofkin.data',
                    'uses' => 'Admin\NextOfKinController@getPatientNextOfKinData',
                ]);
            });


            //admin prescription
            Route::group(['prefix' => 'prescription'], function () {
                //patients admin repeat-presciption
                Route::group(['prefix' => 'repeat'], function () {

                    Route::get('view/{patientId}', [
                        'as'   => 'admin.patients.prescription.repeat.view',
                        'uses' => 'Admin\PatientController@getPatientRepeatPrescriptions',
                    ]);

                    Route::get('eps/{patientId}', [
                        'as'   => 'admin.patients.prescription.patient.eps',
                        'uses' => 'Admin\PatientController@getPatientEPSStatus',
                    ]);
                });

            });

        });
    });
});
