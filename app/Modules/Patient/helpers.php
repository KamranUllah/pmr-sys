<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 29/08/2017
 * Time: 09:47
 */

if (!function_exists('getTotalPatientCount')) {
    function getTotalPatientCount() {
        return App\Modules\Patient\Entities\Patient::all()->count();
    }
}