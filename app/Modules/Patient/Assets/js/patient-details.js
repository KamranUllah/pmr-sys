
document.addEventListener('DOMContentLoaded', function () {

    // viewPatientNotesImported();
    // manageNHSNumber();

    /*new Vue({
        el: '#app',
        data: function () {
            return {
                patientId: NowGP.PatientId,
                isMarking: false,
                isDataImported: false
            }
        },
        methods: {
            markPatientDataImportAsComplete: function () {
                var self = this;

                swal({
                    title: 'Are you sure?',
                    text: "This will mark the patient's data import as completed",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No'
                }).then(function () {
                    self.isMarking = true;
                    axios.post(laroute.route('mark.data.import.as.complete'), {patientId: self.patientId})
                        .then(function (response) {
                            self.isMarking = false;

                            if (typeof response.data.success !== 'undefined' && response.data.success) {
                                self.isDataImported = true;
                                swal({
                                    title: 'Success',
                                    text: "Patient data has been successfully imported",
                                    type: 'success'
                                }).then(function () {
                                    showLoader();
                                    //console.log('he;l;');
                                    window.location.reload();
                                }, function (dismiss) {
                                    if (dismiss === 'cancel') {
                                        // do nothing
                                    }
                                });
                            } else {
                                swal('Error', 'There was a problem importing this patient\'s data. <p>Please try again</p>', 'error');
                            }
                        })
                        .catch(function (error) {
                            self.isMarking = false;
                        })
                }, function (dismiss) {
                    if (dismiss === 'cancel') {
                        // do nothing
                    }
                });


            }
        }
    });*/

    var importTable = $('#not-imported-patients-table').DataTable({
        "searching": {"regex": true},
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        "pageLength": 50,
        //"serverSide": true,
        "processing": true,
        "destroy": true,
        "order": [[ 0, 'desc' ]],
        "language": {
            "emptyTable": "No records match the criteria entered. Please change the criteria and try again",
            "zeroRecords": "No records match the criteria entered. Please change the criteria and try again"
        },
        "ajax": {
            url: NowGP.siteUrl + '/admin/patient/notimported-get',
            dataSrc: 'data',
        },
        "dom": '<"top"ifl>rt<"bottom"flp><"clear">',
        "columns": [
            {data: 'PatientId', name: 'patients.PatientId', orderable: true},
            {data: 'PatientForename', name: 'PatientForename', orderable: true},
            {data: 'PatientSurname', name: 'PatientSurname', orderable: true},
            {data: 'PatientDOB', name: 'PatientDOB', orderable: true},
            {data: 'PatientAddress5', name: 'PatientAddress5', orderable: true},
            {data: 'NHSNumber', name: 'NHSNumber', orderable: true},
            {data: 'DateAdded', name: 'DateAdded', orderable: true},
            {data: 'exemptions.IsExempt', name: 'Exempt'},
            {data: 'exemptions.Reason', name: 'ExemptionReason'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        drawCallback: function( settings ) {
            viewPatientNotesImported();
            manageNHSNumber();
            markAsDisabled();
        }
    });


});

function viewPatientNotesImported(){
    var viewPatientNotesModal = $('#viewPatientNotesModal');
    var viewPatientNotesBtn = $('a.view-note');
    $('#not-imported-patients-table').on('click','a.view-note', function (e) {


        e.preventDefault();

        var patientId = viewPatientNotesBtn.prevObject["0"].activeElement.attributes[2].value;
        $('.add-clinical-notes').attr('data-patientid',patientId);
        console.log("Patient ID:",patientId);

        viewPatientNotesModal.find('.add-clinical-notes').show();
        viewPatientNotesModal.find('.save-clinical-notes').hide();
        viewPatientNotesModal.find('.update-clinical-notes').hide();

        // Set Modal Options
        viewPatientNotesModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        fetchPatientNotes(patientId);
    });

}


function manageNHSNumber(){

    $('#not-imported-patients-table').on('click','a.nhsManage', function (e) {


        e.preventDefault();
        var manageNHSNumberBtn = $(this);
        var patientId = manageNHSNumberBtn.data('patientid');
        var patientNHSNumber = manageNHSNumberBtn.data('nhsnumber');
        console.log("Element:",manageNHSNumberBtn);
        console.log("patientNHSNumber:",patientNHSNumber);
        console.log("You just clicked:",patientId);


        swal({
            html: 'Manage Patient '+patientId+ ' NHS Number',
            input: 'text',
            inputValue: patientNHSNumber,
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            preConfirm: (NHSNumber) => {
                return new Promise((resolve) => {

                    if (NHSNumber.length < 1) {
                        swal.showValidationError(
                            'The field cannot be empty '
                        );
                        resolve()
                    }else{
                        axios.post(laroute.route('admin.patients.update.nhsnumber', { patientId: patientId}), { NHSNumber: NHSNumber})
                            .then(function (response) {

                                if(typeof response.data === 'string' && response.data.indexOf("Access denied") > 0){
                                    swal.showValidationError(
                                        "You don't have permission to this functionality."
                                    );
                                } else if (typeof response.data.error !== "undefined") {
                                    swal.showValidationError(
                                        'An error occurred while updating patient '+patientId+ '  NHS Number'
                                    );
                                }else{
                                    manageNHSNumberBtn.data('nhsnumber',NHSNumber);
                                    manageNHSNumberBtn.attr('data-nhsnumber',NHSNumber);
                                    manageNHSNumberBtn.text(NHSNumber);
                                }
                                resolve()
                            })
                            .catch(function (error) {
                                swal.showValidationError(
                                    'An error occurred while updating patient '+patientId+ '  NHS Number'
                                );
                                resolve()
                            })
                    }
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            if (result.value) {
                swal({
                    type: 'success',
                    title: 'NHS Number successfully updated!',
                    html: 'Submitted NHS Number: ' + result.value
                })
            }
        })
    });
}


function markAsDisabled(){

    $('#not-imported-patients-table').on('click','a.disable-patient', function (e) {


        e.preventDefault();
        var disablePatientBtn = $(this);
        var patientId = disablePatientBtn.data('patientId');
        console.log("You just clicked:",patientId);

        swal({
            title: 'Manage Patient '+patientId,
            html: 'Do you want to mark this patient as disabled? (Please remember to leave a note first)',
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                showLoader();
                axios.post(laroute.route('admin.patients.update.status', { patientId: patientId}), { active: 0})
                    .then(function (response) {
                        hideLoader();
                        if(typeof response.data === 'string' && response.data.indexOf("Access denied") > 0){
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'You don\'t have permission to this functionality.',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                        } else if (typeof response.data.error !== "undefined") {
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'Failed to mark the patient as disabled',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                        } else if (response.status === 200) {
                            new Noty({
                                type: 'success',
                                theme: 'bootstrap-v3',
                                text: 'The patient has been marked as disabled',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                            var importTable = $('#not-imported-patients-table').DataTable().ajax.reload();
                        } else {
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'Failed to mark the patient as disabled',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();                                    }
                    })
                    .catch(function (error) {
                    })
            }
        });
    });
}




$(function () {
    $('.mark-import-complete').off().on('click', function () {
        swal({
            title: 'Are you sure?',
            text: "This will mark the patient's data import as completed",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then(function () {

            var formData = {
                PatientId: NowGP.PatientId,
                PatientForename: $('#PatientForename').val().trim(),
                PatientSurname: $('#PatientSurname').val().trim(),
                NHSNumber: $('#NHSNumber').val().trim(),
                PatientDOB: $('#PatientDOB').val().trim(),
                PatientAddress1: $('#PatientAddress1').val().trim(),
                PatientAddress2: $('#PatientAddress2').val().trim(),
                PatientAddress3: $('#PatientAddress3').val().trim(),
                PatientAddress4: $('#PatientAddress4').val().trim(),
                PatientAddress5: $('#PatientAddress5').val().trim(),

                // GP Details
                GPfirstname: $('#GPfirstname').val(),
                GPlastname: $('#GPlastname').val().trim(),
                SurgeryName: $('#SurgeryName').val().trim(),
                SurgeryAddress1: $('#SurgeryAddress1').val().trim(),
                SurgeryAddress2: $('#SurgeryAddress2').val().trim(),
                SurgeryCounty: $('#SurgeryCounty').val().trim(),
                SurgeryCity: $('#SurgeryCity').val().trim(),
                SurgeryPostcode: $('#SurgeryPostcode').val().trim(),
                SurgeryFax: $('#SurgeryFax').val().trim()
            };

            resetModalFormErrors();
            showLoader();
            $('.processing-spinner').removeClass('hidden');
            $('.mark-import-complete').addClass('hidden');
            $('.data-import-errors').addClass('hidden');
            $('.patient-details-error-message').html('');
            axios.post(laroute.route('mark.data.import.as.complete'), formData)
                .then(function (response) {
                    hideLoader();
                    console.log(response);
                    $('.processing-spinner').addClass('hidden');
                    $('.mark-import-complete').removeClass('hidden');

                    if (typeof response.data.success !== 'undefined' && response.data.success) {
                        //self.isDataImported = true;
                        swal({
                            title: 'Success',
                            text: "Patient data has been successfully imported",
                            type: 'success'
                        }).then(function () {
                            showLoader();
                            //console.log('he;l;');
                            window.location.reload();
                        }, function (dismiss) {
                            if (dismiss === 'cancel') {
                                // do nothing
                            }
                        });
                    } else {
                        hideLoader();
                        $('.processing-spinner').addClass('hidden');
                        $('.mark-import-complete').removeClass('hidden');
                        swal('Error', 'There was a problem importing this patient\'s data. <p>Please try again</p>', 'error');
                    }
                })
                .catch(function (error) {
                    hideLoader();
                    console.log('Error: ', error.response.status);
                    var textErrorMessages;
                    if (error.response.status === 422) {
                        var errors = error.response.data.errors;

                        //console.log(errors);
                        textErrorMessages = '<strong>Error!</strong> Unable to complete data import for patient.  ' +
                            'Please fill out the required fields, and attempt the data import again.';
                        textErrorMessages += '<p>&nbsp;</p><p><ul>';

                        // Iterate through errors object.
                        $.each(errors, function (field, message) {
                            //console.error(field + ': ' + message);
                            //handle arrays
                            if (field.indexOf('.') !== -1) {
                                field = field.replace('.', '[');
                                //handle multi dimensional array
                                for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                    field = field.replace('.', '][');
                                }
                                field = field + "]";
                            }
                            var formGroup = $('[name=' + field + ']').closest('.form-group');
                            formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');

                            textErrorMessages += '<li>' + message + '</li>';
                        });

                        textErrorMessages += '</ul></p>';


                        $('.data-import-errors').removeClass('hidden');
                        $('a > .data-import-errors').parent().addClass('tab-error');
                        $('.patient-details-error-message').html(textErrorMessages);


                        console.log(textErrorMessages);
                    }
                    $('.processing-spinner').addClass('hidden');
                    $('.mark-import-complete').removeClass('hidden');
                })
        }, function (dismiss) {
            if (dismiss === 'cancel') {
                // do nothing
            }
        });
    })

});

function resetModalFormErrors() {
    $('.form-group').removeClass('has-error');
    $('.form-group').find('.help-block').remove();
}