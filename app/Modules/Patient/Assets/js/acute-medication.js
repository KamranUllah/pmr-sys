var patientsAcuteMedicationTable;

$(function () {
    /**
     * Medication Function Calls
     */
    addAcuteMedication();

    //console.log(NowGP.PatientId);

    //patientMedicationTableSearch(NowGP.PatientId);

    patientsAcuteMedicationTable = patientAcuteMedicationTableSearch(NowGP.PatientId);
});


/**
 *
 * @param PatientId
 * @returns {jQuery}
 */
function patientAcuteMedicationTableSearch(PatientId) {
    return $('#patientAcuteMedicationTable').DataTable({
        processing: true,
        destroy: true,
        serverSide: true,
        searching: false,
        lengthChange: false,
        "language": {
            "emptyTable": "No records has been found in the database."
        },
        ajax: NowGP.siteUrl + '/admin/patient/acute-medication/fetch/' + PatientId + '/data',
        columns: [
            {data: 'DatePrescribedOriginally', name: 'DatePrescribedOriginally'},
            {data: 'MedicationId', name: 'MedicationId'},
            {data: 'Name', name: 'Name'},
            {data: 'Dosage', name: 'Dosage'},
            {data: 'PacketSize', name: 'PacketSize'},
            {data: 'Enabled', name: 'Enabled'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "initComplete": function (settings, json) {
            editAcuteMedication();
            deleteAcuteMedication();
        },
        'drawCallback': function () {
            editAcuteMedication();
            deleteAcuteMedication();
        }
    });
}

/**
 * Add Patient Medication
 */
function addAcuteMedication() {

    var manageAcuteMedicationModal = $('#manageAcuteMedicationModal');
    var manageMedicationBtn = $('.add-acute-medication');

    manageMedicationBtn.off().on('click', function (e) {
        e.preventDefault();

        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageAcuteMedicationModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/acute-medication/add')
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageAcuteMedicationModal.find('.modal-title').html('Add Acute Medication');
                manageAcuteMedicationModal.find('.modal-body').html(response.data);
                manageAcuteMedicationModal.modal('show');
                manageAcuteMedicationModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId
                    };

                    $('#DateLastPrescribed').daterangepicker({
                        locale: {
                            format: 'DD/MM/YYYY HH:mm',
                            applyLabel: 'Apply',
                            cancelLabel: 'Cancel',
                        },
                        timePicker: true,
                        singleDatePicker: true,
                        showDropdowns: true,
                        autoUpdateInput: false,
                        startDate: moment(),
                        minDate: moment().subtract('100', 'year'),
                        maxDate: moment()
                    });

                    $('#DateLastPrescribed').on('apply.daterangepicker', function(ev, picker) {
                        $(this).val(picker.startDate.format('DD/MM/YYYY HH:mm'));
                    });

                    $('#DateLastPrescribed').on('cancel.daterangepicker', function(ev, picker) {
                        $(this).val('');
                    });

                    saveAcuteMedication(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });

}


/**
 * Edit Patient Medication
 */
function editAcuteMedication() {
    var manageAcuteMedicationModal = $('#manageAcuteMedicationModal');
    var manageMedicationBtn = $('.edit-acute-medication');


    manageMedicationBtn.off().on('click', function (e) {
        e.preventDefault();

        var medicationId = $(this).data('medication-id');
        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageAcuteMedicationModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/acute-medication/edit/' + medicationId)
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageAcuteMedicationModal.find('.modal-title').html('Edit Acute Medication');
                manageAcuteMedicationModal.find('.modal-body').html(response.data);
                manageAcuteMedicationModal.modal('show');
                manageAcuteMedicationModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId,
                        MedicationId: medicationId
                    };

                    $('#DateLastPrescribed').daterangepicker({
                        locale: {
                            format: 'DD/MM/YYYY HH:mm',
                            applyLabel: 'Apply',
                            cancelLabel: 'Cancel',
                        },
                        timePicker: true,
                        singleDatePicker: true,
                        showDropdowns: true,
                        autoUpdateInput: false,
                        startDate: moment(),
                        minDate: moment().subtract('100', 'year'),
                        maxDate: moment()
                    });

                    $('#DateLastPrescribed').on('apply.daterangepicker', function(ev, picker) {
                        $(this).val(picker.startDate.format('DD/MM/YYYY HH:mm'));
                    });

                    $('#DateLastPrescribed').on('cancel.daterangepicker', function(ev, picker) {
                        $(this).val('');
                    });

                    updateAcuteMedication(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });
}

/**
 * Delete Patient Medication
 */
function deleteAcuteMedication() {
    var deleteAcuteMedication = $('.delete-acute-medication');


    deleteAcuteMedication.off().on('click', function (e) {
        e.preventDefault();

        var medicationId = $(this).data('medication-id');

        swal({
            title: 'Are you sure?',
            text: "Please confirm you wish to delete this medication",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function (result) {
            if(result.value){
                showLoader();
                axios.get(NowGP.siteUrl + '/admin/patient/acute-medication/delete/' + medicationId)
                    .then(function (response) {
                        patientsAcuteMedicationTable.draw();
                        hideLoader();
                    })
                    .catch(function (error) {
                        hideLoader();
                        console.log(error);
                    });
            }
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {

            }
        })
    });
}

/**
 *
 * @param PatientData
 */
function saveAcuteMedication(PatientData) {

    var manageAcuteMedicationModal = $('#manageAcuteMedicationModal');
    var saveAcuteMedicationBtn = $('#saveAcuteMedication');

    console.log(PatientData);
    console.log(saveAcuteMedicationBtn);

    saveAcuteMedicationBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageAcuteMedicationForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        var data = {
                PatientId: PatientData.PatientId,
                Name: $('#AcuteName').val().trim(),
                Dosage: $('#AcuteDosage').val().trim(),
                PacketSize: $('#AcutePacketSize').val().trim(),
                Enabled: $('#AcuteEnabled').val().trim(),
                DateLastPrescribed: $('#DateLastPrescribed').val().trim(),
                Acute: 1
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }


                //manageAcuteMedicationModal.modal('hide');

                new Noty({
                    type: 'success',
                    theme: 'bootstrap-v3',
                    text: 'The medication <strong>' + data.Name + '</strong> has been added for patient <strong>#' + data.PatientId + '</strong>',
                    timeout: 4500,
                    progressBar: true,
                    closeWith: ['click', 'button']
                }).show();

                resetAcuteMedicationForm();
                patientsAcuteMedicationTable.draw();
                hideLoader();

            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                if (error.response.status) {
                    var errors = error.response.data.errors;

                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })
    })


}

/**
 * Update patient Medication
 * @param PatientData
 */
function updateAcuteMedication(PatientData) {

    var manageAcuteMedicationModal = $('#manageAcuteMedicationModal');
    var saveAcuteMedicationBtn = $('#saveAcuteMedication');

    console.log(PatientData);
    console.log(saveAcuteMedicationBtn);

    saveAcuteMedicationBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageAcuteMedicationForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        var data = {
                PatientId: PatientData.PatientId,
                MedicationId: PatientData.MedicationId,
                Name: $('#AcuteName').val().trim(),
                Dosage: $('#AcuteDosage').val().trim(),
                PacketSize: $('#AcutePacketSize').val().trim(),
                Enabled: $('#AcuteEnabled').val().trim(),
                DateLastPrescribed: $('#DateLastPrescribed').val().trim(),
                Acute: 1
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        resetModalFormErrors();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                manageAcuteMedicationModal.modal('hide');

                console.log(patientsAcuteMedicationTable);
                patientsAcuteMedicationTable.draw();
                hideLoader();


            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                console.log(error);
                if (typeof error.response !== 'undefined') {
                    if (error.response.status) {
                        var errors = error.response.data.errors;

                        // Iterate through errors object.
                        $.each(errors, function (field, message) {
                            //console.error(field + ': ' + message);
                            //handle arrays
                            if (field.indexOf('.') != -1) {
                                field = field.replace('.', '[');
                                //handle multi dimensional array
                                for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                    field = field.replace('.', '][');
                                }
                                field = field + "]";
                            }
                            var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                            formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                        });

                        // Reset submit.
                        if (submit.is('button')) {
                            submit.html(submitOriginal).prop('disabled', false);
                        } else if (submit.is('input')) {
                            submit.val(submitOriginal).prop('disabled', false);
                        }
                    }
                }
            })
    })
}

function resetAcuteMedicationForm() {
    $('#manageAcuteMedicationForm').trigger('reset');
}


function resetModalFormErrors() {
    $('.form-group').removeClass('has-error');
    $('.form-group').find('.help-block').remove();
}

