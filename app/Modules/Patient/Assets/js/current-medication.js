var patientsCurrentMedicationTable;

$(function () {
    /**
     * Medication Function Calls
     */
    addCurrentMedication();

    //console.log(NowGP.PatientId);

    //patientCurrentMedicationTableSearch(NowGP.PatientId);

    patientsCurrentMedicationTable = patientCurrentMedicationTableSearch(NowGP.PatientId);
});


/**
 *
 * @param PatientId
 * @returns {jQuery}
 */
function patientCurrentMedicationTableSearch(PatientId) {
    return $('#patientCurrentMedicationTable').DataTable({
        processing: true,
        destroy: true,
        serverSide: true,
        searching: false,
        lengthChange: false,
        "language": {
            "emptyTable": "No records has been found in the database."
        },
        ajax: NowGP.siteUrl + '/admin/patient/current-medication/fetch/' + PatientId + '/data',
        columns: [
            {data: 'DatePrescribedOriginally', name: 'DatePrescribedOriginally'},
            {data: 'MedicationId', name: 'MedicationId'},
            {data: 'Name', name: 'Name'},
            {data: 'Dosage', name: 'Dosage'},
            {data: 'PacketSize', name: 'PacketSize'},
            {data: 'Enabled', name: 'Enabled'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "initComplete": function (settings, json) {
            editCurrentMedication();
            deleteCurrentMedication();
        },
        'drawCallback': function () {
            editCurrentMedication();
            deleteCurrentMedication();
        }
    });
}

/**
 * Add Patient Medication
 */
function addCurrentMedication() {

    var manageCurrentMedicationModal = $('#manageCurrentMedicationModal');
    var manageMedicationBtn = $('.add-current-medication');

    manageMedicationBtn.off().on('click', function (e) {
        e.preventDefault();

        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageCurrentMedicationModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/current-medication/add')
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageCurrentMedicationModal.find('.modal-title').html('Add Current Medication');
                manageCurrentMedicationModal.find('.modal-body').html(response.data);
                manageCurrentMedicationModal.modal('show');
                manageCurrentMedicationModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId
                    };

                    saveCurrentMedication(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });

}


/**
 * Edit Patient Medication
 */
function editCurrentMedication() {
    var manageCurrentMedicationModal = $('#manageCurrentMedicationModal');
    var manageMedicationBtn = $('.edit-current-medication');


    manageMedicationBtn.off().on('click', function (e) {
        e.preventDefault();

        var medicationId = $(this).data('medication-id');
        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageCurrentMedicationModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/current-medication/edit/' + medicationId)
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageCurrentMedicationModal.find('.modal-title').html('Edit Current Medication');
                manageCurrentMedicationModal.find('.modal-body').html(response.data);
                manageCurrentMedicationModal.modal('show');
                manageCurrentMedicationModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId,
                        MedicationId: medicationId
                    };

                    updateCurrentMedication(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });
}

/**
 * Delete Patient Medication
 */
function deleteCurrentMedication() {
    var deleteCurrentMedication = $('.delete-current-medication');


    deleteCurrentMedication.off().on('click', function (e) {
        e.preventDefault();

        var medicationId = $(this).data('medication-id');

        swal({
            title: 'Are you sure?',
            text: "Please confirm you wish to delete this medication",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function (result) {
            if(result.value){
                showLoader();
                axios.get(NowGP.siteUrl + '/admin/patient/current-medication/delete/' + medicationId)
                    .then(function (response) {
                        patientsCurrentMedicationTable.draw();
                        hideLoader();
                    })
                    .catch(function (error) {
                        hideLoader();
                        console.log(error);
                    });
            }
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {

            }
        })
    });
}

/**
 *
 * @param PatientData
 */
function saveCurrentMedication(PatientData) {

    var manageCurrentMedicationModal = $('#manageCurrentMedicationModal');
    var saveCurrentMedicationBtn = $('#saveCurrentMedication');

    console.log(PatientData);
    console.log(saveCurrentMedicationBtn);

    saveCurrentMedicationBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageCurrentMedicationForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        var data = {
                PatientId: PatientData.PatientId,
                Name: $('#CurrentName').val(),
                Dosage: $('#CurrentDosage').val(),
                PacketSize: $('#CurrentPacketSize').val(),
                Enabled: $('#CurrentEnabled').val(),
                Acute: 0
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                new Noty({
                    type: 'success',
                    theme: 'bootstrap-v3',
                    text: 'The medication <strong>' + data.Name + '</strong> has been added for patient <strong>#' + data.PatientId + '</strong>',
                    timeout: 4500,
                    progressBar: true,
                    closeWith: ['click', 'button']
                }).show();

                resetCurrentMedicationForm();
                patientsCurrentMedicationTable.draw();
                //manageCurrentMedicationModal.modal('hide');
                hideLoader();

            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                if (error.response.status) {
                    var errors = error.response.data.errors;

                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })
    })


}

/**
 * Update patient Medication
 * @param PatientData
 */
function updateCurrentMedication(PatientData) {

    var manageCurrentMedicationModal = $('#manageCurrentMedicationModal');
    var saveCurrentMedicationBtn = $('#saveCurrentMedication');

    console.log(PatientData);
    console.log(saveCurrentMedicationBtn);

    saveCurrentMedicationBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageCurrentMedicationForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        var data = {
                PatientId: PatientData.PatientId,
                MedicationId: PatientData.MedicationId,
                Name: $('#CurrentName').val(),
                Dosage: $('#CurrentDosage').val(),
                PacketSize: $('#CurrentPacketSize').val(),
                Enabled: $('#CurrentEnabled').val(),
                Acute: 0
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        resetModalFormErrors();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                manageCurrentMedicationModal.modal('hide');

                console.log(patientsCurrentMedicationTable);
                patientsCurrentMedicationTable.draw();
                hideLoader();


            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                console.log(error);
                if (typeof error.response !== 'undefined') {
                    if (error.response.status) {
                        var errors = error.response.data.errors;

                        // Iterate through errors object.
                        $.each(errors, function (field, message) {
                            //console.error(field + ': ' + message);
                            //handle arrays
                            if (field.indexOf('.') != -1) {
                                field = field.replace('.', '[');
                                //handle multi dimensional array
                                for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                    field = field.replace('.', '][');
                                }
                                field = field + "]";
                            }
                            var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                            formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                        });

                        // Reset submit.
                        if (submit.is('button')) {
                            submit.html(submitOriginal).prop('disabled', false);
                        } else if (submit.is('input')) {
                            submit.val(submitOriginal).prop('disabled', false);
                        }
                    }
                }
            })
    })
}

function resetCurrentMedicationForm() {
    $('#manageCurrentMedicationForm').trigger('reset');
}

function resetModalFormErrors() {
    $('.form-group').removeClass('has-error');
    $('.form-group').find('.help-block').remove();
}

