var patientsAllergyTable;

$(function() {
    /**
     * Allergy Function Calls
     */
    addAllergy();

    //console.log(NowGP.PatientId);

    //patientAllergyTableSearch(NowGP.PatientId);

    patientsAllergyTable = patientAllergyTableSearch(NowGP.PatientId);
});



/**
 *
 * @param PatientId
 * @returns {jQuery}
 */
function patientAllergyTableSearch(PatientId) {
    return $('#patientAllergyTable').DataTable({
        processing: true,
        destroy: true,
        serverSide: true,
        searching: false,
        lengthChange: false,
        "language": {
            "emptyTable": "No records has been found in the database."
        },
        ajax: NowGP.siteUrl + '/admin/patient/allergies/fetch/' + PatientId + '/data',
        columns: [
            {data: 'PatientAllergyId', name: 'PatientAllergyId'},
            {data: 'AllergyName', name: 'AllergyName'},
            {data: 'AllergyDescription', name: 'AllergyDescription'},
            {data: 'Certainty', name: 'Certainty'},
            {data: 'Severity', name: 'Severity'},
            {data: 'DateAdded', name: 'DateAdded'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "initComplete": function (settings, json) {
            editAllergy();
            deleteAllergy();
        },
        'drawCallback': function () {
            editAllergy();
            deleteAllergy();
        }
    });
}

/**
 * Add Patient Allergy
 */
function addAllergy() {

    var manageAllergyModal = $('#manageAllergyModal');
    var manageAllergyBtn = $('.add-allergy');

    manageAllergyBtn.off().on('click', function (e) {
        e.preventDefault();

        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageAllergyModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/allergies/add')
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageAllergyModal.find('.modal-title').html('Add Allergy');
                manageAllergyModal.find('.modal-body').html(response.data);
                manageAllergyModal.modal('show');
                manageAllergyModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId
                    };

                    saveAllergy(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });

}


/**
 * Edit Patient Allergy
 */
function editAllergy() {
    var manageAllergyModal = $('#manageAllergyModal');
    var manageAllergyBtn = $('.edit-allergy');


    manageAllergyBtn.off().on('click', function (e) {
        e.preventDefault();

        var allergyId = $(this).data('allergy-id');
        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageAllergyModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/allergies/edit/' + allergyId)
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageAllergyModal.find('.modal-title').html('Edit Allergy');
                manageAllergyModal.find('.modal-body').html(response.data);
                manageAllergyModal.modal('show');
                manageAllergyModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId,
                        AllergyId: allergyId,
                    };

                    updateAllergy(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });
}

/**
 * Delete Patient Allergy
 */
function deleteAllergy() {
    var deleteAllergy = $('.delete-allergy');


    deleteAllergy.off().on('click', function (e) {
        e.preventDefault();

        var allergyId = $(this).data('allergy-id');

        swal({
            title: 'Are you sure?',
            text: "Please confirm you wish to delete this allergy",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function (result) {
            if(result.value){
                showLoader();
                axios.get(NowGP.siteUrl + '/admin/patient/allergies/delete/' + allergyId)
                    .then(function (response) {
                        patientsAllergyTable.draw();
                        hideLoader();
                    })
                    .catch(function (error) {
                        hideLoader();
                        console.log(error);
                    });
            }
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {

            }
        })
    });
}

/**
 *
 * @param PatientData
 */
function saveAllergy(PatientData) {

    var manageAllergyModal = $('#manageAllergyModal');
    var saveAllergyBtn = $('#saveAllergy');

    console.log(PatientData);
    console.log(saveAllergyBtn);

    saveAllergyBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageAllergyForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        var data = {
                PatientId: PatientData.PatientId,
                AllergyName: $('#AllergyName').val(),
                AllergyDescription: $('#AllergyDescription').val(),
                Certainty: $('#Certainty').val(),
                Severity: $('#Severity').val(),
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                patientsAllergyTable.draw();
                manageAllergyModal.modal('hide');
                hideLoader();

            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                if (error.response.status) {
                    var errors = error.response.data.errors;

                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })
    })


}

/**
 * Update patient Allergy
 * @param PatientData
 */
function updateAllergy(PatientData) {

    var manageAllergyModal = $('#manageAllergyModal');
    var saveAllergyBtn = $('#saveAllergy');

    console.log(PatientData);
    console.log(saveAllergyBtn);

    saveAllergyBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageAllergyForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);



        var data = {
                PatientId: PatientData.PatientId,
                PatientAllergyId: PatientData.AllergyId,
                AllergyName: $('#AllergyName').val(),
                AllergyDescription: $('#AllergyDescription').val(),
                Certainty: $('#Certainty').val(),
                Severity: $('#Severity').val(),
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        resetModalFormErrors();
        axios.post(url, data)
            .then(function (response) {
                hideLoader();
                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                manageAllergyModal.modal('hide');

                console.log(patientsAllergyTable);
                patientsAllergyTable.draw();


            })
            .catch(function (error) {
                console.log(error);
                hideLoader();
                resetModalFormErrors();
                if (typeof error.response !== 'undefined') {
                    if (error.response.status) {
                        var errors = error.response.data.errors;

                        // Iterate through errors object.
                        $.each(errors, function (field, message) {
                            //console.error(field + ': ' + message);
                            //handle arrays
                            if (field.indexOf('.') != -1) {
                                field = field.replace('.', '[');
                                //handle multi dimensional array
                                for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                    field = field.replace('.', '][');
                                }
                                field = field + "]";
                            }
                            var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                            formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                        });

                        // Reset submit.
                        if (submit.is('button')) {
                            submit.html(submitOriginal).prop('disabled', false);
                        } else if (submit.is('input')) {
                            submit.val(submitOriginal).prop('disabled', false);
                        }
                    }
                }
            })
    })
}


function resetModalFormErrors() {
    $('.form-group').removeClass('has-error');
    $('.form-group').find('.help-block').remove();
}

