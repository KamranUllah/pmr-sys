var patientsDiscontinuedMedicationTable;

$(function () {
    /**
     * Medication Function Calls
     */
    addDiscontinuedMedication();

    //console.log(NowGP.PatientId);

    //patientDiscontinuedMedicationTableSearch(NowGP.PatientId);

    patientsDiscontinuedMedicationTable = patientDiscontinuedMedicationTableSearch(NowGP.PatientId);
});


/**
 *
 * @param PatientId
 * @returns {jQuery}
 */
function patientDiscontinuedMedicationTableSearch(PatientId) {
    return $('#patientDiscontinuedMedicationTable').DataTable({
        processing: true,
        destroy: true,
        serverSide: true,
        searching: false,
        lengthChange: false,
        "language": {
            "emptyTable": "No records has been found in the database."
        },
        ajax: NowGP.siteUrl + '/admin/patient/discontinued-medication/fetch/' + PatientId + '/data',
        columns: [
            {data: 'DatePrescribedOriginally', name: 'DatePrescribedOriginally'},
            {data: 'MedicationId', name: 'MedicationId'},
            {data: 'Name', name: 'Name'},
            {data: 'Dosage', name: 'Dosage'},
            {data: 'PacketSize', name: 'PacketSize'},
            {data: 'Enabled', name: 'Enabled'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "initComplete": function (settings, json) {
            editDiscontinuedMedication();
            deleteDiscontinuedMedication();
        },
        'drawCallback': function () {
            editDiscontinuedMedication();
            deleteDiscontinuedMedication();
        }
    });
}

/**
 * Add Patient Medication
 */
function addDiscontinuedMedication() {

    var manageDiscontinuedMedicationModal = $('#manageDiscontinuedMedicationModal');
    var manageMedicationBtn = $('.add-discontinued-medication');

    manageMedicationBtn.off().on('click', function (e) {
        e.preventDefault();

        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageDiscontinuedMedicationModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/discontinued-medication/add')
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageDiscontinuedMedicationModal.find('.modal-title').html('Add Discontinued Medication');
                manageDiscontinuedMedicationModal.find('.modal-body').html(response.data);
                manageDiscontinuedMedicationModal.modal('show');
                manageDiscontinuedMedicationModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId
                    };

                    saveDiscontinuedMedication(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });

}


/**
 * Edit Patient Medication
 */
function editDiscontinuedMedication() {
    var manageDiscontinuedMedicationModal = $('#manageDiscontinuedMedicationModal');
    var manageMedicationBtn = $('.edit-discontinued-medication');


    manageMedicationBtn.off().on('click', function (e) {
        e.preventDefault();

        var medicationId = $(this).data('medication-id');
        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageDiscontinuedMedicationModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/discontinued-medication/edit/' + medicationId)
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageDiscontinuedMedicationModal.find('.modal-title').html('Edit Discontinued Medication');
                manageDiscontinuedMedicationModal.find('.modal-body').html(response.data);
                manageDiscontinuedMedicationModal.modal('show');
                manageDiscontinuedMedicationModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId,
                        MedicationId: medicationId
                    };

                    updateDiscontinuedMedication(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });
}

/**
 * Delete Patient Medication
 */
function deleteDiscontinuedMedication() {
    var deleteDiscontinuedMedication = $('.delete-discontinued-medication');


    deleteDiscontinuedMedication.off().on('click', function (e) {
        e.preventDefault();

        var medicationId = $(this).data('medication-id');

        swal({
            title: 'Are you sure?',
            text: "Please confirm you wish to delete this medication",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function (result) {
            if(result.value){
                showLoader();
                axios.get(NowGP.siteUrl + '/admin/patient/discontinued-medication/delete/' + medicationId)
                    .then(function (response) {
                        patientsDiscontinuedMedicationTable.draw();
                        hideLoader();
                    })
                    .catch(function (error) {
                        hideLoader();
                        console.log(error);
                    });
            }
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {

            }
        })
    });
}

/**
 *
 * @param PatientData
 */
function saveDiscontinuedMedication(PatientData) {

    var manageDiscontinuedMedicationModal = $('#manageDiscontinuedMedicationModal');
    var saveDiscontinuedMedicationBtn = $('#saveDiscontinuedMedication');

    console.log(PatientData);
    console.log(saveDiscontinuedMedicationBtn);

    saveDiscontinuedMedicationBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageDiscontinuedMedicationForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        var data = {
                PatientId: PatientData.PatientId,
                Name: $('#DiscontinuedName').val(),
                Dosage: $('#DiscontinuedDosage').val(),
                PacketSize: $('#DiscontinuedPacketSize').val(),
                Enabled: $('#DiscontinuedEnabled').val(),
                Acute: 0
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                new Noty({
                    type: 'success',
                    theme: 'bootstrap-v3',
                    text: 'The medication <strong>' + data.Name + '</strong> has been added for patient <strong>#' + data.PatientId + '</strong>',
                    timeout: 4500,
                    progressBar: true,
                    closeWith: ['click', 'button']
                }).show();

                resetDiscontinuedMedicationForm();
                patientsDiscontinuedMedicationTable.draw();
                //manageDiscontinuedMedicationModal.modal('hide');
                hideLoader();

            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                if (error.response.status) {
                    var errors = error.response.data.errors;

                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })
    })


}

/**
 * Update patient Medication
 * @param PatientData
 */
function updateDiscontinuedMedication(PatientData) {

    var manageDiscontinuedMedicationModal = $('#manageDiscontinuedMedicationModal');
    var saveDiscontinuedMedicationBtn = $('#saveDiscontinuedMedication');

    console.log(PatientData);
    console.log(saveDiscontinuedMedicationBtn);

    saveDiscontinuedMedicationBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageDiscontinuedMedicationForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        var data = {
                PatientId: PatientData.PatientId,
                MedicationId: PatientData.MedicationId,
                Name: $('#DiscontinuedName').val(),
                Dosage: $('#DiscontinuedDosage').val(),
                PacketSize: $('#DiscontinuedPacketSize').val(),
                Enabled: $('#DiscontinuedEnabled').val(),
                Acute: 0
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        resetModalFormErrors();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                manageDiscontinuedMedicationModal.modal('hide');

                console.log(patientsDiscontinuedMedicationTable);
                patientsDiscontinuedMedicationTable.draw();
                hideLoader();


            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                console.log(error);
                if (typeof error.response !== 'undefined') {
                    if (error.response.status) {
                        var errors = error.response.data.errors;

                        // Iterate through errors object.
                        $.each(errors, function (field, message) {
                            //console.error(field + ': ' + message);
                            //handle arrays
                            if (field.indexOf('.') != -1) {
                                field = field.replace('.', '[');
                                //handle multi dimensional array
                                for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                    field = field.replace('.', '][');
                                }
                                field = field + "]";
                            }
                            var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                            formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                        });

                        // Reset submit.
                        if (submit.is('button')) {
                            submit.html(submitOriginal).prop('disabled', false);
                        } else if (submit.is('input')) {
                            submit.val(submitOriginal).prop('disabled', false);
                        }
                    }
                }
            })
    })
}

function resetDiscontinuedMedicationForm() {
    $('#manageDiscontinuedMedicationForm').trigger('reset');
}

function resetModalFormErrors() {
    $('.form-group').removeClass('has-error');
    $('.form-group').find('.help-block').remove();
}

