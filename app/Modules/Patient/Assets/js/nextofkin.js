var patientsNextOfKinTable;

$(function () {
    /**
     * NextOfKin Function Calls
     */
    addNextOfKin();

    //console.log(NowGP.PatientId);

    //patientNextOfKinTableSearch(NowGP.PatientId);

    if (NowGP.PatientId !== 0)
    {
        patientsNextOfKinTable = patientNextOfKinTableSearch(NowGP.PatientId);
    }

});


/**
 *
 * @param PatientId
 * @returns {jQuery}
 */
function patientNextOfKinTableSearch(PatientId) {
    return $('#patientNextOfKinTable').DataTable({
        processing: true,
        destroy: true,
        serverSide: true,
        searching: false,
        lengthChange: false,
        "language": {
            "emptyTable": "No records has been found in the database."
        },
        ajax: NowGP.siteUrl + '/admin/patient/next-of-kin/fetch/' + PatientId + '/data',
        columns: [
            {data: 'Name', name: 'Name'},
            {data: 'Telephone', name: 'Telephone'},
            {data: 'Relationship', name: 'Relationship'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "initComplete": function (settings, json) {
            editNextOfKin();
            deleteNextOfKin();
        },
        'drawCallback': function () {
            editNextOfKin();
            deleteNextOfKin();
        }
    });
}

/**
 * Add Patient NextOfKin
 */
function addNextOfKin() {

    var manageNextOfKinModal = $('#manageNextOfKinModal');
    var manageNextOfKinBtn = $('.add-nextofkin');

    manageNextOfKinBtn.off().on('click', function (e) {
        e.preventDefault();

        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageNextOfKinModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/next-of-kin/add')
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageNextOfKinModal.find('.modal-title').html('Add Next Of Kin');
                manageNextOfKinModal.find('.modal-body').html(response.data);
                manageNextOfKinModal.modal('show');
                manageNextOfKinModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId
                    };

                    saveNextOfKin(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });

}


/**
 * Edit Patient NextOfKin
 */
function editNextOfKin() {
    var manageNextOfKinModal = $('#manageNextOfKinModal');
    var manageNextOfKinBtn = $('.edit-nextofkin');


    manageNextOfKinBtn.off().on('click', function (e) {
        e.preventDefault();

        var nextOfKinId = $(this).data('nextofkin-id');
        var patientId = NowGP.PatientId;

        // Set Modal Options
        manageNextOfKinModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/next-of-kin/edit/' + nextOfKinId)
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                manageNextOfKinModal.find('.modal-title').html('Edit NextOfKin');
                manageNextOfKinModal.find('.modal-body').html(response.data);
                manageNextOfKinModal.modal('show');
                manageNextOfKinModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId,
                        nextOfKinId: nextOfKinId
                    };

                    updateNextOfKin(PatientData);
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });
}

/**
 * Delete Patient NextOfKin
 */
function deleteNextOfKin() {
    var deleteNextOfKin = $('.delete-nextofkin');


    deleteNextOfKin.off().on('click', function (e) {
        e.preventDefault();

        var nextOfKinId = $(this).data('nextofkin-id');

        swal({
            title: 'Are you sure?',
            text: "Please confirm you wish to delete this next of kin",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function (result) {
            if(result.value){
                showLoader();
                axios.get(NowGP.siteUrl + '/admin/patient/next-of-kin/delete/' + nextOfKinId)
                    .then(function (response) {
                        patientsNextOfKinTable.draw();
                        hideLoader();
                    })
                    .catch(function (error) {
                        hideLoader();
                        console.log(error);
                    });
            }
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {

            }
        })
    });
}

/**
 *
 * @param PatientData
 */
function saveNextOfKin(PatientData) {

    var manageNextOfKinModal = $('#manageNextOfKinModal');
    var saveNextOfKinBtn = $('#saveNextOfKin');

    console.log(PatientData);
    console.log(saveNextOfKinBtn);

    saveNextOfKinBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageNextOfKinForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        var data = {
                PatientId: PatientData.PatientId,
                Name: $('#Name').val(),
                Telephone: $('#Telephone').val(),
                Relationship: $('#Relationship').val()
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        axios.post(url, data)
            .then(function (response) {
                console.log("response",response);
                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                patientsNextOfKinTable.draw();
                manageNextOfKinModal.modal('hide');
                hideLoader();

            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                if (error.response.status) {
                    var errors = error.response.data.errors;

                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })
    })


}

/**
 * Update patient NextOfKin
 * @param PatientData
 */
function updateNextOfKin(PatientData) {

    var manageNextOfKinModal = $('#manageNextOfKinModal');
    var saveNextOfKinBtn = $('#saveNextOfKin');

    console.log(PatientData);
    console.log(saveNextOfKinBtn);

    saveNextOfKinBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageNextOfKinForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');

        console.log(url);


        var data = {
                PatientId: PatientData.PatientId,
                NextOfKinId: PatientData.NextOfKinId,
                Name: $('#Name').val(),
                Telephone: $('#Telephone').val(),
                Relationship: $('#Relationship').val()
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        resetModalFormErrors();
        axios.post(url, data)
            .then(function (response) {

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }

                manageNextOfKinModal.modal('hide');

                console.log(patientsNextOfKinTable);
                patientsNextOfKinTable.draw();
                hideLoader();


            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                console.log(error);
                if (typeof error.response !== 'undefined') {
                    if (error.response.status) {
                        var errors = error.response.data.errors;

                        // Iterate through errors object.
                        $.each(errors, function (field, message) {
                            //console.error(field + ': ' + message);
                            //handle arrays
                            if (field.indexOf('.') != -1) {
                                field = field.replace('.', '[');
                                //handle multi dimensional array
                                for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                    field = field.replace('.', '][');
                                }
                                field = field + "]";
                            }
                            var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                            formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                        });

                        // Reset submit.
                        if (submit.is('button')) {
                            submit.html(submitOriginal).prop('disabled', false);
                        } else if (submit.is('input')) {
                            submit.val(submitOriginal).prop('disabled', false);
                        }
                    }
                }
            })
    })
}

/**
 *
 */
function resetModalFormErrors() {
    $('.form-group').removeClass('has-error');
    $('.form-group').find('.help-block').remove();
}

