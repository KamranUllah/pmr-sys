
$(function () {
    /**
     * Medication Function Calls
     */

});


/**
 * Add Patient Clinical Notes
 */
function addClinicalNotes() {

    var viewPatientNotesModal = $('#viewPatientNotesModal');
    var addPatientNotesBtn = $('.add-clinical-notes');

    addPatientNotesBtn.off().on('click', function (e) {
        e.preventDefault();

        var patientId = addPatientNotesBtn.attr('data-patientid');

        // Set Modal Options
        viewPatientNotesModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/clinical-notes/add')
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                viewPatientNotesModal.find('.modal-title').html('Add Clinical Notes');
                viewPatientNotesModal.find('.modal-body').html(response.data);
                viewPatientNotesModal.find('#patientId').html(patientId);
                $('#savePatientNotes').attr('data-patientid',patientId);
                viewPatientNotesModal.find('.add-clinical-notes').hide();
                viewPatientNotesModal.find('.save-clinical-notes').show();
                viewPatientNotesModal.find('.update-clinical-notes').hide();

                viewPatientNotesModal.modal('show');
                viewPatientNotesModal.on('shown.bs.modal', function () {
                    addClinicalNotes();
                    saveClinicalNotes();
                    editClinicalNotes();
                    updateClinicalNotes();
                    deleteClinicalNotes();
                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });

}


/**
 *
 * Edit Patient Clinical Notes
 */
function saveClinicalNotes() {

    var viewPatientNotesModal = $('#viewPatientNotesModal');
    var savePatientNotesBtn = $('#savePatientNotes');

    var patientId = savePatientNotesBtn.attr('data-patientid');
    console.log(savePatientNotesBtn);

    savePatientNotesBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageClinicalNotes'),
            url = form.attr('action'),
            submit = savePatientNotesBtn;

        console.log(url);


        var data = {
                PatientId: parseInt($('#patientId').text()),
                Notes: $('#Notes').val() ? $('#Notes').val().trim(): '',
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        resetModalFormErrorsClinical();
        axios.post(url, data)
            .then(function (response) {

                if(response.data.success === 1){
                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }

                    new Noty({
                        type: 'success',
                        theme: 'bootstrap-v3',
                        text: 'The notes has been added for patient <strong>#' + data.PatientId + '</strong>',
                        timeout: 4500,
                        progressBar: true,
                        closeWith: ['click', 'button']
                    }).show();

                    resetClinicalNotesForm();
                    hideLoader();
                    fetchPatientNotes(data.PatientId);
                    viewPatientNotesModal.on('shown.bs.modal', function () {
                        addClinicalNotes();
                        saveClinicalNotes();
                        editClinicalNotes();
                        updateClinicalNotes();
                        deleteClinicalNotes();
                    });
                }else{
                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                    $('.generalError').addClass('has-error').append('<p class="alert alert-danger">' + response.data.message + '</p>');
                    hideLoader();
                }
            })
            .catch(function (error) {
                hideLoader();
                if (error.response.status) {
                    var errors = error.response.data.errors;
                    resetModalFormErrorsClinical();
                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })
    })


}


/**
 * Edit Patient Medication
 */
function editClinicalNotes() {
    var viewPatientNotesModal = $('#viewPatientNotesModal');
    var editPatientNotesBtn = $('.edit-note');


    editPatientNotesBtn.off().on('click', function (e) {
        e.preventDefault();
        var ClinicalNoteId = $(this).attr('data-clinical-note-id');
        var patientId = $(this).attr('data-patientid');

        console.log('Clinical ID:',ClinicalNoteId);

        // Set Modal Options
        viewPatientNotesModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/clinical-notes/edit/' + ClinicalNoteId)
            .then(function (response) {
                hideLoader();
                //console.log(response.data);
                viewPatientNotesModal.find('.modal-title').html('Edit Clinical Notes');
                viewPatientNotesModal.find('.modal-body').html(response.data);
                viewPatientNotesModal.find('.add-clinical-notes').hide();
                viewPatientNotesModal.find('.save-clinical-notes').hide();
                viewPatientNotesModal.find('.update-clinical-notes').show();
                viewPatientNotesModal.modal('show');
                viewPatientNotesModal.on('shown.bs.modal', function () {
                    var PatientData = {
                        PatientId: patientId,
                        ClinicalNoteId: ClinicalNoteId
                    };

                    updateClinicalNotes(PatientData);
                    addClinicalNotes();
                    saveClinicalNotes();
                    editClinicalNotes();
                    deleteClinicalNotes();

                });
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });
}

/**
 * Delete Patient Medication
 */
function deleteClinicalNotes() {
    var deleteClinicalNote = $('.delete-note');
    var viewPatientNotesModal = $('#viewPatientNotesModal');


    deleteClinicalNote.off().on('click', function (e) {
        e.preventDefault();

        var ClinicalNoteId = $(this).attr('data-clinical-note-id');
        var patientId = $(this).attr('data-patientid');

        swal({
            title: 'Are you sure?',
            text: "Please confirm you wish to delete this note?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function (result) {
            showLoader();
            if(result.value){
                axios.get(NowGP.siteUrl + '/admin/patient/clinical-notes/delete/' + ClinicalNoteId)
                    .then(function (response) {

                        if(response.data.success === 1){
                            resetClinicalNotesForm();
                            hideLoader();
                            new Noty({
                                type: 'success',
                                theme: 'bootstrap-v3',
                                text: 'Note <strong>#' + ClinicalNoteId + '</strong> has been deleted for patient <strong>#' + patientId + '</strong>',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                            fetchPatientNotes(patientId);
                            viewPatientNotesModal.on('shown.bs.modal', function () {
                                addClinicalNotes();
                                saveClinicalNotes();
                                editClinicalNotes();
                                updateClinicalNotes();
                                deleteClinicalNotes();
                            });

                        }else{
                            hideLoader();
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'There has been an issue deleting Note <strong>#' + ClinicalNoteId + '</strong> for patient <strong>#' + patientId + '</strong>',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                            console.log(response.data.message);
                        }

                    })
                    .catch(function (error) {
                        hideLoader();
                        new Noty({
                            type: 'error',
                            theme: 'bootstrap-v3',
                            text: 'There has been an issue deleting Note <strong>#' + ClinicalNoteId + '</strong> for patient <strong>#' + patientId + '</strong>',
                            timeout: 4500,
                            progressBar: true,
                            closeWith: ['click', 'button']
                        }).show();
                        console.log(error);
                    });
            }else{
                hideLoader();
            }
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {

            }
        })
    });
}


/**
 * Update patient Medication
 * @param PatientData
 */
function updateClinicalNotes(PatientData) {

    var viewPatientNotesModal = $('#viewPatientNotesModal');
    var savePatientNotesBtn = $('#updatePatientNotes');


    console.log(PatientData);
    console.log(savePatientNotesBtn);

    savePatientNotesBtn.off().on('click', function () {
        // Set vars.
        var form = $('#manageClinicalNotes'),
            url = form.attr('action'),
            submit = savePatientNotesBtn;

        console.log(url);


        var data = {
                ClinicalNoteId: $('#NoteId').val(),
                PatientId: $('#patientIdData').val(),
                Notes: $('#Notes').val() ? $('#Notes').val().trim(): '',
            },
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        console.log(data);
        showLoader();
        resetModalFormErrorsClinical();
        axios.post(url, data)
            .then(function (response) {

                if(response.data.success === 1){
                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }

                    new Noty({
                        type: 'success',
                        theme: 'bootstrap-v3',
                        text: 'Note <strong>#' + data.ClinicalNoteId + '</strong> has been updated for patient <strong>#' + data.PatientId + '</strong>',
                        timeout: 4500,
                        progressBar: true,
                        closeWith: ['click', 'button']
                    }).show();

                    resetClinicalNotesForm();
                    hideLoader();
                    fetchPatientNotes(data.PatientId);
                }else{
                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                    $('.generalError').addClass('has-error').append('<p class="alert alert-danger">' + response.data.message + '</p>');
                    hideLoader();
                }


            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrorsClinical();
                console.log(error);
                if (typeof error.response !== 'undefined') {
                    if (error.response.status) {
                        var errors = error.response.data.errors;

                        // Iterate through errors object.
                        $.each(errors, function (field, message) {
                            //console.error(field + ': ' + message);
                            //handle arrays
                            if (field.indexOf('.') != -1) {
                                field = field.replace('.', '[');
                                //handle multi dimensional array
                                for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                    field = field.replace('.', '][');
                                }
                                field = field + "]";
                            }
                            var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                            formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                        });

                        // Reset submit.
                        if (submit.is('button')) {
                            submit.html(submitOriginal).prop('disabled', false);
                        } else if (submit.is('input')) {
                            submit.val(submitOriginal).prop('disabled', false);
                        }
                    }
                }
            })
    })
}

function resetClinicalNotesForm() {
    $('#manageClinicalNotes').trigger('reset');
}


function resetModalFormErrorsClinical() {
    $('.form-group').removeClass('has-error');
    $('.form-group').find('.help-block').remove();
    $('.generalError').removeClass('has-error');
    $('.generalError').find('.alert .alert-danger').remove();
}

