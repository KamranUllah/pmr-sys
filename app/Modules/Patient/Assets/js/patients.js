$(function () {

    // Search for patient


    patientSearch();
    savePatient();
    savePatientGP();
    viewRepeatPrescription();
    viewPatientNotes();

    addClinicalNotes();
    saveClinicalNotes();
    editClinicalNotes();
    updateClinicalNotes();
    deleteClinicalNotes();

    $('#dob').daterangepicker({
        locale: {
            format: 'DD/MM/YYYY',
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
        },
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        minDate: moment().subtract('100', 'year'),
        maxDate: moment()
    });

    $('#dob').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });

    $('#dob').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $('#PatientDOB').daterangepicker({
        locale: {
            format: 'DD/MM/YYYY',
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
        },
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        minDate: moment().subtract('100', 'year'),
        maxDate: moment()
    });

    $('#PatientDOB').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY'));
    });

    $('#PatientDOB').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });


});

/**
 *
 */
function patientSearch() {
    $('#patientSearch').on('submit', function (submission) {

        submission.preventDefault();
        //showLoader();

        // Set vars.
        var form = $('#patientSearch'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');


        var data = form.serialize(),
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }
        showLoader();
        resetModalFormErrors();
        axios.post(url, data)
            .then(function (response) {

                console.log('my response', response);

                if (typeof response !== 'undefined') {
                    var patientsTable = patientTableSearch();
                    patientsTable.draw();
                }

                // if(response.status == 422){
                //     console.log("422")
                // }

                hideLoader();

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }
            })
            .catch(function (error) {

                //console.log('my error 2', error);
                hideLoader();
                resetModalFormErrors();
                if (error.response){
                    var errors = error.response.data.errors;

                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })

    });
}

/**
 *
 */
function savePatient() {
    $('#patientDetailsForm').on('submit', function (submission) {

        submission.preventDefault();
        //showLoader();

        // Set vars.
        var form = $('#patientDetailsForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');


        var data = form.serialize(),
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        showLoader();
        resetModalFormErrors();
        axios.post(url, data)
            .then(function (response) {
                hideLoader();
                console.log("response:",response);
                console.log("data:",response.data);
                if (response.data.patientId > 0 && response.status === 200) {
                    swal({
                        title: 'Success',
                        text: response.data.message,
                        type: 'success',
                        confirmButtonText: 'Ok',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function () {
                        // window.location.reload();
                        showLoader();
                        window.location.href = laroute.route('admin.patients.show', { patientId: response.data.patientId });
                    }, function (dismiss) {
                        // dismiss can be 'cancel', 'overlay',
                        // 'close', and 'timer'
                        if (dismiss === 'cancel') {
                            showLoader();
                            window.location.reload();
                        }
                    });
                }else  {
                    console.log(response);
                    swal({
                        title: 'Error',
                        text: response.data.message,
                        type: 'error',
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function () {

                    }, function (dismiss) {
                        // dismiss can be 'cancel', 'overlay',
                        // 'close', and 'timer'
                        if (dismiss === 'cancel') {

                        }
                    });
                }

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }
            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                if (error.response.status) {
                    var errors = error.response.data.errors;

                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })

    });
}

function savePatientGP() {
    $('#patientGPDetailsForm').on('submit', function (submission) {

        submission.preventDefault();
        //showLoader();

        // Set vars.
        var form = $('#patientGPDetailsForm'),
            url = form.attr('action'),
            submit = form.find('[type=submit]');


        var data = form.serialize(),
            contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var submitOriginal;


        // Please wait.
        if (submit.is('button')) {
            submitOriginal = submit.html();
            submit.html('Please wait...').prop('disabled', true);
        } else if (submit.is('input')) {
            submitOriginal = submit.val();
            submit.val('Please wait...').prop('disabled', true);
        }

        showLoader();
        resetModalFormErrors();
        axios.post(url, data)
            .then(function (response) {
                hideLoader();
                console.log("GP Response:",response);
                if (response.data.patientId > 0 && response.status === 200) {
                    swal({
                        title: 'Success',
                        text: response.data.message,
                        type: 'success',
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-success',
                        buttonsStyling: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function () {
                        showLoader();
                        window.location.reload();
                    }, function (dismiss) {
                        // dismiss can be 'cancel', 'overlay',
                        // 'close', and 'timer'
                        if (dismiss === 'cancel') {

                        }
                    });
                } else {
                    swal({
                        title: 'Error',
                        text: response.data.message,
                        type: 'error',
                        confirmButtonText: 'Ok',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function () {

                    }, function (dismiss) {
                        // dismiss can be 'cancel', 'overlay',
                        // 'close', and 'timer'
                        if (dismiss === 'cancel') {

                        }
                    });
                }

                // Reset submit.
                if (submit.is('button')) {
                    submit.html(submitOriginal).prop('disabled', false);
                } else if (submit.is('input')) {
                    submit.val(submitOriginal).prop('disabled', false);
                }
            })
            .catch(function (error) {
                hideLoader();
                resetModalFormErrors();
                if (error.response.status) {
                    var errors = error.response.data.errors;

                    // Iterate through errors object.
                    $.each(errors, function (field, message) {
                        //console.error(field + ': ' + message);
                        //handle arrays
                        if (field.indexOf('.') != -1) {
                            field = field.replace('.', '[');
                            //handle multi dimensional array
                            for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                                field = field.replace('.', '][');
                            }
                            field = field + "]";
                        }
                        var formGroup = $('[name=' + field + ']', form).closest('.form-group');
                        formGroup.addClass('has-error').append('<p class="help-block">' + message + '</p>');
                    });

                    // Reset submit.
                    if (submit.is('button')) {
                        submit.html(submitOriginal).prop('disabled', false);
                    } else if (submit.is('input')) {
                        submit.val(submitOriginal).prop('disabled', false);
                    }
                }
            })

    });
}

/**
 * View Repeat Prescriptions
 */
function viewRepeatPrescription() {
    var viewRepeatPrescriptionModal = $('#viewRepeatPrescriptionModal');
    var viewRepeatPrescriptionBtn = $('a.view-rp');

    $('#patients-table').on('click','a.view-rp', function (e) {


        e.preventDefault();

        var patientId = viewRepeatPrescriptionBtn.prevObject["0"].activeElement.attributes[2].value ;
        console.log("Patient ID:",patientId);
        // console.log(viewRepeatPrescriptionBtn);
        // console.log(viewRepeatPrescriptionBtn.prevObject["0"].activeElement.attributes[2].value);
        // console.log($(this));
        // console.log("arrivo");

        // Set Modal Options
        viewRepeatPrescriptionModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        showLoader();
        axios.get(NowGP.siteUrl + '/admin/patient/prescription/repeat/view/'+patientId)
            .then(function (response) {
                hideLoader();
                console.log(response.data);
                viewRepeatPrescriptionModal.find('.modal-title').html('Repeat Prescriptions');
                viewRepeatPrescriptionModal.find('.modal-body').html(response.data);
                viewRepeatPrescriptionModal.modal('show');
            })
            .catch(function (error) {
                hideLoader();
                console.log(error);
            });
    });

    // viewPatientNotes();

}


function viewPatientNotes(){
    var viewPatientNotesModal = $('#viewPatientNotesModal');
    var viewPatientNotesBtn = $('a.view-note');
    $('#patients-table').on('click','a.view-note', function (e) {


        e.preventDefault();

        var patientId = viewPatientNotesBtn.prevObject["0"].activeElement.attributes[2].value;
        $('.add-clinical-notes').attr('data-patientid',patientId);
        console.log("Patient ID:",patientId);

        viewPatientNotesModal.find('.add-clinical-notes').show();
        viewPatientNotesModal.find('.save-clinical-notes').hide();
        viewPatientNotesModal.find('.update-clinical-notes').hide();

        // Set Modal Options
        viewPatientNotesModal.modal({
            keyboard: false,
            backdrop: 'static',
            show: false,
        });

        fetchPatientNotes(patientId);
    });

}


function markAsDisabled(){

    $('#patients-table').on('click','a.disable-patient', function (e) {


        e.preventDefault();
        var disablePatientBtn = $(this);
        var patientId = disablePatientBtn.data('patientId');
        console.log("You just clicked:",patientId);

        swal({
            title: 'Manage Patient '+patientId,
            html: 'Do you want to mark this patient as disabled? (Please remember to leave a note first)',
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then(function (result) {
            if (result.value) {
                showLoader();
                axios.post(laroute.route('admin.patients.update.status', { patientId: patientId}), { active: 0})
                    .then(function (response) {
                        hideLoader();
                        if(typeof response.data === 'string' && response.data.indexOf("Access denied") > 0){
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'You don\'t have permission to this functionality.',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                        } else if (typeof response.data.error !== "undefined") {
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'Failed to mark the patient as disabled',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                        } else if (response.status === 200) {
                            new Noty({
                                type: 'success',
                                theme: 'bootstrap-v3',
                                text: 'The patient has been marked as disabled',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                            var patientTable = $('#patients-table').DataTable().ajax.reload();
                        } else {
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'Failed to mark the patient as disabled',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();                                    }
                    })
                    .catch(function (error) {
                    })
            }
        });
    });
}


function markAsEnabled(){

    $('#patients-table').on('click','a.enable-patient', function (e) {


        e.preventDefault();
        var enablePatientBtn = $(this);
        var patientId = enablePatientBtn.data('patientId');
        console.log("You just clicked:",patientId);

        swal({
            title: 'Manage Patient '+patientId,
            html: 'Do you want to mark this patient as enabled? (Please remember to leave a note first)',
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then(function (result) {
            if (result.value) {
                showLoader();
                axios.post(laroute.route('admin.patients.update.status', { patientId: patientId}), { active: 1})
                    .then(function (response) {
                        hideLoader();
                        if(typeof response.data === 'string' && response.data.indexOf("Access denied") > 0){
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'You don\'t have permission to this functionality.',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                        } else if (typeof response.data.error !== "undefined") {
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'Failed to mark the patient as enabled',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                        } else if (response.status === 200) {
                            new Noty({
                                type: 'success',
                                theme: 'bootstrap-v3',
                                text: 'The patient has been marked as enabled',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();
                            var patientTable = $('#patients-table').DataTable().ajax.reload();
                        } else {
                            new Noty({
                                type: 'error',
                                theme: 'bootstrap-v3',
                                text: 'Failed to mark the patient as enabled',
                                timeout: 4500,
                                progressBar: true,
                                closeWith: ['click', 'button']
                            }).show();                                    }
                    })
                    .catch(function (error) {
                    })
            }
        });
    });
}


function fetchPatientNotes(patientId) {
    var viewPatientNotesModal = $('#viewPatientNotesModal');
    showLoader();
    axios.get(NowGP.siteUrl + '/admin/patient/notes/view/'+patientId)
        .then(function (response) {
            hideLoader();
            console.log(response.data);
            viewPatientNotesModal.find('.modal-title').html('Clinical Notes');
            viewPatientNotesModal.find('.modal-body').html(response.data);
            viewPatientNotesModal.find('.add-clinical-notes').show();
            viewPatientNotesModal.find('.save-clinical-notes').hide();
            viewPatientNotesModal.find('.update-clinical-notes').hide();
            viewPatientNotesModal.modal('show');
            // viewPatientNotesModal.on('shown.bs.modal', function () {
            //     addClinicalNotes();
            //     saveClinicalNotes();
            //     editClinicalNotes();
            //     updateClinicalNotes();
            //     deleteClinicalNotes();
            // });
            addClinicalNotes();
            saveClinicalNotes();
            editClinicalNotes();
            updateClinicalNotes();
            deleteClinicalNotes();
        })
        .catch(function (error) {
            hideLoader();
            console.log(error);
        });
}



/**
 *
 * @returns {jQuery}
 */
function patientTableSearch() {
        $.fn.dataTable.ext.errMode = 'none';
        return $('#patients-table').DataTable({

            processing: true,
            serverSide: true,
            destroy: true,
            searching: false,
            deferLoading: true,
            "language": {
                "emptyTable": "No records match the criteria entered. Please change the criteria and try again",
                "zeroRecords": "No records match the criteria entered. Please change the criteria and try again"
            },

            ajax: {
                url: NowGP.siteUrl + '/admin/patient/fetch-patients',
                method: 'POST',
                data: function (d) {
                    d.patient_id = $('input[name=patient_id]').val().trim();
                    d.first_name = $('input[name=first_name]').val().trim();
                    d.last_name = $('input[name=last_name]').val().trim();
                    d.gender = $('select[name=gender]').val().trim();
                    d.dob = $('input[name=dob]').val().trim();
                    d.postcode = $('input[name=postcode]').val().trim();
                    d.nhs_number = $('input[name=nhs_number]').val().trim();
                }
            },
            columns: [
                {data: 'PatientId', name: 'PatientId'},
                {data: 'PatientForename', name: 'PatientForename'},
                {data: 'PatientSurname', name: 'PatientSurname'},
                {data: 'PatientDOB', name: 'PatientDOB'},
                {data: 'PatientSex', name: 'PatientSex'},
                {data: 'NHSNumber', name: 'NHSNumber'},
                {data: 'exemptions.IsExempt', name: 'Exempt'},
                {data: 'exemptions.Reason', name: 'ExemptionReason'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            drawCallback: function( settings ) {
                viewRepeatPrescription();
                viewPatientNotes();
                markAsDisabled();
                markAsEnabled();
            }
        });


}

function resetModalFormErrors() {
    $('.form-group').removeClass('has-error');
    $('.form-group').find('.help-block').remove();
}






