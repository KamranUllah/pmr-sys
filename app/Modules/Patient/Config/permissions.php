<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 14/06/2017
 * Time: 16:01
 */

return [

    // User Management Permissions
    'Patient management' => [
        /**
         * Patient
         */
        'patient.admin.patient.index' => [
            'name'        => '',
            'description' => 'View patients list',
        ],

        'patient.admin.patient.show' => [
            'name'        => '',
            'description' => 'View patient\'s details',
        ],

        'patient.admin.patient.create' => [
            'name'        => '',
            'description' => 'Create a new patient',
        ],

        'patient.admin.patient.store' => [
            'name'        => '',
            'description' => 'Store a new patient\'s details',
        ],

        'patient.admin.patient.update' => [
            'name'        => '',
            'description' => 'Update a patient\'s details',
        ],

        'patient.admin.patient.updateStatus' => [
            'name'        => '',
            'description' => 'Update a patient\'s Active Status',
        ],

        'patient.admin.patient.updateNHSNumber' => [
            'name'        => '',
            'description' => 'Update a patient\'s NHS Number',
        ],


        'patient.admin.patient.updatePatientSurgeryFax' => [
            'name'        => '',
            'description' => 'Update a patient\'s Surgery Fax Number',
        ],

        'mark.data.import.as.complete' => [
            'name'        => '',
            'description' => 'Mark a patient\'s data import as complete (page)',
        ],

        'patient.admin.patient.markPatientDataImportAsComplete' => [
            'name'        => '',
            'description' => 'Access Controller to mark a patient\'s data import as complete (required)',
        ],

        'patient.admin.patient.getPatientsData' => [
            'name'        => '',
            'description' => 'Fetch patient data (required)',
        ],

        'patient.admin.patient.getnotImportedPatients' => [
            'name'        => '',
            'description' => 'Fetch all unimported patient data view ',
        ],

        'patient.admin.patient.getnotImportedPatientsData' => [
            'name'        => '',
            'description' => 'Fetch all unimported patient data ',
        ],


        'patient.admin.patient.getPatientRepeatPrescriptions' => [
            'name'        => '',
            'description' => 'Access Patient Repeat Prescription Data ',
        ],

        'patient.admin.patient.getPatientEPSStatus' => [
            'name'        => '',
            'description' => 'Access Patient EPS Data ',
        ],

        'patient.admin.patient.getClinicalNotes' => [
            'name'        => '',
            'description' => 'Access Patient Clinical Notes ',
        ],


        /**
         * Clinical Notes
         */
        'patient.admin.clinicalnotes.create'          => [
            'name'        => '',
            'description' => 'Add a new clinical notes for patient',
        ],

        'patient.admin.clinicalnotes.store' => [
            'name'        => '',
            'description' => 'Store new clinical notes for patient',
        ],

        'patient.admin.clinicalnotes.edit' => [
            'name'        => '',
            'description' => 'Edit clinical notes for patient',
        ],

        'patient.admin.clinicalnotes.update' => [
            'name'        => '',
            'description' => 'Update clinical notes for patient',
        ],

        'patient.admin.clinicalnotes.destroy' => [
            'name'        => '',
            'description' => 'Delete clinical notes for patient',
        ],

        /**
         * Allergy
         */
        'patient.admin.allergy.create'          => [
            'name'        => '',
            'description' => 'Add a new allergy for patient',
        ],

        'patient.admin.allergy.store' => [
            'name'        => '',
            'description' => 'Store new allergy details for patient',
        ],

        'patient.admin.allergy.edit' => [
            'name'        => '',
            'description' => 'Edit allergy details for patient',
        ],

        'patient.admin.allergy.update' => [
            'name'        => '',
            'description' => 'Update allergy details for patient',
        ],

        'patient.admin.allergy.destroy' => [
            'name'        => '',
            'description' => 'Delete allergy details for patient',
        ],


        'patient.admin.allergy.getPatientAllergiesData' => [
            'name'        => '',
            'description' => 'Fetch patient allergy data (required)',
        ],

        /**
         * Acute Medication
         */
        'patient.admin.acutemedication.create'          => [
            'name'        => '',
            'description' => 'Add a new acute medication for patient',
        ],

        'patient.admin.acutemedication.store' => [
            'name'        => '',
            'description' => 'Store new acute medication details for patient',
        ],

        'patient.admin.acutemedication.edit' => [
            'name'        => '',
            'description' => 'Edit acute medication details for patient',
        ],

        'patient.admin.acutemedication.update' => [
            'name'        => '',
            'description' => 'Update acute medication details for patient',
        ],

        'patient.admin.acutemedication.destroy' => [
            'name'        => '',
            'description' => 'Delete acute medication details for patient',
        ],

        'patient.admin.acutemedication.getPatientMedicationData' => [
            'name'        => '',
            'description' => 'Fetch patient acute medication data (required)',
        ],

        /**
         *  Current Medication
         */
        'patient.admin.currentmedication.create'                 => [
            'name'        => '',
            'description' => 'Add a new current medication for patient',
        ],

        'patient.admin.currentmedication.store' => [
            'name'        => '',
            'description' => 'Store new current medication details for patient',
        ],

        'patient.admin.currentmedication.edit' => [
            'name'        => '',
            'description' => 'Edit current medication details for patient',
        ],

        'patient.admin.currentmedication.update' => [
            'name'        => '',
            'description' => 'Update current medication details for patient',
        ],

        'patient.admin.currentmedication.destroy' => [
            'name'        => '',
            'description' => 'Delete current medication details for patient',
        ],

        'patient.admin.currentmedication.getPatientMedicationData' => [
            'name'        => '',
            'description' => 'Fetch patient current medication data (required)',
        ],


        /**
         *  Discontinued Medication
         */
        'patient.admin.discontinuedmedication.create'                 => [
            'name'        => '',
            'description' => 'Add a new discontinued medication for patient',
        ],

        'patient.admin.discontinuedmedication.store' => [
            'name'        => '',
            'description' => 'Store new discontinued medication details for patient',
        ],

        'patient.admin.discontinuedmedication.edit' => [
            'name'        => '',
            'description' => 'Edit discontinued medication details for patient',
        ],

        'patient.admin.discontinuedmedication.update' => [
            'name'        => '',
            'description' => 'Update discontinued medication details for patient',
        ],

        'patient.admin.discontinuedmedication.destroy' => [
            'name'        => '',
            'description' => 'Delete discontinued medication details for patient',
        ],

        'patient.admin.discontinuedmedication.getPatientMedicationData' => [
            'name'        => '',
            'description' => 'Fetch patient discontinued medication data (required)',
        ],

        /**
         * GP Details
         */
        'patient.admin.gpdetail.update'                            => [
            'name'        => '',
            'description' => 'Update patient\'s GP details',
        ],

        /**
         *  Next Of Kin
         */
        'patient.admin.nextofkin.create'                           => [
            'name'        => '',
            'description' => 'Add a new next of kin for patient',
        ],

        'patient.admin.nextofkin.store' => [
            'name'        => '',
            'description' => 'Store new next of kin details for patient',
        ],

        'patient.admin.nextofkin.edit' => [
            'name'        => '',
            'description' => 'Edit next of kin details for patient',
        ],

        'patient.admin.nextofkin.update' => [
            'name'        => '',
            'description' => 'Update next of kin details for patient',
        ],

        'patient.admin.nextofkin.getPatientNextOfKinData' => [
            'name'        => '',
            'description' => 'Fetch patient next of kin data (required)',
        ],

    ],

];
