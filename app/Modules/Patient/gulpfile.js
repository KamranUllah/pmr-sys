const elixir = require('laravel-elixir');
const argv = require('yargs').argv;

require('laravel-elixir-vue-2');

/*if (typeof moduleName === 'undefined') {
    console.log('Plunder more riffiwobbles!');
    return false;
}*/

elixir(function (mix) {

    mix.styles(
        './app/Modules/Patient/Assets/css/patient.css'
        , 'public/modules/patient/css/patient.css');

    mix.styles(
        './app/Modules/Patient/Assets/css/datepicker_override.css'
        , 'public/modules/patient/css/datepicker_override.css');

    mix.webpack(
        './app/Modules/Patient/Assets/js/patient-details.js'
    , 'public/modules/patient/js/patient-details.js');

    mix.scripts(
        './app/Modules/Patient/Assets/js/acute-medication.js'
        , 'public/modules/patient/js/acute-medication.js');

    mix.scripts(
        './app/Modules/Patient/Assets/js/clinical-notes.js'
        , 'public/modules/patient/js/clinical-notes.js');

    mix.scripts(
        './app/Modules/Patient/Assets/js/allergy.js'
        , 'public/modules/patient/js/allergy.js');

    mix.scripts(
        './app/Modules/Patient/Assets/js/current-medication.js'
        , 'public/modules/patient/js/current-medication.js');

    mix.scripts(
        './app/Modules/Patient/Assets/js/discontinued-medication.js'
        , 'public/modules/patient/js/discontinued-medication.js');

    mix.scripts(
        './app/Modules/Patient/Assets/js/nextofkin.js'
        , 'public/modules/patient/js/nextofkin.js');

    mix.scripts(
        './app/Modules/Patient/Assets/js/patients.js'
        , 'public/modules/patient/js/patients.js');

});
