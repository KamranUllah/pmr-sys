<?php namespace App\Repositories;

class PatientRepository extends BaseRepository {

    protected $model = 'App\Models\Patient';
    protected $relationships = ['prescriptions'];
}