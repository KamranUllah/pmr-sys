<?php
namespace App\Modules\Patient\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\Patient\Entities\ClinicalNotes;

class ClinicalNotesRepository extends BaseRepository
{

    protected $model = ClinicalNotes::class;
    protected $relationships = ['patient'];

    public function getByPatientId($patientId)
    {
        return $this->model
            ->with($this->relationships)
            ->where([['PatientId', '=', $patientId]])
            ->orderBy('CreatedAt', 'desc')
            ->get();
    }

}