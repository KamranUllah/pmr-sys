<?php
/**
 * Created by PhpStorm.
 * User: emman
 * Date: 25/10/2016
 * Time: 21:03
 */

namespace App\Modules\PAtient\Sidebar;

use App\Modules\User\Entities\Role;
use App\Modules\User\Entities\User;
use Maatwebsite\Sidebar\Badge;
use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\SidebarExtender;
use Sentinel;

class AdminSideBarExtender implements SidebarExtender
{

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group('Patient', function (Group $group) {
            $group->hideHeading();
            $group->weight(100);
            $group->item('Patients', function (Item $item) {
                $item->toggleIcon('fa fa-chevron-down');
                $item->icon('fa fa-user');
                $item->weight(14);

                $item->item('Patient List', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-users');
                    $item->route('admin.patients.index');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    if (Sentinel::check()) {
                        $item->authorize(
                            Sentinel::getUser()->hasAccess('patient.admin.patient.index')
                        );
                    }


                });

                $item->item('Add New Patient', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-user');
                    $item->route('admin.patients.add');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    if (Sentinel::check()) {
                        $item->authorize(
                            Sentinel::getUser()->hasAccess('patient.admin.patient.create')
                        );
                    }


                });



                $item->item('Data Import List', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-wrench');
                    $item->route('admin.patients.view.not.imported');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    if (Sentinel::check()) {
                        $item->authorize(
                            Sentinel::getUser()->hasAccess('patient.admin.patient.getnotImportedPatients')
                        );
                    }


                });


            });

            if (Sentinel::check()) {
                $group->authorize(
                    Sentinel::getUser()->hasAccess('patient.admin.*')
                );
            }
        });

        return $menu;
    }
}
