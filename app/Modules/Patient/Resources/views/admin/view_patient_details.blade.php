<div class="col-md-12 col-sm-12 col-xs-12">
    @if ($patient && !$patient->IsDataImportComplete)
    <button
            class="btn btn-primary btn-sm pull-right mark-import-complete"
            @if (!$patient) disabled @endif
            >
        Mark Data Import As Complete
    </button>
    <span class="processing-spinner hidden">
        <i class="fa fa-spinner fa-spin fa-fw"></i> Processing...
    </span>
    @endif
    @if ($patient && $patient->IsDataImportComplete)
    <span
            class="btn btn-success btn-sm pull-right" disabled>
        <i class="fa fa-check fa-lg fa-fw"></i> Patient Data Imported
    </span>
    @endif
    <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger data-import-errors patient-details-error-message hidden">

                </div>
            </div>
        </div>
    <div class="x_panel">
        <div class="x_title">
            <h2>Patient Detail: {{ $patient->full_name ?? 'New Patient' }}</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <!-- TAB NAVIGATION -->
            <ul class="nav nav-tabs" role="tablist">
                <li @if ($patient) class="active" @endif><a href="#clinical" role="tab" data-toggle="tab">Clinical</a>
                </li>
                <li @if (!$patient) class="active" @endif>
                    <a href="#demographic" role="tab" data-toggle="tab">Key
                        demographic information
                        <span class="data-import-errors text-danger hidden">
                            <i class="fa fa-exclamation fa-fw" aria-hidden="true"></i>
                        </span>
                    </a>
                </li>
                <li><a href="#gp" role="tab" data-toggle="tab">GP &amp; Care Providers
                        <span class="data-import-errors text-danger hidden">
                            <i class="fa fa-exclamation fa-fw" aria-hidden="true"></i>
                        </span>
                    </a>
                </li>
                <li><a href="#kin" role="tab" data-toggle="tab">Contacts &amp; Next of Kin</a></li>
            </ul>
            <!-- TAB CONTENT -->
            <div class="tab-content">
                <div class="@if ($patient) active @endif tab-pane fade @if ($patient) in @endif" id="clinical">
                    @include('patient::admin.partials.clinical')
                </div>
                <div class="@if (!$patient) active @endif tab-pane fade @if (!$patient) in @endif" id="demographic">
                    @include('patient::admin.partials.demographic')
                </div>
                <div class="tab-pane fade" id="gp">
                    @include('patient::admin.partials.gp')
                </div>
                <div class="tab-pane fade" id="kin">
                    @include('patient::admin.partials.kin')
                </div>
            </div>

        </div>
    </div>
</div>

@include('patient::admin.modals.manage_next_of_kin')
@include('patient::admin.modals.manage_allergy')
@include('patient::admin.modals.manage_acute_medication')
@include('patient::admin.modals.manage_current_medication')
@include('patient::admin.modals.manage_discontinued_medication')
