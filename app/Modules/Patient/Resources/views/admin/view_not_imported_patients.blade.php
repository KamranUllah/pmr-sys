{{--{{dump($patients)}}--}}
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Repeat Prescription Not Imported Patient</h2>
            <ul class="nav navbar-right panel_toolbox">
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">

                <table id="not-imported-patients-table" class="table table-striped table-bordered dataTable no-footer">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>DOB</th>
                        <th>Postcode</th>
                        <th>NHS Number</th>
                        <th>Date Added</th>
                        <th>Exempt</th>
                        <th>Exemption Reason</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>


        </div>
    </div>
</div>

@include('patient::admin.modals.patient_notes')

{{--@foreach ($patients as $patient)--}}
    {{--{{dump($patient)}}--}}
{{--{{$patient->PatientForename}}--}}
{{--@endforeach--}}