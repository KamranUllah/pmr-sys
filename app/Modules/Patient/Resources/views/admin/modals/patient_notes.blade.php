<div class="modal fade" id="viewPatientNotesModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body" style="overflow-y: auto;">
                Data
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-primary add-clinical-notes" data-patientid="" style="margin-left: 5px;margin-bottom: 0;">Add Note</button>
                <button type="submit" class="btn btn-default btn-success save-clinical-notes" id="savePatientNotes" style="display: none;">Save Note</button>
                <button type="submit" class="btn btn-default btn-success update-clinical-notes" id="updatePatientNotes" style="display: none;">Update Note</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->