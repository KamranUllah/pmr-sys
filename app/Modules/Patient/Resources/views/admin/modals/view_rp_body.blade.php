<p>&nbsp;</p>
<div class="clearfix"></div>
<div class="history-list" style="height: 400px !important;">
    @if ($prescriptionHistory->count() > 0)
    {{--@for ($key = 0; $key < count($prescriptionHistory); $key++)--}}
    @foreach ($prescriptionHistory as $key => $prescription)
    <div class="panel-group history-panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading history-panel-heading" role="tab" id="headingP{!! $key !!}">
                <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseP{!! $key !!}"
                   aria-expanded="true" aria-controls="collapse{!! $key !!}">
                    <?php
                    $keyssue_date = date('M j Y, H:i', strtotime($prescriptionHistory[$key]->dateraised));
                    $signedoff_date = date('F j Y, G:i ',strtotime($prescriptionHistory[$key]->datedoctorsigned));
                    ?>

                    <span>Date Issued:
                            {{ $keyssue_date }}
                                                @if(isset($prescriptionHistory[$key]->repeatPrescription))
                                                    <span style="margin-left: 20px"><strong>{!! $prescriptionHistory[$key]->repeatPrescription ? '<span class="text-success">Repeat</span>' : 'Private'  !!}</strong></span>
                                                @endif
                            </span>

                   <span class="pull-right"> Status: {{$prescriptionHistory[$key]->prescstatus->Description}}</span>
                </a>
            </div>
            <div id="collapseP{{ $key }}" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingP{{ $key }}">
                <div class="panel-body history-panel-body">
                    <div style="display: inline-block;width: 100%;padding: 5px;">
                        <span><strong>Date Signed Off:</strong> {{ $signedoff_date ?: 'Not yet signed off' }}</span>
                        <span class="pull-right"><a class="btn btn-primary"  style="margin-bottom: 0;" href="/repeatprescription/admin/repeat/{{$prescriptionHistory[$key]->prescriptionid}}" target="_blank"> View Full </a></span>
                    </div>
                    @foreach($prescriptionHistory[$key]->prescriptionendorsements as $endorsement)
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <ul class="list-unstyled">
                                <li><strong>Prescribed Item(s):</strong> {{ $endorsement->NM }}</li>
                                <li><strong>Dosage:</strong> {{ $endorsement->Directions1 }}</li>
                                <li><strong>Directions:</strong> {{ $endorsement->Directions2 }}</li>
                                <li><strong>Amount:</strong> {{ $endorsement->Quantity }}</li>
                                {{--<li><strong>Issued By:</strong>--}}
                                    {{--<br/>{{ $endorsement->doctorName }}</li>--}}
                                <li><strong>Off
                                        License:</strong> {{ $endorsement->offLicense ? 'Yes' : 'No' }}</li>
                                <li><strong>Off License
                                        Notes:</strong> {{ $endorsement->offLicenseNotes ?? 'N/A' }}</li>
                            </ul>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @else
    <p>&nbsp;</p>
    <p class='text-center'>No repeat-prescription history available</p>
    @endif
</div>