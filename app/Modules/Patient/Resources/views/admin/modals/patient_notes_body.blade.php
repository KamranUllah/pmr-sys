<p>&nbsp;</p>
<div class="clearfix"></div>

<div class="history-list" style="height: 400px !important;">
    @if ($clinicalNotes->count() > 0)
    {{--@for ($key = 0; $key < count($clinicalNotes); $key++)--}}
    @foreach ($clinicalNotes as $key => $note)
    <div class="panel-group history-panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading history-panel-heading" role="tab" id="headingP{!! $key !!}">
                <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseP{!! $key !!}"
                   aria-expanded="true" aria-controls="collapse{!! $key !!}">
                    Date Issued:
                    <span>{{ date('M j Y, h:i a', strtotime($clinicalNotes[$key]->CreatedAt)) }}</span>
                    <span class='pull-right delete-note' data-patientid="{{ $note->PatientId }}" data-clinical-note-id="{{ $note->ClinicalNoteId }}" style="padding:0 5px;cursor:pointer;"><i class="glyphicon glyphicon-trash"></i></span>
                    <span class='pull-right edit-note' data-patientid="{{ $note->PatientId }}" data-clinical-note-id="{{ $note->ClinicalNoteId }}" style="padding:0 5px;cursor:pointer;"><i class="glyphicon glyphicon-edit"></i></span>
                </a>
            </div>
            <div id="collapseP{{ $key }}" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="headingP{{ $key }}">
                <div class="panel-body history-panel-body">
                    <p><strong>Patient ID:</strong> {{ $note->PatientId }}</p>
                    <p><strong>Added by:</strong> {{ Sentinel::findById($note->UserId)->email}}(#{{$note->UserId}})</p>
                    <p><strong>Note:</strong></p>
                    <p>{{$note ->Note}}</p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @else
    <p>&nbsp;</p>
    <p class='text-center'>No notes available</p>
    @endif
</div>