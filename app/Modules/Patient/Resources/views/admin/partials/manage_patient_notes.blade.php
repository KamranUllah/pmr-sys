{!! BootForm::open()->action($clinicalNotes ? route('admin.patients.clinicalnotes.update', [$clinicalNotes->ClinicalNoteId]) : route('admin.patients.clinicalnotes.store'))->role('form')->id('manageClinicalNotes') !!}
<div class="generalError">
</div>
<div class="form-group">
    <label class="control-label" for="patientId">PatientId: <span id="patientId">{{$clinicalNotes->PatientId ?? null}}</span></label>
    {!! BootForm::hidden('patientIdData', 'patientIdData')->id('patientIdData')->rows(5)->value($clinicalNotes->PatientId ?? null) !!}

</div>
@if($clinicalNotes)
<div class="form-group">
    <label class="control-label" for="patientId">NoteId: <span id="ClinicalNoteId">{{$clinicalNotes->ClinicalNoteId ?? null}}</span></label>
    {!! BootForm::hidden('NoteId', 'NoteId')->id('NoteId')->rows(5)->value($clinicalNotes->ClinicalNoteId ?? null) !!}
</div>
@endif
{!! BootForm::textarea('Notes', 'Notes')->id('Notes')->rows(5)->value($clinicalNotes->Note ?? null) !!}
{!! BootForm::close() !!}