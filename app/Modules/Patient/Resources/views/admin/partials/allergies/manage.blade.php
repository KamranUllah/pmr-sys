{!! BootForm::open()->action($allergy ? route('admin.patients.allergy.update', [$allergy->PatientAllergyId]) : route('admin.patients.allergy.store'))->role('form')->id('manageAllergyForm') !!}
{!! BootForm::text('Allergy name', 'AllergyName')->value($allergy->AllergyName ?? null) !!}
{!! BootForm::textarea('Description', 'AllergyDescription')->rows(4)->value($allergy->AllergyDescription ?? null) !!}
{!! BootForm::textarea('Certainty', 'Certainty')->rows(4)->value($allergy->Certainty ?? null) !!}
{!! BootForm::textarea('Severity', 'Severity')->rows(4)->value($allergy->Severity ?? null) !!}
{!! BootForm::close() !!}