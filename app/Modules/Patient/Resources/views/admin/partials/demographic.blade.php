<p>&nbsp;</p>
{!! BootForm::open()->role('form')->class('form-horizontal')->action($patient ? route('admin.patients.update', [$patient->PatientId]) : route('admin.patients.add'))->id('patientDetailsForm') !!}
<div class="row">
    <div class="col-md-12">
        <p><strong>Last Updated:</strong> {{ $patientLastUpdated ? $patientLastUpdated->created_at->timezone('Europe/London')->format('d/m/Y h:i a') .
        ($patientLastUpdated->user->first_name != '' || $patientLastUpdated->user->last_name != '' ?
            ' by ' . trim($patientLastUpdated->user->first_name . ' ' . $patientLastUpdated->user->last_name) : '')
            : 'N/A' }}</p>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        {!! BootForm::select('Title', 'PatientTitle')->options([
        'null' => 'Select a title',
        'Mr' => 'Mr',
        'Miss' => 'Miss',
        'Ms' => 'Ms',
        'Mrs' => 'Mrs',
        'Dr' => 'Dr'
         ])->required()->select($patient->PatientTitle ?? 'null') !!}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {!! BootForm::text('First Name', 'PatientForename')->value($patient->PatientForename ?? null) !!}
    </div>
    <div class="col-md-6">
        {!! BootForm::text('Last Name', 'PatientSurname')->value($patient->PatientSurname ?? null) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        {!! BootForm::select('Gender', 'PatientSex')->options($genders)->select(1)->required()->select($patient->PatientSex ?? 1) !!}
    </div>
    <div class="col-md-6">
        {!! BootForm::text('NHS Number', 'NHSNumber')->value($patient->NHSNumber ?? null) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        {!! BootForm::text('Date of Birth', 'PatientDOB')->value($patient->PatientDOB ?? Carbon\Carbon::now()->format('d/m/Y')) !!}
    </div>
    <div class="col-md-6">
        {!! BootForm::select('Language', 'LanguagePreferenceId')->options($languages)->select($patient->LanguagePreferenceId ?? 41) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <h4>Home Address</h4>
        {!! BootForm::text('Line 1', 'PatientAddress1')->value($patient->PatientAddress1 ?? null) !!}
        {!! BootForm::text('Line 2 (Optional)', 'PatientAddress2')->value($patient->PatientAddress2 ?? null) !!}
        {!! BootForm::text('Town / City', 'PatientAddress3')->value($patient->PatientAddress3 ?? null) !!}
        {!! BootForm::text('County', 'PatientAddress4')->value($patient->PatientAddress4 ?? null) !!}
        {!! BootForm::text('Postcode', 'PatientAddress5')->value($patient->PatientAddress5 ?? null) !!}
        {!! BootForm::text('Phone Number', 'PatientMobile')->value($patient->PatientMobile ?? null) !!}
    </div>

    <div class="col-md-6">
        <h4>Correspondence Address</h4>

        {!! BootForm::text('Line 1', 'AlternateAddress1')->value($patient->AlternateAddress1 ?? null) !!}
        {!! BootForm::text('Line 2 (Optional)', 'AlternateAddress2')->value($patient->AlternateAddress2 ?? null) !!}
        {!! BootForm::text('Town / City', 'AlternateAddress3')->value($patient->AlternateAddress3 ?? null) !!}
        {!! BootForm::text('County', 'AlternateAddress4')->value($patient->AlternateAddress4 ?? null) !!}
        {!! BootForm::text('Postcode', 'AlternateAddress5')->value($patient->AlternateAddress5 ?? null) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {!! BootForm::textarea('Consent', 'Consent')->rows(4)->value($patient->Consent ?? null) !!}
    </div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div class="row">
    <div class="col-md-12">
        <div class="center-block">
            {!! BootForm::button('Save')->class('btn btn-primary btn-block')->type('submit') !!}
        </div>
    </div>
</div>
{!! BootForm::close() !!}