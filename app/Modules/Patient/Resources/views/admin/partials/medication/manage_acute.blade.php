{!! BootForm::open()->action($medication ? route('admin.patients.acutemedication.update', [$medication->MedicationId]) : route('admin.patients.acutemedication.store'))->role('form')->id('manageAcuteMedicationForm') !!}
{!! BootForm::text('Name', 'Name')->id('AcuteName')->value($medication->Name ?? null) !!}
<div class="row">
    <div class="col-md-4">
        {!!
        BootForm::text('Date Last Prescribed','DateLastPrescribed')
        ->id('DateLastPrescribed')
        ->value($medication->DateLastPrescribed ?? null)
        !!}
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
</div>
{!! BootForm::textarea('Dosage', 'Dosage')->id('AcuteDosage')->rows(5)->value($medication->Dosage ?? null) !!}
{!! BootForm::text('Packet Size', 'PacketSize')->id('AcutePacketSize')->rows(4)->value($medication->PacketSize ?? null) !!}
{!! BootForm::select('Enabled', 'Enabled')->id('AcuteEnabled')->options([1 => 'Yes', 0 => 'No'])->select($medication->Enabled ?? 0) !!}
{!! BootForm::close() !!}
