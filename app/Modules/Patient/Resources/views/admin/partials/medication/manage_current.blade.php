{!! BootForm::open()->action($medication ? route('admin.patients.currentmedication.update', [$medication->MedicationId]) : route('admin.patients.currentmedication.store'))->role('form')->id('manageCurrentMedicationForm') !!}
{!! BootForm::text('Name', 'Name')->id('CurrentName')->value($medication->Name ?? null) !!}
{!! BootForm::textarea('Dosage', 'Dosage')->id('CurrentDosage')->rows(5)->value($medication->Dosage ?? null) !!}
{!! BootForm::text('Packet Size', 'PacketSize')->id('CurrentPacketSize')->rows(4)->value($medication->PacketSize ?? null) !!}
{!! BootForm::select('Enabled', 'Enabled')->id('CurrentEnabled')->options([1 => 'Yes', 0 => 'No'])->select($medication->Enabled ?? 1) !!}
{!! BootForm::close() !!}