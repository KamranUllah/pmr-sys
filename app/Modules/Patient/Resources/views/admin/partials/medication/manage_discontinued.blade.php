{!! BootForm::open()->action($medication ? route('admin.patients.discontinuedmedication.update', [$medication->MedicationId]) : route('admin.patients.discontinuedmedication.store'))->role('form')->id('manageDiscontinuedMedicationForm') !!}
{!! BootForm::text('Name', 'Name')->id('DiscontinuedName')->value($medication->Name ?? null) !!}
{!! BootForm::textarea('Dosage', 'Dosage')->id('DiscontinuedDosage')->rows(5)->value($medication->Dosage ?? null) !!}
{!! BootForm::text('Packet Size', 'PacketSize')->id('DiscontinuedPacketSize')->rows(4)->value($medication->PacketSize ?? null) !!}
{!! BootForm::select('Enabled', 'Enabled')->id('DiscontinuedEnabled')->options([1 => 'Yes', 0 => 'No'])->select($medication->Enabled ?? 0) !!}
{!! BootForm::close() !!}