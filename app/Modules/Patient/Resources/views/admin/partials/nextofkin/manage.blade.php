{!! BootForm::open()->action($nextOfKin ? route('admin.patients.nextofkin.update', [$nextOfKin->NextOfKinId]) : route('admin.patients.nextofkin.store'))->role('form')->id('manageNextOfKinForm') !!}
{!! BootForm::text('Name', 'Name')->value($nextOfKin->Name ?? null) !!}
{!! BootForm::text('Telephone', 'Telephone')->value($nextOfKin->Telephone ?? null) !!}
{!! BootForm::text('Relationship', 'Relationship')->value($nextOfKin->Relationship ?? null) !!}
{!! BootForm::close() !!}