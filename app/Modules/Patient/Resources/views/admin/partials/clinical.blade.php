<p>&nbsp;</p>
{{--<div class="row">
    <div class="col-md-6">
        <p><strong>Created By:</strong> [ Name Of GP ]</p>
    </div>
    <div class="col-md-6">
        <p><strong>Surgery:</strong> [ Surgery Address ]</p>
    </div>
</div>
<p>&nbsp;</p>--}}
<div class="row">
    <div class="col-md-12">
        <p><strong>Last Updated:</strong> {{ $allergyLastUpdated ? $allergyLastUpdated->created_at->timezone('Europe/London')->format('d/m/Y h:i a') .
        ($allergyLastUpdated->user->first_name != '' || $allergyLastUpdated->user->last_name != '' ?
        ' by ' . trim($allergyLastUpdated->user->first_name . ' ' . $allergyLastUpdated->user->last_name) : '')
        : 'N/A' }}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-8">
            <h2>Allergies and Adverse Reactions</h2>
        </div>
        <div class="col-md-4 text-right" style="padding-top: 10px; padding-right: 0px;">
            <a href="javascript:void(0);" class="btn btn-xs btn-success add-allergy @if (!$patient) disabled @endif"
               data-patient-id="{{ $patient->PatientId ?? null }}"><i class="glyphicon glyphicon-edit"></i> Add</a>
            {{--<a href="javascript:void(0);" class="btn btn-xs btn-primary upload-allergy @if (!$patient) disabled @endif"
               data-patient-id="{{ $patient->PatientId ?? null }}"><i class="glyphicon glyphicon-edit"></i> Upload</a>--}}
        </div>
        <table @if ($patient) id="patientAllergyTable" @endif class="table table-bordered table-hover table-striped" style="width: 100%">
            <thead>
            <tr>
                <th class="col-md-1">Id</th>
                <th class="col-md-1">Name</th>
                <th class="col-md-2">Description</th>
                <th class="col-md-2">Certainty</th>
                <th class="col-md-2">Severity</th>
                <th class="col-md-2">Date Added</th>
                <th class="col-md-2">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="7" class="text-center">
                    @if(!$patient)
                        <strong>Please create a new patient in order to add allergy details</strong>
                    @else
                        <strong>There are no recorded allergies available for the patient</strong>
                    @endif
                </td>
            </tr>
            </tbody>

        </table>
    </div>
</div>
<p>&nbsp;</p>
<div class="row">
    <div class="col-md-12">
        <p><strong>Last Updated:</strong> {{ $lastMedicationLastUpdated ? $lastMedicationLastUpdated->created_at->timezone('Europe/London')->format('d/m/Y h:i a') .
        ($lastMedicationLastUpdated->user->first_name != '' || $lastMedicationLastUpdated->user->last_name != '' ?
            ' by ' . trim($lastMedicationLastUpdated->user->first_name . ' ' . $lastMedicationLastUpdated->user->last_name) : '')
            : 'N/A' }}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <h2>Acute Medications {{--(for the 12 month period [start date] to [end date])--}}</h2>
            </div>
            <div class="col-md-4 text-right" style="padding-top: 10px;">
                <a href="javascript:void(0);"
                   class="btn btn-xs btn-success add-acute-medication @if (!$patient) disabled @endif"><i
                            class="glyphicon glyphicon-edit"></i> Add</a>
                {{--<a href="javascript:void(0);"
                   class="btn btn-xs btn-primary upload-medication @if (!$patient) disabled @endif"><i
                            class="glyphicon glyphicon-edit"></i> Upload</a>--}}
            </div>
        </div>

        <table @if ($patient) id="patientAcuteMedicationTable" @endif class="table table-bordered table-hover table-striped" style="width: 100%">
            <thead>
            <tr>
                <th class="col-md-2">Date Prescribed</th>
                <th class="col-md-1">Id</th>
                <th>Medicine Item</th>
                <th class="col-md-2">Dosage</th>
                <th class="col-md-1">Quantity</th>
                <th class="col-md-1">Enabled</th>
                <th class="col-md-2">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="7" class="text-center">
                    @if(!$patient)
                        <strong>Please create a new patient in order to add medication</strong>
                    @else
                        <strong>There are no recorded medications available for the patient</strong>
                    @endif
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<p>&nbsp;</p>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <h2>Current Repeat Medicines</h2>
            </div>
            <div class="col-md-4 text-right" style="padding-top: 10px;">
                <a href="javascript:void(0);"
                   class="btn btn-xs btn-success add-current-medication @if (!$patient) disabled @endif"><i
                            class="glyphicon glyphicon-edit"></i> Add</a>
                {{--<a href="javascript:void(0);"
                   class="btn btn-xs btn-primary upload-medication @if (!$patient) disabled @endif"><i
                            class="glyphicon glyphicon-edit"></i> Upload</a>--}}
            </div>
        </div>

        <table @if ($patient) id="patientCurrentMedicationTable" @endif class="table table-bordered table-hover table-striped" style="width: 100%">
            <thead>
            <tr>
                <th class="col-md-2">Date Prescribed</th>
                <th class="col-md-1">Id</th>
                <th>Medicine Item</th>
                <th class="col-md-2">Dosage</th>
                <th class="col-md-1">Quantity</th>
                <th class="col-md-1">Enabled</th>
                <th class="col-md-2">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="7" class="text-center">
                    @if(!$patient)
                        <strong>Please create a new patient in order to add medication</strong>
                    @else
                        <strong>There are no recorded medications available for the patient</strong>
                    @endif
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


<p>&nbsp;</p>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <h2>Discontinued Repeat Medicines</h2>
            </div>
            <div class="col-md-4 text-right" style="padding-top: 10px;">
                <a href="javascript:void(0);"
                   class="btn btn-xs btn-success add-discontinued-medication @if (!$patient) disabled @endif"><i
                            class="glyphicon glyphicon-edit"></i> Add</a>
                {{--<a href="javascript:void(0);"
                   class="btn btn-xs btn-primary upload-medication @if (!$patient) disabled @endif"><i
                            class="glyphicon glyphicon-edit"></i> Upload</a>--}}
            </div>
        </div>

        <table @if ($patient) id="patientDiscontinuedMedicationTable" @endif class="table table-bordered table-hover table-striped" style="width: 100%">
            <thead>
            <tr>
                <th class="col-md-2">Date Prescribed</th>
                <th class="col-md-1">Id</th>
                <th>Medicine Item</th>
                <th class="col-md-2">Dosage</th>
                <th class="col-md-1">Quantity</th>
                <th class="col-md-1">Enabled</th>
                <th class="col-md-2">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="7" class="text-center">
                    @if(!$patient)
                        <strong>Please create a new patient in order to add a discontinued medication</strong>
                    @else
                        <strong>There are no recorded discontinued medications available for the patient</strong>
                    @endif
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

