<p>&nbsp;</p>
<div class="row">
    <div class="col-md-12">
        <p><strong>Last Updated:</strong> {{ $nextOfKinLastUpdated ? $nextOfKinLastUpdated->created_at->timezone('Europe/London')->format('d/m/Y h:i a') .
        ($nextOfKinLastUpdated->user->first_name != '' || $nextOfKinLastUpdated->user->last_name != '' ?
            ' by ' . trim($nextOfKinLastUpdated->user->first_name . ' ' . $nextOfKinLastUpdated->user->last_name) : '')
            : 'N/A' }}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12 text-right" style="padding-top: 10px; padding-right: 0px;">
            <a href="javascript:void(0);" class="btn btn-xs btn-success add-nextofkin @if (!$patient) disabled @endif"
               data-patient-id="{{ $patient->PatientId ?? null }}"><i class="glyphicon glyphicon-edit"></i> Add</a>
            {{--<a href="javascript:void(0);" class="btn btn-xs btn-primary upload-nextofkin @if (!$patient) disabled @endif"
               data-patient-id="{{ $patient->PatientId ?? null }}"><i class="glyphicon glyphicon-edit"></i> Upload</a>--}}
        </div>
        <table id="patientNextOfKinTable" class="table table-bordered table-hover table-striped" style="width: 100%">
            <thead>
            <tr>
                <th class="col-md-4">Name</th>
                <th class="col-md-4">Telephone</th>
                <th class="col-md-2">Relationship</th>
                <th class="col-md-2">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="4" class="text-center">
                    @if(!$patient)
                        <strong>Please create a new patient in order to add next of kin details</strong>
                    @else
                        <strong>There are no recorded next of kin available for the patient</strong>
                    @endif
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>