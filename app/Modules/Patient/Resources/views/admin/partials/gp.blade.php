<p>&nbsp;</p>
{!! BootForm::open()->role('form')->class('form-horizontal')->action($patient ? route('admin.patients.gpdetails.update', [$patient->PatientId]) : null)->id('patientGPDetailsForm') !!}
<div class="row">
    <div class="col-md-12">
        <p><strong>Last Updated:</strong> {{ $patientLastUpdated ? $patientLastUpdated->created_at->timezone('Europe/London')->format('d/m/Y h:i a') .
        ($patientLastUpdated->user->first_name != '' || $patientLastUpdated->user->last_name != '' ?
            ' by ' . trim($patientLastUpdated->user->first_name . ' ' . $patientLastUpdated->user->last_name) : '')
            : 'N/A' }}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p><strong>EPS Enabled:</strong> {{ isset($patient->eps) && null !== $patient->eps ? round($patient->eps->EstimatedEPS, 2) * 100 . '%' : 'N/A' }}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        {!! BootForm::text('First Name', 'GPfirstname')->value($patient->GPfirstname ?? null) !!}
    </div>
    <div class="col-md-6">
        {!! BootForm::text('Last Name', 'GPlastname')->value($patient->GPlastname ?? null) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        {!! BootForm::text('GMC Code', 'GPGMCCode')->value($patient->GPGMCCode ?? null) !!}
    </div>
    <div class="col-md-6">
        {!! BootForm::text('GP Code', 'GPCode')->value($patient->GPCode ?? null) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h4>Surgery Address</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        {!! BootForm::text('Surgery Name', 'SurgeryName')->value($patient->SurgeryName ?? null) !!}
        {!! BootForm::text('Line 1', 'SurgeryAddress1')->value($patient->SurgeryAddress1 ?? null) !!}
        {!! BootForm::text('Line 2 (Optional)', 'SurgeryAddress2')->value($patient->SurgeryAddress2 ?? null) !!}
        {!! BootForm::text('Town / City', 'SurgeryCity')->value($patient->SurgeryCity ?? null) !!}
        {!! BootForm::text('County', 'SurgeryCounty')->value($patient->SurgeryCounty ?? null) !!}
        {!! BootForm::text('Postcode', 'SurgeryPostcode')->value($patient->SurgeryPostcode ?? null) !!}
    </div>
    <div class="col-md-6">
        {!! BootForm::text('Telephone', 'SurgeryPhone')->value($patient->SurgeryPhone ?? null) !!}
        {!! BootForm::text('Email', 'SurgeryEmail')->value($patient->SurgeryEmail ?? null) !!}
        {!! BootForm::text('Fax', 'SurgeryFax')->value($patient->SurgeryFax ?? null) !!}
    </div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div class="row">
    <div class="col-md-12">
        @if ($patient)
            {!! BootForm::button('Save')->class('btn btn-primary btn-block')->type('submit') !!}
        @else
            <div class="alert alert-info">
                <p class="text-center"><strong>Please create a new patient in order to enter their GP details</strong></p>
            </div>
        @endif
    </div>
</div>
{!! BootForm::close() !!}