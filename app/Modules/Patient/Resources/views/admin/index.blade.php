
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Patients <small>View / Search Patients</small></h2>
            <a href="{{ route('admin.patients.add') }}" class="btn btn-sm btn-primary pull-right" style="color: #ffffff">Add New Patient</a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-12 text-center"><h2>Find a Patient</h2></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {!! BootForm::open()->action(route('admin.patients.data'))->role('form')->method('get')->id('patientSearch')->data('parsley-validate', true) !!}
                    <div class="row">
                        <div class="col-md-3">
                            {!! BootForm::text('Patient ID', 'patient_id') !!}
                        </div>
                        <div class="col-md-3">
                            {!! BootForm::text('First name', 'first_name') !!}
                        </div>
                        <div class="col-md-3">
                            {!! BootForm::text('Last name', 'last_name')/*->required()->data('parsley-required-message', 'Patient\'s last name is required')*/ !!}
                        </div>
                        <div class="col-md-3">
                            {!! BootForm::text('Date of Birth', 'dob')/*->required()->data('parsley-required-message', 'Patient\'s date of birth is required')*/ !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            {!! BootForm::select('Gender', 'gender')->options([999 => 'All', 1 => 'Male', 0 => 'Female', -1 => 'Unknown'])->select(999) !!}
                        </div>
                        <div class="col-md-4">
                            {!! BootForm::text('Full Postcode', 'postcode')
                            ->data('trigger', 'blur')
                            //->pattern('/^(GIR|[A-Z]\d[A-Z\d]??|[A-Z]{2}\d[A-Z\d]??)[ ]??(\d[A-Z]{2})$/ui')
                            //->data('parsley-pattern-message', 'Postcode is of an incorrect format')
                            !!}
                        </div>
                        <div class="col-md-4">
                            {!! BootForm::text('NHS number', 'nhs_number') !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            {!! BootForm::button('Search')->type('submit')->class('btn btn-primary') !!}
                            <p>&nbsp;</p>
                        </div>
                    </div>
                    {!! BootForm::close() !!}
                </div>
            </div>
            <table id="patients-table" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                <tr>
                    <th class="col-md-1">Id</th>
                    <th class="col-md-2">First name</th>
                    <th class="col-md-2">Last name</th>
                    <th class="col-md-2">DOB</th>
                    <th class="col-md-1">Gender</th>
                    <th class="col-md-2">NHS Number</th>
                    <th>Exempt</th>
                    <th>Exemption Reason</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>

        </div>
    </div>
</div>

@include('patient::admin.modals.view_rp')
@include('patient::admin.modals.patient_notes')

