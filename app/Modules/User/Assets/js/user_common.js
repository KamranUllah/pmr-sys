$(function () {

    //Change all sub-rule's permission at once (Allow All/Deny All)
    $(".select-all").click(function (e) {
        e.preventDefault();

        var oThis = $(this);
        var setTo = true;
        if(oThis.val() === 'Allow All'){
            oThis.val('Deny All');
            $('#'+$(this).data('panel')+' .li-option').each(function () {
                var oToggle = $(this).find('.toggle');
                oToggle.find(':checkbox').prop('checked', true);
                oToggle.removeClass('btn-danger');
                oToggle.removeClass('off');
                oToggle.addClass('btn-success');
            });
        } else {
            oThis.val('Allow All');
            setTo = false;
            $('#'+$(this).data('panel')+' .li-option').each(function () {
                var oToggle = $(this).find('.toggle');
                oToggle.find(':checkbox').prop('checked', false);
                oToggle.removeClass('btn-success');
                oToggle.addClass('off');
                oToggle.addClass('btn-danger');
            });
        }
    });
});