$(function () {

    //let users = function() {
    $('#users-table').DataTable({
        processing: true,
        destroy: true,
        serverSide: true,
        ajax: NowGP.siteUrl + '/admin/user/users/list',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'email', name: 'email'},
            {data: 'first_name', name: 'first_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

    $('.user-role-tags').selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });
    //};

});