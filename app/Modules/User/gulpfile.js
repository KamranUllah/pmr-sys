const elixir = require('laravel-elixir');
const argv = require('yargs').argv;

require('laravel-elixir-vue-2');

/*if (typeof moduleName === 'undefined') {
    console.log('Plunder more riffiwobbles!');
    return false;
}*/

elixir(function (mix) {

    mix.scripts(
        './app/Modules/User/Assets/js/admin_user.js',
        'public/modules/user/js/admin_user.js',
        './app/Modules/User/Assets/js/user_common.js',
        'public/modules/user/js/user_common.js');

});
