<style>
    .online {
        color:green;
    }

    .offline {
        color:red;
    }
</style>
<?php
if(is_array($apiQuery)){
    $coreAPi = $apiQuery['responsecoreApiUrl'];
}else{
    $coreAPi = $apiQuery;
}


?>
<div class="login_wrapper">
    <div class="animate form login_form">
        <section class="login_content" style="padding:0;">
            <h1>PEARL</h1>
            <div class="col-xs-12">
                <table class="table ">
                    <thead>
                    <tr>
                        <th>Component</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tr>
                        <td>Database</td>
                        <td><strong class="{{$databaseQuery}}">{{$databaseQuery}}</strong></td>
                    </tr>
                    <tr>
                        <td>API</td>
                        <td><strong class="@if ($coreAPi ===200) online @else offline @endif ">{{$coreAPi}}</strong></td>
                    </tr>
                </table>
            </div>

            @if(App::environment() !== 'production')
                <p><strong>Version: {{ config('pmr.pmr_version') }}</strong></p>
            @endif
        </section>
    </div>
</div>