<div class="login_wrapper" style="max-width: 550px;">
	<div class="animate form login_form">
		<section class="login_content" style="padding:0px;">
			<h1>Reset Password</h1>
			{!! Theme::partial('notification') !!}
			<div>
				{!! BootForm::open()->action(route('user.password.reset'))->role('form')->class('form-signin') !!}
				<p>Please enter your email address below.  A new system generated password will be emailed to you.</p>
				<p>Please check your junk mail in case the email does not appear in your inbox.</p>
				{!! BootForm::email('Email Address', 'email')->required()->hideLabel() !!}
				{!! BootForm::button('Reset Password')->type('submit')->class('btn btn-primary btn-block') !!}
				<div class='text-center'><br />{!! link_to_route('user.login', 'Login') !!}</div>
				{!! BootForm::close() !!}
			</div>
			@if(App::environment() !== 'production')
				<p><strong>Version: {{ config('pmr.pmr_version') }}</strong></p>
			@endif
		</section>
	</div>
</div>-