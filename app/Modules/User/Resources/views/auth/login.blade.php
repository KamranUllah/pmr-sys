<div class="login_wrapper">
    <div class="animate form login_form">
        <section class="login_content">
            {!! Theme::partial('notification') !!}
            {!! BootForm::open()->role('form')->action(route('process.user.login')) !!}
            <h1>Login</h1>
            {!! BootForm::text('Username', 'login')->hideLabel()->placeholder('Username') !!}
            {!! BootForm::password('Password', 'password')->hideLabel()->placeholder('Password') !!}
            <div>
                {!! BootForm::button('Login')->type('submit')->class('btn btn-default submit') !!}

            </div>
            <div class='text-center'><br />{!! link_to_route('user.password.reset', 'Forgotten password') !!}</div>
            {!! BootForm::close() !!}
            @if(App::environment() !== 'production')
                <p><strong>Version: {{ config('pmr.pmr_version') }}</strong></p>
            @endif
        </section>
    </div>
</div>