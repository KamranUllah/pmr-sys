<div class="row">
    <div class="col-md-12">
        <h1>User Roles</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="text-right">
            <a href="{!! route('admin.user.role.add') !!}" class="btn btn-default btn-sm">Create Role</a>
        </div>
    </div>
</div>
<p>&nbsp;</p>
<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>User Roles</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-hovered table-striped">
                    <thead>
                    <tr>
                        <th class="col-md-1">Id</th>
                        <th class="col-md-2">Name</th>
                        <th class="col-md-2">Slug</th>
                        <th>Description</th>
                        <th class="col-md-2 text-center">Created On</th>
                        <th class="col-md-2 text-center">Actions</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($roles as $role)
                        <tr>
                            <td>
                                {{ $role->id }}
                            </td>
                            <td>
                                {{ $role->name }}
                            </td>
                            <td>{{ $role->slug }}</td>
                            <td></td>
                            <td class="text-center"></td>
                            <td class="text-center">
                                <a href="{{ route('admin.user.role.edit', [$role->id]) }}" class="btn btn-sm btn-default"><span
                                            class="fa fa-pencil"></span></a>
                                @if(!$role->primary_role || config('app.debug'))
                                <a href="{{ route('admin.user.role.delete', [$role->id]) }}" class="btn btn-sm btn-danger"><span
                                            class="fa fa-trash-o"></span></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>