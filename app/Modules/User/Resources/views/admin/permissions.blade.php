<div class="row">
    <div class="col-md-12">
        <h2>Available Permissions</h2>
    </div>
</div>

<!-- Accordion with No Space example -->
<div class="panel-group pmd-accordion pmd-accordion-nospace"
     id="accordion5" role="tablist"
     aria-multiselectable="true">
    @foreach(Module::enabled() as $module)
        @if(Config::has(strtolower($module->name . '-permissions')))
            @php($permissions = config(strtolower($module->name . '-permissions')))
            @foreach($permissions as $section => $items)

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading-{{ str_slug($section) }}">
                        <h4 class="panel-title">

                            <a data-toggle="collapse"
                               data-parent="#accordion-{{ str_slug($section) }}"
                               href="#collapse-{{ str_slug($section) }}" aria-expanded="true"
                               aria-controls="collapseOne5"
                               data-expandable="false"> {!! $section !!}

                                <i class="material-icons md-dark pmd-sm pmd-accordion-arrow pull-right">keyboard_arrow_down</i>
                            </a>
                        </h4>
                        <input type="button" id="select-all-{{ str_slug($section) }}" class="btn button-default select-all btnselectall"
                               data-panel="collapse-{{ str_slug($section) }}" value="Allow All"  />
                    </div>
                    <div id="collapse-{{ str_slug($section) }}" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading-{{ str_slug($section) }}">
                        <div class="panel-body">
                            <ul class="list-unstyled">
                                @foreach($items as $permission => $details)
                                    @php($checked = (!empty($assigned_permissions) && array_key_exists($permission, $assigned_permissions)) ? true : null)
                                    <li style="margin-bottom: 5px" class="li-option">
                                        {!!
                                            Form::checkbox('permissions['.$permission.']', true, $checked,
                                            [
                                            'class' => 'permissions-toggle',
                                            'data-toggle' => 'toggle',
                                            'data-on' => 'Allow',
                                            'data-off' => 'Deny',
                                             'data-onstyle' => 'success',
                                             'data-offstyle' => 'danger',
                                             'data-width' => '100'
                                            ])
                                        !!}
                                        {{ $details['description'] }}
                                        <div class="clearfix"></div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    @endforeach
</div> <!-- Accordion with No Space example end -->

<style>
    .btnselectall {
        margin-right: 22px;
        position: relative;
        float: right;
        margin-top: -26px;
    }
</style>