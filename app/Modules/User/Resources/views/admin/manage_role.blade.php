<div class="row">
    <div class="col-md-12">
        <h1>@if($role) Edit @else Add @endif Role @if($role)
                <small>{{ $role->name }}</small>@endif</h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        {!! BootForm::open()->role('form') !!}
        <div class="row">
            <div class="col-md-12">
                {!! Form::button(Html::decode('<span class="fa fa-save"></span> Save'), ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
                <a href="{{ route('admin.user.roles') }}" class="btn btn-default">Cancel</a>
                <p>&nbsp;</p>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a aria-controls="home" role="tab" data-toggle="tab" href="#home">Role</a></li>
                    <li><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Permissions</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <div class="row">
                            <p>&nbsp;</p>

                            <div class="col-md-12">
                                <div class='form-group {!! has_error('name', $errors) !!}'>
                                    {!! Form::label('Name') !!}
                                    {!! Form::text('name', $role ? $role->name : false, ['class' => 'form-control']) !!}
                                    {!! get_error('name',$errors) !!}
                                </div>
                                <div class='form-group {!! has_error('slug', $errors) !!}'>
                                    {!! Form::label('Slug') !!}
                                    {!! Form::text('slug', $role ? $role->slug : false, ['class' => 'form-control', 'placeholder' => 'This will be auto populted based on the name', 'disabled' => 'disabled']) !!}
                                    {!! get_error('slug',$errors) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                        <div class="row">
                            <div class="col-md-12">
                                @include('user::admin.permissions')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! BootForm::close() !!}
</div>

