<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Users <small>View a list of users</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a href="{{ route('admin.user.add') }}" class="btn btn-xs" style="color: #000000 !important;">Add User</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <table id="users-table" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                <tr>
                    <th class="col-md-1">Id</th>
                    <th class="col-md-3">Email</th>
                    <th class="col-md-2">First Name</th>
                    <th class="col-md-2">Last Name</th>
                    <th>Updated At</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>