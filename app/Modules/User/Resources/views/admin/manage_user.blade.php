<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Users
                <small>View a list of users</small>
            </h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-12"><h1>@if($user->id) Edit @else Add @endif User</h1></div>
            </div>

            <p>&nbsp;</p>
            {!! BootForm::open()->role('form')->multipart() !!}
            {{--{!! BootForm::open()->action($user->id ? route('edit', [$user]) : route('new'))->role('form')->id('manageUser') !!}--}}
            <div class='form-group text-right'>
                {!! Form::button('Save User',['type' => 'submit','class'=>'btn btn-primary btn-sm']) !!}
                <a href='{{ route('admin.user.list') }}' class='btn btn-default btn-sm'></i>
                    Cancel</a>
                <p>&nbsp;</p>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- TAB NAVIGATION -->
                    <ul class="nav nav-tabs bar_tabs" role="tablist">
                        <li class="active"><a href="#user" role="tab" data-toggle="tab">User</a></li>
                        @if (!$user->primary_admin)
                            <li><a href="#permission" role="tab" data-toggle="tab">Permissions</a></li>
                        @endif
                    </ul>
                    <!-- TAB CONTENT -->
                    <div class="tab-content">
                        <div class="active tab-pane fade in" id="user">
                            <h2 class="page-header">Details</h2>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class='form-group {!! has_error('email', $errors) !!}'>
                                        {!! Form::label('Email') !!}
                                        {!! Form::email('email',request()->old('email', $user->email),['class'=>'form-control']) !!}
                                        {!! get_error('email',$errors) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class='form-group {!! has_error('first_name', $errors) !!}'>
                                        {!! Form::label('First Name') !!}
                                        {!! Form::text('first_name',request()->old('first_name', $user->first_name),array('class'=>'form-control')) !!}
                                        {!! get_error('first_name',$errors) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='form-group {!! has_error('last_name', $errors) !!}'>
                                        {!! Form::label('Last Name') !!}
                                        {!! Form::text('last_name',request()->old('last_name', $user->last_name),array('class'=>'form-control')) !!}
                                        {!! get_error('last_name',$errors) !!}
                                    </div>
                                </div>
                            </div>

                            <h2 class="page-header">Authorization</h2>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class='form-group'>
                                        {!! Form::label('Roles') !!}
                                        {!! Form::select('roles[]', $roles, request()->old('roles[]', $user->roles->pluck('id')->all()), ['class' => 'form-control user-role-tags', 'multiple' => true]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if (!$user->primary_admin)
                                        {!! Form::label('Activated') !!}
                                        {!! Form::select('activated', ['0' => 'No', '1' => 'Yes'], $activated, ['class' => 'form-control ']) !!}
                                    @else
                                        {!! Form::label('Activated') !!}<br/>
                                        <p><strong>Yes</strong> - <span class="text-info"><em><small>You cannot de-activate yourself</small></em></span>
                                        </p>
                                    @endif
                                </div>
                            </div>

                            <h2 class="page-header">Authentication</h2>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class='form-group {!! has_error('password', $errors) !!}'>
                                        {!! Form::label('Password') !!}
                                        {!! Form::password('password', ['class'=>'form-control']) !!}
                                        {!! get_error('password',$errors) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='form-group {!! has_error('password_confirmation', $errors) !!}'>
                                        {!! Form::label('Password Confirmation') !!}
                                        {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
                                        {!! get_error('password_confirmation',$errors) !!}
                                    </div>
                                </div>
                            </div>

                        </div>
                        @if (!$user->primary_admin)
                            <div class="tab-pane fade" id="permission">
                                @include('user::admin.permissions')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <p>&nbsp;</p>
            {!! BootForm::close() !!}
        </div>
    </div>
</div>