<?php

namespace App\Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'            =>
                "required|min:2|max:32|alpha|"
                . "regex:/^[a-zA-Zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",
            'last_name'             =>
                "required:min:2|max:32|alpha|"
                . "regex:/^[a-zA-Zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúû"
                . "üųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪ"
                . "ŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",
            'email'                 => 'required|email|unique:users,email',
            'username'              => 'sometimes',
            'password'              => 'required|min:6|max:15|confirmed|' .
                'regex:/^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[!#$%&()+,\-.:;<=>?@[\]\^\_q`{}~])' .
                '(?=\S*[\d])(?!\S*([A-Za-z0-9!#$%&()+,\-.:;<=>?@[\]\^\_q`{}~])\1{2})\S*$/',
            'password_confirmation' => 'required',
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get custom validation rules error messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.regex'           => 'First name is of an incorrect format',
            'last_name.regex'            => 'Last name is of an incorrect format',
            'email_register.required'    => 'The email address is required',
            'email_register.email'       => 'Email is of an incorrect format',
            'password_register.required' => 'The password is required',
        ];
    }

}
