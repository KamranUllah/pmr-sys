<?php

Route::group(['middleware' => 'web', 'namespace' => 'App\Modules\User\Http\Controllers'], function () {
    //Route::get('/', 'UserController@index');

    /**
     * ================================================================================================================
     * Admin Routes
     * ================================================================================================================
     */
    Route::group(['prefix' => 'admin'], function () {

        Route::group(['prefix' => 'user'], function () {

            Route::group(['prefix' => 'users'], function () {

                Route::get('/', [
                    'as'   => 'admin.user.list',
                    'uses' => 'Admin\UserController@index',
                ]);

                Route::get('new', [
                    'as'   => 'admin.user.add',
                    'uses' => 'Admin\UserController@create',
                ]);
                Route::post('new', 'Admin\UserController@store');

                Route::get('edit/{user}', [
                    'as'   => 'admin.user.edit',
                    'uses' => 'Admin\UserController@edit',
                ]);
                Route::post('edit/{user}', 'Admin\UserController@update');

                Route::get('delete/{user}', [
                    'as'   => 'admin.user.delete',
                    'uses' => 'Admin\UserController@destroy',
                ]);

                Route::get('list', [
                    'as'   => 'admin.user.list.ajax',
                    'uses' => 'Admin\UserController@getListOfUsers',
                ]);
            });

            Route::group(['prefix' => 'roles'], function () {

                Route::get('/', [
                    'as'   => 'admin.user.roles',
                    'uses' => 'Admin\RoleController@index',
                ]);

                Route::get('create-role', [
                    'as'   => 'admin.user.role.add',
                    'uses' => 'Admin\RoleController@create',
                ]);

                Route::post('create-role', [
                    'as'   => 'admin.user.role.do.add',
                    'uses' => 'Admin\RoleController@store',
                ]);

                Route::get('{role}/edit', [
                    'as'   => 'admin.user.role.edit',
                    'uses' => 'Admin\RoleController@edit',
                ]);

                Route::post('{role}/edit', [
                    'as'   => 'admin.user.role.do.edit',
                    'uses' => 'Admin\RoleController@update',
                ]);

                Route::get('delete-role/{id}', [
                    'as'   => 'admin.user.role.delete',
                    'uses' => 'Admin\RoleController@delete',
                ]);

            });

        });

    });


    // Authentication
    Route::get('login', [
        'as'   => 'user.login',
        'uses' => 'AuthController@login',
    ]);

    Route::post('login', [
        'as'   => 'process.user.login',
        'uses' => 'AuthController@postLogin',
    ]);

    Route::get('logout', [
        'as'   => 'user.logout',
        'uses' => 'AuthController@logout',
    ]);

    Route::get('/forgot-password', [
        'as' => 'user.password.reset',
        'uses' => 'AuthController@resetPassword',
    ]);

    Route::get('/session-timeout-logout', [
        'as' => 'user.session.timeout.logout',
        'uses' => 'AuthController@userIdleSessionTimeOut'
    ]);

    Route::post('/forgot-password', 'AuthController@resetPassword');

    // Authentication
    Route::get('status', [
        'as'   => 'system.status',
        'uses' => 'AuthController@SystemStatus',
    ]);
});
