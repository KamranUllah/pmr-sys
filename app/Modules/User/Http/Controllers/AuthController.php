<?php

namespace App\Modules\User\Http\Controllers;

use App\Modules\Core\Http\Controllers\FrontEndController;
use App\Modules\User\Http\Requests\LoginRequest;
use App\Modules\RepeatPrescription\Repositories\PrescriptionRepository;
use Carbon\Carbon;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Response;
use Sentinel;
use Validator;
use Mail;
use Redirect;
use Reminder;
use Activity;
use DB;
use App\Mail\UserPasswordResetMail;
use App\Services\NowGP\Data\DrNowV2API;



class AuthController extends FrontEndController
{
    private $auth;

    /**
     * @var DrNowV2API
     */
    private $data;

    /**
     * @var prescriptions
     */
    private $prescriptions;

    public function __construct(
        DrNowV2API $DrNowV2API,
        PrescriptionRepository $prescriptions
    )
    {
        parent::__construct();

        $this->DrNowV2API = $DrNowV2API;

        $this->prescriptions = $prescriptions;

        $this->middleware('auth.guest', ['only' => ['login']]);

        $this->setupTheme('gentella-admin', 'login');
    }

    /**
     * @return mixed
     */
    public function login()
    {
        $this->theme->prependTitle('Login');
        return $this->theme->of('user::auth.login')->render();
    }

    /**
     * @param LoginRequest $request
     *
     * @return AuthController|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(LoginRequest $request)
    {

        try {
            $credentials = $request->only(['login', 'password']);

            $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL)
                ? 'email'
                : 'username';

            $request->merge([
                $login_type => $request->input('login'),
            ]);

            // Get user by email first.
            $user = Sentinel::getUserRepository()->findByCredentials([$login_type => $request->get('login')]);

            $remember = (bool)$request->get('remember', false);

            if (Sentinel::authenticate($credentials, $remember)) {

                // Check if the user has a last password change password expiry set.
                if (null === $user->last_password_change || null === $user->password_expiry_date) {
                    Sentinel::getUserRepository()->update($user, [
                        'last_password_change' => Carbon::now(),
                        'password_expiry_date' => Carbon::now()->addMonths(config('core.password_reset_timeframe')),
                    ]);
                }

                auditLog('User has logged in');

                flash()->success('Login has been successful!');

                // Build EPS Table
                getPatientEpsStatus();

                //Moved sent prescriptions older than 14 days to delivered status
                $this->prescriptions->sentToDelivered();

                //Moved rejected prescriptions older than 30 days to pharmacy declined status
                $this->prescriptions->rejectedToDeclined();

//                //Check Endorsement count on Prescription
//                $this->prescriptions->updateEndorsementCount();

                if (Sentinel::getUser()->inRole('super-administrator') || Sentinel::getUser()->inRole('administrator')) {
                    $redirect = redirect()->intended('/admin/main');
                } else {
                    $redirect = redirect()->intended('/admin/dashboard');
                }

                if ($request->ajax()) {
                    return Response::json([
                        'success'  => true,
                        'user'     => request()->user(),
                        'redirect' => $redirect,
                    ], 200);
                }

                return $redirect;
            }

            $errors = 'Failed to login. Please try again!';
        } catch (NotActivatedException $e) {
            $errors = 'Account is not activated.  Please contact an administrator to have your account activated.';

            //return redirect()->to('reactivate')->with('user', $e->getUser());
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $minutes = floor($delay / 60);

            //$errors = "Your account is blocked for {$delay} second(s).";
            $errors = "Your account is blocked for {$minutes} minutes(s).";
        }

        flash()->error($errors);

        return redirect()->back()->withInput()->withErrors($errors);
    }


    public function userIdleSessionTimeOut() {

        auditLog('User has been logged out due to their session timing out');

        if (request()->ajax()) {
            return response('You were logged out as your session had expired!', 401);
        }

        Sentinel::logout();

        flash()->error('You were logged out as your session had expired!');
        return redirect()->route('user.login');
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        auditLog('User has logged out');
        Sentinel::logout();
        flash()->success('You have been logged out!');
        return redirect()->to('login');
    }


    public function resetPassword()
    {
        if (request()->method() === 'POST') {
            $rules = [
                'email' => 'required|email',
            ];

            $validator = Validator::make(request()->all(), $rules);

            if ($validator->fails()) {
                return Redirect::back()
                    ->withInput()
                    ->withErrors($validator);
            }

            $email = request()->get('email');

            $user = Sentinel::findByCredentials(compact('email'));



            if (!$user) {
                flash()->info('We do not have the requested user in our records! .<br />Please try again!');
                return Redirect::back();
            }

            $reminder = Reminder::create($user);

            $generatedPassword = str_random(8);

//            dump($reminder);
//
//            dump($generatedPassword);

            Sentinel::getUserRepository()->update($user, ['password' => $generatedPassword]);

            Mail::to($user->email)->send(new UserPasswordResetMail($user, $generatedPassword));


            /*$sent = Mail::send('user::emails.new_password', compact('user', 'reminder', 'generatedPassword'),
                    function ($m) use ($user) {
                        $m->from('no-reply@nowhealthcaregroup.com');
                        $m->to($user->email)->subject('Password Reset');
                    });*/

            /*if (!$sent) {
                flash()->error('Failed to send reset password email.');
                return Redirect::to('login');
            }*/


            flash()->info('If you have been found, an email containing your new password will have been emailed ' .
                'to you. Please check your junk mail just in case our email got there!');
            return redirect()->route('user.login');

        }

        $this->theme->prependTitle('Reset Password');
        return $this->theme->of('user::auth.reset_password')->render();
    }

    public function SystemStatus(){

        $databaseQuery = false;
        $statusCode = 200;
        try {
            DB::connection()->getPdo();
            if(DB::connection()->getDatabaseName()){
                $databaseQuery = 'online';
            }
        } catch (\Exception $e) {
            $databaseQuery = 'offline';
        }

        $apiQuery = false;

        try{
            $apiQuery = $this->DrNowV2API->testAllApi();

        } catch (\Exception $e) {
            $apiQuery = $e->getResponse()->getStatusCode();
        }

        if( $databaseQuery == 'offline' || $apiQuery !== 200 ){
            $statusCode = 500;
        }

        $this->theme->prependTitle('Status Page');
        return $this->theme->of('user::status', compact('databaseQuery','apiQuery'))->render($statusCode);

    }
}