<?php

namespace App\Modules\User\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use Activation;
use App\Modules\User\Entities\Role;
use App\Modules\User\Entities\User;
use App\Modules\User\Http\Requests\CreateUserRequest;
use App\Modules\User\Http\Requests\EditUserRequest;
use JavaScript;
use Sentinel;
use Reminder;
use Yajra\Datatables\Datatables;

class UserController extends BackEndController
{
    protected $roles;
    protected $users;

    public function __construct(User $user, Role $role)
    {
        parent::__construct();
        $this->users = Sentinel::getUserRepository();
        $this->roles = Sentinel::getRoleRepository();

        $requiredJs = [
            'admin_user.js',
            'user_common.js'
        ];

        $this->addModuleJSFiles($requiredJs, 'user', true);
    }

    /**
     * @title User Listing
     * @description View list of registered users.
     *
     * @return mixed
     */
    public function index()
    {
        auditLog('An administrator has accessed the users list page: ' . __METHOD__);

        $this->theme->prependTitle('User List');
        $this->theme->breadcrumb()->add('Admin Dashboard', route('admin.main'))->add('Users');

        return $this->theme->of('user::admin.admin_index')->render();
    }

    /**
     * @title User Creation
     * @desc Allow user / role to create new users
     *
     * @return mixed
     */
    public function create()
    {

        auditLog('User has accessed the create user page: ' . __METHOD__);

        $user = new User;
        $assigned_permissions = [];
        $roles = collect($this->roles->all())->reject(function($role) {
            if (!LoggedInUser()->inRole('super-administrator')) {
                return $role->slug === 'super-administrator';
            }
        })->pluck('name', 'id');

        $activated = Sentinel::getActivationRepository()->completed($user) ? "1" : "0";
        $this->theme->prependTitle('User List');
        $this->theme->breadcrumb()->add('Users', url('admin/user/users'))->add('Add User');

        return $this->theme->of(
            'user::admin.manage_user',
            compact('roles', 'user', 'assigned_permissions', 'activated')
        )->render();
    }

    public function store(CreateUserRequest $request)
    {
        auditLog('An administrator has created the create user page: ' . __METHOD__);

        $formattedPermissions = [];

        if ($request->has('permissions')) {
            foreach ($request->get('permissions') as $permission => $value) {
                $formattedPermissions[$permission] = getPermValue($value);
            }
        }

        $timestamp = substr(uniqid('', true), 0, -5);
        $username = str_slug($request->get('first_name')) . '.' . str_slug($request->get('last_name')) . '.' . $timestamp;

        $credentials = [
            'email'         => $request->get('email'),
            'name'          => $username,
            'username'      => $username,
            'password'      => $request->get('password'),
            'first_name'    => $request->get('first_name'),
            'last_name'     => $request->get('last_name'),
            'permissions'   => $formattedPermissions,
            'primary_admin' => 0,
        ];


        $user = $this->users->create($credentials);

        // Add roles to user
        if ($roles = array_get(request()->all(), 'roles')) {
            // Get the user roles
            $userRoles = $user->roles->pluck('id')->toArray();

            // Prepare the roles to be added and removed
            $toAdd = array_diff($roles, $userRoles);
            $toDel = array_diff($userRoles, $roles);

            // Detach the user roles
            if (!empty($toDel)) {
                $user->roles()->detach($toDel);
            }

            // Attach the user roles
            if (!empty($toAdd)) {
                $user->roles()->attach($toAdd);
            }
        }


        if ($user) {
            if ((int)$request->get('activated') === 1) {
                $activation = Sentinel::getActivationRepository()->create($user);
                Sentinel::getActivationRepository()->complete($user, $activation->getCode());
            }

            flash()->success($user->email . ' has been created');

            return redirect()->route('admin.user.list');
        }
    }

    /**
     * @title User Edit
     * @desc Allow user / role to edit users
     *
     * @param User $user
     *
     * @return mixed
     */
    public function edit(User $user)
    {
        auditLog('An administrator has accessed the edit user page to edit user #' . $user->id . ': ' . __METHOD__);

        $activated = Sentinel::getActivationRepository()->completed($user) ? true : false;
        $roles = collect($this->roles->all())->reject(function($role) {
            if (!LoggedInUser()->inRole('super-administrator')) {
                return $role->slug === 'super-administrator';
            }
        })->pluck('name', 'id');

        $assigned_permissions = $user->getPermissions();

        $this->theme->prependTitle('User List');
        $this->theme->breadcrumb()->add('Users', url('admin/users'))->add('Edit User');

        return $this->theme->of(
            'user::admin.manage_user',
            compact('user', 'roles', 'activated', 'assigned_permissions')
        )->render();
    }

    /**
     * @param EditUserRequest $request
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditUserRequest $request, User $user)
    {
        
        auditLog('An administrator has updated details for user #' . $user->id . ': ' . __METHOD__);

        $formattedPermissions = [];

        if ($request->has('permissions')) {
            foreach ($request->get('permissions') as $permission => $value) {
                $formattedPermissions[$permission] = getPermValue($value);
            }
        }

        $credentials = [
            'email'       => $request->get('email'),
            //'name'        => $request->get('username'),
            //'username'    => $request->get('username'),
            'first_name'  => $request->get('first_name'),
            'last_name'   => $request->get('last_name'),
            'permissions' => $formattedPermissions,
        ];

        if ($request->has('password')) {
            $credentials['password'] = $request->get('password');
        }

        /** @var User $user */
        $user = $this->users->update($user, $credentials);

        $code = '';

        // Update user roles
        if ($roles = array_get(request()->all(), 'roles')) {
            // Get the user roles
            $userRoles = $user->roles->pluck('id')->toArray();

            // Prepare the roles to be added and removed
            $toAdd = array_diff($roles, $userRoles);
            $toDel = array_diff($userRoles, $roles);

            // Detach the user roles
            if (!empty($toDel)) {
                $user->roles()->detach($toDel);
            }

            // Attach the user roles
            if (!empty($toAdd)) {
                $user->roles()->attach($toAdd);
            }
        }


        if ($user) {
            if ($request->get('activated') === '1') {
                $activation = Activation::exists($user);

                if (!$activation) {
                    $code = Activation::create($user)->code;
                } else {
                    $code = $activation->code;
                }

                Sentinel::getActivationRepository()->complete($user, $code);
            }

            flash()->success($user->email . ' has been updated');

            return redirect()->route('admin.user.list');
        }
    }

    /**
     * @title User Deletion
     * @desc Allow user / role to delete users
     *
     * @param User $user
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        auditLog('An administrator has deleted (soft delete) user #' . $user->id . ': ' . __METHOD__);

        $message = "There was a problem deleting user <strong>{$user->getUserLogin()} ({$user->getUserId()})</strong>";
        $messageType = 'error';
        try {
            $user->delete();
            $message = "User <strong>{$user->getUserLogin()} ({$user->getUserId()})</strong> has been delete successfully";
            $messageType = 'success';
        } catch (\Exception $exception) {

        }

        flash($message, $messageType);
        return redirect()->route('admin.user.list');
    }

    /**
     *
     */
    public function getListOfUsers()
    {
        /*$users = $this->users->paginate(10);

        return response()->json([
            'current'  => $users->currentPage(),
            'rowCount' => $users->perPage(),
            'rows'     => $users->items(),
            'total'    => $users->total(),
        ]);*/


        $users = $this->users->select([
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'created_at',
            'updated_at',
            'primary_admin',
        ]);

        if (!LoggedInUser()->inRole('super-administrator')) {
            $users->where('first_name', '<>', 'Super')->where('last_name', '<>', 'Admin');
        }

        return Datatables::of($users)
            ->addColumn('action',
                function ($user) {
                    $disabled = '';

                    if ($user->primary_admin === 1) {
                        $disabled = 'disabled';
                    }

                    return '<a href="' . route('admin.user.edit',
                            [$user->id]) . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> ' .
                        (
                        $user->primary_admin ?
                            '' :
                            '<a href="' . route('admin.user.delete',
                                [$user->id]) . '" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Delete</a>'
                        );
                })
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }

    /**
     * @param $data
     * @param $rules
     *
     * @return \Illuminate\Support\MessageBag
     */
    protected function validateUser($data, $rules)
    {
        $validator = \Validator::make($data, $rules);

        $validator->passes();

        return $validator->errors();
    }
}