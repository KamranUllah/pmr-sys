<?php namespace App\Modules\User\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use Bugsnag;
use Cartalyst\Sentinel\Roles\EloquentRole;
use App\Modules\User\Entities\Role;
use App\Modules\User\Entities\User;
use App\Modules\User\Http\Requests\CreateRoleRequest;
use App\Modules\User\Http\Requests\EditRoleRequest;
use Exception;
use Flash;
use Module;
use Request;
use Sentinel;
use Yajra\Datatables\Datatables;

class RoleController extends BackEndController
{
    protected $roles;

    protected $users;

    public function __construct(User $user, Role $role)
    {
        parent::__construct();
        $this->users = Sentinel::getUserRepository();
        $this->roles = Sentinel::getRoleRepository();

        $requiredJs = [
            'admin_user.js',
            'user_common.js'
        ];

        $this->addModuleJSFiles($requiredJs, 'user', true);
    }

    /**
     * @acl_action_title View/List Roles
     * @acl_action_desc Allow user to view list of user roles
     * @acl_action_route admin.user.roles
     *
     * @return mixed
     */
    public function index()
    {
        $this->theme->breadcrumb()->add('Roles');

        $roles = collect($this->roles->all())->reject(function ($role) {
            if (!LoggedInUser()->inRole('super-administrator')) {
                return $role->slug === 'super-administrator';
            }
        });
        $this->theme->prependTitle('User Roles');

        return $this->theme->of('user::admin.admin_roles', compact('roles'))->render();
    }

    /**
     * @return mixed
     */
    public function create()
    {

        $role = new Role();
        $this->theme->setTitle('Create User Role');
        $this->theme->breadcrumb()->add('Users', url('admin/user/roles'))->add('Edit Role');

        return $this->theme->of('user::admin.manage_role', compact('role'))->render();
    }

    /**
     * @param Role $role
     * @return mixed
     */
    public function edit(Role $role)
    {
//        if ($role->slug === 'administrator' && !config('app.debug')) {
//            flash()->error('You cannot edit your own admin roles. Please contact a super administrator to update roles');
//            return redirect()->back();
//        }

        $assigned_permissions = $role->getPermissions();

        $this->theme->setTitle('Edit User Role');
        $this->theme->breadcrumb()->add('Users', url('admin/user/roles'))->add('Edit Role');

        return $this->theme->of(
            'user::admin.manage_role',
            compact('role', 'assigned_permissions')
        )->render();
    }

    /**
     * @param Role $role
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function destroy(Role $role)
    {
        if ($role->slug === 'administrator' && !config('app.debug')) {
            flash()->error('You cannot delete your own admin roles');
            return redirect()->back();
        }

        try {
            if ($role->delete()) {
                Flash::success('The role <strong>' . $role->name . '</strong> has been deleted');
            } else {
                Flash::error('There was a problem deleting the role <strong>' . $role->name . '</strong>');
            }
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }

        return redirect()->route('admin.user.roles');
    }

    /**
     * @param CreateRoleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRoleRequest $request)
    {
        $formattedPermissions = [];

        if ($request->has('permissions')) {
            foreach ($request->get('permissions') as $permission => $value) {
                $formattedPermissions[$permission] = getPermValue($value);
            }
        }

        $data = [
            'name'        => e($request->name),
            'slug'        => $request->slug ? e($request->slug) : str_slug(e($request->name)),
            'permissions' => $formattedPermissions,
        ];

        $role = $this->roles->create($data);

        if ($role) {
            Flash::success('The role <strong>' . $role->name . '</strong> has been created');
        }

        return redirect()->route('admin.user.roles');
    }

    /**
     * @param Role $role
     * @param EditRoleRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(Role $role, EditRoleRequest $request)
    {
//        if ($role->primary_role && Sentinel::getUser()->primary_admin && !config('app.debug')) {
//            flash()->error('You cannot edit your own admin roles');
//            return redirect()->back();
//        }

        $formattedPermissions = [];

        if ($request->has('permissions')) {
            foreach ($request->get('permissions') as $permission => $value) {
                $formattedPermissions[$permission] = getPermValue($value);
            }
        }

        $role = $this->roles->findById($role->id);
        $role->name = e($request->get('name'));
        $role->slug = str_slug(e($request->get('name')));
        $role->permissions = $formattedPermissions;

        if ($role->save()) {
            if (\Request::ajax()) {
                return \Response::json([
                    'success' => true,
                    'result'  => $role->getRoleId(),
                    'message' => 'Role has been updated',
                ]);
            }

            \Flash::success('Role updated successfully');

            return redirect()->route('admin.user.roles');
        }

        if (\Request::ajax()) {
            return \Response::json([
                'success' => false,
                'result'  => $role->getRoleId(),
                'message' => 'There was a problem updating this role!',
            ]);
        }

        \Flash::success('There was a problem updating this role!');

        return redirect()->route('admin.user.role.edit', [$role->getRoleId()]);
    }

    /**
     *
     */
    public function getListOfRoles()
    {
        $users = $this->users->select([
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'created_at',
            'updated_at',
        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a href="' . route('admin.user.edit',
                        [$user->id]) . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> ' .
                    '<a href="' . route('admin.user.delete',
                        [$user->id]) . '" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-edit"></i> Delete</a> ';
            })
            ->editColumn('id', '{{$id}}')
            ->make(true);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLatest($id)
    {
        return \Response::json(Role::find($id));
    }
}