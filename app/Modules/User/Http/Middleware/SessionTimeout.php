<?php

namespace App\Modules\User\Http\Middleware;

use Cache;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Sentinel;

class SessionTimeout
{
    protected $session;
    protected $timeout = 3600;

    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->session->has('lastActivityTime')) {
            //if (!$request->wantsJson() || !$request->expectsJson() || !$request->ajax()) {
                $this->session->put('lastActivityTime', time());
            //}
        } elseif (time() - $this->session->get('lastActivityTime') > $this->getTimeOut()) {

            if (Sentinel::check() && Cache::has('user-is-online-' . Sentinel::getUser()->getUserId())) {
                Cache::forget('user-is-online-' . Sentinel::getUser()->getUserId());
            }

            Cache::flush();
            $this->session->forget('lastActivityTime');
            Sentinel::logout();
            return redirect('login')->withErrors(['Your session has expired due to inactivity']);
        }

        //if (!$request->wantsJson() || !$request->expectsJson() || !$request->ajax()) {
            $this->session->put('lastActivityTime', time());
        //}

        return $next($request);
    }

    protected function getTimeOut()
    {
        return config('pmr.timeout') ?: $this->timeout;
    }
}
