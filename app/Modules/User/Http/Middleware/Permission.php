<?php namespace App\Modules\User\Http\Middleware;

use Closure;
use Debugbar;
use Flash;
use Redirect;
use Sentinel;
use Illuminate\Routing\Route;
use Request;

class Permission
{
    protected $route;

    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $action = $this->route->getActionName();

        $actionMethod = substr($action, strpos($action, '@') + 1);

        // Bit of a bastard way of getting the module name
        // But it works
        $actionModule = explode('\\', str_replace(substr($action, strpos($action, '@')), '', $action));

        $moduleName = strtolower($actionModule[2]);

        $controllerName = strtolower(str_replace('Controller', '', $actionModule[6] ?? $actionModule[5]));

        $entityName = $request->segment(1) ?: 'dashboard';

        $permission = ltrim($moduleName . '.' . $entityName . '.' . $controllerName . '.' . $actionMethod, '.');

        if (\App::environment('local')) {
            if (config('app.debug')) {
                //dump($permission);        
                Debugbar::info(Sentinel::getUser());
                Debugbar::addMessage($permission, 'Current Page Permission Name');
            }            
        }

        //dd($permission);

        if (!Sentinel::getUser()->hasAnyAccess($permission)) {
            Flash::error('You do not have access to this function/page please contact your administrator :: ' . $permission);

            return redirect()->route('admin.unauthorised');
//
//            if (config('app.debug')) {
//                //return redirect()->route('user.logout');
//                return redirect()->route('admin.dashboard');
//            }
        }

        return $next($request);
    }

    /**
     * @param Request $request
     * @return string
     * @internal param $segmentPosition
     */
    protected function getModuleName(Request $request)
    {
        return $request->segment(1);
    }

    /**
     * @param Request $request
     * @param         $segmentPosition
     * @return string
     */
    protected function getEntityName(Request $request, $segmentPosition)
    {
        $entityName = $request->segment($segmentPosition);

        return $entityName ?: 'dashboard';
    }
}
