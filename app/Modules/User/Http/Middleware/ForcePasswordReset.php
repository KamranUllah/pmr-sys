<?php namespace App\Modules\User\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Html;
use Sentinel;

class ForcePasswordReset
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (request()->user()) {
            // Check if the users password was changed within last 3 months or not
            // If not ask him to change his password, before he can log in
            $last_pw_changed = new Carbon(request()->user()->last_password_changed);
            $pw_expiry = new Carbon(request()->user()->password_expiry_date);

            //dd(Carbon::now()->diffInDays($pw_expiry));

            // Check if password expires in 3 days
            if (Carbon::now()->diffInDays($pw_expiry) <= config('core.password_expiry_check_timeframe')) {
                //flash()->warning("Your password will expire in 3 days! Please ensure you <a href='" . route('update.user.password') . "'>update your password</a> to minimise disruption in accessing your account.");
                flash()->warning('Your password will expire in 3 days! Please ensure you update your password to minimise disruption in accessing your account.');
            }

            // Check if password has expired
            /*if (Carbon::now() > $pw_expiry) {
                flash()->error('Yor password has expired! You are required to change it before proceeding.');
                return redirect()->route('doctor.dashboard');
            }*/
        } else {
            return redirect()->guest('login');
        }

        return $next($request);
    }
}
