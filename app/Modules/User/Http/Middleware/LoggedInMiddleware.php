<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 15/03/2017
 * Time: 08:33
 */

namespace App\Modules\User\Http\Middleware;

use App\Modules\User\Contracts\Authentication;

class LoggedInMiddleware
{

    private $auth;
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if ($this->auth->check() === false) {
            return redirect()->guest('login');
        }
        return $next($request);
    }
}