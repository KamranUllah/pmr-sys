<?php namespace App\Modules\User\Http\Middleware;

use Closure;
use Flash;
use Redirect;
use Sentinel;

class Admin
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (request()->user())
        {

            //if (config('user.enable_admin_access_by_ip') && request()->user()->inRole('administrator') && !is_admin_ip(request()->getClientIp())) {
            /*if (!request()->user()->inRole('administrator')) {
                Flash::error('You do not have access to this function/page please contact your administrator');
                return redirect()->route('admin.dashboard');
            }*/
        } else {
            return redirect()->guest('login');
        }

        return $next($request);
    }
}
