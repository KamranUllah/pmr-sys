<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 13/09/2016
 * Time: 10:05
 */

namespace App\Modules\User\Http\Middleware;

use Closure;
use Sentinel;
use Cache;
use Carbon\Carbon;

class LogLastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (request()->user()) {
            $expiresAt = Carbon::now()->addMinutes(5);
            Cache::put('user-is-online-' . Sentinel::getUser()->getUserId(), true, $expiresAt);
        }

        return $next($request);
    }
}
