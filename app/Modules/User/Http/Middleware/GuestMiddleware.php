<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 15/03/2017
 * Time: 08:44
 */

namespace App\Modules\User\Http\Middleware;

use Redirect;

class GuestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (null !== $request->user()) {
            return Redirect::to('/');
        }

        return $next($request);
    }
}