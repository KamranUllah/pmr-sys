<?php namespace App\Modules\User\Http\Middleware;

use Cache;
use Carbon\Carbon;
use Sentinel;

class SentinelAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $next
     * @return mixed
     * @throws \RuntimeException
     */
    public function handle($request, $next)
    {
        if (null === $request->user()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            }

            Cache::flush();
            Sentinel::logout();
            return redirect()->guest('login')->with('message', 'Please login');
        }

        return $next($request);
    }
}
