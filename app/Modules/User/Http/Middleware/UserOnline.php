<?php

namespace App\Modules\User\Http\Middleware;

use App\Modules\User\Events\UserOnlineNotificationEvent;
use Closure;
use Illuminate\Http\Request;
use Sentinel;

class UserOnline
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Sentinel::check()) {
            event(new UserOnlineNotificationEvent(Sentinel::getUser()));
        }

        return $next($request);
    }
}
