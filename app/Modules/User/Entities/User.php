<?php
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 09/02/2015
 * Time: 20:13
 */

namespace App\Modules\User\Entities;

use Carbon\Carbon;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;


/**
 * Class User
 * @package CoronaCms\Modules\User\Entities
 */
class User extends EloquentUser implements AuthenticatableContract, CanResetPasswordContract,AuditableContract, UserResolver
{
    use Authenticatable, CanResetPassword;

    use FormAccessible;

    use Notifiable;

    use Auditable;

    /**
     * {@inheritdoc}
     */
    public static function resolveId()
    {
        return \Sentinel::check() ? \Sentinel::getUser()->getUserId() : null;
    }

    protected $loginNames = [
        'email',
//        'username',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'last_login',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'permissions',
        'first_name',
        'last_name',
        'last_login',
        'primary_user',
        'last_password_change',
        'password_expiry_date',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'password',
        'permissions',
        'persistence_codes',
        'last_password_change',
        'password_expiry_date',
    ];

    /**
     * @var array
     */
    protected $appends = ['full_name'];

    /**
     * @return array
     */
    public function getDates()
    {
        return array_merge(
            parent::getDates(),
            [
                'last_login',
                'last_password_change',
                'password_expiry_date',
            ]
        );
    }

    /**
     * Get a list of users that last logged in
     *
     * @param $query
     * @param $limit
     *
     * @return mixed
     */
    public function scopeGetLastLoggedIn($query, $limit)
    {
        return $query->orderBy('last_login', 'desc')->limit($limit);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return string
     */
    public function getTimeagoAttribute()
    {
        $date = Carbon::createFromTimestamp(strtotime($this->created_at))->diffForHumans();

        return $date;
    }
}
