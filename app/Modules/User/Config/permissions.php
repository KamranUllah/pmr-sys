<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 14/06/2017
 * Time: 16:01
 */

return [
    // User Management Permissions
    'User management' => [
        'user.admin.user.index'     => [
            'name'        => '',
            'description' => 'Allows user to access the list of users',
        ],
        'user.admin.user.create'       => [
            'name'        => '',
            'description' => 'Allows user to create new users and assign roles to them',
        ],
        'user.admin.user.store'     => [
            'name'        => '',
            'description' => 'Allows user to complete the creation of a new user',
        ],
        'user.admin.user.edit'      => [
            'name'        => '',
            'description' => 'Edit Users',
        ],
        'user.admin.user.update'    => [
            'name'        => '',
            'description' => 'Update Users',
        ],
        'user.admin.user.destroy' => [
            'name'        => '',
            'description' => 'Disable Users',
        ],
        'user.admin.user.getListOfUsers' => [
            'name'        => '',
            'description' => 'Get a list of user for the Datatables (required)',
        ],
    ],

    // Role Management Permissions
    'Role Management' => [
        'user.admin.role.index'  => [
            'name'        => '',
            'description' => 'View List of Roles',
        ],
        'user.admin.role.create' => [
            'name'        => '',
            'description' => 'Create Roles',
        ],
        'user.admin.role.store'  => [
            'name'        => '',
            'description' => 'Store Roles',
        ],
        'user.admin.role.edit'   => [
            'name'        => '',
            'description' => 'Edit Roles',
        ],
        'user.admin.role.update' => [
            'name'        => '',
            'description' => 'Update Roles',
        ],/*
        'user.admin.role.destroy' => [
            'name'        => '',
            'description' => 'Allow user to delete user roles',
        ],*/
    ],
];
