<?php
/**
 * Created by PhpStorm.
 * User: emman
 * Date: 25/10/2016
 * Time: 21:03
 */

namespace App\Modules\User\Sidebar;

use App\Modules\User\Entities\Role;
use App\Modules\User\Entities\User;
use Maatwebsite\Sidebar\Badge;
use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\SidebarExtender;
use Sentinel;

class AdminSideBarExtender implements SidebarExtender
{

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group('User', function (Group $group) {
            $group->hideHeading();
            $group->weight(100);
            $group->item('User Management', function (Item $item) {
                $item->toggleIcon('fa fa-chevron-down');
                $item->icon('fa fa-user');
                $item->weight(14);

                $item->item('Users', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-users');
                    $item->route('admin.user.list');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    if (Sentinel::check()) {
                        $item->authorize(
                            Sentinel::getUser()->hasAccess('user.admin.user.index')
                        );
                    }

                });

                $item->item('Roles', function (Item $item) {
                    $item->weight(1);
                    $item->icon('fa fa-file');
                    $item->route('admin.user.roles');
                    $item->badge(function (Badge $badge, Role $role) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($role->count());
                    });

                    if (Sentinel::check()) {
                        $item->authorize(
                            Sentinel::getUser()->hasAccess('user.admin.role.index')
                        );
                    }

                });
            });

            if (Sentinel::check()) {
                $group->authorize(
                    Sentinel::getUser()->hasAccess('user.*')
                );
            }
        });

        return $menu;
    }
}
