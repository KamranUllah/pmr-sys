<?php

namespace App\Modules\User\Providers;

use App\Modules\User\Http\Middleware\Admin;
use App\Modules\User\Http\Middleware\GuestMiddleware;
use App\Modules\User\Http\Middleware\LoggedInMiddleware;
use App\Modules\User\Http\Middleware\Permission;
use App\Modules\User\Http\Middleware\SentinelAuth;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

        $this->registerMiddleware();
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('user.php'),
            __DIR__ . '/../Config/permissions.php' => config_path('user_permissions.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'user'
        );

        $this->mergeConfigFrom(
            __DIR__ . '/../Config/permissions.php',
            'user-permissions'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/user');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/user';
        }, \Config::get('view.paths')), [$sourcePath]), 'user');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/user');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'user');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'user');
        }
    }

    /**
     *
     */
    public function registerMiddleware()
    {
        $this->app['router']->aliasMiddleware('sentinel.auth', SentinelAuth::class);
        $this->app['router']->aliasMiddleware('backend.access', Admin::class);
        $this->app['router']->aliasMiddleware('auth.guest', GuestMiddleware::class);
        $this->app['router']->aliasMiddleware('logged.in', LoggedInMiddleware::class);
        $this->app['router']->aliasMiddleware('permissions', Permission::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
