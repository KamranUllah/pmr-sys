<?php

namespace App\Modules\User\Database\Seeders;

use Activation;
use App\Modules\User\Entities\Role;
use App\Modules\User\Entities\User;
use Config;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Module;
use Sentinel;

class CreateSuperAdminSeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        $superAdminRole = Sentinel::getRoleRepository()->findBySlug('super-administrator');

        //dd($superAdminRole);

        if (null === $superAdminRole) {

            $superAdminRole = Sentinel::getRoleRepository()->create([
                'name'         => 'Super Administrator',
                'slug'         => 'super-administrator',
                'primary_role' => 1,
            ]);

            $this->buildPermissions($superAdminRole);
        } else {
            $this->buildPermissions($superAdminRole);
        }

        $superAdmin = Sentinel::getUserRepository()->findByCredentials(['email' => 'super.admin@nhg.com']);

        //dd($superAdmin);

        if (null === $superAdmin) {
            echo 'User Not Found! Super Admin Role will be created.';
            sleep(2);

            $superAdmin = Sentinel::getUserRepository()->create([
                //'name'          => 'Super',
                //'username'      => 'admin',
                'first_name'    => 'Super',
                'last_name'     => 'Admin',
                'email'         => 'super.admin@nhg.com',
                'password'      => 'r6HqC$#iqJHMxn@sQnyA',
                'primary_admin' => 1,
            ]);

            $code = Activation::create($superAdmin)->code;
            Activation::complete($superAdmin, $code);

            // Assign Roles to Users
            $superAdminRole->users()->attach($superAdmin);
        } else {
            $code = Activation::create($superAdmin)->code;
            Activation::complete($superAdmin, $code);

            // Detach Roles to Users
            $superAdminRole->users()->detach($superAdmin);

            // Assign Roles to Users
            $superAdminRole->users()->attach($superAdmin);
        }
    }

    /**
     * @param $role
     *
     * @return array
     */
    private function buildPermissions($role)
    {
        //dd($role);
        $permissible = [];
        foreach (Module::all() as $module) {
            $name = studly_case($module->get('name'));

            if (Config::has(strtolower($module->name . '-permissions'))) {
                $permissions = config(strtolower($module->name . '-permissions'));
                foreach ($permissions as $section => $items) {
                    echo $section . "\r\n";
                    foreach ($items as $key => $value) {
                        $permissible[$key] = true;
                    }
                }
            }
        }

        $role->permissions = $permissible;
        $role->save();
    }
    
}