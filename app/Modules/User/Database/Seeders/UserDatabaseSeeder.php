<?php namespace App\Modules\User\Database\Seeders;

use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Eloquent;
use Illuminate\Database\Seeder;
use DB;

class UserDatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        // Create Users
        DB::table('users')->truncate();
        DB::table('roles')->truncate();
        DB::table('role_users')->truncate();



        $admin = Sentinel::getUserRepository()->create([
            'name'          => 'admin',
            'username'      => 'admin',
            'email'         => 'admin@nhg.com',
            'password'      => 'Password1',
            'primary_admin' => 1,
        ]);

        $clinical = Sentinel::getUserRepository()->create([
            'name'          => 'clinical',
            'username'      => 'clinical',
            'email'         => 'clinical@nhg.com',
            'password'      => 'Password1',
            'primary_admin' => 0,
        ]);

        $l1_pharmacist = Sentinel::getUserRepository()->create([
            'name'          => 'l1_pharmacist',
            'username'      => 'l1_pharmacist',
            'email'         => 'l1_pharmacist@nhg.com',
            'password'      => 'Password1',
            'primary_admin' => 0,
        ]);

        $l2_pharmacist = Sentinel::getUserRepository()->create([
            'name'          => 'l2_pharmacist',
            'username'      => 'l2_pharmacist',
            'email'         => 'l2_pharmacist@nhg.com',
            'password'      => 'Password1',
            'primary_admin' => 0,
        ]);

        $lead_pharmacist = Sentinel::getUserRepository()->create([
            'name'          => 'lead_pharmacist',
            'username'      => 'lead_pharmacist',
            'email'         => 'lead_pharmacist@nhg.com',
            'password'      => 'Password1',
            'primary_admin' => 0,
        ]);

        // Create Activations
        DB::table('activations')->truncate();

        // Activate Admin
        $code = Activation::create($admin)->code;
        Activation::complete($admin, $code);

        // Activate Clinical
        $code = Activation::create($clinical)->code;
        Activation::complete($clinical, $code);

        // Activate Level 1 Pharmacist
        $code = Activation::create($l1_pharmacist)->code;
        Activation::complete($l1_pharmacist, $code);

        // Activate Level 2 Pharmacist
        $code = Activation::create($l2_pharmacist)->code;
        Activation::complete($l2_pharmacist, $code);

        // Activate Lead Pharmacist
        $code = Activation::create($lead_pharmacist)->code;
        Activation::complete($lead_pharmacist, $code);

        // Create Roles
        $administratorRole = Sentinel::getRoleRepository()->create([
            'name'        => 'Administrator',
            'slug'        => 'administrator',
            'permissions' => [

                // Dashboard Management
                'dashboard.admin.dashboard.index' => true,

                // User Management
                'user.admin.user.index'           => true,
                'user.admin.user.create'          => true,
                'user.admin.user.store'           => true,
                'user.admin.user.edit'            => true,
                'user.admin.user.update'          => true,
                'user.admin.user.destroy'         => true,

                // Role Management
                'user.admin.role.index'           => true,
                'user.admin.role.create'          => true,
                'user.admin.role.store'           => true,
                'user.admin.role.edit'            => true,
                'user.admin.role.update'          => true,
                'user.admin.role.destroy'         => true,
            ],
        ]);

        $clinicalRole = Sentinel::getRoleRepository()->create([
            'name'        => 'Clinical',
            'slug'        => 'clinical',
            'permissions' => [],
        ]);

        $l1_PharmacistRole = Sentinel::getRoleRepository()->create([
            'name'        => 'Pharmacist (Level 1)',
            'slug'        => 'pharmacist-level-1',
            'permissions' => [],
        ]);

        $l2_PharmacistRole = Sentinel::getRoleRepository()->create([
            'name'        => 'Pharmacist (Level 2)',
            'slug'        => 'pharmacist-level-2',
            'permissions' => [],
        ]);

        $lead_PharmacistRole = Sentinel::getRoleRepository()->create([
            'name'        => 'Lead Pharmacist',
            'slug'        => 'lead-pharmacist',
            'permissions' => [],
        ]);

        // Assign Roles to Users
        $administratorRole->users()->attach($admin);
    }
}
