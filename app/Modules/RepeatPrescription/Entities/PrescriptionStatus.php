<?php

namespace App\Modules\RepeatPrescription\Entities;

use App\Modules\RepeatPrescription\Entities\Prescription;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;

class PrescriptionStatus extends Model
{

    use Filterable, Auditable;

    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'prescriptionstatus';

    protected $primaryKey = 'StatusId';

    protected $fillable = [
        "Description",
    ];

    public $timestamps = false;


}
