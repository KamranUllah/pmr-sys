<?php namespace App\Modules\RepeatPrescription\Entities;

use App\Modules\RepeatPrescription\Entities\Prescriptionstatus;
use Illuminate\Database\Eloquent\Model;

class Endorsement extends Model
{

    protected $fillable = [
        'dnpid',
        'drug_name',
        'direction_1',
        'direction_2',
        'quantity',
        'units',
        'prescription_id',
        'status'
    ];


    protected $statuses = [
        1 => 'waiting',
        2 => 'ready',
        8 => 'sent',
        9 => 'delivered'];

    const RAISED_DR_NOT_SIGNED = 1;
    const ESCALATED = 13;
    const DR_REJECTED = 56;
    const DR_SIGNEDOFF = 2;
    const SENT = 8;

    public function getJobType()
    {
        //probably query DB for the correct Id
        return 8;
    }

    public function prescription()
    {
        return $this->belongsTo('\App\Models\Prescription');
    }

    public function status()
    {
        return $this->belongsTo(Prescriptionstatus::class);
    }


}
