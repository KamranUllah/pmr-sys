<?php namespace App\Modules\RepeatPrescription\Entities;

use App\Modules\Patient\Entities\MainBasePatient;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class Prescription extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $connection = 'sqlsrv_mainbase';
    protected $primaryKey = 'prescriptionid';

    protected $fillable = [
        'prescriptionstatus',
        'dateraised',
        'dateexpiry',
        'datepharmacyreceived',
        'datepharmacydespatched',
        'doctorgmc',
        'doctorname',
        'sgncode',
        'couriernotes',
        'deliverydetail',
        'courier_notes',
        'trackingref',
        'patient',
        'endorsements',
        'sentfax'
    ];

   //public $dates = ['dateraised', 'dateexpiry', 'datepharmacyreceived', 'datepharmacydespatched'];

    //protected $statuses = [[1,'raisedNotSigned'],[13, 'escalated'],[56, 'drRejected'],[2,'drSignedOff'],[8,'orderSent']];

    const RAISED_DR_NOT_SIGNED = 1;
    const ESCALATED = 13;
    const DR_REJECTED = 56;
    const DR_SIGNEDOFF = 2;
    const SENT = 8;
    const DELIVERED = 9;
    const CANCELLED_CLINICAL = 30;
    const PHARMACY_DECLINED = 15;


    /**
     * The "booting" method of the model.
     * Added scope to remove any data older than 90 days from the management
     *
     * @return void
     */
//    protected static function boot()
//    {
//        parent::boot();
//        if(config('pmr.remove_90_days')) {
//
//            static::addGlobalScope('archive', function (Builder $builder) {
//                $fromDate =  Carbon::now()->subMonth(3);
//                $toDate = Carbon::now();
//                $builder->whereBetween('dateraised', array($fromDate->toDateTimeString(), $toDate->toDateTimeString()) );
//
//            });
//
//        }
//    }



    public function getJobType()
    {
        //probably query DB for the correct Id
        return 8;
    }

    public function getDates()
    {
        return [];
    }

    public function patient()
    {
        return $this->belongsTo(MainBasePatient::class, 'patientid');
    }

    public function prescriptionendorsements()
    {
        return $this->hasMany(PrescriptionEndorsements::class, 'prescription');
    }

    public function prescstatus()
    {
        return $this->belongsTo(PrescriptionStatus::class, 'prescriptionstatus', 'StatusId');
    }

    public function faxrecords()
    {
        return $this->hasMany(FaxRecords::class, 'ReferenceId','prescriptionid');
    }

    public function notes()
    {
        return $this->hasMany(PrescriptionNotes::class, 'PrescriptionId','prescriptionid');
    }


    public function prescourier()
    {
        return $this->belongsTo(Couriers::class, 'courier','courierid');
    }


    public function getEndorsementTotalAttribute()
    {
        return $this->prescriptionendorsements->count();

    }


    public function getSentTotalAttribute()
    {
        return $this->faxrecords->where('Status','=',3)->count();

    }

    // Helpers

    public function getStatus()
    {
        $value = '';
        switch($this->prescriptionstatus)
        {
            case 1: 
                $value = 'raisedNotSigned';
                break;
            case 13:
                $value = 'escalated';
                break;
            case 56:
                $value = 'drRejected';
                break;
            case 2:
                $value = 'drSignedOff';
                break;
            case 8:
                $value = 'orderSent';
                break;
        }
        return $value;
    }

    public function getStatusString()
    {
        $value = '';
        switch($this->prescriptionstatus)
        {
            case 1: 
                $value = 'Raised - Not Signed';
                break;
            case 13:
                $value = 'Escalated';
                break;
            case 56:
                $value = 'Dr Rejected';
                break;
            case 2:
                $value = 'Dr Signed Off';
                break;
            case 8:
                $value = 'Sent';
                break;
            case 9:
                $value = 'Delivered';
                break;
            case 15:
                $value = 'Pharmacist Declined';
                break;
            case 30:
                $value = 'Clinical Cancelled';
                break;
        }
        return $value;
    }

    public function getStatusId($statusString)
    {
        $value = '';
        switch($statusString)
        {
            case 'Raised - Not Signed':
                $value = 1;
                break;
            case 'Escalated':
                $value = 13;
                break;
            case 'Dr Rejected':
                $value = 56;
                break;
            case 'Dr Signed Off':
                $value = 2;
                break;
            case 'Sent':
                $value = 8;
                break;
        }
        return $value;
    }

    public function getDateEscalated()
    {
        return $this->getStatusDateLogged(Prescription::ESCALATED);
    }

    public function getDateNotSignedOff()
    { 
        return $this->getStatusDateLogged(Prescription::RAISED_DR_NOT_SIGNED);
    }

    public function getDateRejected()
    {
        return $this->getStatusDateLogged(Prescription::DR_REJECTED);   
    }

    public function getDateSignedOff()
    {
        return $this->getStatusDateLogged(Prescription::DR_SIGNEDOFF);   
    }

    public function getDateSent()
    {
        return $this->getStatusDateLogged(Prescription::SENT);   
    }

    private function getStatusDateLogged($statusLogged)
    {
        $dateAdded = DB::connection('sqlsrv_mainbase')->select('exec sp_GetPrescriptionLogDateAdded @PrescriptionId=?,@NewPrescriptionStatusId=?',
                                    array($this->prescriptionid, $statusLogged));   
        if (null === $dateAdded || empty($dateAdded) === true)
        {  
            return ''; 
        }

        return $dateAdded[0]->dateadded;
    }

    public function formatOrNa($value)
    {
        return $this->{$value} ? $this->{$value}->format('d/m/Y') : 'n/a';
    }
}
