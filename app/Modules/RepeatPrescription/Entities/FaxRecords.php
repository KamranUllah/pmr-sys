<?php namespace App\Modules\RepeatPrescription\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Models\Audit;

class FaxRecords extends Model
{
   protected $connection = 'sqlsrv_mainbase';

    protected $table = 'FaxRecords';

    protected $primaryKey = 'FaxRecordId';

    public $timestamps = true;

    protected $fillable = [
        'FaxRecordId',
        'faxid',
        'status',
        'referenceid',
        'faxtype',
        'timesent',
        'lastupdated'
    ];


    const CREATED_AT = 'TimeSent';
    const UPDATED_AT = 'LastUpdated';


    protected $dates = ['LastUpdated','TimeSent'];


    const HALTED = 1;
    const PENDING = 2;
    const SENT = 3;
    const FAILED = 3;
    const UNKNOWN = 5;




    public function prescription()
    {
        return $this->belongsTo(Prescription::class);
    }

}
