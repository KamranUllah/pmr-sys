<?php namespace App\Modules\RepeatPrescription\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Models\Audit;

class PrescriptionEndorsements extends Model
{
   protected $connection = 'sqlsrv_mainbase';

    protected $table = 'PrescriptionEndorsements';

    protected $primaryKey = 'EndorsementId';

    public $timestamps = false;

    protected $fillable = [
        'dnpid',
        'nm',
        'directions1',
        'directions2',
        'quantity',
        'units',
        'prescription',
        'Status'
    ];

    protected $statuses = [
        1 => 'waiting',
        2 => 'ready',
        8 => 'sent',
        9 => 'delivered'];

    const RAISED_DR_NOT_SIGNED = 1;
    const ESCALATED = 13;
    const DR_REJECTED = 56;
    const DR_SIGNEDOFF = 2;
    const SENT = 8;
    const DELIVERED = 9;
    const CANCELLED_CLINICAL = 30;

    public function prescription()
    {
        return $this->belongsTo(Prescription::class);
    }

    public function endorsementstatus()
    {
        return $this->belongsTo(PrescriptionStatus::class, 'Status', 'StatusId');
    }

}
