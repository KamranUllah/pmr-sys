<?php

namespace App\Modules\RepeatPrescription\Entities;

use App\Modules\RepeatPrescription\Entities\Prescription;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Patient extends Model
{

    use Filterable, Auditable;

    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'patients';

    protected $primaryKey = 'PatientId';

    protected $fillable = [
        "PatientId",
        "PatientTitle",
        "PatientForename",
        "PatientSurname",
        "PatientDOB",
        "PatientSex",
        "PatientEmail",
        "PatientPassword",
        "PatientMobile",
        "PatientAddress1",
        "PatientAddress2",
        "PatientAddress3",
        "PatientAddress4",
        "PatientAddress5",
        "PatientNI",
        "PatientAllergies",
        "PatientPreExisting",
        "PatientAvatar",
        "PatientPrMed",
        "PaymentType",
        "Active",
        "BankName",
        "BankAccountName",
        "BankAccountNumber",
        "BankAccountSortcode",
        "AlternateAddress1",
        "AlternateAddress2",
        "AlternateAddress3",
        "AlternateAddress4",
        "AlternateAddress5",
        "AccountType",
        "Parent1",
        "Parent2",
        "PrimaryAccount",
        "DateJoined",
        "DateLeft",
        "DDPrice",
        "AppPrice",
        "CDDPrice",
        "DDAppts",
        "DDAnniversary",
        "SurgeryName",
        "SurgeryPhone",
        "GPfirstname",
        "GPlastname",
        "SurgeryAddress1",
        "SurgeryAddress2",
        "SurgeryCity",
        "SurgeryCounty",
        "SurgeryPostcode",
        "WorkAddress1",
        "WorkAddress2",
        "WorkCity",
        "WorkCounty",
        "WorkPostcode",
        "WorkPhone",
        "UserType",
        "PolicyNumber",
        "ValidStartDate",
        "ValidEndDate",
        "CompanyType",
        "PersonalAccessCode",
        "IsAvatarSet",
        "IsNameSet",
        "IsAddressSet",
        "LanguagePreferenceId",
        "DateVerified",
        "ccg_id",
        "NHSNumber",
        "GPGMCCode",
        "GPCode",
        "SurgeryFax",
        "SurgeryEmail",
        "NextOfKinId",
        "Consent"
    ];

    protected $dates = ['DateJoined'];

    public function prescriptions()
    {
        return $this->hasMany(Prescription::class, 'patientid');
    }

}
