<?php namespace App\Modules\RepeatPrescription\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Models\Audit;

class PrescriptionNotes extends Model
{

    use SoftDeletes;

    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'PrescriptionNotes';

    protected $primaryKey = 'PrescriptionNoteId';

    protected $fillable = [
        'PrescriptionNoteId',
        'PrescriptionId',
        'UserId',
        'SourceSystem',
        'Note',
        'CreatedAt',
        'UpdatedAt',
        'DeletedAt',
    ];


    const CREATED_AT = 'CreatedAt';
    const UPDATED_AT = 'UpdatedAt';
    const DELETED_AT = 'DeletedAt';


    protected $dates = ['CreatedAt','UpdatedAt','DeletedAt'];

    public $timestamps = true;




    public function prescription()
    {
        return $this->belongsTo(Prescription::class);
    }

}
