<?php

namespace App\Modules\RepeatPrescription\Entities;

use App\Modules\RepeatPrescription\Entities\Prescription;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;

class Couriers extends Model
{


    protected $connection = 'sqlsrv_mainbase';

    protected $table = 'couriers';

    protected $primaryKey = 'courierid';

    public $timestamps = false;

    public function prescription()
    {
        return $this->belongsTo(Prescription::class);
    }
}
