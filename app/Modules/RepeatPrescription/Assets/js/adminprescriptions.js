$(document).ready(function() {
    $('#formularyTable').DataTable();
    $('#dispensaryTable').DataTable();


    $('#printContent').click(function(){
        //do something
        var DocumentContainer = document.getElementById('printable-script');
        var WindowObject = window.open("", "PrintWindow",
            "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
        WindowObject.document.writeln('<!DOCTYPE html>');
        WindowObject.document.writeln('<html><head><title>Patient Repeat Prescription Request</title>');
        // var str = '<link rel="stylesheet" href="{{ asset('stylesheets/app.css') }}" />';
        // WindowObject.document.writeln(str);
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.writeln('</body></html>');
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    });

    /*$('.print-pdf').on('click', function() {
        var prescriptionId = $(this).data('prescription-id');
        console.log(prescriptionId);

        showLoader('Generating PDF document');
        axios.post(laroute.route('repeat.pdf.download', {id: prescriptionId}))
            .then(function(response) {
                return response.data;
            })
            .then(function(pdf) {
                hideLoader();
            })
            .catch(function(error) {
                hideLoader();
            });
    })*/


    

    $('#cmd').click(function () {

        var pdf = new jsPDF('p', 'pt', 'letter');
        // source can be HTML-formatted string, or a reference
        // to an actual DOM element from which the text will be scraped.
        source = $('#printable-script')[0];

        // we support special element handlers. Register them with jQuery-style
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors
        // (class, of compound) at this time.
        specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#editor': function (element, renderer) {
                // true = "handled elsewhere, bypass text extraction"
                return true
            }
        };
        margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
            source, // HTML string or DOM elem ref.
            margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width, // max width of content on PDF
                'elementHandlers': specialElementHandlers
            },

            function (dispose) {
                // dispose: object with X, Y of the last line add to the PDF
                //          this allow the insertion of new lines after html
                var fileName = $( "#prescriptionId" ).html();
                pdf.save('repeatprescription'+fileName+'.pdf');
            }, margins
        );
    });





} );