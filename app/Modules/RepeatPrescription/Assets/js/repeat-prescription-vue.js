
//repeat prescription import
import repeatPrescriptionsComponent from '../../Resources/components/landing';
import manageRepeatPrescriptionsComponent from '../../Resources/components/repeat-prescriptions/manage';
import importComponent from '../../Resources/components/import/import';
import updateImportComponent from '../../Resources/components/import/update';
import updateImportComponentPSL from '../../Resources/components/import/update-psl';
import resultImportComponent from '../../Resources/components/import/result';
import resultImportComponentPSL from '../../Resources/components/import/result-psl';
// Repeat Prescriptions Feature
// Start Of VueJS usage
document.addEventListener('DOMContentLoaded', function() {
    /**
     *
     * @type {[*]}
     */
    const routes = [{
        path: '/',
        component: repeatPrescriptionsComponent,
        meta: {
            title: 'Repeat Prescription'
        },
        children: [
            {
                path: '/manage/:status',
                component: manageRepeatPrescriptionsComponent,
                name: "managePrescriptions",
                meta: {
                    title: 'Manage Repeat Prescriptions'
                },
            },
            {
                path: '/import',
                component: importComponent,
                name: "import-csv",
                meta: {
                    title: 'Upload Documents'
                },
            },
            {
                path: '/import/update/rm',
                component: updateImportComponent,
                name: "updateImport",
                meta: {
                    title: 'Repeat Prescriptions Update RM '
                },
            },
            {
                path: '/import/update/psl',
                component: updateImportComponentPSL,
                name: "updateImportPSL",
                meta: {
                    title: 'Repeat Prescriptions Update PSL '
                },
            },
            {
                path: '/import/update/result/rm',
                component: resultImportComponent,
                name: "import-result",
                meta: {
                    title: 'Repeat Prescriptions Update Result'
                },
            },
            {
                path: '/import/update/result/psl',
                component: resultImportComponentPSL,
                name: "import-result-psl",
                meta: {
                    title: 'Repeat Prescriptions Update Result'
                },
            },
        ]
    },
    ];

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.

    const router = new VueRouter({
        routes // short for routes: routes
    });

    switch (NowGP.currentRoute) {
        case 'admin.repeatprescription.index':
            let repeatPrescriptionVm = new Vue({
                el: '#repeatPrescriptionApp',
                router
            });

            break;
    }
});

