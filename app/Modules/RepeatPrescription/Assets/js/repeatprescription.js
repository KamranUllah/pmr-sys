$(function () {
    $('[data-toggle="popover"]').popover({
        html: true,
        placement: 'auto left'
    });


    /**
     * ENDORSEMENTS
     */

    $('.btn-endorsements').on('click', function() {

            $('.opened').hide().removeClass('opened');
            if($(this).hasClass('btn-clicked')){
                $(this).removeClass('btn-clicked');
                var oEndor = $(this).parents('.tr-rp').next('.endorsements');
                oEndor.slideUp();
                oEndor.removeClass('opened');
                $(this).parents('.tr-rp').next().next().find('ul').show();
            } else {
                $('.btn-clicked').removeClass('btn-clicked');
                var oEndor = $(this).parents('.tr-rp').next('.endorsements');
                oEndor.toggle('slide');
                oEndor.toggleClass('opened');
                $(this).parents('.tr-rp').next().next().find('ul').hide();
                $(this).toggleClass('btn-clicked');
            }

    });

    $('.endorsement-action').on('click', function () {
        if(!$(this).hasClass('disabled')) {

            var oButton = $(this);
            var actionType = $(this).data('action-type');
            var actionStatusId = $(this).data('action-type-id');
            var actionMessage = $(this).data('action-message');
            var actionUrl = $(this).data('action-url');
            var endorsementId = $(this).data('endorsement-id');
            var prescriptionId = $(this).data('prescription-id');
            var prescriptionData = $(this).parents('.endorsements').next('.prescription-btns').find('ul.list-inline').data('json');

            /*
            console.log(actionType);
            console.log(actionMessage);
            console.log(actionUrl);
            console.log(endorsementId);
            console.log('prescriptionData');
            console.log(prescriptionData);
            */

            if (actionType === 'endorsement-mark-as-sent') {
                doEndorsementMarkAsSent(oButton, actionType, actionMessage, actionUrl, endorsementId, prescriptionId, actionStatusId, prescriptionData);
            } else if (actionType === 'endorsement-mark-as-signedoff') {
                doEndorsementMarkAsSignedOff(oButton, actionType, actionMessage, actionUrl, endorsementId, prescriptionId, actionStatusId, prescriptionData);
            } else if (actionType === 'endorsement-mark-as-rejected') {
                doEndorsementMarkAsRejected(oButton, actionType, actionMessage, actionUrl, endorsementId, prescriptionId, actionStatusId, prescriptionData);
            }
        }
    });

    /**
     * Change button as disabled
     * @param oButton
     */
    function changeButtonState(oButton){
        oButton.addClass('disabled');
    }

    /**
     * This function is for check the all endorsement's status in the selected endorsement belonging prescription
     * If all endorsements going to o be same, new status; then returns RUE to change prescription status as well
     * @param endorsementId
     * @param actionStatusId
     * @param prescriptionData
     * @returns {boolean}
     */
    function checkIsLastEndorsementWithThisStatus(endorsementId, actionStatusId, prescriptionData) {

        // loop the json and
        //  1. get is this a only one -> ask to change presc
        //  2. if there are  more other than this
        //      if all are in the status of going to change -> ask o change prescription

        showLoader();
        var updatePrescStatus = true;
        if(prescriptionData.endorsements.length === 1){
            return true;
        } else {
            $.each(prescriptionData.endorsements, function() {
                //console.log('eId: '+this.eId+' id:'+endorsementId+' |eStatus: '+this.eStatus+' |actStatus:'+actionStatusId);
                if(parseInt(this.eId) !== parseInt(endorsementId) && parseInt(this.eStatus) !== parseInt(actionStatusId)){
                    updatePrescStatus = false;
                    return false;
                }
            });
        }
        return updatePrescStatus;
    }

    /**
     * Update the prescription JSON data with changed endorsement new status
     * @param endorsementId
     * @param newStatusId
     * @param prescriptionData
     * @returns {*}
     */
    function updatePrescriptionData(oButton, endorsementId, newStatusId, prescriptionData){
        $.each(prescriptionData.endorsements, function(index, data) {
            if(parseInt(this.eId) === parseInt(endorsementId)){
                //console.log('--changed endor: '+endorsementId+' |oldsts: '+prescriptionData.endorsements[index].eStatus+'|new sts:'+newStatusId);
                prescriptionData.endorsements[index].eStatus = parseInt(newStatusId);
                jPrescriptionData = JSON.stringify(prescriptionData);
                oButton.parents('.endorsements').next('.prescription-btns').find('ul.list-inline').attr('data-json', jPrescriptionData);
                return true;
            }
        });
    }

    /**
     * Do the process of the selected Endorsement mark as rejected
     * @param oButton
     * @param actionType
     * @param actionMessage
     * @param actionUrl
     * @param id
     * @param prescriptionId
     * @param actionStatusId
     * @param prescriptionData
     */
    function doEndorsementMarkAsRejected(oButton, actionType, actionMessage, actionUrl, endorsementId, prescriptionId, actionStatusId, prescriptionData){
        swal({
            title: '',
            html: actionMessage,
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                showLoader();

                var isPrescriptionUpdate = checkIsLastEndorsementWithThisStatus(endorsementId, actionStatusId, prescriptionData);

                // repeat.status.markAsRejected
                axios.post(laroute.route(actionUrl,  {id: endorsementId}))
                    .then(function (response) {
                        if(typeof response.data === 'string' && response.data.indexOf("Access denied") > 0){
                            hideLoader();
                            swal('Access denied!', "You don't have permission to change the endorsement status as rejected.", 'error');
                        }
                        else if (parseInt(response.data) > 0) {
                            // update prescription status
                            if(isPrescriptionUpdate === true){
                                hideLoader();
                                var prescActionType = "mark-as-rejected";
                                var prescActionUrl = "repeat.status.markAsRejected";
                                var prescActionMessage = "Do you want to mark this repeat prescription as rejected?";
                                doMarkAsRejected(prescActionType, prescActionMessage, prescActionUrl, prescriptionId);
                            } else {
                                hideLoader();
                                updatePrescriptionData(oButton, endorsementId, actionStatusId, prescriptionData);
                                changeButtonState(oButton);
                                swal('', 'The endorsement marked as rejected.', 'success');
                            }
                        } else if (typeof response.data.error !== "undefined") {
                            hideLoader();
                            swal('Failed to mark as rejected.', response.data.error, 'error');
                        }

                    })
                    .catch(function (error) {
                        hideLoader();
                        swal({
                            type: 'error',
                            title: '',
                            html: 'An error occurred while marking as rejected. Please try again.'
                        })
                    })

                hideLoader();
            }
        })
    }


    /**
     * Do the process of the selected Endorsement mark as signed-off
     * @param oButton
     * @param actionType
     * @param actionMessage
     * @param actionUrl
     * @param id
     * @param prescriptionId
     * @param actionStatusId
     * @param prescriptionData
     */
    function doEndorsementMarkAsSignedOff(oButton, actionType, actionMessage, actionUrl, endorsementId, prescriptionId, actionStatusId, prescriptionData){
        swal({
            title: '',
            text: actionMessage,
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                showLoader();

                var isPrescriptionUpdate = checkIsLastEndorsementWithThisStatus(endorsementId, actionStatusId, prescriptionData);
                axios.post(laroute.route(actionUrl,  {id: endorsementId}))
                    .then(function (response) {

                        if(typeof response.data === 'string' && response.data.indexOf("Access denied") > 0){
                            hideLoader();
                            swal('Access denied!', "You don't have permission to change the endorsement status as signed-off.", 'error');
                        }
                        else if (parseInt(response.data) > 0) {
                            // update prescription status
                            if(isPrescriptionUpdate === true){
                                hideLoader();
                                var prescActionType = "mark-as-signedoff";
                                var prescActionUrl = "repeat.status.markAsSignedOff";
                                var prescActionMessage = "Do you want to mark this repeat prescription as signed off?";
                                doMarkAsSignedOff(prescActionType, prescActionMessage, prescActionUrl, prescriptionId);
                            } else {
                                hideLoader();
                                updatePrescriptionData(oButton, endorsementId, actionStatusId, prescriptionData);
                                changeButtonState(oButton);
                                swal('', 'The endorsement marked as signed-off.', 'success');
                            }
                        } else if (typeof response.data.error !== "undefined") {
                            hideLoader();
                            swal('Failed to mark as signed-off.', response.data.error, 'error');
                        }
                    })
                    .catch(function (error) {
                        hideLoader();
                        swal({
                            type: 'error',
                            title: '',
                            html: 'An error occurred. No action found! Please try again.'
                        })
                    })
            }
        })
    }


    /**
     * Do the process of the selected Endorsement mark as sent
     * @param oButton
     * @param actionType
     * @param actionMessage
     * @param actionUrl
     * @param id
     * @param prescriptionId
     * @param actionStatusId
     * @param prescriptionData
     */
    function doEndorsementMarkAsSent(oButton, actionType, actionMessage, actionUrl, endorsementId, prescriptionId, actionStatusId, prescriptionData){
        swal({
            title: '',
            html: actionMessage,
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                showLoader();

                var isPrescriptionUpdate = checkIsLastEndorsementWithThisStatus(endorsementId, actionStatusId, prescriptionData);

                // repeat.status.markAsRejected
                axios.post(laroute.route(actionUrl,  {id: endorsementId}))
                    .then(function (response) {
                        if(typeof response.data === 'string' && response.data.indexOf("Access denied") > 0){
                            hideLoader();
                            swal('Access denied!', "You don't have permission to change the endorsement status as sent.", 'error');
                        }
                        else if (parseInt(response.data) > 0) {
                            // update prescription status
                            if(isPrescriptionUpdate === true){
                                hideLoader();
                                var prescActionType = "mark-as-sent";
                                var prescActionUrl = "repeat.status.markAsSent";
                                var prescActionMessage = "Do you want to mark this prescription as sent?";
                                doMarkAsSent(prescActionType, prescActionMessage, prescActionUrl, prescriptionId);
                            } else {
                                hideLoader();
                                updatePrescriptionData(oButton, endorsementId, actionStatusId, prescriptionData);
                                changeButtonState(oButton);
                                swal('', 'The endorsement marked as sent.', 'success');
                            }
                        } else if (typeof response.data.error !== "undefined") {
                            hideLoader();
                            swal('Failed to mark as sent.', response.data.error, 'error');
                        }
                    })
                    .catch(function (error) {
                        hideLoader();
                        swal({
                            type: 'error',
                            title: '',
                            html: 'An error occurred while marking as sent. Please try again.'
                        })
                    })
                hideLoader();
            }
        })
    }



    /**
     * PRESCRIPTION
     */

    $('.prescription-action').on('click', function () {
        var actionType = $(this).data('action-type');
        var actionMessage = $(this).data('action-message');
        var actionUrl = $(this).data('action-url');
        var prescriptionId = $(this).data('prescription-id');

        if (actionType === 'mark-as-sent') {
            doMarkAsSent(actionType, actionMessage, actionUrl, prescriptionId);
        } else if (actionType === 'resend-fax') {
            doResendFax(actionType, actionMessage, actionUrl, prescriptionId)
        } else if (actionType === 'check-eps-status') {
            var patientId = $('.prescription-action').data('patient-id');
            doCheckEpsStatus(actionType, actionMessage, actionUrl, prescriptionId, patientId);
        } else if (actionType === 'mark-as-rejected') {
            doMarkAsRejected(actionType, actionMessage, actionUrl, prescriptionId);
        }
        else {
            doMarkAsSignedOff(actionType, actionMessage, actionUrl, prescriptionId)
        }
    })

    /**
     * Do the process of the selected Prescription mark as rejected
     * @param actionType
     * @param actionMessage
     * @param actionUrl
     * @param prescriptionId
     */
    function doMarkAsRejected(actionType, actionMessage, actionUrl, prescriptionId){
        swal({
            title: '',
            html: actionMessage,
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                showLoader();
                axios.post(laroute.route(actionUrl,  {id: prescriptionId}))
                    .then(function (response) {
                        hideLoader();
                        if(typeof response.data === 'string' && response.data.indexOf("Access denied") > 0){
                            swal('Access denied!', "You don't have permission to change the status as rejected.", 'error');
                        } else if (typeof response.data.error !== "undefined") {
                            swal('Failed to mark as rejected.', response.data.error, 'error')
                        } else if (response.status === 200) {
                            swal({'title':'Done!', html:'The repeat prescription marked as rejected. <br/><strong>Wait!</strong> Page reloading...', type:'success', showConfirmButton: false});
                            hideLoader();
                            location.reload();
                        } else {
                            swal('Failed to mark as rejected.', 'An error occurred while marking as rejected. Please try again.', 'error')
                        }
                    })
                    .catch(function (error) {
                        swal('Failed to mark as rejected.', 'An error occurred while marking as rejected. Please try again.', 'error')
                    })
            }
        })
    }

    /**
     * Do the process of the selected Prescription mark as sent
     * @param actionType
     * @param actionMessage
     * @param actionUrl
     * @param prescriptionId
     */
    function doMarkAsSent(actionType, actionMessage, actionUrl, prescriptionId){
        swal({
            title: '',
            text: actionMessage,
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                // Ask for the tracking number
                swal({
                    title: '',
                    html: 'Please enter prescription\'s tracking reference number (optional)',
                    input: 'text',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: (trackingRef) => {
                        return new Promise((resolve) => {
                            // repeat.status.markAsSent
                            axios.post(laroute.route(actionUrl, {id: prescriptionId}), {
                                trackingref: trackingRef
                            })
                                .then(function (response) {
                                    hideLoader();
                                    location.reload();
                                })
                                .catch(function () {
                                    hideLoader();
                                    swal({
                                        type: 'error',
                                        title: '',
                                        html: 'An error occurred while changing the status as sent. Please try again.'
                                    })
                                })
                        })
                    },
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    allowOutsideClick: false
                })
            }
        })
    }

    /**
     * Do the process of the selected Prescription, resend the fax
     * @param actionType
     * @param actionMessage
     * @param actionUrl
     * @param prescriptionId
     */
    function doResendFax(actionType, actionMessage, actionUrl, prescriptionId){
        swal({
            title: '',
            html: 'Re-send fax for this prescription? <p class="small">Please ensure that you have enough credits available on the fax account.</p>',
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                showLoader();
                // repeat.status.resendFax
                axios.post(laroute.route(actionUrl,  {id: prescriptionId}))
                    .then(function (response) {
                        hideLoader();
                        if(typeof response.data === 'string' && response.data.indexOf("Access denied") > 0){
                            swal('Access denied!', "You don't have permission to access resend fax function.", 'error');
                        }
                        if (typeof response.data.id !== "undefined") {
                            swal('', 'Fax successfully sent', 'success');
                        } else if (typeof response.data.error !== "undefined") {
                            swal('Failed to send fax', response.data.error, 'error')
                        }
                    })
                    .catch(function (error) {
                        hideLoader();
                        swal({
                            type: 'error',
                            title: '',
                            html: 'An error occurred while re-sending the fax. Please try again.'
                        })
                    })
            }
        })
    }

    /**
     * Do the process of the selected Prescription: check the EPS status
     * @param actionType
     * @param actionMessage
     * @param actionUrl
     * @param prescriptionId
     */
    function doCheckEpsStatus(actionType, actionMessage, actionUrl, prescriptionId, patientId){
        showLoader();
        axios.get(laroute.route(actionUrl,  {patientId: patientId}))
            .then(function (response) {
                if (null === response.data.epsMatchValue) {
                    swal({
                        title: '',
                        html: 'EPS Status Unknown',
                        type: 'info'
                    })
                } else {
                    swal({
                        title: '',
                        html: 'EPS Matched<br />(' + response.data.espStatusMatched + '%)',
                        type: 'info'
                    })
                }

                hideLoader();
            })
            .catch(function () {
                hideLoader();
                swal({
                    type: 'error',
                    title: '',
                    html: 'An error occurred while changing EPS status.  Please try again.'
                })
            })
    }

    /**
     * Do the process of the selected Prescription mark as signed-off
     * @param actionType
     * @param actionMessage
     * @param actionUrl
     * @param prescriptionId
     */
    function doMarkAsSignedOff(actionType, actionMessage, actionUrl, prescriptionId){
        swal({
            title: '',
            text: actionMessage,
            type: 'question',
            showCancelButton: true,
            allowEscapeKey: false,
            allowEnterKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if (result.value) {
                showLoader();
                axios.post(laroute.route(actionUrl,  {id: prescriptionId}))
                    .then(function (response) {
                        hideLoader();
                        location.reload();
                    })
                    .catch(function () {
                        hideLoader();
                        swal({
                            type: 'error',
                            title: '',
                            html: 'An error occurred. No action found! Please try again.'
                        })
                    })
            }
        })
    }

});
