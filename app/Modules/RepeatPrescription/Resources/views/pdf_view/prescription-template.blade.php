<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Page Description">
    <meta name="author" content="DevAdmin">
    <title>Page Title</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
</head>
<body>
<div class="col-xs-12">
    <div class="headerInfo col-xs-12 no-padding">
        <h3 class="text-center"><strong>Patient Repeat Prescription Request</strong></h3>
        <p>
            Request made by {!! $prescription->Patient->PatientForename !!} {!! $prescription->Patient->PatientSurname !!}<br>
            <strong>Date:</strong> {!!$prescription->dateraised !!}<br>
            <strong>Patient Contact Number:</strong> {!! $prescription->Patient->PatientMobile !!}
        </p>
    </div>
    <div class="myInfo col-xs-12 no-padding">
        <h4><strong>Patient Information</strong></h4>
        <ul class="list-unstyled">
            <li><strong>Name:</strong> {!! $prescription->Patient->PatientForename !!} {!! $prescription->Patient->PatientSurname !!}</li>
            <li><strong>Address:</strong> {!! $prescription->Patient->PatientAddress1 !!} {!! $prescription->Patient->PatientAddress2 !!} {!! $prescription->Patient->PatientAddress3 !!} {!! $prescription->Patient->PatientAddress4 !!} {!! $prescription->Patient->PatientAddress5 !!}</li>
            <li><strong>Date of Birth:</strong> {!! $prescription->Patient->PatientDOB !!}</li>
            <li><strong>NHS Number:</strong> {!! $prescription->Patient->NHSNumber !!}</li>
        </ul>
    </div>
    <div class="surgeryInfo col-xs-12 no-padding">
        <h4><strong>Doctor Information</strong></h4>
        <ul class="list-unstyled">
            <li><strong>Doctor Name:</strong> {!! $prescription->Patient->GPfirstname !!} {!! $prescription->Patient->GPlastname !!}</li>
            <li><strong>Surgery Name:</strong> {!! $prescription->Patient->SurgeryName !!}</li>
            <li><strong>Surgery Address:</strong> {!! $prescription->Patient->SurgeryAddress1 !!} {!! $prescription->Patient->SurgeryAddress2 !!} {!! $prescription->Patient->SurgeryCity !!} {!! $prescription->Patient->SurgeryCounty !!} {!! $prescription->Patient->SurgeryPostcode !!}</li>
        </ul>
    </div>
    <hr/>
    <h3>Items</h3>
    <p>This is not a third party. This request was generated by
        <strong>{!! $prescription->Patient->PatientForename !!} {!! $prescription->Patient->PatientSurname !!}</strong>
        using the now patient App. Under the NHS constitution I have exercised my patient choice right
        and I have actively chosen to use this service. I have given my consent for you as my GP to send my prescription
        electronically to Now Pharmacy FTQ25</p>
    <table class="table table-striped table-bordered" width="100%" align="left">
        <thead>
        <tr>
            <th style="text-align: left; padding: 5px;">Item</th>
        </tr>
        </thead>
        <tbody>
        @foreach($prescription->prescriptionendorsements as $key => $item)
            <tr>
                <td style="text-align: left;  padding: 5px;">
                    {!! $item->NM !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>