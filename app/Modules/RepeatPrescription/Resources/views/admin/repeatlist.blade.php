<div class="x_panel">
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                {!! BootForm::open()->url()->role('form')->method('post')->id('repeatprescriptionSearch')->data('status', $status) !!}
                <div class="row">
                    <div class="col-md-4">
                            {!! BootForm::text('NHS number', 'nhs_number')->value($nhs_number) !!}
                            {!! BootForm::hidden('status')->value($status) !!}
                    </div>
                    <div class="col-md-4 btn-placing">
                            {!! BootForm::button('Search')->type('submit')->class('btn btn-primary') !!}
                    </div>
                </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
</div>

<div class="x_panel">
    <div class="x_title">
        <h2>Repeat Prescriptions: {{ $status }}</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        @if($prescriptions !== null && $prescriptions->count() > 0)
            <table class="table">
                <thead>
                <tr>
                    <th class="col-md-2">
                        Patient Name
                    </th>
                    <th class="col-md-1">
                        Items
                    </th>
                    <th>
                        Date Raised
                    </th>
                    <th>
                        Date Escalated
                    </th>
                    <th>
                        Date Rejected
                    </th>
                    <th>
                        Date Signed Off
                    </th>
                    <th>
                        Date Sent
                    </th>
                    <th>
                        NHS Number
                    </th>
                    <th>
                        EPS
                    </th>
                    <th class="col-md-2 text-right">
                        Actions
                    </th>
                </tr>
                </thead>
                <tbody>
                {{--@php(dd($prescriptions[0]->Patient))--}}
                <?php $x = 0; ?>
                @foreach($prescriptions as $prescription)
                    <?php $trclass = ($x%2===0)?'tr-even':'tr-odd'; $endors = []; ?>
                    <tr class="tr-rp {{ $trclass }}">
                        <td>
                            <a href="/admin/patient/view/{{ $prescription->Patient->PatientId ?? '' }}" target="_blank">
                                <strong>{{ $prescription->Patient->PatientForename ?? '' }} {{ $prescription->Patient->PatientSurname  ?? ''}}</strong>
                            </a>
                        </td>
                        <td>
                            {{ $prescription->prescriptionendorsements->count() }}
                        </td>
                        <td>
                            {{ $prescription->dateraised }}
                        </td>
                        <td>
                            {{ $prescription->getDateEscalated() }}
                        </td>
                        <td>
                            {{ $prescription->getDateRejected() }}
                        </td>
                        <td>
                            {{ $prescription->getDateSignedOff() }}
                        </td>
                        <td>
                            {{ $prescription->getDateSent() }}
                        </td>
                        <td>
                            {{ $prescription->Patient->NHSNumber }}
                        </td>
                        <td>
                            @if (null !== $prescription->Patient->eps)
                                <strong>EPS:</strong> {{ round($prescription->Patient->eps->EstimatedEPS, 2) * 100 }}
                                %
                            @else
                                <strong>EPS:</strong> N/A
                            @endif
                        </td>
                        <td class="text-right">
                            @if (strtolower(trim($prescription->Patient->PatientAddress5)) !== strtolower(trim($prescription->Patient->AlternateAddress5)))
                                <a href="javascript:void(0)" data-toggle="popover" title="Address Details"
                                   data-content="
                                        <h6><strong>Home Address</strong></h6>
                                        <ul class='list-unstyled'>
                                            <li>{{ $prescription->Patient->PatientAddress1 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->PatientAddress2 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->PatientAddress3 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->PatientAddress4 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->PatientAddress5 ?? ''  }}</li>
                                        </ul>
                                        <h6><strong>Delivery Address</strong></h6>
                                        <ul class='list-unstyled'>
                                            <li>{{ $prescription->Patient->AlternateAddress1 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->AlternateAddress2 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->AlternateAddress3 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->AlternateAddress4 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->AlternateAddress5 ?? ''  }}</li>
                                        </ul>
                                    ">
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-home fa-stack-1x"></i>
                                      <i class="fa fa-ban fa-stack-2x text-danger"></i>
                                    </span>
                                </a>
                            @else
                                <a href="javascript:void(0)" data-toggle="popover" title="Address Details"
                                   data-content="
                                        <h6><strong>Home Address</strong></h6>
                                        <ul class='list-unstyled'>
                                            <li>{{ $prescription->Patient->PatientAddress1 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->PatientAddress2 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->PatientAddress3 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->PatientAddress4 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->PatientAddress5 ?? ''  }}</li>
                                        </ul>
                                        <h6><strong>Delivery Address</strong></h6>
                                        <ul class='list-unstyled'>
                                            <li>{{ $prescription->Patient->AlternateAddress1 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->AlternateAddress2 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->AlternateAddress3 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->AlternateAddress4 ?? ''  }}</li>
                                            <li>{{ $prescription->Patient->AlternateAddress5 ?? ''  }}</li>
                                        </ul>
                                    ">
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-circle fa-stack-2x text-success"></i>
                                      <i class="fa fa-home fa-stack-1x" style="color: #ffffff !important"></i>
                                    </span>
                                </a>
                            @endif
                            <a href="{{ URL::route('admin.prescriptions.repeatshow', $prescription->prescriptionid) }}"
                               class="btn btn-primary btn-xs">
                                View
                            </a>

                            <a href="javascript:void(0)"
                               class="btn btn-primary btn-xs btn-endorsements">
                                Endorsements
                            </a>

                        </td>
                    </tr>
                    <tr class="endorsements {{ $trclass }}">
                        <td colspan="10">
                            <div class="row-subtable">
                                <div class="row row-endorsements">Endorsements</div>
                                <div class="row">
                                    @if($prescription->prescriptionendorsements->count() > 0)
                                        <table class="table table-striped">
                                            <tr>
                                                <th class="col-md-1">#</th>
                                                <th class="col-md-3">Item</th>
                                                <th class="col-md-1">Quantity</th>
                                                <th class="col-md-1">Units</th>
                                                <th class="col-md-1">Status</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                            <?php
                                            $n = 1;
                                            $endors =  ['pId' => (int)$prescription->prescriptionid,
                                                        'pStatus' => (int)$prescription->prescriptionstatus,
                                                        'endorsements' => []];
                                            ?>
                                            @foreach($prescription->prescriptionendorsements as $endorsement)
                                                <?php
                                                $endors['endorsements'][] = ['eId' => $endorsement->endorsementid, 'eStatus' => (int)$endorsement->Status];
                                                ?>
                                                <tr>
                                                    <td class="col-md-1">
                                                        {{ $n }}
                                                    </td>
                                                    <td class="col-md-3">
                                                        {{ $endorsement->NM ?? '' }}
                                                    </td>
                                                    <td>
                                                        {{ $endorsement->Quantity ?? '' }}
                                                    </td>
                                                    <td>
                                                        {{ $endorsement->Units ?? '' }}
                                                    </td>
                                                    <td>
                                                        {{ \App\Modules\RepeatPrescription\Repositories\EndorsementRepository::getStatusString($endorsement->Status) ?? '' }}
                                                    </td>
                                                    <td class="text-right">
                                                        <ul class="list-inline pull-right col-md-12">

                                                            @if((int)$prescription->prescriptionstatus === \App\Modules\RepeatPrescription\Entities\Prescription::RAISED_DR_NOT_SIGNED ||
                                                            (int)$prescription->prescriptionstatus === \App\Modules\RepeatPrescription\Entities\Prescription::ESCALATED)

                                                                <li class="">
                                                                    @if((int)$endorsement->Status !== \App\Modules\RepeatPrescription\Entities\PrescriptionEndorsements::DR_REJECTED)
                                                                    <button type="button"
                                                                            class="btn btn-danger btn-xs mark-as-rejected endorsement-action"
                                                                            data-action-type="endorsement-mark-as-rejected"
                                                                            data-action-type-id="{{ \App\Modules\RepeatPrescription\Entities\PrescriptionEndorsements::DR_REJECTED }}"
                                                                            data-action-url="repeat.endorsements.status.markAsRejected"
                                                                            data-action-message="Do you want to mark this endorsement as rejected?"
                                                                            data-endorsement-id="{{ $endorsement->endorsementid }}"
                                                                            data-prescription-id="{{ $prescription->prescriptionid }}">
                                                                        <i class="fa fa-times"></i>
                                                                        Mark As Rejected
                                                                    </button>
                                                                    @else
                                                                        <button type="button" class="disabled btn btn-default btn-xs">
                                                                            <i class="fa fa-times"></i>
                                                                            Mark As Rejected
                                                                        </button>
                                                                    @endif
                                                                </li>

                                                                <li class="">
                                                                    @if((int)$endorsement->Status !== \App\Modules\RepeatPrescription\Entities\PrescriptionEndorsements::DR_SIGNEDOFF)
                                                                        <button type="button"
                                                                                class="btn btn-warning btn-xs mark-as-signed-off endorsement-action"
                                                                                data-action-type="endorsement-mark-as-signedoff"
                                                                                data-action-type-id="{{ \App\Modules\RepeatPrescription\Entities\PrescriptionEndorsements::DR_SIGNEDOFF }}"
                                                                                data-action-url="repeat.endorsements.status.markAsSignedOff"
                                                                                data-action-message="Do you want to mark this endorsement as signed off?"
                                                                                data-endorsement-id="{{ $endorsement->endorsementid }}"
                                                                                data-prescription-id="{{ $prescription->prescriptionid }}">
                                                                            <i class="fa fa-check"></i>
                                                                            Mark As Signed Off
                                                                        </button>
                                                                    @else
                                                                        <button type="button" class="disabled btn btn-default btn-xs">
                                                                            <i class="fa fa-check"></i>
                                                                            Mark As Signed Off
                                                                        </button>
                                                                    @endif
                                                                </li>
                                                            @endif

                                                            @if((int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::SENT &&
                                                                (int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::DR_REJECTED )
                                                                <li class="">
                                                                @if((int)$endorsement->Status !== \App\Modules\RepeatPrescription\Entities\PrescriptionEndorsements::SENT)
                                                                    <button type="button"
                                                                            class="btn btn-success btn-xs mark-as-sent endorsement-action"
                                                                            data-action-type="endorsement-mark-as-sent"
                                                                            data-action-type-id="{{ \App\Modules\RepeatPrescription\Entities\PrescriptionEndorsements::SENT }}"
                                                                            data-action-url="repeat.endorsements.status.markAsSent"
                                                                            data-action-message="Do you want to mark this endorsement as sent?"
                                                                            data-endorsement-id="{{ $endorsement->endorsementid }}"
                                                                            data-prescription-id="{{ $prescription->prescriptionid }}">
                                                                        <i class="fa fa-times"></i>
                                                                        Mark As Sent
                                                                    </button>
                                                                @else
                                                                    <button type="button" class="disabled btn btn-default btn-xs">
                                                                        <i class="fa fa-times"></i>
                                                                        Mark As Sent
                                                                    </button>
                                                                @endif
                                                                </li>
                                                            @endif

                                                        </ul>
                                                    </td>
                                                </tr>
                                                <?php $n++; ?>
                                            @endforeach
                                        </table>
                                    @else
                                        <p>No endorsement found!</p>
                                    @endif
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="prescription-btns tr-btns {{ $trclass }}">
                        <td colspan="6" class="freezed-cells"></td>
                        <td colspan="4" class="data-cells">
                            <ul class="list-inline pull-right"
                                data-json='<?php echo json_encode($endors); ?>'>

                                @if((int)$prescription->prescriptionstatus === \App\Modules\RepeatPrescription\Entities\Prescription::RAISED_DR_NOT_SIGNED ||
                                (int)$prescription->prescriptionstatus === \App\Modules\RepeatPrescription\Entities\Prescription::ESCALATED)
                                    @if((int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::DR_REJECTED)
                                        <li>
                                            <button type="button"
                                                    class="btn btn-danger btn-xs mark-as-rejected prescription-action"
                                                    data-action-type="mark-as-rejected"
                                                    data-action-url="repeat.status.markAsRejected"
                                                    data-action-message="Do you want to mark this prescription and all the endorsements of this prescription as rejected?"
                                                    data-prescription-id="{{ $prescription->prescriptionid }}">
                                                <i class="fa fa-times"></i>
                                                Mark As Rejected
                                            </button>
                                        </li>
                                    @endif
                                    @if((int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::DR_SIGNEDOFF)
                                        <li>
                                            <button type="button"
                                                    class="btn btn-warning btn-xs mark-as-signed-off prescription-action"
                                                    data-action-type="mark-as-signedoff"
                                                    data-action-url="repeat.status.markAsSignedOff"
                                                    data-action-message="Do you want to mark this prescription and all the endorsements of this prescription as signed off?"
                                                    data-prescription-id="{{ $prescription->prescriptionid }}">
                                                <i class="fa fa-check"></i>
                                                Mark As Signed Off
                                            </button>
                                        </li>
                                    @endif
                                @endif

                                @if((int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::SENT &&
                                    (int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::DR_REJECTED)
                                    <li>
                                        <button type="button"
                                                class="btn btn-success btn-xs mark-as-sent prescription-action"
                                                data-action-type="mark-as-sent"
                                                data-action-url="repeat.status.markAsSent"
                                                data-action-message="Do you want to mark this prescription and all the endorsements of this prescription as sent?"
                                                data-prescription-id="{{ $prescription->prescriptionid }}">
                                            <i class="fa fa-times"></i>
                                            Mark As Sent
                                        </button>
                                    </li>
                                @endif

                                <li>
                                    <button type="button" class="btn btn-success btn-xs resend-fax prescription-action"
                                            data-action-type="resend-fax"
                                            data-action-url="repeat.status.resendFax"
                                            data-action-message="Do you want to resend this fax?"
                                            data-prescription-id="{{ $prescription->prescriptionid }}">
                                        <i class="fa fa-fax"></i>
                                        Re-send Fax
                                    </button>
                                </li>

                            </ul>
                        </td>
                    </tr>
                    <?php $x++; ?>
                @endforeach
                </tbody>
            </table>

            {{ $prescriptions->render() }}
        @else
            <p>
                No prescriptions to show
            </p>
        @endif
    </div>
</div>
<style>
    .btn-placing {
        margin-top: 23px;
    }
    .endorsements{
        display: none;
        background-color: #eff0f1 !important;
    }
    .row-endorsements {
        font-weight: bold;
        padding: 5px 0;
    }
    .row-subtable {
        float: right;
        width: 98%;
    }
    .row-subtable .row {
        margin-right: 2px;
    }
    .btn-clicked {
        color: #EFF0F1 !important;
        background-color: #A6A6A6;
        border-color: #EFF0F1;
    }
    .tr-even{
        background-color: #ffffff;
    }
    .tr-odd{
        background-color: #f9f9f9;
    }
    .tr-btns .freezed-cells {
        border-top: none;
    }
    .tr-btns .data-cells {
        border-top: 1px dashed #efefef;
    }
</style>