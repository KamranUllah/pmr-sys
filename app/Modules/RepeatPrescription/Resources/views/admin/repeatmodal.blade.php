<div class="col-xs-12" style="text-align: left;">
    <div class="col-xs-12 no-padding">
        <h4>Repeat Prescription</h4>
        <p><strong>ID:</strong> {{ $prescription->prescriptionid }}</p>
        <p><strong>Status:</strong> {{ ucwords($prescription->getStatusString()) }}</p>
        <p><strong>Delivery Address:</strong><br>{{ $prescription->address }}</p>
        <p><strong>Items:</strong> {{ $prescription->prescriptionendorsements->count() }}</p>
    </div>
    <div class="col-xs-12 no-padding">
        <h4>Patient</h4>
        <p><strong>ID:</strong> {{ $prescription->patientid }}</p>
        <p><strong>Name:</strong> {{ $prescription->Patient->PatientForename }} {{ $prescription->Patient->PatientSurname }}</p>
        <p><strong>Address:</strong> {{ $prescription->Patient->PatientAddress1 ? : '' }} {{ $prescription->Patient->PatientAddress2 ? : ''  }} {{ $prescription->Patient->PatientAddress3 ? : ''  }} {{ $prescription->Patient->PatientAddress4 ? : ''  }} {{ $prescription->Patient->PatientAddress5 ? : ''  }}</p>
        <p><strong>Contact Number:</strong> {{ $prescription->Patient->PatientMobile }}</p>
    </div>
</div>