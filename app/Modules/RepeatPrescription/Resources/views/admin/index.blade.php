
<div class="col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Repeat Prescriptions</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>
                Patient Name
            </th>
            <th>
                Items
            </th>
            <th>
                Issue Date
            </th>
            <th>
                Completed Date
            </th>
            <th>
                Collected By Courier
            </th>
            <th>
                Delivered To Patient
            </th>

            <th>
                View
            </th>
        </tr>
        </thead>
            </table>
        </div>
    </div>
</div>