<h2>Endorsements</h2>
<table  class="table table-striped table-bordered">
    <thead>
    <tr>
        <th class="col-md-2">#</th>
        <th class="col-md-8">Item</th>
        <th class="col-md-1">Quantity</th>
        <th class="col-md-1">Units</th>
    </tr>
    </thead>
    <tbody>
    @foreach($prescription->prescriptionendorsements as $key => $item)
        <tr>
            <td>
                {{ $key + 1 }}
            </td>
            <td>
                {{ $item->NM }} <br/>
                {{ $item->Directions1 }} <br/>
                {{ $item->Directions2 }}
            </td>
            <td>
                {{ $item->Quantity }}
            </td>
            <td>
                {{ $item->Units ?? 'N/A' }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>