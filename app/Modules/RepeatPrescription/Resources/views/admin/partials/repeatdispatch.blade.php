<h2>
    Dispatch Information
</h2>
<table  class="table table-striped table-bordered" data-equalizer-watch>
    <tbody>
    <tr>
        <td class="col-md-2">
            <strong>
                Courier
            </strong>
        </td>
        <td>
            {{ $prescription->courier_name ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Collected
            </strong>
        </td>
        <td>
            {{ $prescription->collected_at ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Delivered
            </strong>
        </td>
        <td>
            {{ $prescription->delivered_at ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Ref
            </strong>
        </td>
        <td>
            {{ $prescription->trackingref ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Status
            </strong>
        </td>
        <td>
            {{ $prescription->getStatusString() }}
        </td>
    </tr>

    </tbody>
</table>