<h2>
    Prescriber
</h2>
<table class="table table-striped table-bordered" data-equalizer-watch>
    <tbody>
    <tr>
        <td class="col-md-2">
            <strong>
                Date Prescribed
            </strong>
        </td>
        <td>
            {{ $prescription->raised_at ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Date Expired
            </strong>
        </td>
        <td>
            {{ $prescription->expired_at ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Date Completed
            </strong>
        </td>
        <td>
            {{ $prescription->completed_at ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Prescriber
            </strong>
        </td>
        <td>
            {{ $prescription->doctorname ?? 'N/A'  }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                G.M.C.
            </strong>
        </td>
        <td>
            {{ $prescription->doctorgmc ?? 'N/A' }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                SGN
            </strong>
        </td>
        <td>
            {{ $prescription->sgncode ?? 'N/A'  }}
        </td>
    </tr>
	<tr>
        <td>
            <strong>
                Prescription ID
            </strong>
        </td>
        <td>
            {{ $prescription->prescriptionid ?? 'N/A'  }}
        </td>
    </tr>
	<tr>
        <td>
            <strong>
                Prescribers Address
            </strong>
        </td>
        <td>
            {{ $prescription->Patient->PatientAddress1 }}<br />
            {{ $prescription->Patient->PatientAddress2 }}<br />
            {{ $prescription->Patient->PatientAddress3 }}<br />
            {{ $prescription->Patient->PatientAddress4 }}<br />
            {{ $prescription->Patient->PatientAddress5 }}


            <?php
			//echo '1 Lowry Plaza,<br>Salford Quays,<br>Manchester <br>M50 3UB';
			?>
        </td>
    </tr>

    </tbody>
</table>