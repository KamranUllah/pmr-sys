<h2>
    Patient
</h2>
<table class="table table-striped table-bordered" data-equalizer-watch>
    <tbody>
    <tr>
        <td class="col-md-2">
            <strong>
                Patient
            </strong>
        </td>
        <td>
            {{ $prescription->Patient->PatientForename }} {{ $prescription->Patient->PatientSurname }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Address
            </strong>
        </td>
        <td>
            {{ $prescription->Patient->PatientAddress1 }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Contact Number
            </strong>
        </td>
        <td>
            {{ $prescription->Patient->PatientMobile }}
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                DOB
            </strong>
        </td>
        <td>
            {{ $prescription->Patient->PatientDOB }}
            <ul class="list-unstyled">
                <li><strong>Exempt: </strong> {{ $patientExemptions && $patientExemptions->IsExempt ? 'Yes' : 'No' }}</li>
                @if ($patientExemptions && $patientExemptions->IsExempt)
                    <li><strong>Exemption Reason: </strong>{{ $patientExemptions->Reason ?? 'N/A' }}</li>
                    <li><strong>Certificate Number (if available): </strong>{{ (null !== $patientExemptions->CertificateNumber && strlen(trim($patientExemptions->CertificateNumber)) > 0) ? $patientExemptions->CertificateNumber : 'N/A' }}</li>
                @endif
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                Age
            </strong>
        </td>
        <td>
            {{ \Carbon\Carbon::createFromFormat('d/m/Y', $prescription->Patient->PatientDOB)->age }}
        </td>
    </tr>

    </tbody>
</table>