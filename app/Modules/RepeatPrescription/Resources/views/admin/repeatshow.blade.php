<div class="col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Repeat Prescription:
                {{ ucwords($prescription->getStatusString()) }}</h2>
            <ul class="nav navbar-right panel_toolbox">
                {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                {{--</li>--}}
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <ul class="small-block-grid-3 prescription-list" data-equalizer>
                <li class="col-xs-12 no-padding">
                    @include('repeatprescription::admin.partials.repeatpatient')
                </li>
                <li class="col-xs-12 no-padding">
                    @include('repeatprescription::admin.partials.repeatprescriber')
                </li>
                <li class="col-xs-12 no-padding">
                    @include('repeatprescription::admin.partials.repeatdispatch')
                </li>
                <li class="col-xs-12 no-padding">
                    @include('repeatprescription::admin.partials.repeatendorsements')
                </li>
            </ul>


            <div class="text-center col-xs-12">
                <div id="editor"></div>
                <ul class="button-group prescription-action-buttons">
                    <li>
                        <button type="submit" class="btn btn-primary btn-xs print-pdf" id="printContent" data-prescription-id="{{ $prescription->prescriptionid }}">
                            <i class="fa fa-print"></i>
                            Print Script
                        </button>
                    </li>
                    <li>
                        <button type="submit" class="btn btn-primary btn-xs download-pdf" id="cmd" data-prescription-id="{{ $prescription->prescriptionid }}">
                            <i class="fa fa-print"></i>
                            Download Script
                        </button>
                    </li>

                    @if((int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::RAISED_DR_NOT_SIGNED)
                        <li>
                            {!! Form::open(['route' => ['repeat.status.markAsNotSignedOff', $prescription->prescriptionid]]) !!}
                            <button type="submit" class="btn btn-primary btn-xs">
                                <i class="fa fa-check"></i>
                                Mark As Not Signed Off
                            </button>
                            {!! Form::close() !!}
                        </li>
                    @endif
                    @if((int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::DR_REJECTED)
                    <li>
                        {!! Form::open(['route' => ['repeat.status.markAsRejected', $prescription->prescriptionid]]) !!}
                        <button type="submit" class="btn btn-primary btn-xs">
                            <i class="fa fa-check"></i>
                            Mark As Rejected
                        </button>
                        {!! Form::close() !!}
                    </li>
                    @endif
                    @if((int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::DR_SIGNEDOFF)
                    <li>
                        {!! Form::open(['route' => ['repeat.status.markAsSignedOff', $prescription->prescriptionid]]) !!}
                        <button type="submit" class="btn btn-primary btn-xs">
                            <i class="fa fa-home"></i>
                            Mark As Signed Off
                        </button>
                        {!! Form::close() !!}
                    </li>
                    @endif


                    <li>
                        <button type="button" class="btn btn-primary btn-xs  prescription-action"
                                data-action-type="resend-fax"
                                data-action-url="repeat.status.resendFax"
                                data-action-message="Do you want re-send a fax for this prescription?"
                                data-prescription-id="{{ $prescription->prescriptionid }}">
                            <i class="fa fa-fax"></i>
                            Re-send Fax
                        </button>
                    </li>
                </ul>
            </div>

            @if((int)$prescription->prescriptionstatus !== \App\Modules\RepeatPrescription\Entities\Prescription::SENT)
                <div id="mark-as-sent" class="reveal-modal small col-xs-12" data-reveal>
                    <div class="row">
                        <div class="small-12 small-centered columns">
                            <h2 class="text-center" style="border-bottom: 2px solid #E6E9ED;">Mark Prescription As
                                Sent</h2>
                            {!! Form::open(['route' => ['repeat.status.markAsSent', $prescription->prescriptionid], 'method' => 'post', 'class' => '.form-horizontal']) !!}
                            <div class="text-center">
                                <div class="form-group">
                                    {!! Form::label('trackingref', 'Tracking Reference*:',['class' => 'tracking-text control-label']) !!}
                                    {!! Form::text('trackingref', null, [
                                    'prescriptionid' => 'trackingref',
                                    'placeholder' => 'Reference provided by courier ',
                                    'class' => 'tracking-input form-control',
                                    'style' => 'width: 100% !important;padding: 5px'
                                    ])
                                    !!}
                                </div>
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-xs">
                                    <i class="fa fa-check"></i>
                                    Mark As Sent
                                </button>
                                <p>
                                    <small class="label alert">* indicates a required field</small>
                                </p>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
                </div>
            @endif
            <div id="prescriptionId" style="display: none">{{ $prescription->prescriptionid }}</div>
            <div id="printable-script" class="col-xs-12" style="display: none;color:black !important;">
                <div class="headerInfo col-xs-12 no-padding">
                    <h3 style="text-align: center;color:black;"><strong><i>Patient Repeat Prescription Request</i></strong></h3>
                    <p>
                         Request made by {{ $prescription->Patient->PatientForename }} {{ $prescription->Patient->PatientSurname }}<br>
                        {{$prescription->dateraised }}<br>
                        Patient Contact Number: {{ $prescription->Patient->PatientMobile }}
                    </p>
                </div>
                <div class="myInfo col-xs-12 no-padding">
                    <h2><strong><i>My Information</i></strong></h2>
                    <ul>
                        <li>Name  :{{ $prescription->Patient->PatientForename }} {{ $prescription->Patient->PatientSurname }}</li>
                        <li>Address  :{{ $prescription->Patient->PatientAddress1 }} {{ $prescription->Patient->PatientAddress2 }} {{ $prescription->Patient->PatientAddress3 }} {{ $prescription->Patient->PatientAddress4 }} {{ $prescription->Patient->PatientAddress5 }}</li>
                        <li>Date of Birth  :{{ $prescription->Patient->PatientDOB }}</li>
                        <li>NHS Number  :{{ $prescription->Patient->NHSNumber }}</li>
                    </ul>
                </div>
                <div class="surgeryInfo col-xs-12 no-padding">
                    <h2><strong>Doctor Information</strong></h2>
                    <ul>
                        <li>Doctor Name  :{{ $prescription->Patient->GPfirstname }} {{ $prescription->Patient->GPlastname }}</li>
                        <li>Surgery Name  :{{ $prescription->Patient->SurgeryName }}</li>
                        <li>Surgery Address  :{{ $prescription->Patient->SurgeryAddress1 }} {{ $prescription->Patient->SurgeryAddress2 }} {{ $prescription->Patient->SurgeryCity }} {{ $prescription->Patient->SurgeryCounty }} {{ $prescription->Patient->SurgeryPostcode }}</li>
                    </ul>
                </div>
                <hr/>
                <h2>Items</h2>
                <p>This is not a third party. This request was generated by <strong>{{ $prescription->Patient->PatientForename }} {{ $prescription->Patient->PatientSurname }}</strong> using the now patient App. Under the NHS constitution I have exercised my patient choice right
                    and I have actively chosen to use this service. I have given my consent for you as my GP to send my prescription electronically to Now Pharmacy FTQ25</p>
                <table  class="table table-striped table-bordered" width="100%" border="1" align="left" style="border: 1px solid black;text-align: left;width:100%; ">
                    <thead>
                    <tr>
                        <th style="text-align: left;">Item</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($prescription->prescriptionendorsements as $key => $item)
                        <tr>
                            <td style="text-align: left;">
                                {{ $item->NM }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<style type="text/css">

    .prescription-list {
        list-style-type: none;
        padding: 0px;
    }

    .prescription-list li h2 {
        border-bottom: 2px solid #E6E9ED;
    }

    .no-padding {
        padding: 0px;
    }

    .prescription-action-buttons {
        list-style-type: none;
        display: inline-block;
        margin-top: 20px;
    }

    .prescription-action-buttons li {
        float: left;
    }


</style>

