<?php namespace App\Modules\RepeatPrescription\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\RepeatPrescription\Entities\Prescription;
use App\Modules\RepeatPrescription\Entities\PrescriptionNotes;
use Carbon\Carbon;

class PrescriptionRepository extends BaseRepository
{

    protected $model = Prescription::class;
    protected $relationships = ['patient', 'prescriptionendorsements', 'prescstatus','faxrecords','notes','prescourier'];

    public function getByStatus($status, $paginate = 0)
    {
        if ($paginate === 0) {
            return $this->model
                ->with($this->relationships)
                ->where([['prescriptionstatus', '=', $status],['repeatprescription', '=', 1]])
                ->orderBy('dateraised', 'desc')
                ->get();
        } else {
            return $this->model
                ->with($this->relationships)
                ->where([['prescriptionstatus', '=', $status],['repeatprescription', '=', 1]])
                ->orderBy('dateraised', 'desc')
                ->paginate($paginate);
        }


    }


    public function getRepeat($paginate = 0,array $params = []){

        $query = $this->model
            ->with($this->relationships)
             ->where('repeatprescription', '=', 1);

            //display only requested repeat prescriptions

            if(array_key_exists('status',$params) && ($params['status'] !== 'ALL'
                    && $params['status'] !== 'RAISEDNOTSIGNED'
                    && $params['status'] !== 'ESCALATED'
                    && $params['status'] !== 'REJECTED'
                    && $params['status'] !== 'SIGNEDOFF'
                    && $params['status'] !== 'DELIVERED'
                    && $params['status'] !== 'CANCELLED'
                    && $params['status'] !== 'SENT') ){
                return null;
            }

            if(!(array_key_exists('status',$params))){
                $params['status'] = null;
            }


        switch ($params['status']) {
            case "RAISEDNOTSIGNED":
                $query->where([['prescriptionstatus', '=', Prescription::RAISED_DR_NOT_SIGNED]]);
                break;
            case "ESCALATED":
                $query->where([['prescriptionstatus', '=', Prescription::ESCALATED]]);
                break;
            case "REJECTED":
                $query->where([['prescriptionstatus', '=', Prescription::DR_REJECTED]]);
                break;
            case "SIGNEDOFF":
                $query->where([['prescriptionstatus', '=', Prescription::DR_SIGNEDOFF]]);
                break;
            case "SENT":
                $query->where([['prescriptionstatus', '=', Prescription::SENT]]);
                break;
            case "DELIVERED":
                $query->where([['prescriptionstatus', '=', Prescription::DELIVERED]]);
                break;
            case "CANCELLED":
                $query->where([['prescriptionstatus', '=', Prescription::CANCELLED_CLINICAL]]);
                break;
            default:
                $query->where(function ($q) {
                    $q->where([['prescriptionstatus', '=', Prescription::RAISED_DR_NOT_SIGNED]])
                        ->orWhere([['prescriptionstatus', '=', Prescription::ESCALATED]])
                        ->orWhere([['prescriptionstatus', '=', Prescription::DR_REJECTED]])
                        ->orWhere([['prescriptionstatus', '=', Prescription::DR_SIGNEDOFF]])
                        ->orWhere([['prescriptionstatus', '=', Prescription::SENT]]);
                });
        }


            //filter by data
            if(array_key_exists('filterby',$params)){

                $query->where(function ($q) use ($params) {
                    $q->where('prescriptionid','like','%'.$params['filterby'].'%')
                        ->orWhere('patientid','like','%'.$params['filterby'].'%')
                        ->orWhere('patient','like','%'.$params['filterby'].'%')
                        ->orWhere('dateraised','like','%'.$params['filterby'].'%')
                        ->orwhereHas('patient', function ($q) use ($params) {
                            $q->where('NHSNumber','like', '%'.$params['filterby'].'%');
                        });
                });

            }


        if(config('pmr.remove_90_days')) {

            $fromDate =  Carbon::now()->subMonth(3);
            $toDate = Carbon::now();
            $query->whereBetween('dateraised', array($fromDate->toDateTimeString(), $toDate->toDateTimeString()));

        }



        if ($paginate === 0) {
            return $query
                ->orderBy(array_key_exists('sortby',$params) ? $params['sortby']: 'dateraised', array_key_exists('type',$params) ? $params['type'] : 'desc')
                ->get();
        } else {
            return $query
                ->orderBy(array_key_exists('sortby',$params) ? $params['sortby']: 'dateraised', array_key_exists('type',$params) ? $params['type'] : 'desc')
                ->paginate($paginate);
        }

    }


    public function countRepeatPrescriptions(){

        // Temporarily increase memory limit to 256MB
        ini_set('memory_limit','256M');

        $query = $this->model
            ->where(function ($q) {
                $q->where([['prescriptionstatus', '=', Prescription::RAISED_DR_NOT_SIGNED]])
                    ->orWhere([['prescriptionstatus', '=', Prescription::ESCALATED]])
                    ->orWhere([['prescriptionstatus', '=', Prescription::DR_REJECTED]])
                    ->orWhere([['prescriptionstatus', '=', Prescription::DR_SIGNEDOFF]])
                    ->orWhere([['prescriptionstatus', '=', Prescription::SENT]]);
            })
            ->where([['repeatprescription', '=', 1]]);

        if(config('pmr.remove_90_days')) {

            $fromDate =  Carbon::now()->subMonth(3);
            $toDate = Carbon::now();
            return $query
                ->whereBetween('dateraised', array($fromDate->toDateTimeString(), $toDate->toDateTimeString()))
                ->orderBy('dateraised', 'desc')
                ->get();

        }

        return $query
            ->orderBy('dateraised', 'desc')
            ->get();
    }

    public function updateEndorsementCount(){

        // Temporarily increase memory limit to 256MB
        ini_set('memory_limit','256M');

        Prescription::where('endorsements', '=', 0)->get()->each(function($item){
            $item->endorsements = $item->endorsement_total;
            $item->save();
        });
    }

    public function updateSentFaxCount($params){

        // Temporarily increase memory limit to 256MB
        ini_set('memory_limit','256M');

        $query = Prescription::where('repeatprescription', '=', 1);

        if(array_key_exists('status',$params)){

            switch ($params['status']) {
                case "RAISEDNOTSIGNED":
                    $query->where([['prescriptionstatus', '=', Prescription::RAISED_DR_NOT_SIGNED]]);
                    break;
                case "ESCALATED":
                    $query->where([['prescriptionstatus', '=', Prescription::ESCALATED]]);
                    break;
                case "REJECTED":
                    $query->where([['prescriptionstatus', '=', Prescription::DR_REJECTED]]);
                    break;
                case "SIGNEDOFF":
                    $query->where([['prescriptionstatus', '=', Prescription::DR_SIGNEDOFF]]);
                    break;
                case "SENT":
                    $query->where([['prescriptionstatus', '=', Prescription::SENT]]);
                    break;
                default:
                    $query->where(function ($q) {
                        $q->where([['prescriptionstatus', '=', Prescription::RAISED_DR_NOT_SIGNED]])
                            ->orWhere([['prescriptionstatus', '=', Prescription::ESCALATED]])
                            ->orWhere([['prescriptionstatus', '=', Prescription::DR_REJECTED]])
                            ->orWhere([['prescriptionstatus', '=', Prescription::DR_SIGNEDOFF]])
                            ->orWhere([['prescriptionstatus', '=', Prescription::SENT]]);
                    });
            }
        }

        $query->get()->each(function($item){
            $item->sentfax = $item->sent_total;
            $item->save();
        });
    }


    public function rejectedToDeclined(){
        // Temporarily increase memory limit to 256MB
        ini_set('memory_limit','256M');

        try{
            $query = $this->model
                ->where([['prescriptionstatus', '=', Prescription::DR_REJECTED],['repeatprescription', '=', 1]]);

            $fromDate =  Carbon::now()->subDays(30);
            $toDate = Carbon::now();
            $query->whereNotBetween('dateraised', array($fromDate->toDateTimeString(), $toDate->toDateTimeString()))
                ->update(['prescriptionstatus' => Prescription::PHARMACY_DECLINED]);

            auditLog('[BACKEND SYSTEM] Moved all prescriptions older than 30 days from DR REJECTED status to PHARMACY DECLINED status');

        }catch(\Exception $exception){

        }
    }

    public function sentToDelivered(){
        // Temporarily increase memory limit to 256MB
        ini_set('memory_limit','256M');

        try{
            $query = $this->model
                ->where([['prescriptionstatus', '=', Prescription::SENT],['repeatprescription', '=', 1]]);

            $fromDate =  Carbon::now()->subDays(14);
            $toDate = Carbon::now();
            $query->whereNotBetween('dateraised', array($fromDate->toDateTimeString(), $toDate->toDateTimeString()))
                ->update(['prescriptionstatus' => Prescription::DELIVERED]);

            auditLog('[BACKEND SYSTEM] Moved all prescriptions older than 30 days from SENT status to DELIVERED status');

        }catch(\Exception $exception){

        }

    }

    public function getAllByPatientId($patientId)
    {

        return $this->model
            ->with($this->relationships)
            ->where([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::RAISED_DR_NOT_SIGNED],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::ESCALATED],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::DR_REJECTED],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::DR_SIGNEDOFF],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::SENT],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::CANCELLED_CLINICAL],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::DELIVERED],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::PHARMACY_DECLINED],['repeatprescription', '=', 1]])
            ->orderBy('dateraised', 'desc')
            ->get();
    }

    public function getByPatientId($patientId)
    {
        return $this->model
            ->with($this->relationships)
            ->where([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::RAISED_DR_NOT_SIGNED],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::ESCALATED],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::DR_REJECTED],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::DR_SIGNEDOFF],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::SENT],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::CANCELLED_CLINICAL],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::DELIVERED],['repeatprescription', '=', 1]])
            ->orWhere([['patientid', '=', $patientId],['prescriptionstatus', '=', Prescription::PHARMACY_DECLINED],['repeatprescription', '=', 1]])
            ->orderBy('dateraised', 'desc')
            ->get();
    }

    public function getByImportPatientData($importData)
    {

        //filter out titles Mr, Miss and Mrs
        $PatientNameFull = array_filter(explode(" ", $importData['patient_name']),function($item){
            $titleArray = ['Mr','Mrs','Miss','Ms'];
            $counter = 0;
            foreach($titleArray as $title){
                if(strtolower($item) === strtolower($title)){
                    $counter= $counter + 1;
                }
            }


            $result = $counter > 0 ? false : true;
            return $result;

        });

        //select only prescriptions which are repeats and in signed off status
        $query = $this->model
            ->with($this->relationships)
            ->where([['repeatprescription', '=', 1],['prescriptionstatus', '=', Prescription::DR_SIGNEDOFF]]);


        //select only the ones where the patient first name and last name initials match database
        $query->whereHas('patient', function ($q) use ($PatientNameFull) {
            $firstNameCharacter = substr(array_shift($PatientNameFull), 0, 1);
            $lastNameCharacter = substr(array_pop($PatientNameFull), 0, 1);

            $q->where([['PatientForename', 'like', $firstNameCharacter.'%'],['PatientSurname', 'like', $lastNameCharacter.'%']])
                ->orWhere([['PatientForename', 'like', $lastNameCharacter.'%'],['PatientSurname', 'like', $firstNameCharacter.'%']]);
        });

        //select where first line,second line and postcode contained in prescription address data
        //check also patient database and match also where patient address match import address data
        $query->where(function ($q) use ($importData) {
            $q->where([['address', 'like', '%'.$importData['patient_address_line_1'].'%'],['address', 'like', '%'.$importData['patient_address_line_2'].'%'],['address', 'like', '%'.$importData['patient_postcode'].'%']])
                ->orwhereHas('patient', function ($q) use ($importData) {
                    $q->where('PatientAddress5', 'like', '%'.$importData['patient_postcode'].'%')
                        ->where([['PatientAddress1', 'like', '%'.$importData['patient_address_line_1'].'%'],['PatientAddress2', 'like', '%'.$importData['patient_address_line_2'].'%']])
                        ->orWhere([['PatientAddress2', 'like', '%'.$importData['patient_address_line_1'].'%'],['PatientAddress1', 'like', '%'.$importData['patient_address_line_2'].'%']]);
                });
        });



        //display by raised day and pick one prescriptionid
        return $query->orderBy('dateraised', 'desc')
            ->pluck('prescriptionid');

    }

    public function getByPSLImportPatientData($importData)
    {

        //filter out titles Mr, Miss and Mrs
        $PatientNameFull = array_filter(explode(" ", $importData['patient_name']),function($item){
            $titleArray = ['Mr','Mrs','Miss','Ms'];
            $counter = 0;
            foreach($titleArray as $title){
                if(strtolower($item) === strtolower($title)){
                    $counter= $counter + 1;
                }
            }


            $result = $counter > 0 ? false : true;
            return $result;

        });

        $totalItem = $importData['endorsements'];


        //select only prescriptions which are repeats and in not signed off and escalated status
        $query = $this->model
            ->with($this->relationships)
            ->where([['repeatprescription', '=', 1],['endorsements','>=',$totalItem]]);

        $query->where(function($q){
            $q->where('prescriptionstatus', '=', Prescription::RAISED_DR_NOT_SIGNED);
            $q->orWhere('prescriptionstatus', '=', Prescription::ESCALATED);
             });


        //select only the ones where the patient first name and last name initials match database
        $query->whereHas('patient', function ($q) use ($PatientNameFull) {
            $firstNameCharacter = substr(array_shift($PatientNameFull), 0, 1);
            $lastNameCharacter = substr(array_pop($PatientNameFull), 0, 1);

            $q->where([['PatientForename', 'like', $firstNameCharacter.'%'],['PatientSurname', 'like', $lastNameCharacter.'%']])
                ->orWhere([['PatientForename', 'like', $lastNameCharacter.'%'],['PatientSurname', 'like', $firstNameCharacter.'%']]);
        });

        //select where first line,second line and postcode contained in prescription address data
        //check also patient database and match also where patient address match import address data

        $query->where(function ($q) use ($importData) {
            $q->where([['address', 'like', '%'.$importData['address_line'].'%'],['address', 'like', '%'.$importData['postcode'].'%']])
                ->orwhereHas('patient', function ($q) use ($importData) {
                    $q->where('PatientAddress5', 'like', '%'.$importData['postcode'].'%')
                        ->where([['PatientAddress1', 'like', '%'.$importData['address_line'].'%']])
                         ->orWhere([['PatientAddress2', 'like', '%'.$importData['address_line'].'%']]);
                });
        });




        //display by raised day and pick one prescriptionid
        return $query->orderBy('dateraised', 'desc')
            ->pluck('prescriptionid');

    }

    public function getByNHSNumber($status, $NHSNumber = null, $paginate = 0)
    {
        if (null !== $NHSNumber) {
            if ($paginate === 0) {
                return $this->model
                    ->with($this->relationships)
                    ->whereHas('patient', function ($q) use ($NHSNumber) {
                        $q->where('NHSNumber', $NHSNumber);
                    })
                    ->where([['prescriptionstatus', '=', $status],['repeatprescription', '=', 1]])
                    ->orderBy('dateraised', 'desc')
                    ->get();
            } else {
                return $this->model
                    ->with($this->relationships)
                    ->whereHas('patient', function ($q) use ($NHSNumber) {
                        $q->where('NHSNumber', $NHSNumber);
                    })
                    ->where([['prescriptionstatus', '=', $status],['repeatprescription', '=', 1]])
                    ->orderBy('dateraised', 'desc')
                    ->paginate($paginate);
            }
        } else {
            return null;
        }
    }

    public function getPrivate($paginate = 0,array $params = []){


        $query = $this->model
            ->with($this->relationships)
            ->where('repeatprescription', '=', 0);

        $query->where(function ($q) {
            $q->where([['prescriptionstatus', '!=', 10]]);
//                ->where([['prescriptionstatus', '!=', 11]])
//                ->where([['prescriptionstatus', '!=', 12]])
//                ->where([['prescriptionstatus', '!=', 14]])
//                ->where([['prescriptionstatus', '!=', 15]])
//                ->where([['prescriptionstatus', '!=', 50]])
//                ->where([['prescriptionstatus', '!=', 51]])
//                ->where([['prescriptionstatus', '!=', 52]])
//                ->where([['prescriptionstatus', '!=', 54]])
//                ->where([['prescriptionstatus', '!=', 55]]);
        });


        //filter by data
        if(array_key_exists('filterby',$params)){

            $query->where(function ($q) use ($params) {
                $q->where('prescriptionid','like','%'.$params['filterby'].'%')
                    ->orWhere('patientid','like','%'.$params['filterby'].'%')
                    ->orWhere('patient','like','%'.$params['filterby'].'%')
                    ->orWhere('dateraised','like','%'.$params['filterby'].'%')
                    ->orwhereHas('patient', function ($q) use ($params) {
                        $q->where('NHSNumber','like', '%'.$params['filterby'].'%');
                    });
            });

        }



        if ($paginate === 0) {
            return $query
                ->orderBy(array_key_exists('sortby',$params) ? $params['sortby']: 'dateraised', array_key_exists('type',$params) ? $params['type'] : 'desc')
                ->get();
        } else {
            return $query
                ->orderBy(array_key_exists('sortby',$params) ? $params['sortby']: 'dateraised', array_key_exists('type',$params) ? $params['type'] : 'desc')
                ->paginate($paginate);
        }

    }






}