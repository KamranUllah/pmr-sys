<?php
namespace App\Modules\RepeatPrescription\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\RepeatPrescription\Entities\PrescriptionNotes;

class PrescriptionNotesRepository extends BaseRepository
{

    protected $model = PrescriptionNotes::class;
    protected $relationships = [];

    public function getByPrescriptionId($prescriptionId)
    {
        return $this->model
            ->with($this->relationships)
            ->where([['PrescriptionId', '=', $prescriptionId]])
            ->orderBy('CreatedAt', 'desc')
            ->get();
    }

}