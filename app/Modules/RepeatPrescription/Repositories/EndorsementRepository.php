<?php namespace App\Modules\RepeatPrescription\Repositories;

use App\Modules\Core\Repositories\BaseRepository;
use App\Modules\RepeatPrescription\Entities\PrescriptionEndorsements;
use App\Modules\RepeatPrescription\Entities\Endorsement;

class EndorsementRepository extends BaseRepository
{

    protected $model = PrescriptionEndorsements::class;
    protected $relationships = ['endorsementstatus'];

    public function getOtherEndorsementsOfPrescription($endorsementId)
    {
        $endorsement = $this->getById($endorsementId);
        $prescriptionId = $endorsement->prescription;

        return $this->model
            ->with($this->relationships)
            ->where([['prescription', '=', $prescriptionId],['endorsementId', '!=', $endorsementId]])
            ->get();
    }

    public function getbyPrescriptionId($prescriptionId)
    {
        return $this->model
            ->with($this->relationships)
            ->where([['prescription', '=', $prescriptionId]])
            ->get();
    }

    public static function getStatusString($statusId)
    {
        $value = '';
        switch($statusId)
        {
            case PrescriptionEndorsements::DR_SIGNEDOFF: //1
                $value = 'Raised - Not Signed';
                break;
            case PrescriptionEndorsements::ESCALATED: // 13
                $value = 'Escalated';
                break;
            case PrescriptionEndorsements::DR_REJECTED: // 56
                $value = 'Dr Rejected';
                break;
            case PrescriptionEndorsements::DR_SIGNEDOFF: //2
                $value = 'Dr Signed Off';
                break;
            case PrescriptionEndorsements::SENT: // 8
                $value = 'Sent';
                break;
            default:
                $value = '';
                break;
        }
        return $value;
    }
}