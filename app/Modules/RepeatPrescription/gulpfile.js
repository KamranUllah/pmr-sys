const elixir = require('laravel-elixir');
// const argv = require('yargs').argv;

require('laravel-elixir-vue-2');

/*if (typeof moduleName === 'undefined') {
    console.log('Plunder more riffiwobbles!');
    return false;
}*/

elixir(function (mix) {

    mix.styles(
        './app/Modules/RepeatPrescription/Assets/css/rp.css'
        , 'public/modules/repeatprescription/css/rp.css');

    mix.webpack(
        './app/Modules/RepeatPrescription/Assets/js/repeat-prescription-vue.js'
        , 'public/modules/repeatprescription/js/repeat-prescription-vue.js');


    mix.scripts(
        './app/Modules/RepeatPrescription/Assets/js/adminmedicine.js'
        , 'public/modules/repeatprescription/js/adminmedicine.js');

    mix.scripts(
        './app/Modules/RepeatPrescription/Assets/js/adminprescriptions.js'
        , 'public/modules/repeatprescription/js/adminprescriptions.js');

    mix.scripts(
        './app/Modules/RepeatPrescription/Assets/js/repeatprescriptions.js'
        , 'public/modules/repeatprescription/js/repeatprescriptions.js');

});
