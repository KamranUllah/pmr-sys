<?php

Route::group(['middleware' => 'web', 'prefix' => 'repeatprescription', 'namespace' => 'App\\Modules\RepeatPrescription\Http\Controllers'], function()
{
    Route::group(['prefix' => 'admin'], function () {

        Route::get('/', [
            'as'   => 'admin.repeatprescription.index',
            'uses' => 'Admin\RepeatPrescriptionController@index',
        ]);


        Route::get(
            '/get-prescriptions',
            'Admin\RepeatPrescriptionController@getRepeatPrescriptions'
        )->name('admin.repeat.prescriptions.get');

        Route::get(
            '/count-prescriptions',
            'Admin\RepeatPrescriptionController@countRepeatPrescriptions'
        )->name('admin.repeat.prescriptions.count');



        Route::group(['prefix' => 'repeatprescription'], function () {

            Route::get(
                'page/{page}',
                'Admin\RepeatPrescriptionController@showPage'
            )->name('admin.repeatprescription.show.page');

            Route::get(
                'data-import',
                'Admin\RepeatPrescriptionController@showImport'
            )->name('admin.repeatprescription.import');

            Route::post('/get-prescription-items', [
                'as'   => 'admin.repeatprescription.items.fetch',
                'uses' => 'Admin\RepeatPrescriptionController@getItems',
            ]);

            Route::post('/get-prescription-notes', [
                'as'   => 'admin.repeatprescription.notes.fetch',
                'uses' => 'Admin\PrescriptionNotesController@getNotes',
            ]);

            Route::post('/create-prescription-notes', [
                'as'   => 'admin.repeatprescription.notes.create',
                'uses' => 'Admin\PrescriptionNotesController@store',
            ]);

            Route::post('/update-prescription-notes', [
                'as'   => 'admin.repeatprescription.notes.update',
                'uses' => 'Admin\PrescriptionNotesController@update',
            ]);

            Route::post('/delete-prescription-notes', [
                'as'   => 'admin.repeatprescription.notes.destroy',
                'uses' => 'Admin\PrescriptionNotesController@destroy',
            ]);

            Route::post('/get-prescription-user', [
                'as'   => 'admin.repeatprescription.userbyid',
                'uses' => 'Admin\PrescriptionNotesController@userbyId',
            ]);


            Route::post('/get-import-data', [
                'as'   => 'admin.repeatprescription.import.data',
                'uses' => 'Admin\RepeatPrescriptionController@matchImportData',
            ]);

            Route::post('/post-import-data', [
                'as'   => 'admin.repeatprescription.import.update',
                'uses' => 'Admin\PrescriptionController@updateImportData',
            ]);


            Route::post('/get-psl-import-data', [
                'as'   => 'admin.repeatprescription.import.psl.data',
                'uses' => 'Admin\RepeatPrescriptionController@matchPSLImportData',
            ]);

            Route::post('/post-psl-import-data', [
                'as'   => 'admin.repeatprescription.import.psl.update',
                'uses' => 'Admin\PrescriptionController@updatePSLImportData',
            ]);

            Route::any('raisedNotSigned', [
                'as'   => 'admin.repeatprescription.raisedNotSigned',
                'uses' => 'Admin\RepeatPrescriptionController@raisedNotSigned',
            ]);

            Route::any('escalated', [
                'as'   => 'admin.repeatprescription.escalated',
                'uses' => 'Admin\RepeatPrescriptionController@escalated',
            ]);

            Route::any('rejected', [
                'as'   => 'admin.repeatprescription.rejected',
                'uses' => 'Admin\RepeatPrescriptionController@rejected',
            ]);

            Route::any('signedOff', [
                'as'   => 'admin.repeatprescription.signedOff',
                'uses' => 'Admin\RepeatPrescriptionController@signedOff',
            ]);

            Route::any('orderSent', [
                'as'   => 'admin.repeatprescription.orderSent',
                'uses' => 'Admin\RepeatPrescriptionController@orderSent',
            ]);            
        });
        Route::group(['prefix' => 'repeat'], function () {

            Route::get('{id}', [
                'as'   => 'admin.prescriptions.repeatshow',
                'uses' => 'Admin\PrescriptionController@show',
            ]);

            Route::get('/view/summary/{id}', [
                'as'   => 'admin.prescriptions.repeatsummary',
                'uses' => 'Admin\PrescriptionController@summary',
            ]);

            Route::post('{id}/update-status/markAsNotSignedOff', [
                'as'   => 'repeat.status.markAsNotSignedOff',
                'uses' => 'Admin\PrescriptionController@markAsNotSignedOff',
            ]);

            Route::post('{id}/update-status/markAsEscalated', [
                'as'   => 'repeat.status.markAsEscalated',
                'uses' => 'Admin\PrescriptionController@markAsEscalated',
            ]);

            Route::post('{id}/update-status/markAsRejected', [
                'as'   => 'repeat.status.markAsRejected',
                'uses' => 'Admin\PrescriptionController@markAsRejected',
            ]);
            
            Route::post('{id}/update-status/markAsSignedOff', [
                'as'   => 'repeat.status.markAsSignedOff',
                'uses' => 'Admin\PrescriptionController@markAsSignedOff',
            ]);
            
            Route::post('{id}/update-status/markAsSent', [
                'as'   => 'repeat.status.markAsSent',
                'uses' => 'Admin\PrescriptionController@markAsSent',
            ]);

            Route::post('{id}/update-status/markAsDelivered', [
                'as'   => 'repeat.status.markAsDelivered',
                'uses' => 'Admin\PrescriptionController@markAsDelivered',
            ]);

            Route::post('{id}/update-status/markAsCancelled', [
                'as'   => 'repeat.status.markAsCancelled',
                'uses' => 'Admin\PrescriptionController@markAsCancelled',
            ]);

            Route::post('update/trackingref/{prescriptionId}', [
                'as'   => 'admin.repeatprescription.update.trackingref',
                'uses' => 'Admin\PrescriptionController@updatePrescriptionTracking',
            ]);

            Route::any('{id}/update-status/resend-fax', [
                'as'   => 'repeat.status.resendFax',
                'uses' => 'Admin\PrescriptionController@resendFax',
            ]);

            Route::any('{id}/generate-prescription-pdf', [
                'as'   => 'repeat.pdf.generate',
                'uses' => 'Admin\PrescriptionController@generatePrescriptionPDF',
            ]);

            Route::any('{id}/download-prescription-pdf', [
                'as'   => 'repeat.pdf.download',
                'uses' => 'Admin\PrescriptionController@downloadPrescriptionPDF',
            ]);

            Route::any('{id}/view-prescription-pdf', [
                'as'   => 'repeat.pdf.view',
                'uses' => 'Admin\PrescriptionController@viewPrescriptionPDF',
            ]);

            /**
             * ENDORSEMENTS
             */

            Route::get('{endorsementId}/{statusId}/endorsement-status/isLast', [
                'as'   => 'repeat.endorsements.status.islastendorsement',
                'uses' => 'Admin\EndorsementController@isLastEndorsementToThisStatus',
            ]);

            Route::post('{id}/update-endorsement-status/markAsNotSignedOff', [
                'as'   => 'repeat.endorsements.status.markAsNotSignedOff',
                'uses' => 'Admin\EndorsementController@markAsNotSignedOff',
            ]);

            Route::post('{id}/update-endorsement-status/markAsRejected', [
                'as'   => 'repeat.endorsements.status.markAsRejected',
                'uses' => 'Admin\EndorsementController@markAsRejected',
            ]);

            Route::post('{id}/update-endorsement-status/markAsSent', [
                'as'   => 'repeat.endorsements.status.markAsSent',
                'uses' => 'Admin\EndorsementController@markAsSent',
            ]);

            Route::post('{id}/update-endorsement-status/markAsSignedOff', [
                'as'   => 'repeat.endorsements.status.markAsSignedOff',
                'uses' => 'Admin\EndorsementController@markAsSignedOff',
            ]);

        });    
    });
});
