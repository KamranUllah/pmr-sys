<?php

namespace App\Modules\RepeatPrescription\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetRepeatPrescription extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'FromDate' => 'date_format:d/m/Y',
            'ToDate' => 'date_format:d/m/Y|after:FromDate',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
