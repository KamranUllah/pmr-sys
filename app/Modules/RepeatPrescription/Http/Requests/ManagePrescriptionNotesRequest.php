<?php

namespace App\Modules\RepeatPrescription\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManagePrescriptionNotesRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'PrescriptionNoteId' => 'required',
            'PrescriptionId' => 'required',
            'note' => 'required|max:1000',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
