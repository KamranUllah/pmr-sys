<?php namespace App\Modules\RepeatPrescription\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\RepeatPrescription\Http\Requests\MarkScriptAsSent;
use App\Modules\Core\Http\Requests\FaxRequest;
use App\Modules\RepeatPrescription\Entities\Prescription;
use App\Modules\RepeatPrescription\Events\PrescriptionStatusChanged;
use App\Modules\RepeatPrescription\Repositories\PrescriptionRepository;

use App\Modules\RepeatPrescription\Entities\Endorsement;
use App\Modules\RepeatPrescription\Events\EndorsementStatusChanged;
use App\Modules\RepeatPrescription\Repositories\EndorsementRepository;

use App\Modules\Patient\Entities\MainBasePatient;
use App\Services\Helpers\TelecommsCloud\Phapic\PdoStorageInterface;
use App\Services\Helpers\TelecommsCloud\Phapic\Phapic;
use Elibyy\TCPDF\Facades\TCPDF as PDF;
use File;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\DB;
use Event;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Sentinel;


class EndorsementController extends BackEndController
{

    /**
     * @var EndorsementRepository
     */
    private $endorsement;

    /**
     * EndorsementController constructor.
     * @param EndorsementRepository $endorsement
     * @param MainBasePatient $mainBasePatient
     */
    public function __construct(
        EndorsementRepository $endorsement,
        MainBasePatient $mainBasePatient
    ) {
        parent::__construct();
        $this->endorsement = $endorsement;
        $this->mainBasePatient = $mainBasePatient;

        $this->theme->asset()
            ->container('footer-custom')
            ->add('admin-medicine-scripts', \Bust::url('modules/repeatprescription/js/adminmedicine.js'));
        $this->theme->asset()
            ->container('footer-custom')
            ->add('admin-rp-scripts', \Bust::url('modules/repeatprescription/js/adminprescriptions.js'));
        $this->theme->asset()
            ->container('footer-custom')
            ->add('rp-scripts', \Bust::url('modules/repeatprescription/js/repeatprescription.js'));
    }


    /**
     * This function is check the given endorsement's prescription has other endorsements, if so check all are going to be same new status
     * If so, returns TRUE
     * @param $endorsementId
     * @param $statusString
     * @return bool
     */
    public function isLastEndorsementToThisStatus($endorsementId, $statusString)
    {
        /*
        const RAISED_DR_NOT_SIGNED = 1;
        const ESCALATED = 13;
        const DR_REJECTED = 56;
        const DR_SIGNEDOFF = 2;
        const SENT = 8;
        */
        $statusId = null;
        switch($statusString){
            case 'endorsement-mark-as-sent':
                $statusId = Endorsement::SENT;
                break;
            case 'endorsement-mark-as-rejected':
                $statusId = Endorsement::DR_REJECTED;
                break;
            case 'endorsement-mark-as-signedoff':
                $statusId = Endorsement::DR_SIGNEDOFF;
                break;
        }
        $otherEndorsements = $this->endorsement->getOtherEndorsementsOfPrescription($endorsementId);

        if($otherEndorsements === null || $otherEndorsements->count() === 0){
            return true;
        } else {
            $otherEndorsements = collect($otherEndorsements)->reject(function ($endor) use ($statusId) {
                return $endor->Status !== $statusId;
            });
            if($otherEndorsements === null || $otherEndorsements->count() === 0){
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsRejected($id)
    {
        $endorsement = $this->endorsement->getById($id);
        //dd($endorsement);
        $previousStatus = $endorsement->status;
        $endorsement->status = Endorsement::DR_REJECTED;
        //$prescription->completed_at = Carbon::now();
        //$prescription->save();
        //$j = $endorsement->getJobType();

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PUT_PrescriptionEndorsementStatus @par_endorsement_id=?,@par_status=?',
            array($id, $endorsement->status));
        //->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
        //array($j, $id, $previousStatus, $endorsement->status, "updated by " . Sentinel::getUser()->username, 1));

        Event::fire(new EndorsementStatusChanged($endorsement));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }


    /**
     * Mark endorsement as Sent
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsNotSignedOff($id)
    {
        $endorsement = $this->endorsement->getById($id);
        $endorsement->status = Endorsement::RAISED_DR_NOT_SIGNED;

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PUT_PrescriptionEndorsementStatus @par_endorsement_id=?,@par_status=?',
                array($id, $endorsement->status));

        Event::fire(new EndorsementStatusChanged($endorsement));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }

    /**
     * Mark endorsement as Sent
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsSent($id)
    {
        $endorsement = $this->endorsement->getById($id);
        $endorsement->status = Endorsement::SENT;

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PUT_PrescriptionEndorsementStatus @par_endorsement_id=?,@par_status=?',
                array($id, $endorsement->status));

        Event::fire(new EndorsementStatusChanged($endorsement));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }

    /**
     * Mark endorsement as Signed-off
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsSignedOff($id)
    {
        $endorsement = $this->endorsement->getById($id);
        $endorsement->status = Endorsement::DR_SIGNEDOFF;

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PUT_PrescriptionEndorsementStatus @par_endorsement_id=?,@par_status=?',
                array($id, $endorsement->status));

        Event::fire(new EndorsementStatusChanged($endorsement));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }

}
