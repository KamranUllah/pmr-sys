<?php

namespace App\Modules\RepeatPrescription\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\RepeatPrescription\Entities\PrescriptionNotes;
use App\Modules\RepeatPrescription\Repositories\PrescriptionNotesRepository;
use App\Modules\RepeatPrescription\Http\Requests\GetPrescriptionNotesRequest;
use App\Modules\RepeatPrescription\Http\Requests\DeletePrescriptionNotesRequest;
use App\Modules\RepeatPrescription\Http\Requests\ManagePrescriptionNotesRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Datatables;
use Sentinel;

class PrescriptionNotesController extends BackEndController
{
    /**
     * @var PrescriptionNotes
     */
    private $PrescriptionNotes;

    /**
     * PrescriptionNotesController constructor.
     * @param PrescriptionNotes $PrescriptionNotes
     */
    public function __construct(
        PrescriptionNotes $PrescriptionNotes)
    {
        parent::__construct();
        $this->PrescriptionNotes = $PrescriptionNotes;
    }


    public function getNotes(GetPrescriptionNotesRequest $request)
    {
        $result = false;

        $PrescriptionId = $request->get('PrescriptionId');

        try{
            $result = $this->PrescriptionNotes->where('PrescriptionId',$PrescriptionId)->get();


        }catch (\Exception $exception) {

            $message = 'There was a problem attempting to fetch a Prescription notes for Prescription #' . clean($PrescriptionId).'Exception: '.$exception ;

            \Bugsnag::notifyError(
                'Unable to fetch note',
                ''.$message,
                null,
                'error'
            );

            return response()->json($message, 500);

        }

        return response()->json($result);
    }


    /**
     * Store a newly created resource in storage.
     * @param ManagePrescriptionNotesRequest|Request $request
     * @return Response
     */
    public function store(ManagePrescriptionNotesRequest $request)
    {

        $result = false;

        try {
            $result = $this->PrescriptionNotes->create(
                [
                    'PrescriptionId'          => clean($request->get('PrescriptionId')),
                    'UserId'        => LoggedInUser()->getUserId(),
                    'SourceSystem'  => 'PEARL',
                    'Note'        => clean($request->get('note')),
                    'CreatedAt'          => Carbon::now(),
                ]
            );

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has created a note for Prescription Id: ' . $request->get('PrescriptionId'));

        } catch (\Exception $exception) {

            $message = 'There was a problem attempting to create a Prescription note for Prescription #' . clean($request->get('PrescriptionId')).'Exception: '.$exception;

            \Bugsnag::notifyError(
                'Unable to create note',
                ''.$message,
                null,
                'error'
            );

            return response()->json($message, 500);

        }


        return response()->json($result);

    }

    /**
     * Update the specified resource in storage.
     * @param ManagePrescriptionNotesRequest|Request $request
     * @param $PrescriptionNoteId
     * @return Response
     */
    public function update(ManagePrescriptionNotesRequest $request)
    {

        $result = false;

        $PrescriptionNoteId = $request->get('PrescriptionNoteId');
        try {
            $this->PrescriptionNotes->find($PrescriptionNoteId)->update(
                [
                    'PrescriptionNoteId'   => $PrescriptionNoteId,
                    'PrescriptionId'          => $request->get('PrescriptionId'),
                    'UserId'        => LoggedInUser()->getUserId(),
                    'SourceSystem'  => 'PEARL',
                    'Note'        => clean($request->get('note')),
                    'UpdatedAt'          => Carbon::now(),
                ]
            );

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has updated Note #'.$PrescriptionNoteId.' for Prescription Id: ' . $request->get('PrescriptionId'));

        } catch (\Exception $exception) {

            $message =  'There was a problem attempting to update Prescription note'.$PrescriptionNoteId.'data for Prescription #' . clean($request->get('PrescriptionId').'Exception:'.$exception);
            \Bugsnag::notifyError(
                'Unable to update Prescription note',
                ''.$message,
                 null,
                'error'
            );

            return response()->json($message, 500);

        }

        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @return Response
     */
    public function destroy(DeletePrescriptionNotesRequest $request)
    {
        $result = false;
        $PrescriptionNoteId = $request->get('PrescriptionNoteId');

        try {
            $result = $this->PrescriptionNotes->find($PrescriptionNoteId)->delete();

            auditLog('User ID: ' . LoggedInUser()->getUserId() . ' has deleted Prescription note id: ' . $PrescriptionNoteId);

        } catch(\Exception $exception) {

            $message =  'There was a problem attempting to delete Prescription note'.$PrescriptionNoteId.'data for Prescription #' . clean($request->get('PrescriptionId').'Exception:'.$exception);

            \Bugsnag::notifyError(
                'Unable to delete Prescription note',
                ''.$message,
                null,
                'error'
            );

            return response()->json($message, 500);


        }

        return response()->json($result);
    }



    /**
     * Remove the specified resource from storage.
     * @param $PrescriptionNoteId
     * @return Response
     */
    public function userbyId(Request $request)
    {

        return Sentinel::findById($request->get('userId'))->email;

    }

}
