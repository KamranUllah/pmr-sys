<?php namespace App\Modules\RepeatPrescription\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\RepeatPrescription\Entities\Prescription;
use App\Modules\RepeatPrescription\Entities\FaxRecords;
use App\Modules\RepeatPrescription\Http\Requests\GetRepeatPrescription;
use App\Modules\RepeatPrescription\Http\Requests\RepeatPrescriptionSearchRequest;
use App\Modules\RepeatPrescription\Http\Requests\GetPrescriptionItemsRequest;
use App\Modules\RepeatPrescription\Repositories\PrescriptionRepository;
use App\Modules\RepeatPrescription\Repositories\EndorsementRepository;
use App\Modules\RepeatPrescription\Http\Requests\RepeatPrescriptionImportData;
use App\Modules\RepeatPrescription\Http\Controllers\Admin\PrescriptionController;
use DB;
use Faker\Factory;
use Faker\Generator;
use File;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class RepeatPrescriptionController extends BackEndController
{

    /**
     * @var prescriptions
     */
    private $prescriptions;


    /**
     * @var prescriptioncontroller
     */
    private $prescriptioncontroller;

    /**
     * @var endorsements
     */
    private $endorsements;

    public function __construct(
        PrescriptionController $prescriptioncontroller,
        PrescriptionRepository $prescriptions,
        EndorsementRepository $endorsements)
    {
        parent::__construct();

        $this->theme->asset()
            ->container('footer-custom')
            ->add('admin-medicine-scripts', \Bust::url('modules/repeatprescription/js/adminmedicine.js'));
        $this->theme->asset()
            ->container('footer-custom')
            ->add('admin-rp-scripts', \Bust::url('modules/repeatprescription/js/adminprescriptions.js'));
        $this->theme->asset()
            ->container('footer-custom')
            ->add('rp-scripts', \Bust::url('modules/repeatprescription/js/repeatprescription.js'));
//        $this->theme->asset()
//            ->container('footer-custom')
//            ->add('rp-scripts', \Bust::url('modules/repeatprescription/js/repeat-prescription-vue.js'));

        $this->prescriptioncontroller = $prescriptioncontroller;
        $this->prescriptions = $prescriptions;
        $this->endorsements = $endorsements;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->theme->breadcrumb()->add('Repeat Prescriptions');
        $this->theme->prependTitle('Repeat Prescriptions');
        $this->theme->setSectionTitle('Repeat Prescriptions');
        $this->theme->asset()
        ->container('footer-custom')
        ->add('rp-scripts', \Bust::url('modules/repeatprescription/js/repeat-prescription-vue.js'));
        return $this->theme->of('repeatprescription::admin.repeat_vue')->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prescription = $this->prescriptions->getById($id);
        $this->theme->prependTitle(ucwords($prescription->getStatus()));
        //return $this->theme->of('repeatprescription::admin.show', compact('prescription'))->render();
    }

    /**
     * Display the specified resource.
     *
     */
    public function showPage($page)
    {
        return redirect('/repeatprescription/admin#/manage/'.$page);
    }


    /**
     * Display the specified resource.
     *
     */
    public function showImport()
    {
        return redirect('/repeatprescription/admin#/import');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @return mixed
     */
    public function raisedNotSigned()
    {
        $nhs_number = '';
        if (request()->has('nhs_number') && trim(request()->get('nhs_number')) !== '') {
            $nhs_number = request()->get('nhs_number');
            $prescriptions = $this->prescriptions->getByNHSNumber(Prescription::RAISED_DR_NOT_SIGNED, $nhs_number, 50);
        } else {
            $prescriptions = $this->prescriptions->getByStatus(Prescription::RAISED_DR_NOT_SIGNED, 50);
        }
        $status = 'Not Signed';
        $this->theme->prependTitle("Prescriptions {$status}");
        return $this->theme->of('repeatprescription::admin.repeatlist', compact('prescriptions', 'status', 'nhs_number'))->render();
    }

    /**
     * @return mixed
     */
    public function escalated()
    {
        $nhs_number = '';
        if (request()->has('nhs_number') && trim(request()->get('nhs_number')) !== '') {
            $nhs_number = request()->get('nhs_number');
            $prescriptions = $this->prescriptions->getByNHSNumber(Prescription::ESCALATED, $nhs_number, 50);
        } else {
            $prescriptions = $this->prescriptions->getByStatus(Prescription::ESCALATED, 50);
        }
        $status = 'Escalated';
        $this->theme->prependTitle("Prescriptions {$status}");
        return $this->theme->of('repeatprescription::admin.repeatlist', compact('prescriptions', 'status', 'nhs_number'))->render();
    }

    /**
     * @return mixed
     */
    public function rejected()
    {
        $nhs_number = '';
        if (request()->has('nhs_number') && trim(request()->get('nhs_number')) !== '') {
            $nhs_number = request()->get('nhs_number');
            $prescriptions = $this->prescriptions->getByNHSNumber(Prescription::DR_REJECTED, $nhs_number, 50);
        } else {
            $prescriptions = $this->prescriptions->getByStatus(Prescription::DR_REJECTED, 50);
        }
        $status = 'Dr Rejected';
        $this->theme->prependTitle("Prescriptions {$status}");
        return $this->theme->of('repeatprescription::admin.repeatlist', compact('prescriptions', 'status', 'nhs_number'))->render();
    }

    /**
     * @return mixed
     */
    public function signedOff()
    {
        $nhs_number = '';
        if (request()->has('nhs_number') && trim(request()->get('nhs_number')) !== '') {
            $nhs_number = request()->get('nhs_number');
            $prescriptions = $this->prescriptions->getByNHSNumber(Prescription::DR_SIGNEDOFF, $nhs_number, 50);
        } else {
            $prescriptions = $this->prescriptions->getByStatus(Prescription::DR_SIGNEDOFF, 50);
        }
        $status = 'Dr Signed Off';
        $this->theme->prependTitle("Prescriptions {$status}");
        return $this->theme->of('repeatprescription::admin.repeatlist', compact('prescriptions', 'status', 'nhs_number'))->render();
    }

    /**
     * @return mixed
     */
    public function orderSent()
    {
        $nhs_number = '';
        if (request()->has('nhs_number') && trim(request()->get('nhs_number')) !== '') {
            $nhs_number = request()->get('nhs_number');
            $prescriptions = $this->prescriptions->getByNHSNumber(Prescription::SENT, $nhs_number, 50);
        } else {
            $prescriptions = $this->prescriptions->getByStatus(Prescription::SENT, 50);
        }
        $status = 'Prescription Sent';
        $this->theme->prependTitle("Prescriptions {$status}");
        return $this->theme->of('repeatprescription::admin.repeatlist', compact('prescriptions', 'status', 'nhs_number'))->render();
    }


    /**
     * @param RepeatPrescriptionImportData $importData
     * @return \Illuminate\Http\JsonResponse
     */
    public function matchImportData(RepeatPrescriptionImportData $request)
    {
        $importData = $request->importData;
        $repeatPrescriptions = [];

        foreach ($importData as $data) {
            $result = $this->prescriptions->getByImportPatientData($data);

            if($result){
                $repeatData = [];

                foreach ($result as $prescriptionIds){
                    $prescriptions = [
                        'prescriptionId' => $prescriptionIds,
                        'trackingNumber' => $data['tracking_number']
                    ];
                    array_push($repeatData, $prescriptions);
                }

                $newData = [
                    'csvData' => $data,
                    'repeatPrescription' => $repeatData
                ];

                array_push($repeatPrescriptions, $newData);
            }
        }

        return response()->json($repeatPrescriptions);
    }



    /**
     * @param RepeatPrescriptionImportData $importData
     * @return \Illuminate\Http\JsonResponse
     */
    public function matchPSLImportData(RepeatPrescriptionImportData $request)
    {
        $importData = $request->importData;
        $repeatPrescriptions = [];

        $acceptableDescription = 'Total';
        $PSLImportData = array_filter($importData, function ($item) use ($acceptableDescription) {
            if (strtolower($item['item_description']) === strtolower($acceptableDescription)) {
                return true;
            }
            return false;
        });

        //Check Endorsement count on Prescription
        $this->prescriptions->updateEndorsementCount();

        foreach ($PSLImportData as $data) {

            $fetchDetails = explode("- ", $data['patient_name']);
            $patientFullName = str_replace(',', '' , $fetchDetails[0]);
            $patientAddressDetails = explode(",", $fetchDetails[1]);
            $pslData = [
                'patient_name' => trim($patientFullName),
                'address_line' => array_key_exists(0, $patientAddressDetails) ? trim($patientAddressDetails[0]) : '',
                'postcode' =>  array_key_exists(1, $patientAddressDetails) ? trim($patientAddressDetails[1]) : '',
                'endorsements' => $data['item_total']
            ];
            $result = $this->prescriptions->getByPSLImportPatientData($pslData);

            if($result){
                $repeatData = [];

                foreach ($result as $prescriptionIds){
                    $prescriptions = [
                        'prescriptionId' => $prescriptionIds
                    ];
                    array_push($repeatData, $prescriptions);
                }

                $newData = [
                    'csvData' => $pslData,
                    'repeatPrescription' => $repeatData
                ];

                array_push($repeatPrescriptions, $newData);
            }
        }

        return response()->json($repeatPrescriptions);
    }


    public function countRepeatPrescriptions(){

        $result = false;
        try {

            //call the service to get the repeat prescriptions
            $prescriptions = $this->prescriptions->countRepeatPrescriptions();

            $result = [
                'total' => $prescriptions->count(),
                'raisedNotSigned' =>  $prescriptions->where('prescriptionstatus', '=', Prescription::RAISED_DR_NOT_SIGNED)->count(),
                'escalated' => $prescriptions->where('prescriptionstatus', '=', Prescription::ESCALATED)->count(),
                'drRejected' => $prescriptions->where('prescriptionstatus', '=', Prescription::DR_REJECTED)->count(),
                'drSignedOff' => $prescriptions->where('prescriptionstatus', '=', Prescription::DR_SIGNEDOFF)->count(),
                'sent' => $prescriptions->where('prescriptionstatus', '=', Prescription::SENT)->count(),
            ];

        } catch (Exception $ex) {
            auditLog('Attempt to fetch repeat prescriptions counter failed with Exception:'.$ex);
            \Bugsnag::notifyException($ex);

//            return response()->json('Attempt to fetch repeat prescriptions counter failed with Exception:'.$ex,500);
        }




        return response()->json($result);
    }


    public function getRepeatPrescriptions(GetRepeatPrescription $request) {

        $params = [];
        $searchFilters = $request->has('filter') ? $request->get('filter') : null;

        $searchSortable = $request->has('sort') ? $request->get('sort') : null;

        $statusFilters = $request->has('status') ? strtoupper($request->get('status')) : null;

        if($searchFilters !== null){
            $params['filterby'] = $searchFilters;
        }

        if($searchSortable !== null){
            $data = explode('|',$searchSortable);
            $params['sortby'] = $data[0];
            $params['type'] = $data[1];
        }

        if($statusFilters !== null){
            $params['status'] = $statusFilters;
        }

        if(array_key_exists('status',$params) && ( $params['status'] === "RAISEDNOTSIGNED" || $params['status'] === "ESCALATED")){


            //get pending fax status from telecoms cloud api
            $this->getAllFaxStatus();

            //update prescription fax counter
            $this->prescriptions->updateSentFaxCount($params);

            //Check Endorsement count on Prescription
            $this->prescriptions->updateEndorsementCount();
        }

        $prescriptions = $this->prescriptions->getRepeat(50,$params);

        return response()->json($prescriptions);
    }



    public function getItems(GetPrescriptionItemsRequest $request)
    {
        $result = false;

        $PrescriptionId = $request->get('PrescriptionId');

        try{
//            $result = $this->PrescriptionNotes->where('PrescriptionId',$PrescriptionId)->get();
//
            $result = $this->endorsements->getbyPrescriptionId($PrescriptionId);


        }catch (\Exception $exception) {

            $message = 'There was a problem attempting to fetch Items for Prescription #' . clean($PrescriptionId).'Exception: '.$exception ;

            \Bugsnag::notifyError(
                'Unable to items note',
                ''.$message,
                null,
                'error'
            );

            return response()->json($message, 500);

        }

        return response()->json($result);
    }


    public function getAllFaxStatus(){

        //get fax records
        $pendingFaxes =  FaxRecords::where('faxid',"!=","Error")
                    ->where(function ($q) {
                        $q->where([['status', '=', FaxRecords::PENDING]])
                            ->orWhere([['status', '=', FaxRecords::UNKNOWN]]);
                    })->get()->each(function($fax){
                        $checkFax = $this->prescriptioncontroller->getSentFaxStatus($fax->FaxId);
                        FaxRecords::where('faxid',"=", $fax->FaxId)->update(['status' =>$checkFax->status_code,'timesent' =>$checkFax->submitted_date]);

            });

    }

}
