<?php namespace App\Modules\RepeatPrescription\Http\Controllers\Admin;

use App\Modules\Core\Http\Controllers\BackEndController;
use App\Modules\Core\Http\Requests\FaxRequest;
use App\Modules\RepeatPrescription\Entities\Prescription;
use App\Modules\RepeatPrescription\Entities\PrescriptionEndorsements;
use App\Modules\RepeatPrescription\Entities\FaxRecords;
use App\Modules\RepeatPrescription\Events\PrescriptionStatusChanged;
use App\Modules\RepeatPrescription\Http\Requests\MarkScriptAsSent;
use App\Modules\RepeatPrescription\Http\Requests\RepeatPrescriptionUpdateData;
use App\Modules\RepeatPrescription\Repositories\PrescriptionRepository;
use App\Modules\Patient\Entities\MainBasePatient;
use App\Services\Helpers\TelecommsCloud\Phapic\PdoStorageInterface;
use App\Services\Helpers\TelecommsCloud\Phapic\Phapic;
use Elibyy\TCPDF\Facades\TCPDF as PDF;
use File;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\DB;
use Event;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Sentinel;
use Illuminate\Http\Request;



class PrescriptionController extends BackEndController
{

    /**
     * @var PrescriptionRepository
     */
    private $prescriptions;

    /**
     * PrescriptionController constructor.
     *
     * @param PrescriptionRepository $prescriptions
     * @param MainBasePatient $mainBasePatient
     */
    public function __construct(
        PrescriptionRepository $prescriptions,
        MainBasePatient $mainBasePatient
    ) {
        parent::__construct();
        $this->prescriptions = $prescriptions;
        $this->mainBasePatient = $mainBasePatient;

        $this->theme->asset()
            ->container('footer-custom')
            ->add('admin-medicine-scripts', \Bust::url('modules/repeatprescription/js/adminmedicine.js'));
        $this->theme->asset()
            ->container('footer-custom')
            ->add('admin-rp-scripts', \Bust::url('modules/repeatprescription/js/adminprescriptions.js'));
        $this->theme->asset()
            ->container('footer-custom')
            ->add('rp-scripts', \Bust::url('modules/repeatprescription/js/repeatprescription.js'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prescription = $this->prescriptions->getById($id);

        $patientExemptions = DB::connection('sqlsrv_mainbase')
            ->table('PatientExemptions')
            ->select([
                'PatientExemptions.PatientExemptionId',
                'PatientExemptions.IsExempt',
                'PatientExemptions.ExemptionReasonId',
                'PatientExemptions.CertificateNumber',
                'ExemptionReasons.Reason',
                'ExemptionReasons.Description',
            ])
            ->leftJoin('ExemptionReasons',
                'ExemptionReasons.ExemptionReasonId',
                '=',
                'PatientExemptions.ExemptionReasonId')
            ->leftJoin('Patients', 'Patients.PatientId', '=', 'PatientExemptions.PatientId')
            ->where('PatientExemptions.PatientId', '=', $prescription->Patient->PatientId)
            ->orderBy('CreatedDate', 'desc')
            ->first();

        $this->theme->prependTitle(ucwords($prescription->getStatus()));
        return $this->theme->of('repeatprescription::admin.repeatshow', compact('prescription', 'patientExemptions'))
            ->render();
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function summary($id)
    {
        $prescription = $this->prescriptions->getById($id);

        $patientExemptions = DB::connection('sqlsrv_mainbase')
            ->table('PatientExemptions')
            ->select([
                'PatientExemptions.PatientExemptionId',
                'PatientExemptions.IsExempt',
                'PatientExemptions.ExemptionReasonId',
                'PatientExemptions.CertificateNumber',
                'ExemptionReasons.Reason',
                'ExemptionReasons.Description',
            ])
            ->leftJoin('ExemptionReasons',
                'ExemptionReasons.ExemptionReasonId',
                '=',
                'PatientExemptions.ExemptionReasonId')
            ->leftJoin('Patients', 'Patients.PatientId', '=', 'PatientExemptions.PatientId')
            ->where('PatientExemptions.PatientId', '=', $prescription->Patient->PatientId)
            ->orderBy('CreatedDate', 'desc')
            ->first();

        return view('repeatprescription::admin.repeatmodal', compact('prescription', 'patientExemptions'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsNotSignedOff($id)
    {
        $prescription = $this->prescriptions->getById($id);
        $previousStatus = $prescription->prescriptionstatus;
        $prescription->prescriptionstatus = Prescription::RAISED_DR_NOT_SIGNED;
        //$prescription->completed_at = Carbon::now();
        //$prescription->save();
        $j = $prescription->getJobType();

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
                array($j, $id, $previousStatus, $prescription->prescriptionstatus, "updated by #" . LoggedInUser()->getUserId() . " - ".LoggedInUser()->getUserLogin() , 1));

        Event::fire(new PrescriptionStatusChanged($prescription));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsEscalated($id)
    {
        $prescription = $this->prescriptions->getById($id);
        $previousStatus = $prescription->prescriptionstatus;
        $prescription->prescriptionstatus = Prescription::ESCALATED;
        //$prescription->completed_at = Carbon::now();
        //$prescription->save();

        // Update the Endorsement status
        PrescriptionEndorsements::where('prescription', '=', $id)->update(['Status' => PrescriptionEndorsements::ESCALATED]);

        $j = $prescription->getJobType();

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
                array($j, $id, $previousStatus, $prescription->prescriptionstatus, "updated by #" . LoggedInUser()->getUserId() . " - ".LoggedInUser()->getUserLogin() , 1));

        Event::fire(new PrescriptionStatusChanged($prescription));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsRejected($id)
    {
        $prescription = $this->prescriptions->getById($id);
        $previousStatus = $prescription->prescriptionstatus;
        $prescription->prescriptionstatus = Prescription::DR_REJECTED;
        //$prescription->completed_at = Carbon::now();
        //$prescription->save();

        // Update the Endorsement status
        PrescriptionEndorsements::where('prescription', '=', $id)->update(['Status' => PrescriptionEndorsements::DR_REJECTED]);

        $j = $prescription->getJobType();

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
                array($j, $id, $previousStatus, $prescription->prescriptionstatus, "updated by #" . LoggedInUser()->getUserId() . " - ".LoggedInUser()->getUserLogin() , 1));

        Event::fire(new PrescriptionStatusChanged($prescription));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsSignedOff($id)
    {
        $prescription = $this->prescriptions->getById($id);

        $prescriptionPatientId = $prescription->patientid;
        $patientsData = $this->mainBasePatient
            ->where('PatientId', '=', $prescription->patientid)
            ->get();
        $previousStatus = $prescription->prescriptionstatus;
        $prescription->prescriptionstatus = Prescription::DR_SIGNEDOFF;
        //$prescription->completed_at = Carbon::now();
        //$prescription->save();

        // Update the Endorsement status
        PrescriptionEndorsements::where('prescription', '=', $id)->update(['Status' => PrescriptionEndorsements::DR_SIGNEDOFF]);

        $j = $prescription->getJobType();

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
                array($j, $id, $previousStatus, $prescription->prescriptionstatus, "updated by #" . LoggedInUser()->getUserId() . " - ".LoggedInUser()->getUserLogin() , 1));

        Event::fire(new PrescriptionStatusChanged($prescription));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }

    /**
     * @param MarkScriptAsSent $request
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsSent(MarkScriptAsSent $request, $id)
    {
        $prescription = $this->prescriptions->getById($id);
        $prescriptionPatientId = $prescription->patientid;
        $patientsData = $this->mainBasePatient
            ->where('PatientId', '=', $prescription->patientid)
            ->get();
        $previousStatus = $prescription->prescriptionstatus;
        $prescription->prescriptionstatus = Prescription::SENT;
        $prescription->trackingref = $request->get('trackingref');
        $prescription->datepharmacydespatched = Carbon::now();
        $prescription->save();

        // Update the Endorsement status
        PrescriptionEndorsements::where('prescription', '=', $id)->update(['Status' => PrescriptionEndorsements::SENT]);

        $j = $prescription->getJobType();

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
                array($j, $id, $previousStatus, $prescription->prescriptionstatus, "updated by #" . LoggedInUser()->getUserId() . " - ".LoggedInUser()->getUserLogin() , 1));

        //add one credit if status changed to Mark as sent and if patient is Now Patient]
        //Now Patient company ccg_id = 22
        if ($result) {

            $AddCredit = DB::connection('sqlsrv_mainbase')
                ->statement('exec sp_PUT_AddPMRCredits "' . $patientsData[0]->PatientEmail . '","' . $prescriptionPatientId . '",1');

            if ($AddCredit) {
                flash()->success('Patient #' . $prescriptionPatientId . ' has been updated and credit has been added');
            }

        }

        Event::fire(new PrescriptionStatusChanged($prescription));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);

    }



    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsDelivered($id)
    {
        $prescription = $this->prescriptions->getById($id);

        $prescriptionPatientId = $prescription->patientid;
        $patientsData = $this->mainBasePatient
            ->where('PatientId', '=', $prescription->patientid)
            ->get();
        $previousStatus = $prescription->prescriptionstatus;
        $prescription->prescriptionstatus = Prescription::DELIVERED;
        //$prescription->completed_at = Carbon::now();
        //$prescription->save();

        // Update the Endorsement status
        PrescriptionEndorsements::where('prescription', '=', $id)->update(['Status' => PrescriptionEndorsements::DELIVERED]);

        $j = $prescription->getJobType();

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
                array($j, $id, $previousStatus, $prescription->prescriptionstatus, "updated by #" . LoggedInUser()->getUserId() . " - ".LoggedInUser()->getUserLogin() , 1));

        Event::fire(new PrescriptionStatusChanged($prescription));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsCancelled($id)
    {
        $prescription = $this->prescriptions->getById($id);

        $prescriptionPatientId = $prescription->patientid;
        $patientsData = $this->mainBasePatient
            ->where('PatientId', '=', $prescription->patientid)
            ->get();
        $previousStatus = $prescription->prescriptionstatus;
        $prescription->prescriptionstatus = Prescription::CANCELLED_CLINICAL;
        //$prescription->completed_at = Carbon::now();
        //$prescription->save();

        // Update the Endorsement status
        PrescriptionEndorsements::where('prescription', '=', $id)->update(['Status' => PrescriptionEndorsements::CANCELLED_CLINICAL]);

        $j = $prescription->getJobType();

        $result = DB::connection('sqlsrv_mainbase')
            ->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
                array($j, $id, $previousStatus, $prescription->prescriptionstatus, "updated by #" . LoggedInUser()->getUserId() . " - ".LoggedInUser()->getUserLogin() , 1));

        Event::fire(new PrescriptionStatusChanged($prescription));

        if (request()->ajax() || request()->wantsJson()) {
            return response()->json($id, 200);
        }

        return redirect()->route('admin.prescriptions.repeatshow', $id);
    }


    /**
 * @param RepeatPrescriptionUpdateData $updateDate
 * @return \Illuminate\Http\JsonResponse
 */
    public function updateImportData(RepeatPrescriptionUpdateData $request)
    {
        $updateData = $request->updateData;
        $queryData = [];

        foreach ($updateData as $data) {
            $prescriptionId = $data['prescriptionId'];
            $prescriptionTracking =  $data['trackingNumber'];

            $updatePrescription = false;
//            $updateEndorsement = false;
            $AddCredit = false;

            //update prescription data
            $prescription = $this->prescriptions->getById($prescriptionId);
            $prescriptionPatientId = $prescription->patientid;
            $patientsData = $this->mainBasePatient
                ->where('PatientId', '=', $prescription->patientid)
                ->get();
            $previousStatus = $prescription->prescriptionstatus;
            $previousStatusText = $prescription->getStatusString();
            $prescription->prescriptionstatus = Prescription::SENT;
            $prescription->trackingref = $prescriptionTracking;
            $prescription->datepharmacydespatched = Carbon::now();
            $updatePrescription = $prescription->save();

            // Update the Endorsement status
            $updateEndorsement = PrescriptionEndorsements::where('prescription', '=', $prescriptionId)->update(['Status' => PrescriptionEndorsements::SENT]);


            $j = $prescription->getJobType();

            $result = DB::connection('sqlsrv_mainbase')
                ->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
                    array($j, $prescriptionId, $previousStatus, $prescription->prescriptionstatus, "updated by #" . LoggedInUser()->getUserId() . " - ".LoggedInUser()->getUserLogin() , 1));

            //add one credit if status changed to Mark as sent and if patient is Now Patient]
            //Now Patient company ccg_id = 22
            if ($updatePrescription) {
                $AddCredit = DB::connection('sqlsrv_mainbase')
                    ->statement('exec sp_PUT_AddPMRCredits "' . $patientsData[0]->PatientEmail . '","' . $prescriptionPatientId . '",1');
                Event::fire(new PrescriptionStatusChanged($prescription));
            }

            $queryResult = [
                'PrescriptionId' => $prescriptionId,
                'NewStatus'=> $prescription->getStatusString(),
                'OldStatus' => $previousStatusText,
                'TrackingNumber' =>$prescriptionTracking,
                'PrescriptionUpdate' => $updatePrescription ? true : false,
                'EndorsementUpdate' => $updateEndorsement ? true : false,
                'CreditUpdate' => $AddCredit ? true : false,
            ];

            array_push($queryData, $queryResult);
        }

        return response()->json($queryData);
    }



    /**
     * @param RepeatPrescriptionUpdateData $updateDate
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePSLImportData(RepeatPrescriptionUpdateData $request)
    {
        $updateData = $request->updateData;
        $queryData = [];

        foreach ($updateData as $data) {
            $prescriptionId = $data['prescriptionId'];

            $updatePrescription = false;


            //update prescription data
            $prescription = $this->prescriptions->getById($prescriptionId);
            $prescriptionPatientId = $prescription->patientid;
            $patientsData = $this->mainBasePatient
                ->where('PatientId', '=', $prescription->patientid)
                ->get();
            $previousStatus = $prescription->prescriptionstatus;
            $previousStatusText = $prescription->getStatusString();
            $prescription->prescriptionstatus = Prescription::DR_SIGNEDOFF;
            $prescription->datepharmacydespatched = Carbon::now();
            $updatePrescription = $prescription->save();

            // Update the Endorsement status
            $updateEndorsement = PrescriptionEndorsements::where('prescription', '=', $prescriptionId)->update(['Status' => PrescriptionEndorsements::SENT]);


            $j = $prescription->getJobType();

            $result = DB::connection('sqlsrv_mainbase')
                ->statement('exec sp_PrescriptionJobLogInsert @JobTypeId=?,@PrescriptionId=?,@OldPrescriptionStatusId=?,@NewPrescriptionStatusId=?,@LogEntry=?,@Succeeded=?',
                    array($j, $prescriptionId, $previousStatus, $prescription->prescriptionstatus, "updated by #" . LoggedInUser()->getUserId() . " - ".LoggedInUser()->getUserLogin() , 1));


            $queryResult = [
                'PrescriptionId' => $prescriptionId,
                'NewStatus'=> $prescription->getStatusString(),
                'OldStatus' => $previousStatusText,
                'PrescriptionUpdate' => $updatePrescription ? true : false,
                'EndorsementUpdate' => $updateEndorsement ? true : false,
            ];

            array_push($queryData, $queryResult);
        }

        return response()->json($queryData);
    }

    public function generatePrescriptionPDF($id, $mode = 'I')
    {
        //ini_set('memory_limit', '100MB');


        $prescription = $this->prescriptions->getById($id);

        $prescriptionPatientId = $prescription->patientid;
        $patientsData = $this->mainBasePatient
            ->where('PatientId', '=', $prescription->patientid)
            ->get();


        /*
                try {
                    $html = \View::make('repeatprescription::pdf_view.prescription-template',
                        compact('prescription', 'patientsData'))->render();
                } catch (\Throwable $e) {
                }*/

        $filename = 'rp-' . $id . '-' . $prescriptionPatientId . '_' . str_slug($prescription->Patient->PatientForename) . '-' . str_slug($prescription->Patient->PatientSurname);
        $htmlPath = "pdfs/{$filename}.html";
        $pdfPath = "pdfs/{$filename}.pdf";

        $htmlContent = \View::make('repeatprescription::pdf_view.prescription-template',
            compact('prescription', 'patientsData'))->render();

        File::put(storage_path($htmlPath), $htmlContent);

        try {
            $tcPDF = new Mpdf([
                'margin_left'   => 20,
                'margin_right'  => 15,
                'margin_top'    => 20,
                'margin_bottom' => 25,
                'margin_header' => 10,
                'margin_footer' => 10
            ]);
            $tcPDF->SetDisplayMode('fullpage');
        } catch (MpdfException $e) {
        }
        $tcPDF->SetTitle($filename . '.pdf');
        //$tcPDF->AddPage();
        try {
            $tcPDF->WriteHTML(File::get(storage_path($htmlPath)));
        } catch (FileNotFoundException $e) {
        } catch (MpdfException $e) {
        }

        try {
            return $tcPDF->Output($filename . '.pdf', $mode);
        } catch (MpdfException $e) {
        }
    }

    public function downloadPrescriptionPDF($id)
    {
        $prescription = $this->prescriptions->getById($id);

        $prescriptionPatientId = $prescription->patientid;
        $patientsData = $this->mainBasePatient
            ->where('PatientId', '=', $prescription->patientid)
            ->get();

        $filename = 'rp-' . $id . '-' . $prescriptionPatientId . '_' . str_slug($prescription->Patient->PatientForename) . '-' . str_slug($prescription->Patient->PatientSurname) . '.pdf';
        $pdfPath = "pdfs/{$filename}";
        if ($this->generatePrescriptionPDF($id, 'I')) {
            //$this->generatePrescriptionPDF($id);
            //return PDF::loadFile($pdfPath)->stream($filename);
        }

        //return PDF::loadFile($pdfPath)->stream($filename);
    }

    public function viewPrescriptionPDF($id)
    {

        $prescription = $this->prescriptions->getById($id);

        $prescriptionPatientId = $prescription->patientid;
        $patientsData = $this->mainBasePatient
            ->where('PatientId', '=', $prescription->patientid)
            ->get();
        $previousStatus = $prescription->prescriptionstatus;

    }

    private function getAuthToken()
    {
        if (request()->session()->has('tc')) {
            request()->session()->forget('tc');
        }

        $clientId = config('telecomms-cloud.client_id');
        $secret = config('telecomms-cloud.secret');
        $grantUrl = config('telecomms-cloud.auth_url');

        $client = new Client();

        $data = ['client_id' => $clientId, 'client_secret' => $secret];

        $options = [
            'json'    => $data,
            'headers' => ['Content-Type' => 'application/json'],
            'verify' => false
        ];

        $result = $client->post($grantUrl, $options);
        $tcData = json_decode($result->getBody());

        $faxTokenData = [
            'tc' => [
                'access_token' => $tcData->access_token,
                'expires_at'   => Carbon::now()->addSeconds($tcData->expires_in)
            ]
        ];

        request()->session()->put($faxTokenData);
        return $tcData->access_token;
    }

    /**
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \RuntimeException
     * @throws FileNotFoundException
     * @throws MpdfException
     */
    public function resendFax($id)
    {

        $grantUrl2 = 'https://api.telecomscloud.com/v1/fax/outbound';
        $result = false;

        // Generate PDF for repeat prescription
        $prescription = $this->prescriptions->getById($id);

        $prescriptionPatientId = $prescription->patientid;
        $patientsData = $this->mainBasePatient
            ->where('PatientId', '=', $prescription->patientid)
            ->first();

        if ((null === $patientsData->SurgeryFax) || ('' === $patientsData->SurgeryFax)) {
            return response()->json([
                'error' => 'Unable to send fax due to missing patient\'s GP Surgery Fax number'
            ]);
        }

        if (strlen($patientsData->SurgeryFax) < 8) {
            return response()->json([
                'error' => 'Unable to send fax due to a short patient\'s GP Surgery Fax number'
            ]);
        }

        //check fax credit
        $faxStatus = $this->getFaxStatus();

        if($faxStatus && $faxStatus->credit_balance < 1){
            return response()->json([
                'error' => 'Unable to send fax as there is no credit on the account'
            ]);
        }


        $surgeryFax  = app()->environment() === 'production' ? $patientsData->SurgeryFax : '01512102520';

        $client = new Client();

        if ($faxToken = $this->getAuthToken()) {

            $data = ['access_token' => $faxToken,
                     'to'           => $surgeryFax,
                     'from'         => '01616730001'
            ];

            $filename = 'rp-' . $id . '-' . $prescriptionPatientId . '_' . str_slug($prescription->Patient->PatientForename) . '-' . str_slug($prescription->Patient->PatientSurname);
            $htmlPath = "pdfs/{$filename}.html";
            $pdfPath = "pdfs/{$filename}.pdf";

            $htmlContent = \View::make('repeatprescription::pdf_view.prescription-template',
                compact('prescription', 'patientsData'))->render();

            File::put(storage_path($htmlPath), $htmlContent);

            $tcPDF = new Mpdf([
                'margin_left'   => 20,
                'margin_right'  => 15,
                'margin_top'    => 20,
                'margin_bottom' => 25,
                'margin_header' => 10,
                'margin_footer' => 10
            ]);
            $tcPDF->SetDisplayMode('fullpage');

            $tcPDF->SetTitle($filename . '.pdf');
            $tcPDF->WriteHTML(File::get(storage_path($htmlPath)));
            $tcPDF->Output(storage_path($pdfPath), 'F');
            // End of Fax Generation

            if (File::exists(storage_path($pdfPath))) {
                $options = [
                    'query' => $data,
                    'verify' => false,
                    'multipart' => [
                        [
                            'name'         => 'fax_document',
                            'content-type' => 'multipart/form-data',
                            'contents'     => fopen(storage_path($pdfPath), 'rb'),
                            'filename'     => 'impersonate.pdf',
                            'mime'         => 'application/pdf',
                        ]
                    ],
                ];



                try {
                    $request = $client->post($grantUrl2, $options);
                    $result = json_decode($request->getBody());
                    auditLog('Logged in User Id #' . LoggedInUser()->getUserId() . ' (' . LoggedInUser()->email . ') has sent a fax for Prescription Id#' . $id);
                } catch (\Exception $exception) {
                    \Bugsnag::notifyException($exception);
                    return response()->json($exception->getMessage(), $exception->getCode());
                }
            }


            //create new fax record value
            $faxRecord = new FaxRecords;
            $faxRecord->faxtype = 1;
            $faxRecord->referenceid = $id;
            //add success fax record
            if($result->id){
                $faxRecord->faxid = $result->id;
                $faxRecord->status = 0;
                $faxRecord->save();

            }else{
                $faxRecord->faxid = 'Error';
                $faxRecord->status = 0;
                $faxRecord->save();
            }

            //check fax status if successful
            if($result->id){
                $faxStatus = $this->getSentFaxStatus($result->id);
                FaxRecords::where('faxid',"=", $result->id)->update(['status' =>$faxStatus->status_code]);
            }

            return response()->json($result);

        }else{

            return response()->json([
                'error' => 'Unable to send fax due to fax provider API authentication issue'
            ]);
        }
    }

    public function getFaxStatus()
    {

        $client = new Client();
        $result = false;

        if ($faxToken = $this->getAuthToken()) {

            $accountInfo = 'https://api.telecomscloud.com/v1/account/info';

            $options = [
                'headers' => [
                    'Authorization' => 'Bearer '.$faxToken
                ],
                'verify' => false
            ];


            $request = $client->get($accountInfo,$options);
            $result = json_decode($request->getBody());
        }


        return $result;


    }


    public function getSentFaxStatus($faxId){
        $client = new Client();
        $result = false;

        if ($faxToken = $this->getAuthToken()) {

            $accountInfo = 'https://api.telecomscloud.com/v1/fax/outbound/'.$faxId.'/status';

            $options = [
                'headers' => [
                    'Authorization' => 'Bearer '.$faxToken
                ],
                'verify' => false
            ];


            $request = $client->get($accountInfo,$options);
            $result = json_decode($request->getBody());
        }


        return $result;

    }




    /**
     * Update Prescription Tracking Number
     * Request $request
     * @param  int $prescriptionId
     *
     * @return Response
     */
    public function updatePrescriptionTracking(Request $request, $prescriptionId)
    {

        if(!$request->get('trackingNumber')){
            return response()->json('Prescription tracking Number is not valid', 500);
        }


        // try to find patient
        try {
            $prescription = $this->prescriptions->getById($prescriptionId);
        } catch (\Exception $exception) {
            \Bugsnag::notifyError(
                'Unable to find prescription',
                'There was a problem attempting to find prescription # ' . $prescriptionId . ' :: ' . $exception->getMessage(),
                null,
                'error'
            );

            return response()->json('There was a problem finding the prescription', 500);

        }
        $prescription->trackingref = $request->get('trackingNumber');
        //try to update the patient data
        if ($prescription->save()) {
            auditLog('User #' . \Sentinel::getUser()
                    ->getUserId() . ' has updated tracking Number for Prescription #' . $prescription->prescriptionid);
            if ($request->ajax()) {
                return response()->json([
                    'prescriptionId' => $prescription->prescriptionid,
                    'message'   => 'Prescription tracking Number has been updated successfully',
                ]);
            }


        } else {
            \Bugsnag::notifyError(
                'Unable to update prescription',
                'There was a problem attempting to update  tracking Number for Prescription # ' . $prescriptionId . ' :: ',
                null,
                'error'
            );

            return response()->json('There was a problem updating  tracking Number for Prescription ', 500);
        }

    }

}
