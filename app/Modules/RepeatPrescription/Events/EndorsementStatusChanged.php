<?php namespace App\Modules\RepeatPrescription\Events;

use App\Modules\RepeatPrescription\Entities\PrescriptionEndorsements;
use Event;
use Illuminate\Queue\SerializesModels;

class EndorsementStatusChanged extends Event {

	use SerializesModels;
    /**
     * @var PrescriptionEndorsements
     */
    public $endorsement;

    /**
     * Create a new event instance.
     * @param Endorsement $endorsement
     */
	public function __construct(PrescriptionEndorsements $endorsement)
	{
		//
        $this->endorsement = $endorsement;
    }

}
