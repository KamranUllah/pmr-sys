<?php namespace App\Modules\RepeatPrescription\Events;

use App\Modules\RepeatPrescription\Entities\Prescription;
use Event;
use Illuminate\Queue\SerializesModels;

class PrescriptionStatusChanged extends Event {

	use SerializesModels;
    /**
     * @var Prescription
     */
    public $prescription;

    /**
     * Create a new event instance.
     * @param Prescription $prescription
     */
	public function __construct(Prescription $prescription)
	{
		//
        $this->prescription = $prescription;
    }

}
