<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 14/06/2017
 * Time: 16:01
 */

return [

    // User Management Permissions
    'Repeat prescription management' => [
        /**
         * Repeat Prescription
         */
        'repeatprescription.repeatprescription.repeatprescription.index' => [
            'name'        => '',
            'description' => 'Repeat Admin Index page',
        ],


        'repeatprescription.repeatprescription.repeatprescription.showPage'          => [
            'name'        => '',
            'description' => 'View Repeat Prescription Vue Pages',
        ],

        'repeatprescription.repeatprescription.repeatprescription.showImport'          => [
            'name'        => '',
            'description' => 'View Data Import Page',
        ],

        'repeatprescription.repeatprescription.repeatprescription.getRepeatPrescriptions' => [
            'name'        => '',
            'description' => 'Get all repeat prescriptions',
        ],

        'repeatprescription.repeatprescription.repeatprescription.countRepeatPrescriptions' => [
            'name'        => '',
            'description' => 'Count all repeat prescriptions',
        ],

        'repeatprescription.repeatprescription.repeatprescription.matchImportData' => [
            'name'        => '',
            'description' => 'Repeat Admin Match Royal Mail Import Data',
        ],


        'repeatprescription.repeatprescription.repeatprescription.matchPSLImportData' => [
            'name'        => '',
            'description' => 'Repeat Admin Match Positive Solution Import Data',
        ],



        //Notes Repeat Prescription Permissions
        'repeatprescription.repeatprescription.prescriptionnotes.getNotes'     => [
            'name'        => '',
            'description' => 'Show Repeat Prescription Notes Data',
        ],

        'repeatprescription.repeatprescription.prescriptionnotes.store'     => [
            'name'        => '',
            'description' => 'Create Repeat Prescription Notes Data',
        ],

        'repeatprescription.repeatprescription.prescriptionnotes.update'     => [
            'name'        => '',
            'description' => 'Update Repeat Prescription Notes Data',
        ],

        'repeatprescription.repeatprescription.prescriptionnotes.destroy'     => [
            'name'        => '',
            'description' => 'Delete Repeat Prescription Notes Data',
        ],

        'repeatprescription.repeatprescription.prescriptionnotes.userbyId'     => [
            'name'        => '',
            'description' => 'Get Repeat Prescription Notes User Data',
        ],




        'repeatprescription.repeatprescription.repeatprescription.show' => [
            'name'        => '',
            'description' => 'View repeat prescription details',
        ],

        'repeatprescription.repeatprescription.repeatprescription.getItems'     => [
            'name'        => '',
            'description' => 'Show Repeat Prescription Item Data',
        ],

        'repeatprescription.repeatprescription.repeatprescription.raisedNotSigned' => [
            'name'        => '',
            'description' => 'View raised but not signed off repeat prescriptions',
        ],

        'repeatprescription.repeatprescription.repeatprescription.escalated' => [
            'name'        => '',
            'description' => 'View escalated repeat prescriptions',
        ],

        'repeatprescription.repeatprescription.repeatprescription.rejected' => [
            'name'        => '',
            'description' => 'View rejected repeat prescriptions',
        ],

        'repeatprescription.repeatprescription.repeatprescription.signedOff' => [
            'name'        => '',
            'description' => 'View signed off repeat prescriptions',
        ],

        'repeatprescription.repeatprescription.repeatprescription.orderSent' => [
            'name'        => '',
            'description' => 'View sent repeat prescriptions',
        ],

        /*'repeatprescription.admin.repeatprescription.index' => [
            'name'        => '',
            'description' => 'View patients list',
        ],*/

        /*'repeatprescription.admin.repeatprescription.create' => [
            'name'        => '',
            'description' => 'Create a new patient',
        ],

        'repeatprescription.admin.repeatprescription.store' => [
            'name'        => '',
            'description' => 'Store a new patient\'s details',
        ],*/

        /*'repeatprescription.admin.repeatprescription.update' => [
            'name'        => '',
            'description' => 'Update a patient\'s details',
        ],

        'repeatprescription.admin.repeatprescription.getPatientsData' => [
            'name'        => '',
            'description' => 'Fetch patient data (required)',
        ],*/


        /**
         * Prescription
         */

        'repeatprescription.repeatprescription.prescription.show'          => [
            'name'        => '',
            'description' => 'View prescription details',
        ],

        'repeatprescription.repeatprescription.prescription.summary'          => [
            'name'        => '',
            'description' => 'View prescription summary details',
        ],

        'repeatprescription.repeatprescription.prescription.markAsNotSignedOff' => [
            'name'        => '',
            'description' => 'Mark prescription as doctor not signed off',
        ],

        'repeatprescription.repeatprescription.prescription.markAsEscalated' => [
            'name'        => '',
            'description' => 'Mark prescription as escalated',
        ],

        'repeatprescription.repeatprescription.prescription.markAsRejected' => [
            'name'        => '',
            'description' => 'Mark prescription as rejected',
        ],

        'repeatprescription.repeatprescription.prescription.markAsSignedOff' => [
            'name'        => '',
            'description' => 'Mark prescription as signed off',
        ],

        'repeatprescription.repeatprescription.prescription.markAsSent' => [
            'name'        => '',
            'description' => 'Mark prescription as sent',
        ],

        'repeatprescription.repeatprescription.prescription.markAsDelivered' => [
            'name'        => '',
            'description' => 'Mark prescription as delivered',
        ],

        'repeatprescription.repeatprescription.prescription.markAsCancelled' => [
            'name'        => '',
            'description' => 'Mark prescription as cancelled',
        ],

        'repeatprescription.repeatprescription.prescription.updatePrescriptionTracking' => [
            'name'        => '',
            'description' => 'Update prescription tracking Number',
        ],

        'repeatprescription.repeatprescription.prescription.updateImportData' => [
            'name'        => '',
            'description' => 'Update Repeat Prescription Royal Mail Import CSV Data',
        ],


        'repeatprescription.repeatprescription.prescription.updatePSLImportData' => [
            'name'        => '',
            'description' => 'Update Repeat Prescription Positive Solution Import CSV Data',
        ],

        'repeatprescription.repeatprescription.prescription.resendFax' => [
            'name'        => '',
            'description' => 'Re-send prescription Fax',
        ],

        'repeatprescription.repeatprescription.prescription.generatePrescriptionPDF' => [
            'name'        => '',
            'description' => 'Generate Repeat Prescription PDF',
        ],

        'repeatprescription.repeatprescription.prescription.downloadPrescriptionPDF' => [
            'name'        => '',
            'description' => 'Download Repeat Prescription PDF',
        ],

        'repeatprescription.repeatprescription.prescription.viewPrescriptionPDF' => [
            'name'        => '',
            'description' => 'View Repeat Prescription PDF',
        ],



        //Endorsements

        'repeatprescription.repeatprescription.endorsement.isLastEndorsementToThisStatus' => [
            'name'        => '',
            'description' => 'Check endorsement status',
        ],

        'repeatprescription.repeatprescription.endorsement.markAsNotSignedOff' => [
            'name'        => '',
            'description' => 'Mark endorsement as doctor not signed off',
        ],

        'repeatprescription.repeatprescription.endorsement.markAsRejected' => [
            'name'        => '',
            'description' => 'Mark endorsement as rejected',
        ],

        'repeatprescription.repeatprescription.endorsement.markAsSignedOff' => [
            'name'        => '',
            'description' => 'Mark endorsement as signed-off',
        ],

        'repeatprescription.repeatprescription.endorsement.markAsSent' => [
            'name'        => '',
            'description' => 'Mark endorsement as sent',
        ],

    ],

];
