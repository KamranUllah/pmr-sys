<?php
/**
 * Created by PhpStorm.
 * User: emman
 * Date: 25/10/2016
 * Time: 21:03
 */

namespace App\Modules\RepeatPrescription\Sidebar;

use App\Modules\User\Entities\Role;
use App\Modules\User\Entities\User;
use Maatwebsite\Sidebar\Badge;
use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\SidebarExtender;

class AdminSideBarExtender implements SidebarExtender
{

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group('Repeat Prescriptions', function (Group $group) {
            $group->hideHeading();
            $group->weight(100);
            $group->item('Repeat Prescriptions', function (Item $item) {
                $item->toggleIcon('fa fa-chevron-down');
                $item->icon('fa fa-medkit');
                $item->weight(14);


                $item->item('Upload Documents', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-file');
//                    $item->url('/repeatprescription/admin#/import');
                    $item->route('admin.repeatprescription.import');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                    });
                });


                $item->item('Waiting GP Sign Off', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-clock-o');
//                    $item->route('admin.repeatprescription.raisedNotSigned');
//                    $item->url('/repeatprescription/admin#/manage/raisednotsigned');
                    $item->route('admin.repeatprescription.show.page','raisednotsigned');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    //if (Sentinel::check()) {
                    //    $item->authorize(
                    //        Sentinel::getUser()->hasAccess('repeatprescription.admin.prescription.index')
                    //    );
                    //}
                });

                $item->item('Escalated', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-exclamation-triangle');
//                    $item->route('admin.repeatprescription.escalated');
//                    $item->url('/repeatprescription/admin#/manage/escalated');
                    $item->route('admin.repeatprescription.show.page','escalated');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    //if (Sentinel::check()) {
                    //    $item->authorize(
                    //        Sentinel::getUser()->hasAccess('repeatprescription.admin.prescription.index')
                    //    );
                    //}

                });

                $item->item('Dr Rejected', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-close');
//                    $item->route('admin.repeatprescription.rejected');
//                    $item->url('/repeatprescription/admin#/manage/rejected');
                    $item->route('admin.repeatprescription.show.page','rejected');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    //if (Sentinel::check()) {
                    //    $item->authorize(
                    //        Sentinel::getUser()->hasAccess('repeatprescription.admin.prescription.index')
                    //    );
                    //}

                });


                $item->item('Dr Signed Off', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-check');
//                    $item->route('admin.repeatprescription.signedOff');
//                    $item->url('/repeatprescription/admin#/manage/signedoff');
                    $item->route('admin.repeatprescription.show.page','signedoff');

                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                    //if (Sentinel::check()) {
                    //    $item->authorize(
                    //        Sentinel::getUser()->hasAccess('repeatprescription.admin.prescription.index')
                    //    );
                    //}

                });

                $item->item('Sent Out', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-truck');
//                    $item->route('admin.repeatprescription.orderSent');
//                    $item->url('/repeatprescription/admin#/manage/sent');
                    $item->route('admin.repeatprescription.show.page','sent');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                });


                $item->item('Delivered', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-thumbs-up');
//                    $item->route('admin.repeatprescription.orderSent');
//                    $item->url('/repeatprescription/admin#/manage/sent');
                    $item->route('admin.repeatprescription.show.page','delivered');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                });


                $item->item('Cancelled', function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-trash');
//                    $item->route('admin.repeatprescription.orderSent');
//                    $item->url('/repeatprescription/admin#/manage/sent');
                    $item->route('admin.repeatprescription.show.page','cancelled');
                    $item->badge(function (Badge $badge, User $user) {
                        $badge->setClass('bg-green');
                        //$badge->setValue($user->count());
                    });

                });

            });


            //if (null !== request()->user()) {
                //$group->authorize(
                    //request()->user()->hasAccess('user.admin.*')
                //);
            //}
        });

        return $menu;
    }
}
