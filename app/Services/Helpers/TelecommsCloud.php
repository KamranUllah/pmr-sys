<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 20/12/2017
 * Time: 16:50
 */

namespace App\Services\Helpers;

use GuzzleHttp\Client;

class TelecommsCloud
{

    protected $clientId;

    protected $secret;

    protected $url;


    public function __construct($proxy = false) {
        $this->clientId = config('telecomms-cloud.client_id');
        $this->secret = config('telecomms-cloud.secret');
        $this->url = config('telecomms-cloud.auth_url');

        $config = [];
        if ($proxy) {
            $config['default']['proxy'] = $proxy;
            $config['default']['debug'] = true;
        }
        $serviceDescription = json_decode(file_get_contents(__DIR__ . '/tc-service.json'), true);
        $description = new Description($serviceDescription);
        $client = new GuzClient(['base_url' => $baseUri]);
        $client->setDefaultOption('allow_redirects', false);
        if ($proxy) {
            $client->setDefaultOption('proxy', $proxy);
        }
        $this->guzzleClient = $client;
        parent::__construct($client, $description, $config);
    }


    public function GetAuthToken() {
        $client = new Client();

        $res = $client->request('POST', $this->url);

        return $res->getBody();
    }


    public function sendFax() {

    }
}