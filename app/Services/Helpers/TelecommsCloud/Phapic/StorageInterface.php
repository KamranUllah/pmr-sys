<?php
namespace App\Services\Helpers\TelecommsCloud\Phapic;

interface StorageInterface
{
    public function setToken($accessToken, $expiresDate);
    public function getToken();
}
