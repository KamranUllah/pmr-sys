<?php
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 05/09/2016
 * Time: 18:18
 */

namespace App\Services;

use Facuz\Theme\Asset;

class CmsAsset extends Asset
{
    /**
     * Get an asset container instance.
     *
     * <code>
     *		// Get the default asset container
     *		$container = Asset::container();
     *
     *		// Get a named asset container
     *		$container = Asset::container('footer');
     * </code>
     *
     * @param  string            $container
     * @return CmsAssetContainer
     */
    public static function container($container = 'default')
    {
        if (! isset(static::$containers[$container])) {
            static::$containers[$container] = new CmsAssetContainer($container);
        }

        return static::$containers[$container];
    }
}
