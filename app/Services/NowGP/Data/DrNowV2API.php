<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 01/07/2016
 * Time: 14:19
 */

namespace App\Services\NowGP\Data;


use App\Modules\Doctor\Entities\Appointment;
use Cache;
use Carbon\Carbon;
use GuzzleHttp\Client;

/**
 * Class DrNowV2API
 * @package App\Services
 */
class DrNowV2API
{
    /**
     * @var mixed
     */
    protected $authBaseUrl;

    /**
     * @var mixed
     */
    protected $adminBaseUrl;

    /**
     * @var mixed
     */
    protected $baseUrl;

    /**
     * @var
     */
    protected $guzzleClient;

    /**
     * @var mixed
     */
    protected $APIUsername;

    /**
     * @var mixed
     */
    protected $APIPassword;

    /**
     * @var mixed
     */
    protected $conversationApiUrl;

    /**
     * @var mixed
     */
    protected $coreApiUrl;

    /**
     * @var mixed
     */
    public $newCoreUrlPhase2;

    const PatientDocTypeReferral = 1;
    const PatientDocTypeFitForWork = 2;

    /**
     * DrNowV2API constructor.
     */
    public function __construct()
    {
        $this->baseUrl = config('pmr.apiv2_base_url');
        $this->authBaseUrl = config('pmr.apiv2_auth_base_url');
        $this->adminBaseUrl = config('pmr.apiv2_admin_base_url');
        $this->APIUsername = config('pmr.apiv2_Username');
        $this->APIPassword = config('pmr.apiv2_Password');
        //$this->conversationApiUrl = config('chat.conversation_api_url');
        $this->coreApiUrl = config('pmr.core_api_url');
        $this->newCoreUrlPhase2 = config('pmr.phase2_new_core_url');
    }

    /**
     * A simple wrapper around guzzle to make handling the calls easier.
     * It is prepared to handle calls that require us to be logged in. These
     * calls need the session to contain a the PatientId and the Generated call
     * By default these data must be present when we log in. On potential source
     * of proplems could be that the key timed out, leaving us with an invalid
     * value in the session, please keep this in mind.<br>
     * The function will not handles guzzles exceptions,<br>
     * The function will throw ErrorException if the session is not present and
     * the $loginRequired is set to true
     * @param string $action POST, GET, PUT.. etc.
     * @param string $url The url we send the request to, the whole url is necessary
     * @param boolean $loginRequired If the call requires us to be logged in or not. By default: false
     * @param array $json The data we are sending. It must be an array. By default: null
     * @param array $headers The headers that we want to send (aside from the login headers). By default: null
     * @param null $queryParams
     * @return mixed The value returned by guzzle
     */
    protected function sendGuzzleRequest(
        $action,
        $url,
        $loginRequired = false,
        $json = null,
        $headers = null,
        $queryParams = null
    ) {
        $client = new Client();

        $options = [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'auth'          => [$this->APIUsername, $this->APIPassword],
            'headers'       => [
                'X-Pagination-Page'  => isset($queryParams['page']) ? $queryParams['page'] : 1,
                'X-Pagination-Limit' => isset($queryParams['limit']) ? $queryParams['limit'] : 10,
            ],
        ];

        if (null !== $json) {
            $options['json'] = $json;
        }

        if (null !== $headers) {
            $options['headers'] = array_merge($options['headers'], $headers);
        }

        if (null !== $queryParams) {
            $options['query'] = $queryParams;
        }

        if ($loginRequired) {
            /*$user = Request::session()->get('user');
            $accessKeyHeaders = [
                    'X_DRNOW_SIGNATURE_1' => $user['GeneratedKey'],
                    'X_DRNOW_PATIENT_ID_1' => getPrimaryAccountId(),
            ];
            if (array_key_exists('headers', $options)) {
                array_merge($options['headers'], $accessKeyHeaders);
            } else {
                $options['headers'] = $accessKeyHeaders;
            }*/
        }

        dd("hiu");


        // SSL Validation
        $options['verify'] = storage_path('cacert/cacert.pem');

        return $client->request($action, $url, $options);
    }

    /**
     * Get a list of all testkits available
     *
     * @return mixed
     */
    public function getAllTestKits()
    {

        $client = new Client();

        $response = $client->request('GET', $this->baseUrl . 'testkits', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * Get testkit details by Category Id and Testkit Id
     *
     * @param $categoryId
     * @param $testkitId
     * @return mixed
     */
    public function getTestKitById($categoryId, $testkitId)
    {
        $client = new Client();

        $response = $client->request('GET',
            $this->baseUrl . 'testkits/category/' . $categoryId . '/product/' . $testkitId, [
                'content-type'  => 'application/json',
                'cache-control' => 'no-cache',
                'verify'        => false,
                'auth'          => [$this->APIUsername, $this->APIPassword],
            ]);

        return json_decode($response->getBody());
    }

    /**
     * Get testkits by category id
     *
     * @param $categoryId
     * @return mixed
     */
    public function getTestKitsByCategoryId($categoryId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->baseUrl . 'testkits/category/' . $categoryId, [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * Get all testkit categories
     *
     * @return mixed
     */
    public function getTestKitCategories()
    {
        $client = new Client();

        $response = $client->request('GET', $this->baseUrl . 'testkits', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * Issue testkits to patient
     *
     * @param array $testKit
     * @return mixed
     */
    public function issueTestkits(array $testKit)
    {
        $client = new Client();

        //dd($testKit);

        $response = $client->request('POST', $this->adminBaseUrl . 'testkits/referral', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
            'json'          => $testKit,
        ]);

        return json_decode($response->getBody());
    }

    /**
     * Get history of testkits issued to patient
     *
     * @param $patientId
     * @return mixed
     */
    public function getPatientTestkitHistory($patientId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . 'testkits/referral/' . $patientId, [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * Get history of referrals issued to patient
     *
     * @param $patientId
     * @return mixed
     */
    public function getPatientReferralHistory($patientId)
    {
        $client = new Client();
        // the url of the call GET /api/patientDocument/{patientId}/{docType}/patient
        // the type must be 1, as 1 is the Referral type patient document
        $response = $client->request('GET', $this->adminBaseUrl . 'patientDocument/' . $patientId . '/1/patient', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * Get history of fit for work notes issued to patient
     *
     * @param $patientId
     * @return mixed
     */
    public function getPatientFitForWorkHistory($patientId)
    {
        $client = new Client();
        // the url of the call GET /api/patientDocument/{patientId}/{docType}/patient
        // the type must be 2, as 2 is the Referral type patient document
        $response = $client->request('GET',
            $this->adminBaseUrl . 'patientDocument/' . $patientId . '/2/patient?showExpired=true&showPurchased=true',
            [
                'content-type'  => 'application/json',
                'cache-control' => 'no-cache',
                'verify'        => false,
                'auth'          => [$this->APIUsername, $this->APIPassword],
            ]);

        return json_decode($response->getBody());
    }

    /**
     * Get history of quesitons previously asked by a patient
     *
     * @param $patientId
     * @return mixed
     */
    public function getPatientAAQHistory($patientId)
    {
        $client = new Client();
        // the url of the call GET /api/askaquestion/patient/{patientId}/
        $response = $client->request('GET', $this->adminBaseUrl . 'askaquestion/patient/' . $patientId, [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * Get testkit by appointment Id
     *
     * @param Appointment $appointmentId
     * @param Patient $patientId
     * @return mixed
     */
    public function getTestkitsByPatientIdAndAppointmentId($patientId, $appointmentId)
    {
        $client = new Client();

        $response = $client->request('GET',
            $this->adminBaseUrl . 'testkits/referral/' . $patientId . '/' . $appointmentId, [
                'content-type'  => 'application/json',
                'cache-control' => 'no-cache',
                'verify'        => false,
                'auth'          => [$this->APIUsername, $this->APIPassword],
            ]);

        return json_decode($response->getBody());
    }

    /**
     * @param $appointmentId
     * @return mixed
     */
    public function getRiskratingForAppointment($appointmentId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . 'riskrating/' . $appointmentId . '/appointment', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * @param array $riskrating
     * @return mixed
     */
    public function saveRiskrating(array $riskrating)
    {
        $client = new Client();

        //dd($testKit);

        $response = $client->request('POST', $this->adminBaseUrl . 'riskrating', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
            'json'          => $riskrating,
        ]);

        return json_decode($response->getBody());
    }

    //Requests sends to MAIN api server not the -admin one
    public function getPatientDocumentByAppointmentId($appointmentId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . 'PatientDocument/' . $appointmentId . '/appointments',
            [
                'content-type'  => 'application/json',
                'cache-control' => 'no-cache',
                'verify'        => false,
                'auth'          => [$this->APIUsername, $this->APIPassword],
            ]);

        return json_decode($response->getBody());
    }

    //Requests sends to MAIN api server not the -admin one
    public function storePatientDocument(array $patientdocument)
    {

        $client = new Client();

        $response = $client->request('POST', $this->adminBaseUrl . 'PatientDocument', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
            'json'          => $patientdocument,
        ]);

        return json_decode($response->getBody());
    }

    //Requests sends to MAIN api server not the -admin one
    public function updatePatientDocument($documentId, array $patientdocument)
    {

        $client = new Client();

        $response = $client->request('PUT', $this->adminBaseUrl . 'patientDocument/' . $documentId, [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
            'json'          => $patientdocument,
        ]);

//        dd($response);
        return json_decode($response->getBody());
    }

    /**
     * Get patient's risk rating history
     *
     * @param $patientId
     * @return mixed
     */
    public function getRiskratingHistoryForPatient($patientId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . 'riskrating/' . $patientId . '/patient', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }


    /**
     * @param array $params
     * @return mixed
     */
    public function getQuestions(array $params)
    {
        $client = new Client();

        $startDate = isset($params['startDate']) ? $params['startDate'] : Carbon::yesterday()->subWeek()->format('Y-m-d');
        $endDate = isset($params['endDate']) ? $params['endDate'] : Carbon::today()->format('Y-m-d');

        $response = $client->request('GET',
            $this->adminBaseUrl . "askaquestion/doctor?startDate={$startDate}&endDate={$endDate}", [
                'content-type'  => 'application/json',
                'cache-control' => 'no-cache',
                'verify'        => false,
                'auth'          => [$this->APIUsername, $this->APIPassword],
            ]);

        return json_decode($response->getBody());
    }

    /*
     * Returns an object with Count property that contains the number of unanswered questions for the doctor requested
     */
    /**
     * @param $doctorId
     * @return mixed
     */
    public function getNumberOfUnansweredQuestions($doctorId)
    {
        $client = new Client();

        //dd($this->baseUrl . "askaquestion/doctor/{$doctorId}/unanswered/count");
        $response = $client->request('GET', $this->adminBaseUrl . "askaquestion/doctor/{$doctorId}/unanswered/count", [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * @param $doctorId
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    public function getListOfQuestionsForPeriod($doctorId, $startDate, $endDate)
    {
        $client = new Client();

        $response = $client->request('GET',
            $this->adminBaseUrl . "askaquestion/doctor/{$doctorId}?startDate={$startDate}&endDate={$endDate}", [
                'content-type'  => 'application/json',
                'cache-control' => 'no-cache',
                'verify'        => false,
                'auth'          => [$this->APIUsername, $this->APIPassword],
            ]);

        return json_decode($response->getBody());
    }

    /**
     * @param $patientId
     * @param $questionId
     * @return mixed
     */
    public function getQuestionInformation($patientId, $questionId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->baseUrl . "askaquestion/{$patientId}/{$questionId}", [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * @param $doctorId
     * @return mixed
     */
    public function getListOfUnansweredQuestions($doctorId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . "askaquestion/doctor/{$doctorId}/unanswered", [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /*
     * The answer needs to contain the following keys: QuestionId, Answer, ICDCode, DoctorId, PatientId
     */
    /**
     * @param array $answer
     * @return mixed
     */
    public function postAnswer(array $answer)
    {
        $client = new Client();

        $response = $client->request('POST', $this->adminBaseUrl . "askaquestion/doctor/answer", [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
            'json'          => $answer,
        ]);

        return json_decode($response->getBody());
    }

    /**
     * Get Patient Info by Id
     *
     * This will return a json response the typical patient info
     *
     * @param int $patientId this is the patients unique ID
     * @return json An object with the patients basic information.
     */
    public function getPatientInfoById($patientId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . 'patients/' . $patientId, [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * Get Bundled Patient Info by Id
     *
     * This will return bundled json response of much of the patient info often used in separate API calls.
     *
     * @param int $patientId this is the patients unique ID
     * @return json An object with the patients basic information, address, contact details
     */
    public function getBundledPatientInfoById($patientId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . 'patients/' . $patientId . '/bundled', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * @param $patientId
     * @return mixed
     */
    public function getPatientPreExisting($patientId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . 'patients/' . $patientId . '/mHistory', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * @param $patientId
     * @return mixed
     */
    public function getPatientAllergies($patientId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . 'patients/' . $patientId . '/allergies', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * get Patient GP Info By Patient Id
     *
     * This will return a patients GP related information
     *
     * @param int $patientId this is the patients unique ID
     * @return json An object with the patients gp information, address, contact details, name
     */
    public function getPatientGPInfoByPatientId($patientId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->adminBaseUrl . 'patients/' . $patientId . '/gp', [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * @param $params
     * @return mixed
     * @internal param $patientName
     */
    public function getPatientByName($params)
    {
        $response = $this->sendGuzzleRequest('GET', $this->adminBaseUrl . "patients/search/{$params['patientName']}",
            false, null, null, ['limit' => 1000]);

        return json_decode($response->getBody());
    }

    /**
     * @param array $params
     * @return mixed
     * @internal param $patientId
     */
    public function getPatientAppointmentsByPatientId(array $params)
    {
        $response = $this->sendGuzzleRequest('GET', $this->adminBaseUrl . 'appointments', false, null, null, $params);

        return json_encode([
            'totalRecords' => $response->getHeader('X-Pagination-Count')[0],
            'data'         => json_decode($response->getBody()),
        ]);
    }

    /**
     * @param $patientId
     * @param $appointmentId
     * @return mixed
     */
    public function getPatientNotesByPatientIdAndAppointmentId($patientId, $appointmentId)
    {
        $response = $this->sendGuzzleRequest('GET',
            $this->adminBaseUrl . "appointments/patientnotes/{$patientId}/{$appointmentId}", false);

        return json_decode($response->getBody());
    }

    /**
     * Get Doctor Details
     *
     * @param $doctorId
     * @param bool $cache
     * @return mixed
     */
    public function getDoctorInfo($doctorId)
    {
        $response = $this->sendGuzzleRequest('GET', $this->adminBaseUrl . 'doctors/' . $doctorId, false);
        return json_decode($response->getBody());
    }

    /**
     * Get Company Details By Compnay Id
     *
     * This will return a json response the typical company info
     *
     * @param int $companyId this is the company unique ID
     * @return json An object with the patient's company basic information.
     */
    public function getCompanyInfoById($companyId)
    {
        $client = new Client();

        $response = $client->request('GET', $this->baseUrl . 'companies/' . $companyId, [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'verify'        => false,
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ]);


        return json_decode($response->getBody());
    }


    /**
     * Test Random API Urls
     *
     * @return mixed
     */
    public function testAllApi()
    {
//        $responsebaseUrl = $this->sendGuzzleRequest('GET', $this->baseUrl , false);
        $responsecoreApiUrl = $this->sendGuzzleRequest('GET', $this->coreApiUrl.'/unauthenticated/agreements/openingTerms' , false);
//        $responseadminBaseUrl = $this->sendGuzzleRequest('GET', $this->adminBaseUrl , false);
//        $responseauthBaseUrl = $this->sendGuzzleRequest('GET', $this->authBaseUrl, false);

        $response = [
//            'responsebaseUrl' =>json_decode($responsebaseUrl->getBody()),
            'responsecoreApiUrl' =>$responsecoreApiUrl->getStatusCode(),
//            'responseadminBaseUrl' =>json_decode($responseadminBaseUrl->getBody()),
//            'responseauthBaseUrl' =>json_decode($responseauthBaseUrl->getBody()),
        ];
        return $responsecoreApiUrl->getStatusCode();
    }
}
