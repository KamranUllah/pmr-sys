<?php

namespace App\Services\NowGP\Data;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Request;

/**
 * Class ApiHelper
 *
 * @package App\Services
 */
class BaseApi
{
    /**
     * @var mixed
     */
    protected $authBaseUrl;

    /**
     * @var mixed
     */
    protected $adminBaseUrl;

    /**
     * @var mixed
     */
    protected $baseUrl;

    /**
     * @var
     */
    protected $guzzleClient;

    /**
     * @var mixed
     */
    protected $APIUsername;

    /**
     * @var mixed
     */
    protected $APIPassword;

    /**
     * @var mixed
     */
    protected $conversationApiUrl;

    /**
     * @var mixed
     */
    protected $coreApiUrl;


    /**
     * ApiHelper constructor.
     */
    public function __construct()
    {
        $this->baseUrl = config('pmr.apiv2_base_url');
        $this->authBaseUrl = config('pmr.apiv2_auth_base_url');
        $this->adminBaseUrl = config('pmr.apiv2_admin_base_url');
        $this->APIUsername = config('pmr.apiv2_Username');
        $this->APIPassword = config('pmr.apiv2_Password');
        $this->coreApiUrl = config('pmr.core_api_url');
    }

    /**
     * A simple wrapper around guzzle to make handling the calls easier.
     * It is prepared to handle calls that require us to be logged in. These
     * calls need the session to contain a the PatientId and the Generated call
     * By default these data must be present when we log in. On potential source
     * of proplems could be that the key timed out, leaving us with an invalid
     * value in the session, please keep this in mind.<br>
     * The function will not handles guzzles exceptions,<br>
     * The function will throw ErrorException if the session is not present and
     * the $loginRequired is set to true
     * @param string $action POST, GET, PUT.. etc.
     * @param string $url The url we send the request to, the whole url is necessary
     * @param boolean $loginRequired If the call requires us to be logged in or not. By default: false
     * @param array $json The data we are sending. It must be an array. By default: null
     * @param array $headers The headers that we want to send (aside from the login headers). By default: null
     * @return mixed The value returned by guzzle
     */
    public function sendGuzzleRequest($action, $url, $loginRequired = false, $json = null, $headers = null)
    {
        $client = new Client();

        $options = [
            'content-type'  => 'application/json',
            'cache-control' => 'no-cache',
            'auth'          => [$this->APIUsername, $this->APIPassword],
        ];
        if (!is_null($json)) {
            $options['json'] = $json;
        }
        if (!is_null($headers)) {
            $options['headers'] = $headers;
        }

        if ($loginRequired) {
            /*$user = Request::session()->get('user');
            $accessKeyHeaders = [
                'X_DRNOW_SIGNATURE_1' => $user['GeneratedKey'],
                'X_DRNOW_PATIENT_ID_1' => getPrimaryAccountId(),
            ];
            if (array_key_exists('headers', $options)) {
                array_merge($options['headers'], $accessKeyHeaders);
            } else {
                $options['headers'] = $accessKeyHeaders;
            }*/
        }

        // SSL Validation
        $options['verify'] = storage_path('cacert/cacert.pem');

        return $client->request($action, $url, $options);
    }
}
