<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 15/08/2017
 * Time: 11:38
 */

namespace App\Services\NowGP\Data\Repositories;

use App\Services\NowGP\Data\Models\PatientsAdminApi;
use App\Services\NowGP\Data\Repositories\Interfaces\PatientsAdminInterface;

class PatientsAdminRepository implements PatientsAdminInterface
{
    /**
     * @var PatientsAdminApi
     */
    protected $patientsAdminApi;

    public function __construct(PatientsAdminApi $patientsAdminApi) {
        $this->patientsAdminApi = $patientsAdminApi;
    }

    /**
     * @param $patientId
     * @return bool|mixed
     */
    public function markDataImportAsComplete($patientId)
    {
        $result = false;

        try {
            $result = $this->patientsAdminApi->markDataImportAsComplete($patientId);
        } catch (\Exception $exception) {
            \Bugsnag::notifyException($exception);
        }

        return $result;
    }
}