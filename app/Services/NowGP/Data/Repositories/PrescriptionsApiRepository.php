<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 15/08/2017
 * Time: 11:38
 */

namespace App\Services\NowGP\Data\Repositories;

use App\Services\NowGP\Data\Models\PrescriptionsApi;
use App\Services\NowGP\Data\Repositories\Interfaces\PrescriptionsApiInterface;


class PrescriptionsApiRepository implements PrescriptionsApiInterface
{
    /**
     * @var PrescriptionsApi
     */
    protected $prescriptionsApi;

    public function __construct(PrescriptionsApi  $prescriptionsApi) {
        $this->prescriptionsApi = $prescriptionsApi;
    }

    /**
     * @param $filterType
     *
     * @return mixed
     */
    public function getPrescriptions($filterType)
    {
        $result = false;

        try {
            $result = $this->prescriptionsApi->getPatientPrescriptions($filterType);
        } catch (\Exception $ex) {
            $errorMsg = "Error retrieving prescriptions for prescriptions from API" . $ex->getMessage();
            auditLog($errorMsg);
            \Bugsnag::notifyError('Request Exception', $errorMsg);
        }

        return $result;
    }
}