<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 13/09/2017
 * Time: 09:36
 */

namespace App\Services\NowGP\Data\Repositories\Interfaces;


interface PatientsPrescriptionAdminInterface
{

    /**
     * @param $patientId
     *
     * @param $filterType
     *
     * @return mixed
     */
    public function getPatientPrescriptions($patientId, $filterType);

}