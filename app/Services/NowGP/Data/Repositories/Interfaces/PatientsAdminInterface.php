<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 15/08/2017
 * Time: 11:36
 */

namespace App\Services\NowGP\Data\Repositories\Interfaces;


interface PatientsAdminInterface
{
    public function markDataImportAsComplete($patientId);
}