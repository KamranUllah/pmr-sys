<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 15/08/2017
 * Time: 11:38
 */

namespace App\Services\NowGP\Data\Repositories;

use App\Services\NowGP\Data\Models\PatientsPrescriptionAdminApi;
use App\Services\NowGP\Data\Repositories\Interfaces\PatientsPrescriptionAdminInterface;


class PatientsPrescriptionAdminRepository implements PatientsPrescriptionAdminInterface
{
    /**
     * @var PatientsAdminApi
     */
    protected $patientsPrescriptionAdminApi;

    public function __construct(PatientsPrescriptionAdminApi  $patientsPrescriptionAdminApi) {
        $this->patientsPrescriptionAdminApi = $patientsPrescriptionAdminApi;
    }

    /**
     * @param $patientId
     *
     * @param $filterType
     *
     * @return mixed
     */
    public function getPatientPrescriptions($patientId, $filterType)
    {
        $result = false;

        try {
            $result = $this->patientsPrescriptionAdminApi->getPatientPrescriptions($patientId, $filterType);
        } catch (\Exception $ex) {
            $errorMsg = "Error retrieving prescriptions for patient #: {$patientId}" . $ex->getMessage();
            auditLog($errorMsg);
            \Bugsnag::notifyError('Request Exception', $errorMsg);
        }

        return $result;
    }
}