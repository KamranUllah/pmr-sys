<?php

namespace App\Services\NowGP\Data\Models;

use App\Services\NowGP\Data\BaseApi;
use App\Services\NowGP\Data\Repositories\Interfaces\PatientsPrescriptionAdminInterface;

class PatientsPrescriptionAdminApi extends BaseApi implements PatientsPrescriptionAdminInterface
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $patientId
     * @param $filterType
     *
     * @return mixed
     */
    public function getPatientPrescriptions($patientId, $filterType)
    {
        $filterType = $filterType ?? 2;

        $action = $this->coreApiUrl . "prescriptionadmin/patient/{$patientId}/prescriptions?prescriptionType={$filterType}";

        $response = $this->sendGuzzleRequest('GET', $action, true, []);

        return json_decode($response->getBody());
    }
}
