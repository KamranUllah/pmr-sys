<?php

namespace App\Services\NowGP\Data\Models;

use App\Services\NowGP\Data\BaseApi;
use App\Services\NowGP\Data\Repositories\Interfaces\PatientsAdminInterface;

class PatientsAdminApi extends BaseApi implements PatientsAdminInterface
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $patientId
     * @return mixed
     */
    public function markDataImportAsComplete($patientId)
    {
        //dd($this->coreApiUrl . "patientsadmin/patients/{$patientId}/dataimport/complete");

        $response = $this->sendGuzzleRequest(
            'PUT',
            $this->coreApiUrl . "patientsadmin/patients/{$patientId}/dataimport/complete",
            true,
            null
        );

        return json_decode($response->getBody());
    }
}
