<?php
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 05/09/2016
 * Time: 15:11
 */

namespace App\Services;

use Facuz\Theme\Theme;

/**
 * Class CmsTheme
 * @package CoronaCms\Services
 */
class CmsTheme extends Theme
{
    /**
     * @param $view
     * @param $callback
     * @param null $layout
     */
    public function partialCreator($view, $callback, $layout = null)
    {
        if (! is_array($view)) {
            $view = array($view);
        }

        // Partial path with namespace.
        $path = $this->getThemeNamespace('partials');

        // This code support partialWithLayout.
        if (null !== $layout) {
            $path = $path.'.'.$layout;
        }

        $view = array_map(function($v) use ($path) {
            return $path.'.'.$v;
        }, $view);

        $this->view->creator($view, $callback);
    }
}
