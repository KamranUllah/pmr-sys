<?php
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 05/09/2016
 * Time: 18:18
 */

namespace App\Services;

use Facuz\Theme\AssetContainer;
use File;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request;

class CmsAssetContainer extends AssetContainer
{
    public $useVersioning = false;

    public $files;

    /**
     * Force versioning on added asset
     *
     * @param  boolean $use
     * @return AssetContainer
     */
    public function addVersioning($use = true)
    {
        $this->useVersioning = $use;

        return $this;
    }

    /**
     * Disable asset versioning.
     *
     * @return void
     */
    public function removeVersioning()
    {
        $this->useVersioning = false;
    }

    /**
     * Check if asset versioning is enabled
     *
     * @return boolean
     */
    public function isUsingVersioning()
    {
        return (boolean) $this->useVersioning;
    }

    /**
     * Generate a URL to an application asset.
     *
     * @param  string  $path
     * @param  bool    $secure
     * @return string
     */
    protected function configAssetUrl($path, $secure = null)
    {
        static $assetUrl;

        // Remove this.
        $i = 'index.php';

        if (URL::isValidUrl($path)) {
            return $path;
        }

        // Finding asset url config.
        if (is_null($assetUrl)) {
            $assetUrl = \Config::get('theme.assetUrl', '');
        }

        // Using asset url, if available.
        if ($assetUrl) {
            $base = rtrim($assetUrl, '/');

            // Asset URL without index.
            $basePath = str_contains($base, $i) ? str_replace('/'.$i, '', $base) : $base;
        } else {
            if (is_null($secure)) {
                $scheme = Request::getScheme().'://';
            } else {
                $scheme = $secure ? 'https://' : 'http://';
            }

            // Get root URL.
            $root  = Request::root();
            $start = starts_with($root, 'http://') ? 'http://' : 'https://';
            $root  = preg_replace('~'.$start.'~', $scheme, $root, 1);

            // Asset URL without index.
            $basePath = str_contains($root, $i) ? str_replace('/'.$i, '', $root) : $root;
        }


        return $this->getAssetSourceUrlWithModifiedTime($basePath.'/'.$path, pathinfo($basePath.'/'.$path)['basename']);
    }

    /**
     * Get asset source URL with Modified time.
     *
     * @param  string  $source
     * @param  string  $file
     *
     * @return string
     */
    protected function getAssetSourceUrlWithModifiedTime($source, $file)
    {
        if (File::exists(public_path($source)) && $this->useVersioning) {
            // We can only append mtime to locally defined path since we need
            // to extract the file.
            $mtime = File::lastModified(public_path($source));
            $source = preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $source);
            $source = str_replace(public_path() . DIRECTORY_SEPARATOR, '', $source);
        }

        return $source;
    }
}
