<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserPasswordResetMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var
     */
    private $userData;
    /**
     * @var
     */
    private $generatedPassword;

    /**
     * Create a new message instance.
     *
     * @param $userData
     * @param $generatedPassword
     */
    public function __construct($userData, $generatedPassword)
    {
        //
        $this->userData = $userData;
        $this->generatedPassword = $generatedPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('user::emails.new_password')
            ->from('no-reply@nowhealthcaregroup.com')
            ->subject('Password Reset')
            ->with([
                'user'     => $this->userData,
                'generatedPassword' => $this->generatedPassword,
            ]);
    }
}
