<?php

namespace App\Presenters;

use App\Sidebar\BaseSideBar;
use Maatwebsite\Sidebar\Presentation\SidebarRenderer;

class SidebarCreator
{
    /**
     * @var BaseSideBar
     */
    protected $sidebar;

    /**
     * @var SidebarRenderer
     */
    protected $renderer;

    /**
     * @param BaseSideBar $sidebar
     * @param SidebarRenderer       $renderer
     */
    public function __construct(BaseSideBar $sidebar, SidebarRenderer $renderer)
    {
        $this->sidebar  = $sidebar;
        $this->renderer = $renderer;
    }

    /**
     * @param $view
     */
    public function create($view): void
    {
        $view->sidebar = $this->renderer->render($this->sidebar);
    }
}
