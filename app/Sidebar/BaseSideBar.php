<?php

namespace App\Sidebar;

use Illuminate\Contracts\Container\Container;
use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\ShouldCache;
use Maatwebsite\Sidebar\Sidebar;
use Maatwebsite\Sidebar\Traits\CacheableTrait;
use Module;


class BaseSideBar implements Sidebar, ShouldCache
{
    use CacheableTrait;

    /**
     * @var Menu
     */
    protected $menu;

    protected $modules;
    /**
     * @var Container
     */
    protected $container;

    /**
     * @param Menu $menu
     * @param Module $modules
     * @param Container $container
     */
    public function __construct(Menu $menu, Module $modules, Container $container)
    {
        $this->menu = $menu;
        $this->modules = $modules;
        $this->container = $container;
    }

    /**
     * Build your sidebar implementation here
     */
    public function build()
    {
        foreach (Module::enabled() as $module) {
            $name = studly_case($module->get('name'));
            $class = 'App\\Modules\\' . $name . '\\Sidebar\\AdminSideBarExtender';
            if (class_exists($class)) {
                $extender = $this->container->make($class);
                $this->menu->add(
                    $extender->extendWith($this->menu)
                );
            }
        }
    }

    /**
     * @return Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }
}
