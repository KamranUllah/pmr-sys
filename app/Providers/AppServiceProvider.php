<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Log;
use Psr\Log\LoggerInterface;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (str_contains(url('/'), 'ngrok.io')) {
            \URL::forceSchema('https');
        }

        if ($this->app->environment() === 'local') {
            if (config('app.debug')) {
                if (class_exists(\Barryvdh\Debugbar\ServiceProvider::class)) {
                    $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
                }

                if (class_exists(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class)) {
                    $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
                }
            }

            if (class_exists(\Laracasts\Generators\GeneratorsServiceProvider::class)) {
                $this->app->register(\Laracasts\Generators\GeneratorsServiceProvider::class);
            }
        }

        if (class_exists(\Themonkeys\Cachebuster\CachebusterServiceProvider::class)) {
            $this->app->register(\Themonkeys\Cachebuster\CachebusterServiceProvider::class);
        }

        if (class_exists(\GrahamCampbell\Exceptions\ExceptionsServiceProvider::class)) {
            $this->app->register(\GrahamCampbell\Exceptions\ExceptionsServiceProvider::class);
        }

        if (class_exists(\Nwidart\Modules\LaravelModulesServiceProvider::class)) {
            $this->app->register(\Nwidart\Modules\LaravelModulesServiceProvider::class);
        }

        if (class_exists(\Collective\Html\HtmlServiceProvider::class)) {
            $this->app->register(\Collective\Html\HtmlServiceProvider::class);
        }

        if (class_exists(\Bepsvpt\SecureHeaders\SecureHeadersServiceProvider::class)) {
            $this->app->register(\Bepsvpt\SecureHeaders\SecureHeadersServiceProvider::class);
        }

        if (class_exists(CmsThemeProvider::class)) {
            $this->app->register(CmsThemeProvider::class);
        }

        if (class_exists(\Intervention\Image\ImageServiceProvider::class)) {
            $this->app->register(\Intervention\Image\ImageServiceProvider::class);
        }

        if (class_exists(\Cartalyst\Sentinel\Laravel\SentinelServiceProvider::class)) {
            $this->app->register(\Cartalyst\Sentinel\Laravel\SentinelServiceProvider::class);
        }

        if (class_exists(\Laracasts\Utilities\JavaScript\JavaScriptServiceProvider::class)) {
            $this->app->register(\Laracasts\Utilities\JavaScript\JavaScriptServiceProvider::class);
        }

        if (class_exists(\Laracasts\Flash\FlashServiceProvider::class)) {
            $this->app->register(\Laracasts\Flash\FlashServiceProvider::class);
        }

        if (class_exists(\AdamWathan\BootForms\BootFormsServiceProvider::class)) {
            $this->app->register(\AdamWathan\BootForms\BootFormsServiceProvider::class);
        }

        /*if (class_exists(\Collective\Html\HtmlServiceProvider::class)) {
            $this->app->register(\Collective\Html\HtmlServiceProvider::class);
        }*/

        if (class_exists(\Bugsnag\BugsnagLaravel\BugsnagServiceProvider::class)) {
            $this->app->register(\Bugsnag\BugsnagLaravel\BugsnagServiceProvider::class);
            $this->app->alias('bugsnag.multi', Log::class);
            $this->app->alias('bugsnag.multi', LoggerInterface::class);
        }

        if (class_exists(\Maatwebsite\Sidebar\SidebarServiceProvider::class)) {
            $this->app->register(\Maatwebsite\Sidebar\SidebarServiceProvider::class);
        }

        if (class_exists(\Lord\Laroute\LarouteServiceProvider::class)) {
            $this->app->register(\Lord\Laroute\LarouteServiceProvider::class);
        }

        if (class_exists(\Yajra\Datatables\DatatablesServiceProvider::class)) {
            $this->app->register(\Yajra\Datatables\DatatablesServiceProvider::class);
        }

        if (class_exists(\EloquentFilter\ServiceProvider::class)) {
            $this->app->register(\EloquentFilter\ServiceProvider::class);
        }

        if (class_exists(\OwenIt\Auditing\AuditingServiceProvider::class)) {
            $this->app->register(\OwenIt\Auditing\AuditingServiceProvider::class);
        }

        /*if (class_exists(\Barryvdh\DomPDF\ServiceProvider::class)) {
            $this->app->register(\Barryvdh\DomPDF\ServiceProvider::class);
        }*/


        /*if (class_exists(\Vinkla\Pusher\PusherServiceProvider::class)) {
            $this->app->register(\Vinkla\Pusher\PusherServiceProvider::class);
        }*/

    }

    /**
     * Create aliases for the dependency.
     */
    /*private function registerAliases()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Datatables', \Yajra\Datatables\Datatables::class);
    }*/
}
