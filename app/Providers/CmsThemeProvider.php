<?php

namespace App\Providers;

use App\Services\CmsAsset;
use App\Services\CmsTheme;
use Illuminate\Routing\Router;
use Facuz\Theme\ThemeServiceProvider;

class CmsThemeProvider extends ThemeServiceProvider
{


    /**
     * Bootstrap the application events.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        $configPath = __DIR__.'/../config/theme.php';

        // Publish config.
        $this->publishes([$configPath => config_path('theme.php')], 'config');

        //$router->aliasMiddleware('theme', Middleware\ThemeLoader::class);

        // Register blade directives:
        $this->addToBlade(['dd', 'dd(%s);']);
        $this->addToBlade(['d', 'dump(%s);']);

        $this->addToBlade(['get', 'Theme::get(%s);']);
        $this->addToBlade(['getIfHas', 'Theme::has(%1$s) ? Theme::get(%1$s) : ""']);

        $this->addToBlade(['partial', 'Theme::partial(%s);']);
        $this->addToBlade(['sections', 'Theme::partial("sections.".%s);']);
        $this->addToBlade(['content', null, 'Theme::content();']);

        $this->addToBlade(['asset', 'Theme::asset()->absUrl(%s);']);

        $this->addToBlade(['styles', 'Theme::asset()->container(%s)->styles();', 'Theme::asset()->styles();']);
        $this->addToBlade(['scripts', 'Theme::asset()->container(%s)->scripts();', 'Theme::asset()->scripts();']);
    }

    /**
     *
     */
    public function registerTheme()
    {
        $this->app->singleton('theme', function ($app) {
            return new CmsTheme($app['config'], $app['events'], $app['view'], $app['asset'], $app['files'], $app['breadcrumb'], $app['manifest']);
        });

        $this->app->alias('theme', 'Facuz\Theme\Contracts\Theme');
    }

    /**
     * Register asset provider.
     *
     * @return void
     */
    public function registerAsset()
    {
        $this->app->singleton('asset', function ($app) {
            return new CmsAsset();
        });
    }
}
