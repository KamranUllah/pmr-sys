<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 20/12/2017
 * Time: 17:00
 */

return [
    'client_id' => env('TELECOMMS_CLOUD_CLIENT_ID', null),
    'secret'    => env('TELECOMMS_CLOUD_SECRET', null),
    'auth_url'    => env('TELECOMMS_API_URL', null),
];