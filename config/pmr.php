<?php
/**
 * Created by PhpStorm.
 * User: DevAdmin
 * Date: 26/07/2017
 * Time: 16:33
 */

return [
    'environment_name'     => env('ENV_NAME', 'LIVE'),
    'pmr_version'          => env('PMR_VERSION', null),

    /**
     * New API v2 Endpoints
     */
    'apiv2_base_url'       => env('API_v2_BASE_URL', null),
    'apiv2_auth_base_url'  => env('API_v2_AUTH_BASE_URL', null),
    'apiv2_admin_base_url' => env('API_v2_ADMIN_BASE_URL', null),
    'apiv2_Username'       => env('API_v2_Username', null),
    'apiv2_Password'       => env('API_v2_Password', null),
    'apiv2_main_post_url'  => env('API_v2_MAIN_POST_URL', null),

    'core_api_url' => env('CORE_API_URL', null),

    'enable_chat'    => env('ENABLE_CHAT', false),
    'enable_v2_chat' => env('ENABLE_V2_CHAT', false),

    'chat_interval_check'             => env('CHAT_INTERVAL_CHECK', 10), // In seconds
    'new_conversation_interval_check' => env('NEW_CONVERSATION_INTERVAL_CHECK', 5), // In seconds
    'new_chat_interval_check'         => env('NEW_CHAT_INTERVAL_CHECK', 10), // In seconds

    'timeout' => env('USER_TIMEOUT', 1200),

    'removed_test_data' => env('REMOVE_TEST_DATA', true),
    'remove_90_days' => env('REMOVE_90_DAYS', true),
];