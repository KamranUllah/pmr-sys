<?php

$protocol = 'https://';
if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === 'off') {
    $protocol = 'http://';
}

$wss_protocol = 'wss://';
$ws_protocol = 'ws://';

return [

    /*
     * X-Content-Type-Options
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
     *
     * Available Value: 'nosniff'
     */

    'x-content-type-options' => 'nosniff',

    /*
     * X-Download-Options
     *
     * Reference: https://msdn.microsoft.com/en-us/library/jj542450(v=vs.85).aspx
     *
     * Available Value: 'noopen'
     */

    'x-download-options' => 'noopen',

    /*
     * X-Frame-Options
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
     *
     * Available Value: 'deny', 'sameorigin', 'allow-from <uri>'
     */

    'x-frame-options' => 'sameorigin',

    /*
     * X-Permitted-Cross-Domain-Policies
     *
     * Reference: https://www.adobe.com/devnet/adobe-media-server/articles/cross-domain-xml-for-streaming.html
     *
     * Available Value: 'all', 'none', 'master-only', 'by-content-type', 'by-ftp-filename'
     */

    'x-permitted-cross-domain-policies' => 'none',

    /*
     * X-XSS-Protection
     *
     * Reference: https://blogs.msdn.microsoft.com/ieinternals/2011/01/31/controlling-the-xss-filter
     *
     * Available Value: '1', '0', '1; mode=block'
     */

    'x-xss-protection' => '1; mode=block',

    /*
     * Referrer-Policy
     *
     * Reference: https://w3c.github.io/webappsec-referrer-policy
     *
     * Available Value: 'no-referrer', 'no-referrer-when-downgrade', 'origin', 'origin-when-cross-origin',
     *                  'same-origin', 'strict-origin', 'strict-origin-when-cross-origin', 'unsafe-url'
     */

    'referrer-policy' => 'no-referrer',

    /*
     * HTTP Strict Transport Security
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/Security/HTTP_strict_transport_security
     *
     * Please ensure your website had set up ssl/tls before enable hsts.
     */

    'hsts' => [
        'enable' => env('SECURITY_HEADER_HSTS_ENABLE', false),

        'max-age' => 15552000,

        'include-sub-domains' => false,
    ],

    /*
     * Public Key Pinning
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/Security/Public_Key_Pinning
     *
     * hpkp will be ignored if hashes is empty.
     */

    'hpkp' => [
        'hashes' => [
            // [
            //     'algo' => 'sha256',
            //     'hash' => 'hash-value',
            // ],
        ],

        'include-sub-domains' => false,

        'max-age' => 15552000,

        'report-only' => false,

        'report-uri' => null,
    ],

    /*
     * Content Security Policy
     *
     * Reference: https://developer.mozilla.org/en-US/docs/Web/Security/CSP
     *
     * csp will be ignored if custom-csp is not null. To disable csp, set custom-csp to empty string.
     *
     * Note: custom-csp does not support report-only.
     */

    'custom-csp' => env('SECURITY_HEADER_CUSTOM_CSP', null),

    'csp' => [
        'report-only' => false,

        'report-uri' => env('CONTENT_SECURITY_POLICY_REPORT_URI', false),

        'upgrade-insecure-requests' => false,

        'base-uri' => [
            //
        ],

        'default-src' => [
            //
            'self' => true,

            'blob' => true,
        ],

        'child-src' => [
            //
            'allow' => [
                $protocol . 'accounts.google.com',
                $protocol . 'apis.google.com',
                $protocol . 'cdnjs.cloudflare.com',
                $protocol . 'ajax.googleapis.com',
                $protocol . 'code.jquery.com',
                $protocol . 'www.googletagmanager.com',
                $protocol . 'www.google-analytics.com',
                $protocol . '*.cloudfront.net',
                $protocol . 'disqus.com',
                $protocol . '*.disqus.com',
                $protocol . '*.disquscdn.com',
                $protocol . '*.addthis.com',
                $protocol . '*.twitter.com',
                $protocol . '*.bootstrapcdn.com',
                $protocol . '*.googlesyndication.com',
                $protocol . '*.onesignal.com',
                $protocol . 'onesignal.com',
                $protocol . 'securionpay.com',
                $protocol . '*.securionpay.com',
                $protocol . 'freegeoip.net',
                $protocol . '*.freegeoip.net',
                $protocol . '*.googleapis.com',
                $protocol . '*.ngrok.io',
                $protocol . '*.wp.com',
                $protocol . '*.gstatic.com',
                $protocol . '*.pusher.com',
                $protocol . '*.amazonaws.com',
                $protocol . '*.cloudflare.com',
                $wss_protocol . '*.pusher.com',
            ],

            'self' => true,
        ],

        'script-src' => [
            'allow' => [
                $protocol . 'accounts.google.com',
                $protocol . 'apis.google.com',
                $protocol . 'cdnjs.cloudflare.com',
                $protocol . 'ajax.googleapis.com',
                $protocol . 'code.jquery.com',
                $protocol . 'www.googletagmanager.com',
                $protocol . 'www.google-analytics.com',
                $protocol . '*.cloudfront.net',
                $protocol . '*.disqus.com',
                $protocol . '*.disquscdn.com',
                $protocol . '*.addthis.com',
                $protocol . '*.twitter.com',
                $protocol . '*.bootstrapcdn.com',
                $protocol . '*.googlesyndication.com',
                $protocol . '*.addthisedge.com',
                $protocol . '*.linkedin.com',
                $protocol . '*.facebook.com',
                $protocol . '*.onesignal.com',
                $protocol . 'onesignal.com',
                $protocol . 'securionpay.com',
                $protocol . '*.securionpay.com',
                $protocol . 'freegeoip.net',
                $protocol . '*.freegeoip.net',
                $protocol . '*.googleapis.com',
                $protocol . '*.ngrok.io',
                $protocol . '*.wp.com',
                $protocol . '*.gstatic.com',
                $protocol . '*.cloudflare.com',
                $protocol . '*.pusher.com',
                $protocol . '*.amazonaws.com',
                $wss_protocol . '*.pusher.com',
            ],

            'hashes' => [
                // ['sha256' => 'hash-value'],
            ],

            'nonces' => [
                //
            ],

            'self' => true,

            'unsafe-inline' => true,

            'unsafe-eval' => true,

            'data' => true,
        ],

        'style-src' => [
            'allow'         => [
                $protocol . 'fonts.googleapis.com',
                $protocol . '*.disqus.com',
                $protocol . '*.disquscdn.com',
                $protocol . '*.twitter.com',
                $protocol . '*.bootstrapcdn.com',
                $protocol . '*.linkedin.com',
                $protocol . '*.facebook.com',
                $protocol . '*.onesignal.com',
                $protocol . 'onesignal.com',
                $protocol . 'securionpay.com',
                $protocol . '*.securionpay.com',
                $protocol . '*.ngrok.io',
                $protocol . '*.wp.com',
                $protocol . '*.gstatic.com',
                $protocol . '*.cloudflare.com',
                $protocol . '*.pusher.com',
                $protocol . '*.amazonaws.com',
                $wss_protocol . '*.pusher.com',
            ],
            'self'          => true,
            'unsafe-inline' => true,
        ],

        'img-src' => [
            'allow' => [
                //
                $protocol . 'www.google-analytics.com',
                $protocol . '*.imgix.net',
                $protocol . '*.bugsnag.com',
                $protocol . '*.bootstrapcdn.com',
                $protocol . '*.disqus.com',
                $protocol . '*.disquscdn.com',
                $protocol . '*.linkedin.com',
                $protocol . '*.facebook.com',
                $protocol . '*.onesignal.com',
                $protocol . 'onesignal.com',
                $protocol . 'securionpay.com',
                $protocol . '*.securionpay.com',
                $protocol . 'freegeoip.net',
                $protocol . '*.freegeoip.net',
                $protocol . '*.ngrok.io',
                $protocol . '*.wp.com',
                $protocol . '*.gstatic.com',
                $protocol . '*.googleapis.com',
                $protocol . '*.ggpht.com',
                $protocol . 'propeller.in',
                $protocol . 'placehold.it',
                $protocol . '*.pusher.com',
                $protocol . '*.amazonaws.com',
                $protocol . '*.cloudflare.com',
                $wss_protocol . '*.pusher.com',
            ],

            'types' => [
                //
            ],

            'self' => true,

            'data' => true,
        ],

        /*
         * The following directives are all use 'allow' and 'self' flag.
         *
         * Note: default value of 'self' flag is false.
         */

        'font-src' => [
            //
            'allow' => [
                $protocol . 'fonts.gstatic.com',
                $protocol . '*.bootstrapcdn.com',
                $protocol . '*.linkedin.com',
                $protocol . '*.facebook.com',
                $protocol . '*.onesignal.com',
                $protocol . 'onesignal.com',
                $protocol . 'securionpay.com',
                $protocol . '*.securionpay.com',
                $protocol . '*.ngrok.io',
                $protocol . '*.wp.com',
                $protocol . '*.gstatic.com',
                $protocol . '*.pusher.com',
                $protocol . '*.cloudflare.com',
                $wss_protocol . '*.pusher.com',
                $protocol . '*.amazonaws.com',
                $wss_protocol . '*.pusherapp.com',
            ],
            'self'  => true,
            'data'  => true,
        ],

        'connect-src' => [
            //
            'allow' => [
                $protocol . '*.googleapis.com',
                $protocol . '*.ngrok.io',
                $protocol . '*.onesignal.com',
                $protocol . 'onesignal.com',
                $protocol . '*.wp.com',
                $protocol . '*.gstatic.com',
                $protocol . '*.pusher.com',
                $protocol . '*.amazonaws.com',
                $protocol . '*.cloudflare.com',
                $wss_protocol . '*.pusher.com',
                $wss_protocol . '*.pusherapp.com',
                $ws_protocol . '*.pusher.com',
                $ws_protocol . '*.pusherapp.com',
            ],
            'self'  => true,
            'data'  => true,
            'blob'  => true,
        ],

        'form-action' => [
            //
            'allow' => [
                $protocol . '*.googleapis.com',
                $protocol . '*.ngrok.io',
                $protocol . '*.onesignal.com',
                $protocol . 'onesignal.com',
                $protocol . '*.gstatic.com',
                $protocol . '*.pusher.com',
                $protocol . '*.amazonaws.com',
                $protocol . '*.cloudflare.com',
                $wss_protocol . '*.pusher.com',
                $wss_protocol . '*.pusherapp.com',
            ],
            'self'  => true,
            'data'  => true,
        ],

        'frame-ancestors' => [
            //
            $protocol . '*.disqus.com',
            $protocol . '*.disquscdn.com',
            $protocol . '*.ngrok.io',
            $protocol . '*.onesignal.com',
            $protocol . 'onesignal.com',
            $protocol . '*.gstatic.com',
            $protocol . '*.pusher.com',
            $protocol . '*.amazonaws.com',
            $protocol . '*.cloudflare.com',
            $wss_protocol . '*.pusher.com',
            $wss_protocol . '*.pusherapp.com',
        ],

        'media-src' => [
            //
            'allow' => [
                $protocol . '*.ngrok.io',
                $protocol . '*.pusher.com',
                $protocol . '*.amazonaws.com',
                $wss_protocol . '*.pusher.com',
                $wss_protocol . '*.pusherapp.com',
                $protocol . '*.cloudflare.com',
                //'blob:' . $protocol . '*.ngrok.io',
            ],
            'data'  => true,
            'types' => [
                'blob:',
                'filesystem:',
            ],
        ],

        'object-src' => [
            'allow' => [
                $protocol . '*.ngrok.io',
                $protocol . '*.pusher.com',
                $protocol . '*.amazonaws.com',
                $wss_protocol . '*.pusher.com',
                $wss_protocol . '*.pusherapp.com',
                $protocol . '*.cloudflare.com',
            ],
            'self'  => true,
            'data'  => true,
            'blob'  => true,
        ],

        /*
         * plugin-types only support 'allow'.
         */

        'plugin-types' => [
            //
        ],
    ],

];
