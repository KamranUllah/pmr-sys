<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'sqlsrv_pmr'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => env('DB_PREFIX', ''),
        ],

        'sqlsrv_orig' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'nh-db.database.secure.windows.net'),
            'database' => env('DB_DATABASE', 'drn_doctorv2'),
            'username' => env('DB_USERNAME', 'Nhdr'),
            'password' => env('DB_PASSWORD', 'dzrgrrN4jAEhh8uW'),
            'charset' => 'utf8',
            'prefix' => env('DB_PREFIX', ''),
        ],

        'sqlsrv_pmr' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST_PMR', 'nh-db.database.secure.windows.net'),
            'database' => env('DB_DATABASE_PMR', 'drn_pmr'),
            'username' => env('DB_USERNAME_PMR', 'Nhmainbase'),
            'password' => env('DB_PASSWORD_PMR', '9ENHs6jkDkp4rFQw'),
            'charset' => 'utf8',
            'prefix' => env('DB_PREFIX_PMR', ''),
        ],

        'sqlsrv_mainbase' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST_MAINBASE', 'nh-db.database.secure.windows.net'),
            'database' => env('DB_DATABASE_MAINBASE', 'drn_mainbasev2'),
            'username' => env('DB_USERNAME_MAINBASE', 'Nhmainbase'),
            'password' => env('DB_PASSWORD_MAINBASE', '9ENHs6jkDkp4rFQw'),
            'charset' => 'utf8',
            'prefix' => env('DB_PREFIX_MAINBASE', ''),
        ],

        'sqlsrv_mainbase_orig' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST_MAINBASE', 'localhost'),
            'database' => env('DB_DATABASE_MAINBASE', 'forge'),
            'username' => env('DB_USERNAME_MAINBASE', 'forge'),
            'password' => env('DB_PASSWORD_MAINBASE', ''),
            'charset' => 'utf8',
            'prefix' => env('DB_PREFIX_MAINBASE', ''),
        ],

        'sqlsrv_audit' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST_AUDIT', 'nh-db.database.secure.windows.net'),
            'database' => env('DB_DATABASE_AUDIT', 'Drn_audit'),
            'username' => env('DB_USERNAME_AUDIT', 'nhaudit'),
            'password' => env('DB_PASSWORD_AUDIT', 'QdJU8kXm59gTjSpNY'),
            'charset' => 'utf8',
            'prefix' => env('DB_PREFIX_AUDIT', ''),
        ],

        'sqlsrv_audit_orig' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST_AUDIT', 'localhost'),
            'database' => env('DB_DATABASE_AUDIT', 'forge'),
            'username' => env('DB_USERNAME_AUDIT', 'forge'),
            'password' => env('DB_PASSWORD_AUDIT', ''),
            'charset' => 'utf8',
            'prefix' => env('DB_PREFIX_AUDIT', ''),
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
